let survey_monkey = {
    disableSurveyInfo() {
        $('#preview_survey_monkey').hide();
        $('.script_content_area').html('');
        $('.survey_info').hide();
        var expires = (new Date(Date.now() - 86400*1000)).toUTCString();
        var survey_value = Date.now();
        document.cookie = "smcx_0_last_shown_at=" + survey_value + "; expires=" + expires + ";path=/;";
        document.cookie = "cdp_seg=" + survey_value + "; expires=" + expires + ";path=/; domain=.surveymonkey.com";
        document.cookie = "ep201=" + survey_value + "; expires=" + expires + ";path=/; domain=.surveymonkey.com";
        document.cookie = "attr_multitouch=" + survey_value + "; expires=" + expires + ";path=/; domain=.surveymonkey.com";
        document.cookie = "CX_413909822=" + survey_value + "; expires=" + expires + ";path=/; domain=.surveymonkey.com";
        document.cookie = "CX_412042968=" + survey_value + "; expires=" + expires + ";path=/; domain=.surveymonkey.com";
        document.cookie = "ep203=" + survey_value + "; expires=" + expires + ";path=/; domain=.surveymonkey.com";

        $('#smcx-sdk').remove();
        $('#__smcx__').remove();
        $('.smcx-widget').remove();
    },
    showPreview()
    {
        $('#survey_employee_status').prop('checked', false);
        try {
            let script_content = $('textarea[name="script_content"]').val();
            if (script_content.search(/^\<script\>/) < 0 || script_content.search(/\<\/script\>$/) < 0) {
                throw new Error('埋込みコードが正しくありません。');
            }
            $('.script_content_area').html(script_content);
            setTimeout(function () {
                let height = $('.smcx-modal').height();
                let width = $('.smcx-modal').width();
                if (width != null && height != null){
                    let top = height/2 + 10;
                    let left = width/2;
                    $('.survey_check_box').css('top', 'calc(50% +  ' + top +'px)');
                    $('.survey_check_box').css('left', 'calc(50% -  ' + left +'px)');
                }
                $('.survey_info').show();
                $('#preview_survey_monkey').show();
            }, 1000);

        } catch (err) {
            $('.non-session .message-content').text('埋込みコードが正しくありません。');
            $('.non-session').addClass('alert-danger');
            $('.non-session').show();
            setTimeout(function () {
                $('.alert-dismissible').fadeOut();
            }, 3000);
        }

    },
    addEventListener() {
        $(document)
            .on('click', '#survey_employee_status, .smcx-modal-close, .smcx-pull-left, .smcx-pull-right', this.disableSurveyInfo)
            .on('click', '.btn-preview_survey_monkey', this.showPreview)
    },
    init() {
        this.addEventListener();
    }
};
$(document).ready(function () {
    survey_monkey.init();
});