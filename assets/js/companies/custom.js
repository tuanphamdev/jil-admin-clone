$(document).ready(function() {
    function clearEnigma() {
        $("#enigma_status" ).removeClass("fa fa-check-circle enigma-flag-green");
        $("#enigma_status" ).removeClass("fa fa-close enigma-flag-red");
        $("#enigma_code" ).removeClass("enigma-code-invalid");
        $("#enigma_code" ).removeClass("enigma-code-valid");
        $("#enigma_search" ).html("");
        $("#enigma_name" ).html("");
    }
    function setEnigmaOn() {
        $("#enigma_status" ).removeClass("fa fa-close enigma-flag-red");
        $("#enigma_status" ).addClass("fa fa-check-circle enigma-flag-green");
        $("#enigma_code" ).removeClass("enigma-code-invalid");
        $("#enigma_code" ).addClass("enigma-code-valid");
    }
    function setEnigmaOff() {
        $("#enigma_status" ).removeClass("fa fa-check-circle enigma-flag-green");
        $("#enigma_status" ).addClass("fa fa-close enigma-flag-red");
        $("#enigma_code" ).addClass("enigma-code-invalid");
        $("#enigma_code" ).removeClass("enigma-code-valid");
    }
    function check_company_info(company_code, version) {
        clearEnigma();
        $("#enigma_search" ).html('検索...');
        $.ajax({
            url: "/companies/check_enigma_company",
            type: "post",
            data:{company_code:company_code, version:version},
            dataType: "json"
        }).done(function (data) {
            $("#enigma_search" ).html("");
            enigma_name = $("#enigma_name");
            if(data.enigma_status == '0') {
                enigma_name.html("");
                setEnigmaOff();
            }
            else {
                var name = data.enigma_name;
                enigma_name.html(name);
                setEnigmaOn();
            }
        });
    }

    $(document).on("change", "#prefecture", function () {
        var prefecture_id = $( "#prefecture" ).val();
        var city_ele = $("#city");
        iso_id = $(this).find(':selected').attr('data-iso_id');

        var option = "<option value='' selected> 未選択 </option>";
        if(prefecture_id != ''){
            $.ajax({
                url: "/companies/cities/" + iso_id,
                type: "get",
                dataType: "json"
            }).done(function (data) {
                if (data && prefecture_id) {
                    $.each(data, function (k, v) {
                        option += "<option value=" + v.id + ">" + v.name + "</option>";
                    });
                }else{

                }
                city_ele.html(option);
            });
        }
        city_ele.html(option);
    });

    // set default date picker for end_date (update)
    start_date = $('#form_update_company #start_date').val();
    if(start_date != ''){
        $('#form_update_company #end_date').datepicker('setStartDate', new Date(start_date));
    }

    $("input[name='enigma_version']").click(function(){
        var enigma_name = $("#enigma_name");
        var enigma_version = $("input[name='enigma_version']:checked").val();
        var company_code = $("#enigma_code").val();
        company_code = company_code.trim();
        if (enigma_version == '0' && company_code.length == 0) {
            clearEnigma();
            return;
        }
        if (enigma_version == '0') {
            return;
        }
        if (company_code.length == 0) {
            return;
        }
        check_company_info(company_code, enigma_version);
    });

    $("#enigma_code").change(function() {
        var enigma_name = $( "#enigma_name" );
        var enigma_version = $("input[name='enigma_version']:checked").val();
        var company_code = $(this).val();
        company_code = company_code.trim();
        if (enigma_version == '0' && company_code.length == 0) {
            clearEnigma();
            return;
        }
        if (enigma_version == '0') {
            return;
        }
        if (company_code.length == 0) {
            return;
        }
        check_company_info(company_code, enigma_version);
    });

});

$('#start_date_from,#end_date_from').on('change', function() {
    start_date_from = $('#start_date_from').val();
    start_date_to = $('#start_date_to').val();
    end_date_from = $('#end_date_from').val();
    end_date_to = $('#end_date_to').val();

    if(start_date_from != '' && (start_date_to == '' || start_date_to < start_date_from)){
        $('#start_date_to').val(start_date_from);
    }
    if(start_date_from != ''){
        $('#start_date_to').datepicker('setStartDate', new Date(start_date_from));
    }else{
        $('#start_date_to').datepicker('setStartDate', new Date('1970-01-01'));

    }

    if(end_date_from != '' && (end_date_to == '' || end_date_to < end_date_from)){
        $('#end_date_to').val(end_date_from);
    }
    if(end_date_from != ''){
        $('#end_date_to').datepicker('setStartDate', new Date(end_date_from));
    }else{
        $('#end_date_to').datepicker('setStartDate', new Date('1970-01-01'));

    }

});

$('#form_create_company #start_date').on('change', function() {
    start_date = $('#start_date').val();
    end_date = $('#end_date').val();

    if(start_date != '' && start_date >= current_date()){
        $('#end_date').datepicker('setStartDate', new Date(start_date));
    }else if(start_date < current_date()){
        $('#end_date').datepicker('setStartDate', new Date());
    }
});

$('#form_update_company #start_date').on('change', function() {
    start_date = $('#start_date').val();
    end_date = $('#end_date').val();

    if(start_date != ''){
        $('#end_date').datepicker('setStartDate', new Date(start_date));
    }else if(start_date == ''){
        $('#end_date').datepicker('setStartDate', new Date('1970-01-01'));
    }
});

$("select[name='keihi']").change(function() {
    let value_selected = $(this).val();
    if (value_selected == 1) {
        $('.option_ai_ocr, .option_ebook_store, .option_credit_card').show();
        $('.option_timestamp').show();
    } else {
        $('.option_ai_ocr, .option_ebook_store, .option_credit_card').hide();
        $('.option_timestamp').hide();
        $('#option_ai_ocr_no_use').prop("checked", true);
        $('#option_timestamp_no_use').prop("checked", true);
        $('#option_ebook_store_no_use').prop("checked", true);
        $('#option_credit_card_no_use').prop("checked", true);
    }
});

$("select[name='kintai']").change(function() {
    let value_selected = $(this).val();
    if (value_selected == 1) {
        $('.option_paid_management').show();
    } else {
        $('.option_paid_management').hide();
        $('#option_paid_management_no_use').prop("checked", true);
        $('#option_shift_management_no_use').prop("checked", true);
        $('#option_overtime_management_no_use').prop("checked", true);
        $('#option_kintai_alligate_no_use').prop("checked", true);
        $('#option_kintai_sense_link_no_use').prop("checked", true);
    }
});

$("select[name='salary']").change(function() {
    let value_selected = $(this).val();
    if (value_selected == 1) {
        $('.option_payslip').show();
    } else {
        $('.option_payslip').hide();
        $('#option_payslip_no_use').prop("checked", true);
    }
});

$("select[name='signing']").change(function() {
    let value_selected = $(this).val();
    if (value_selected == 1) {
        $('.option_maximum_signing').show();
        $('.option_signing_folder').show();
        $('.option_signing_internal_workflow').show();
        $('.option_signing_batch_import').show();
        $('.signing_options').show();
        // Set default vaule on when enbale signing
        $('input[name="electronic_signature_long_term"]').filter('[value="1"]').prop('checked', true).trigger('change');
    } else {
        $('.signing_options').hide();
        $('.option_maximum_signing').hide();
        $('.option_signing_folder').hide();
        $('.option_signing_internal_workflow').hide();
        $('.option_signing_batch_import').hide();
        $('.option_maximum_import_signed_file').hide();
        $('input[name="maximum_signing"]').val('');
        $("select[name='signing_folder']").val(0).trigger('change');
        $("select[name='import_signed_file']").val(0).trigger('change');
        $('input[name="signing_folder_range"]').filter('[value="0"]').prop('checked', true);
        $('input[name="signing_internal_workflow"]').filter('[value="0"]').prop('checked', true);
        $('input[name="signing_batch_import"]').filter('[value="0"]').prop('checked', true);
        $('.signing_options').find('input').each(function (index, selector){
            if ($(selector).val() == 0) {
                $(selector).prop('checked', true).trigger('change');
            }
        });
    }
});

$("select[name='signing_folder']").change(function() {
    let value_selected = $(this).val();
    if (value_selected == 1) {
        $('.option_signing_folder_range').show();
    } else {
        $('.option_signing_folder_range').hide();
        $('input[name="signing_folder_range"]').filter('[value="0"]').prop('checked', true);
    }
});
$("select[name='import_signed_file']").change(function() {
    let value_selected = $(this).val();
    if (value_selected == 1) {
        $('.option_maximum_import_signed_file').show();
    } else {
        $('.option_maximum_import_signed_file').hide();
        $('input[name="maximum_import_signed_file"]').val('');
        $('input[name="import_signed_file_timestamp"]').filter('[value="0"]').prop('checked', true);
    }
});


$('select.is_contract').change(function () {
    let enable_signing = false;
    let other_service = false;
    let arr = $('.is_contract').serializeArray();

    $.each(arr, function (index, contract){
        if (contract.name == 'signing' && contract.value == 1) {
            enable_signing = true;
        }
        if (contract.name !== "signing" && contract.value == 1) {
            other_service = true;
        }
    });
    // only enable signing
    if (enable_signing === true && other_service === false) {
        $('select[name="jinjer_db"] option').each(function (){
            if ($(this).val() == 1) {
                $(this).hide();
                $(this).prop('selected', false);
            }else{
                $(this).show();
                $(this).prop('selected', true);
            }
        });

    }

    if (other_service === true) {
        $('select[name="jinjer_db"] option').each(function (){
            if ($(this).val() == 1) {
                $(this).show();
                $(this).prop('selected', true);
            }else{
                $(this).hide();
                $(this).prop('selected', false);
            }
        })
    }
    if (enable_signing === false && other_service === false) {
        $('select[name="jinjer_db"] option').each(function (){
            $(this).show();
            // Default jinjer_db is off
            if ($(this).val() == 0) {
                $(this).prop('selected', true);
            }else {
                $(this).prop('selected', false);
            }
        });
    }
});

$('input[name="is_docomo"]').change(function () {
    let is_docomo = $('input[name="is_docomo"]:checked').val();
    if (is_docomo == 1) {
        $('.docomo-identifier_account').show();
    }else{
        $('.docomo-identifier_account').hide();
    }
});