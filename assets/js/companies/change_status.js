
let change_status = {
    companies: [],
    companies_checked_name: 'companies_checked_name',
    companies_checked_value: [],
    companies_unchecked_name: 'companies_unchecked_name',
    companies_unchecked_value: [],
    check_all() {
        $('.checkbox_one').prop('checked', true);
        window.sessionStorage.removeItem(change_status.companies_unchecked_name);
        window.sessionStorage.removeItem(change_status.companies_checked_name);

        window.sessionStorage.setItem(change_status.companies_checked_name, JSON.stringify([$('.checkbox_all').val()]));
    },
    uncheck_all() {
        $('.checkbox_one').prop('checked', false);
        window.sessionStorage.removeItem(change_status.companies_checked_name);
        window.sessionStorage.removeItem(change_status.companies_unchecked_name);
    },
    check_all_handle()
    {
        if ( $(this).prop('checked') ) {
            change_status.check_all();
        }else{
            change_status.uncheck_all();
        }
    },
    addSesstionItem(item_value, key){
        let session_value = [];
        if (window.sessionStorage.getItem(key) != null) {
            session_value = JSON.parse(window.sessionStorage.getItem(key));
        }
        session_value.push(item_value);
        window.sessionStorage.setItem(key, JSON.stringify(session_value));
    },
    removeSesstionItem(item_value, key){
        let session_value = [];
        if (window.sessionStorage.getItem(key) != null) {
            session_value = JSON.parse(window.sessionStorage.getItem(key));
            let index = session_value.indexOf(item_value);
            if (index > -1) {
                session_value.splice(index, 1);
                window.sessionStorage.setItem(key, JSON.stringify(session_value));
            }
        }
    },
    check_one(element)
    {
        let item_value = $(element).val();
        change_status.removeSesstionItem(item_value, change_status.companies_unchecked_name);
        change_status.addSesstionItem(item_value, change_status.companies_checked_name);

    },
    uncheck_one(element)
    {
        $('.checkbox_all').prop('checked', false);
        let item_value = $(element).val();
        change_status.removeSesstionItem(item_value, change_status.companies_checked_name);
        change_status.addSesstionItem(item_value, change_status.companies_unchecked_name);
    },
    check_one_handle()
    {
        if ( $(this).prop('checked') ) {
            change_status.check_one(this);
        }else{
            change_status.uncheck_one(this);
        }
    },
    showModal() {
        let companies_id_checked = JSON.parse(window.sessionStorage.getItem(change_status.companies_checked_name));
        if (companies_id_checked == null || companies_id_checked.length == 0) {
            $('#error-checkbox').show();
            setTimeout(function () {
                $('#error-checkbox').fadeOut();
            }, 3000);
        } else {
            $('#modal_change_status').modal('show');
        }
    },
    closeError() {
        $('#error-checkbox').hide();
    },
    choseTypeModal()
    {
        let value = $('input[name="type_change"]:checked').val();
        if (value == 'type_contract') {
            $('.type_contract_area').show();
            $('.duration_contract_area').hide();
        } else {
            $('.type_contract_area').hide();
            $('.duration_contract_area').show();
        }
    },
    submitChangeType()
    {
        let formData = $('#form-change_type_contract').serializeArray();
        let companies_id_checked = JSON.parse(window.sessionStorage.getItem(change_status.companies_checked_name));
        let companies_id_unchecked = JSON.parse(window.sessionStorage.getItem(change_status.companies_unchecked_name));
        $.each(companies_id_checked, function( index, value ) {
            formData.push({name:'company_ids[]', value: value});
        });
        if (companies_id_unchecked != null) {
            $.each(companies_id_unchecked, function( index, value ) {
                formData.push({name:'company_ids_unchecked[]', value: value});
            });
        }
        $.ajax({
            type: 'POST',
            url:  '/companies/change_type_contract_batch' + window.location.search,
            data: formData,
            success: function (res) {
                window.sessionStorage.removeItem(change_status.companies_checked_name);
                window.sessionStorage.removeItem(change_status.companies_unchecked_name);
                window.location.href = '/companies';
            },
            error: function (errors) {
                console.log(errors);
            }
        });
    },
    initCheckbox()
    {
        let checked = JSON.parse(window.sessionStorage.getItem(change_status.companies_checked_name));
        let unchecked = JSON.parse(window.sessionStorage.getItem(change_status.companies_unchecked_name));
        if (checked != null && checked.includes('all')) {
            if (unchecked == null) {
                $('.checkbox_all').prop('checked', true);
                $( ".checkbox_one" ).each(function( index ) {
                    $(this).prop('checked', true);
                });
            } else{
                $( ".checkbox_one" ).each(function( index ) {
                    if (!unchecked.includes($(this).val()) ){
                        $(this).prop('checked', true);
                    }
                });
            }
        } else if (checked != null && !checked.includes('all')) {
            $( ".checkbox_one" ).each(function( index ) {
                if (checked.includes($(this).val()) ){
                    $(this).prop('checked', true);
                }
            });
        }
    },
    addEventListener() {
        $(document)
            .on('click', '.checkbox_all', this.check_all_handle)
            .on('click', '.checkbox_one', this.check_one_handle)
            .on('click', '.btn-change_status', this.showModal)
            .on('click', '#error-checkbox .close', this.closeError)
            .on('change', 'input[name="type_change"]', this.choseTypeModal)
            .on('click', '.btn-change_status-store', this.submitChangeType)
    },
    init() {
        this.addEventListener();
        this.initCheckbox();
        setTimeout(function () {
            $('.alert-dismissible').fadeOut();
        }, 3000);
    }
};
$(document).ready(function () {
    change_status.init();
});