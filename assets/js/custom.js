//date picker
$(".datepicker").datepicker({ 
    clearBtn: true,
    autoclose: true,
    todayBtn: 'linked',
    format:'yyyy-mm-dd',
    language:'ja',
    orientation: 'auto bottom',
    todayHighlight: true,
    calendarWeeks: true
});


$(document).on("click", ".datepicker", function(e){
    $(this).datepicker({
      clearBtn: true,
      autoclose: true,
      todayBtn: 'linked',
      format:'yyyy-mm-dd',
      language:'ja',
      orientation: 'auto bottom',
      todayHighlight: true,
      calendarWeeks: true
});

});
$('.disable-to-current').datepicker('setStartDate', new Date());

// $('.datepicker').datepicker({
//     beforeShow: function(input, inst)
//     {
//         inst.dpDiv.css({marginTop: -input.offsetHeight + 'px', marginLeft: input.offsetWidth + 'px'});
//     }
// });

$(document).on('click', '#button-submit-save, .loading', function () {
    $('body').loadingModal({animation: 'circle'});
});

function current_date(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    return yyyy + '-' + mm + '-' + dd;
}

$('#use_start').on('change', function() {
    use_start = $('#use_start').val();

    if(use_start != ''){
        $('#use_end').datepicker('setStartDate', new Date(use_start));
    }
    else {
        $('#use_end').datepicker('setStartDate', new Date('1970-01-01'));
    }
});

$('#created_date_start').on('change', function() {
    var created_date_start = $('#created_date_start').val();
    var created_date_end = $('#created_date_end').val();
    if(created_date_start != '' && (created_date_end == '' || created_date_end < created_date_start)){
        $('#created_date_end').val(created_date_start);
    }
    if(created_date_start != ''){
        $('#created_date_end').datepicker('setStartDate', new Date(created_date_start));
    } else {
        $('#created_date_end').datepicker('setStartDate', new Date('1970-01-01'));
    }
});
