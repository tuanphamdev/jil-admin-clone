var avatar = function () {

    var cachedResponse = function (url, element) {
        caches.open('jinjer_admin').then(function (cache) {
            return cache.match(url).then(function (response) {
                if (response) {
                    console.log('Found response in cache:', url);
                    return response;
                }

                console.log('Not found in cache, fetching request from the network: ', url);
                return fetch(url).then(function (networkResponse) {
                    console.log('Put response to cache', url);
                    cache.put(url, networkResponse.clone());

                    return networkResponse;
                });
            });
        }).then(function (response) {
            return response.text();
        }).then(function (data) {
            $(element).attr('src', data);
        }).catch(function () {
            // Handles exceptions that arise from match() or fetch().
            console.log("Request is fail");
            $(element).attr('src', '/assets/images/avatars/bg.png');
        });
    };

    var loadAvatar = function(){
        $('#department-tree .group-box-header__icon img, .employee-list__content img, .personal-profile__image img, .topnav-list__item--name img, .cheang-list__item01 img').each(function (index, element) {
            var avatar_id = $(element).attr('avatar-id');
            var core_company_id = $(element).attr('core-company-id');
            if (typeof avatar_id !== "undefined" && avatar_id > 0 && core_company_id > 0) {
                var url = "/admin_employees/avatar/" + avatar_id + '/' + core_company_id;
                cachedResponse(url, element);
            }
        })
    }

    var documentReady = function(){
        $(document).ready(function(){
            loadAvatar()
        });
    }

    var init = function () {
       documentReady()
    }

    return {
        init: function () {
            init();
        }
    }
}

$(function () {
    avatar().init()
})
