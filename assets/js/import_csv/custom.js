$(document).ready(function() {
    'use-strict';

    //Upload csv bank
    $('#csv_bank').filer({
        limit: 1,
        maxSize: null,
        changeInput: true,
        appendTo: null,
        extensions: ['csv'],
        changeInput: '<div class="jFiler-input-dragDrop">\n' +
            '    <div class="jFiler-input-inner">\n' +
            '        <div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div>\n' +
            '      \t<a class="jFiler-input-choose-btn btn btn-custom">Browse Files</a>\n' +
            '  \t</div>\n' +
            '</div>',
        showThumbs: true,
        theme: "dragdropbox",
        addMore: false,
        clipBoardPaste: true,
        excludeName: null,
        beforeShow: function(){return true},
        captions: {
            errors: {
                filesType: "アップロードしようとしたファイルは、許可されていない種類です。"
            }
        }
    });
    $('#status_filter').change(function () {
        var status_filter = 1;
        if ($(this).prop('checked') == false) {
            status_filter = 0;
        }
        var location = window.location;
        var url = location.pathname;
        if (url.match(/^search\/([0-9]+)$/)) {
            url = url.replace(/^search\/([0-9]+)$/, '/search/1');
        } else {
            url = url.replace(/([0-9]+)/, '1');
        }
        var base_url = location.origin + url;
        var params = new window.URLSearchParams(location.search);
        params.set("status_filter", status_filter);
        var param_obj = {};

        params.forEach(function (value,key) {
            param_obj[key] = value;
        });
        var url_encoded = $.param(param_obj, true );
        document.location.href = base_url + "?" + url_encoded;
    });
});

