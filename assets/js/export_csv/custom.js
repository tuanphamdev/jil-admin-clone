$(document).ready(function() {
    'use-strict';

    $("#csv_company").click(function(e) {
        var url = "/export_csv/company_download";
        window.open(url,'_self');
    });

    $("#csv_company_staff").click(function(e) {
        var url = "/export_csv/company_info";
        window.open(url,'_self');
    });

    $("#csv_check_in_btn").click(function(e) {
        startDate = $('#start_date_check_in').val();
        endDate = $('#end_date_check_in').val();
        var url = "/export_csv/count_timecards?start_time="+startDate+"&end_time="+endDate;
        window.open(url,'_blank');
    });

    $("#csv_tighten_btn").click(function(e) {
        startDate = $('#start_date_tighten').val();
        endDate = $('#end_date_tighten').val();
        var url = "/export_csv/count_tightens?start_time="+startDate+"&end_time="+endDate;
        window.open(url,'_blank');
    });

    $("#csv_retire_btn").click(function(e) {
        startDate = $('#start_date_retire').val();
        endDate = $('#end_date_retire').val();
        var url = "/export_csv/retire_staff?start_date_retire="+startDate+"&end_date_retire="+endDate;
        window.open(url,'_self');
        $('#csv_retire').modal('toggle');
        $(".alert").hide();
    });

    $("#csv_tokyo_marine_btn").click(function(e) {
        var url = "/export_csv/tokyo_marine_staff";
        window.open(url,'_self');
    });

    $("#csv_health_keihi_btn").click(function(e) {
        startDate = $('#start_date_health_keihi').val();
        endDate = $('#end_date_health_keihi').val();
        var url = "/export_csv/csv_health_keihi?start_date="+startDate+"&end_date="+endDate;
        window.open(url,'_blank');
    });

  $("#csv_health_salary_btn").click(function(e) {
    startDate = $('#start_date_health_salary').val();
    endDate = $('#end_date_health_salary').val();
    var url = "/export_csv/csv_health_salary?start_date="+startDate+"&end_date="+endDate;
    window.open(url,'_blank');
  });

    $("#csv_health_signing_btn").click(function(e) {
        startDate = $('#start_date_health_signing').val();
        endDate = $('#end_date_health_signing').val();
        var url = "/export_csv/csv_health_signing?start_date="+startDate+"&end_date="+endDate;
        window.open(url,'_blank');
    });
});

$('#registered_date_from').on('change', function() {
    registered_date_from = $('#registered_date_from').val();

    if(registered_date_from != ''){
        $('#registered_date_to').datepicker('setStartDate', new Date(registered_date_from));
    }
    else {
        $('#registered_date_to').datepicker('setStartDate', new Date('1970-01-01'));
    }
});

$('#start_date_retire').on('change', function() {
    start_date_retire = $('#start_date_retire').val();
    end_date_retire = $('#end_date_retire').val();

    if(start_date_retire != ''){
        if(end_date_retire < start_date_retire){
            $('#end_date_retire').val(start_date_retire);
        }
        $('#end_date_retire').datepicker('setStartDate', new Date(start_date_retire));
    }
    else {
        $('#end_date_retire').datepicker('setStartDate', new Date('1970-01-01'));
    }
});
$(".datepicker-salary").datepicker({
  clearBtn: true,
  autoclose: true,
  todayBtn: 'linked',
  format:'yyyy-mm',
  language:'ja',
  orientation: 'auto bottom',
  todayHighlight: true,
  calendarWeeks: true
});
