$(function () {
    function matchCustom(params, data) {
        if ($.trim(params.term) === '' || data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
            var modifiedData = $.extend({}, data, true);
            return modifiedData;
        }

        if (typeof data.text === 'undefined') {
            return null;
        }

        return null;
    }

    $('select.filter_text').select2({
        matcher: matchCustom,
        theme: "jinjer",
        language: {
            noResults: function (params) {
                return "検索結果がありません。";
            }
        }
    });
});