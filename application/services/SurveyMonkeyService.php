<?php


namespace Jinjer\services;

class SurveyMonkeyService
{
    protected $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('survey_monkey_model');
        $this->CI->load->driver('cache', ['adapter' => 'redis', 'backup' => 'file']);
    }

    public function getLastSurveyMonkey()
    {
        return $this->CI->survey_monkey_model->as_array()
            ->fields('*')
            ->limit(1)
            ->get();
    }

    public function resetSurveyMonkeyEmployeesStatusCache()
    {
        $employeesStatus = $this->CI->cache->getKeys(EMPLOYEE_SURVEY_PREFIX_CACHE . '*');
        foreach ($employeesStatus as $key) {
            $this->CI->cache->delete($key);
            $this->CI->cache->save($key, EMPLOYEE_SURVEY_STATUS_IS_SHOW);
        }
    }

    public function checkStatusUpdateSurveyMonkey($input, $oldSurveyMonkey)
    {
        $isUpdate = false;
        if (empty($oldSurveyMonkey)) {
            $isChangeScriptContent = true;
            return [$isChangeScriptContent, $isUpdate];
        }
        $isChangeScriptContent = $input['script_content'] != $oldSurveyMonkey['script_content'];
        foreach ($input as $key => $value) {
            if ($key != 'script_content' && $value != $oldSurveyMonkey[$key]) {
                $isUpdate = true;
            }
        }
        return [$isChangeScriptContent, $isUpdate];
    }
}
