<?php


namespace Jinjer\services;

class AgreementService
{
    protected $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model(['agreement_model', 'company_model']);
        $this->CI->load->library('clients/core_company_client');
    }

    public function validate($input)
    {
        if (empty($input['company_ids'])) {
            return ['company_ids' =>  '変更する企業を選択して下さい'];
        }
        if (isset($input['type_change']) && $input['type_change'] == 'type_contract') {
            return $this->validateFormTypeContractBatch($input);
        } elseif (isset($input['type_change']) && $input['type_change'] == 'duration_contract') {
            return $this->validateFormDurationContractBatch($input);
        }
        return [];
    }
    private function validateFormTypeContractBatch($input)
    {
        if (!isset($input['type_contract']) || $input['type_contract'] == '') {
            return ['type' => '契約タイプを選択して下さい。'];
        }
        return [];
    }
    private function validateFormDurationContractBatch($input)
    {
        if (!isset($input['start_date']) && !isset($input['end_date'])
            || (empty($input['start_date']) && empty($input['end_date']))) {
            return ['date' => '契約期間を入力してください。'];
        }
        if (isset($input['start_date'], $input['end_date'])
            && ((!empty($input['start_date']) && !is_format_date($input['start_date']))
                ||(!empty($input['end_date']) && !is_format_date($input['end_date']))
            )) {
            return ['date' => '契約期間を入力してください。'];
        }
        if (isset($input['start_date'], $input['end_date'])
            && !empty($input['start_date']) && !empty($input['end_date'])
            && strtotime($input['start_date']) > strtotime($input['end_date'])) {
            return ['date' => '契約期間は開始日＜終了日となるように入力してください。'];
        }
        return [];
    }

    public function handleSaveChangeTypeBatch($input)
    {
        $company_ids_chunk = array_chunk($input['company_ids'], 300);
        $totalRow = 0;
        $errors = [];
        foreach ($company_ids_chunk as $company_ids_input) {
            try {
                $this->CI->db->trans_begin();
                if ($input['type_change'] == 'type_contract') {
                    $this->CI->db->from('agreements')
                        ->set('type', $input['type_contract'])
                        ->where_in('company_id', $company_ids_input)
                        ->update();
                    $params = [
                        'company_ids' => $company_ids_input,
                        'contract_type' => $input['type_contract']
                    ];
                    $res = $this->CI->core_company_client->update_contract_type($params);
                    if (!$res['status']) {
                        throw new \Exception("Can't update contract type", 500);
                    }
                    $totalRow = $totalRow + $this->CI->db->affected_rows();
                } elseif ($input['type_change'] == 'duration_contract') {
                    $agreements = $this->CI->db->select(['agreements.id', 'agreements.company_id', 'companies.code', 'agreements.start_date', 'agreements.end_date', 'agreements.deleted_at'])
                        ->from('agreements')
                        ->join('companies', 'agreements.company_id = companies.id')
                        ->where_in('company_id', $company_ids_input)
                        ->get()
                        ->result_array();
                    $dataUpdate = [];
                    $params = [];
                    foreach ($agreements as $agreement) {
                        if (!empty($input['start_date']) && !empty($input['end_date'])) {
                            $params['company_ids'][] = $agreement['company_id'];
                            $params['start_date'] = $input['start_date'];
                            $params['end_date'] = $input['end_date'];
                            $dataUpdate[$agreement['id']] = [
                                'id' => $agreement['id'],
                                'company_id' => $agreement['company_id'],
                                'start_date' => $input['start_date'],
                                'end_date' => $input['end_date'],
                            ];
                        }
                        if (empty($input['start_date']) || empty($input['end_date'])) {
                            if (!empty($input['start_date']) && strtotime($input['start_date']) > strtotime($agreement['end_date'])) {
                                $errors[$agreement['company_id']] = sprintf($this->CI->lang->line('company_fail_update_contract_message'), $agreement['code']);
                                continue;
                            } elseif (!empty($input['start_date'])) {
                                $params['company_ids'][] = $agreement['company_id'];
                                $params['start_date'] = $input['start_date'];
                                $dataUpdate[$agreement['id']] = [
                                    'id' => $agreement['id'],
                                    'start_date' => $input['start_date'],
                                ];
                            }
                            if (!empty($input['end_date']) && strtotime($input['end_date']) < strtotime($agreement['start_date'])) {
                                $errors[$agreement['company_id']] = sprintf($this->CI->lang->line('company_fail_update_contract_message'), $agreement['code']);
                                continue;
                            } elseif (!empty($input['end_date'])) {
                                $params['company_ids'][] = $agreement['company_id'];
                                $params['end_date'] = $input['end_date'];
                                $dataUpdate[$agreement['id']] = [
                                    'id' => $agreement['id'],
                                    'company_id' => $agreement['company_id'],
                                    'end_date' => $input['end_date'],
                                ];
                            }
                        }
                    }
                    if (!empty($dataUpdate)) {
                        $this->CI->db->update_batch('agreements', $dataUpdate, 'id');
                        $res = $this->CI->core_company_client->update_contract_type($params);
                        if (!$res['status']) {
                            throw new \Exception("Can't update contract date", 500);
                        }
                        $totalRow = $totalRow + count($dataUpdate);
                    }
                }
                $this->CI->db->trans_commit();
            } catch (\Exception $e) {
                $this->CI->db->trans_rollback();
                log_message('info', 'Change type contract fail with company_ids: ' . json_encode($company_ids_input));
                continue;
            }
        }
        return [$totalRow, $errors];
    }
}
