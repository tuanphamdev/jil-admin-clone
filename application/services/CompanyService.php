<?php


namespace Jinjer\services;

class CompanyService
{
     static $services = [
        'kintai', 'roumu', 'jinji', 'keihi', 'ai_ocr', 'ebook_store', 'credit_card', 'timestamp', 'my_number',
        'work_vital', 'salary', 'workflow', 'employee_contract', 'signing', 'year_end_tax_adjustment', 'ultra',
        'signing_folder', 'signing_folder_range', 'signing_internal_workflow', 'signing_batch_import',
        'import_signed_file', 'ip_limit', 'send_sms', 'electronic_signature_long_term', 'electronic_signature_timestamp', 'electronic_signature_authentication',
        'send_function_digital_signature', 'imprint_registration', 'attach_file_receiver', 'attach_file_sender',
        'alert_function', 'language_support','kintai_sense_link', 'kintai_alligate',
     ];

    /**
     * Check is only enable signing service
     *
     * @param array $data
     *
     * @return mixed
     */
     public static function getValueJinjerDB(array $data)
     {
         $contracts_name = [
            'kintai',
            'roumu',
            'jinji',
            'keihi',
            'my_number',
            'work_vital',
            'salary',
            'workflow',
            'employee_contract',
            'year_end_tax_adjustment',
            'signing',
            'bi'
         ];
         $data_contracts = array_only($data, $contracts_name);
         $enable_services = array_filter($data_contracts, function ($item) {
            return $item == 1;
         });
         $numberService = count($enable_services);
         if (isset($enable_services['signing']) &&  $numberService == 1) {
             return 0;
         }
         if ($numberService >= 1) {
             return 1;
         }
         return false;
     }

     public static function setOptionSigning($params)
     {
         if (isset($params['signing']) && $params['signing'] == 0) {
             $params['maximum_signing'] = '';
             $params['signing_folder'] = 0;
             $params['signing_folder_range'] = 0;
             $params['signing_internal_workflow'] = 0;
             $params['signing_batch_import'] = 0;
             $params['import_signed_file'] = 0;
             $params['maximum_import_signed_file'] = '';
             $params['import_signed_file_timestamp'] = 0;
             $params['ip_limit'] = 0;
             $params['send_sms'] = 0;
             $params['electronic_signature_long_term'] = 0;
             $params['electronic_signature_timestamp'] = 0;
             $params['electronic_signature_authentication'] = 0;
             $params['send_function_digital_signature'] = 0;
             $params['imprint_registration'] = 0;
             $params['attach_file_receiver'] = 0;
             $params['attach_file_sender'] = 0;
             $params['alert_function'] = 0;
             $params['language_support'] = 0;
         }
         if (!isset($params['import_signed_file']) || isset($params['import_signed_file']) && $params['import_signed_file'] == 0) {
             $params['maximum_import_signed_file'] = '';
             $params['import_signed_file_timestamp'] = 0;
         }
         return $params;
     }

     public static function setDateEnableServiceForCreate($params)
     {
         $date_default = jp_current_date_from_utc('now');

         foreach (CompanyService::$services as $value) {
             $params['date_enable_' . $value] = $params[$value] ? $date_default : null;
         }
         return $params;
     }

     public static function setOptionKintai($params)
     {
         if (isset($params['kintai']) && $params['kintai'] == 0) {
             $params['kintai_sense_link'] =  0;
             $params['kintai_alligate'] = 0;
         }
         return $params;
     }
}
