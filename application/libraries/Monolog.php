<?php

use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Logger;

class Monolog
{
    const BASE_LOG_PATH = '/var/log/jinjer-admin/';

    const EXTERNAL = 'external';
    const QUERY = 'query';
    const EXCEPTION = 'exception';
    const DEBUG = 'debug';
    const CRONJOB = 'cronjob';
    const INFO = 'info';

    public function __construct()
    {
    }
    protected function get_channel()
    {
        $ci = &get_instance();
        return $ci->router->fetch_class() . "-->" . $ci->router->fetch_method();
    }
    protected function get_path($log_type = 'debug')
    {
        return self::BASE_LOG_PATH.$log_type.'/'.$log_type.'_'.date('Y-m-d').'.log';
    }

    public function logger($log_type)
    {
        $logger = new Logger($this->get_channel());
        $handler = new StreamHandler($this->get_path($log_type), $log_type);
        $formatter = new LineFormatter(null, null, false, true);
        $handler->setFormatter($formatter);
        $logger->pushHandler($handler);
        $logger->pushHandler(new FirePHPHandler());
        return $logger;
    }
}
