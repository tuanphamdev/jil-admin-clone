<?php defined('BASEPATH') or exit('No direct script access allowed');
//use Jinjer\traits\Company_validation_trait;

class MY_Form_validation extends CI_Form_validation
{
    //use Company_validation_trait;
    public function __construct()
    {
        parent::__construct();
    }
/*
|--------------------------------------------------------------------------
| Your customize validation
|--------------------------------------------------------------------------
|
| Implement all your customize validation
|
*/

    /**
     * Valid Telephone
     *
     * @param string $str
     * @return bool
     */
    public function valid_tel($str)
    {
        $format_1 = '(^[0-9]{2}-[0-9]{4}-[0-9]{4}$)';
        $format_2 = '(^[0-9]{3}-[0-9]{3,4}-[0-9]{4}$)';
        $format_3 = '(^[0-9]{4}-[0-9]{2,3}-[0-9]{4}$)';
        $format_4 = '(^[0-9]{5}-[0-9]{1,2}-[0-9]{4}$)';
        $format_5 = '(^[0-9]{5}-[0-9]{2}-[0-9]{3}$)';
        $format_6 = '(^[0-9]{4,5}-[0-9]{3}-[0-9]{3}$)';
        $format_7 = '(^[0-9]{3}-[0-9]{4}-[0-9]{3}$)';
        $regex = '/' . $format_1 . '|' . $format_2 . '|' . $format_3 . '|' . $format_4 . '|' . $format_5 . '|' . $format_6 . '|' . $format_7 . '/';
        if (!preg_match($regex, $str)) {
            $this->set_message('valid_tel', $this->CI->lang->line('my_form_validation_valid_tel'));
            return false;
        }
        return true;
    }

    /**
     * Valid furigana text
     *
     * @param string $str
     * @return bool
     */
    public function valid_furigana($str)
    {
        if (! preg_match("/^(?:\xE3\x82[\xA1-\xBF]|\xE3\x83[\x80-\xB6]|ー| | |　)+$/", mb_convert_encoding($str, 'UTF-8'))) {
            $this->set_message('valid_furigana', $this->CI->lang->line('my_form_validation_valid_furigana'));
            return false;
        }
        return true;
    }

     /**
     * Valid furigana text and numberic
     *
     * @param string $str
     * @return bool
     */
    public function valid_furigana_address($str)
    {
        if (! preg_match("/^(?:\xE3\x82[\xA1-\xBF]|\xE3\x83[\x80-\xB6]|ー| | |　|[0-9]|-|[A-Z]|[a-z])+$/", mb_convert_encoding($str, 'UTF-8'))) {
            $this->set_message('valid_furigana_address', $this->CI->lang->line('my_form_validation_valid_furigana_address'));
            return false;
        }
        return true;
    }

    /**
     * Valid zip
     *
     * @param string $str
     * @return bool
     */
    public function valid_zip($str)
    {
        $regex = '/^\d{3}-\d{4}$/';
        if (!preg_match($regex, $str)) {
            $this->set_message('valid_zip', $this->CI->lang->line('my_form_validation_valid_zip'));
            return false;
        }
        return true;
    }

    public function valid_date_format($str)
    {
        $str = str_replace(array('.', '-', '\\\\'), '/', $str);
        $regex = '/^[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}$/';
        $date_arr  = explode('/', $str);
        if (!preg_match($regex, $str) || !checkdate($date_arr[1], $date_arr[2], $date_arr[0])) { // month, day, year
            $this->set_message('valid_date_format', $this->CI->lang->line('my_form_validation_valid_date_format'));
            return false;
        }

        return true;
    }

    public function greater_than_date($str, $field_compare)
    {
        if (!empty($str)
            && isset($this->_field_data[$field_compare], $this->_field_data[$field_compare]['postdata'])
            && empty($this->_field_data[$field_compare]['error'])
            && strtotime($str) < strtotime($this->_field_data[$field_compare]['postdata'])
        ) {
            $this->set_message('greater_than_date', $this->CI->lang->line('my_form_validation_valid_greater_than_date'));
            return false;
        }
        return true;
    }

    /**
     * Valid is natural greater than zero
     *
     * @param string $str
     * @return bool
     */
    public function is_natural_greater_than_zero($str)
    {
        $regex = '/^[1-9][0-9]*$/';
        if (!preg_match($regex, $str)) {
            $this->set_message('is_natural_greater_than_zero', $this->CI->lang->line('my_form_validation_valid_natural_greater_than_zero'));
            return false;
        }
        return true;
    }

    /**
     * Valid enigma code
     *
     * @param string $str
     * @return bool
     */
    public function is_enigma_code($str)
    {
        $regex = '/^[a-zA-Z0-9~\`!@#$%^&*\(\)_\+-=\\\{\}\[\]:"";\'<>?,.\/\|]+$/';
        if (!preg_match($regex, $str)) {
            $this->set_message('is_enigma_code', $this->CI->lang->line('my_form_validation_valid_enigma_code'));
            return false;
        }
        return true;
    }








/*
|--------------------------------------------------------------------------
| Model Validation
|--------------------------------------------------------------------------
|
| Implement model validation, belong to your project's logic
|
*/

    /**
     * Company model validation
     *
     * @param string $str
     * @param string $field
     * @return mixed
     */
    public function company_validate($str, $field)
    {
        return $this->CI->company_model->validate($str, $field);
    }

    /**
     * Agreement model validation
     *
     * @param string $str
     * @param string $field
     * @return mixed
     */
    public function agreement_validate($str, $field)
    {
        return $this->CI->agreement_model->validate($str, $field);
    }

    /**
     * User model validation
     *
     * @param string $str
     * @param string $field
     * @return mixed
     */
    public function user_validate($str, $field)
    {
        return $this->CI->user_model->validate($str, $field);
    }

    /**
     * Ip model validation
     *
     * @param string $str
     * @param string $field
     * @return mixed
     */
    public function ip_validate($str, $field)
    {
        return $this->CI->ip_model->validate($str, $field);
    }

    /**
     * TS password ip model validation
     *
     * @param string $str
     * @param string $field
     * @return mixed
     */
    public function ts_password_ip_validate($str, $field)
    {
        return $this->CI->ts_password_ip_model->validate($str, $field);
    }

    /**
     * Information model validation
     *
     * @param string $str
     * @param string $field
     * @return mixed
     */
    public function information_validate($str, $field)
    {
        return $this->CI->information_model->validate($str, $field);
    }

    /**
     * Company model validation
     *
     * @param string $str
     * @param string $field
     * @return mixed
     */
    public function plans_validate($str, $field)
    {
        return $this->CI->plan_model->validate($str, $field);
    }
}
