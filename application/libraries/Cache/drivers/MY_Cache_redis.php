<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MY_Cache_redis extends \CI_Cache_redis
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * List all keys with pattern search
     *
     * @param $pattern
     *
     * @return array
     */
    public function getKeys($pattern)
    {
        return $this->_redis->keys($pattern);
    }
}
