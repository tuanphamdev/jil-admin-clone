<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MY_Cache_file extends \CI_Cache_file
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * List all keys with pattern search
     *
     * @param $pattern
     *
     * @return array
     */
    public function getKeys($pattern)
    {
        $files =  glob($this->_cache_path . $pattern);
        $filenames = [];
        foreach ($files as $file) {
            $filenames[] = basename($file);
        }
        return $filenames;
    }
}
