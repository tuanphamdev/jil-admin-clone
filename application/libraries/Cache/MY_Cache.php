<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Cache extends \CI_Cache
{
    public function __construct($config = array())
    {
        parent::__construct($config);
    }

    public function getKeys($pattern)
    {
        if (!method_exists($this->{$this->_adapter}, 'getKeys')) {
            return [];
        } else {
            return $this->{$this->_adapter}->getKeys($pattern);
        }
    }
}
