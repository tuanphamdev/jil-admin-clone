<?php
namespace Jinjer\libraries;

final class User_info
{
    public $user_id;
    public $api_token;
    public $email;
    public $password;
    public $user_name;

    private static $inst;
    /**
     * Call this method to get singleton
     *
     * @return User info
     */
    public static function instance()
    {
        if (!isset(self::$inst)) {
            self::$inst = new User_info();
        }
        return self::$inst;
    }

    /**
     * Private constructor so nobody else can instantiate it
     *
     */
    final private function __construct()
    {
    }
}
