<?php
use Firebase\JWT\JWT;

class Token
{

    /**
     * get token
     *
     * @param $data
     * @return string
     */
    public static function get_token($data, $secret_key = null)
    {
        $_CI = & get_instance();
        if (empty($secret_key)) {
            $_CI->load->config('jwt');
            $secret_key = $_CI->config->item('secret_key');
        }
        return JWT::encode($data, $secret_key);
    }

    /**
     * get payload
     *
     * @param $token
     * @param $leeway
     * @return bool|array
     */
    public static function get_payload($token, $secret_key = null, $leeway = null)
    {
        try {
            $_CI = & get_instance();
            $_CI->load->config('jwt');
            if (!empty($leeway) && is_numeric($leeway)) {
                JWT::$leeway = $leeway;
            }
            if (empty($secret_key)) {
                $_CI->load->config('jwt');
                $secret_key = $_CI->config->item('secret_key');
            }
            return (array)JWT::decode($token, $secret_key, array($_CI->config->item('alg')));
        } catch (Exception $e) {
            return false;
        }
    }
}
