<?php
class Ip_client extends MY_Client
{
    
    public function __construct()
    {
        parent::__construct();
    }

    public function list($params)
    {
        return $this->get('ips/list', ['query' => $params]);
    }

    public function create($params)
    {
        return $this->post('ips/create', ['form_params' => $params]);
    }

    public function update($params)
    {
        return $this->post('ips/update', ['form_params' => $params]);
    }

    public function detail($params)
    {
        return $this->get('ips/detail', ['query' => $params]);
    }

    public function delete($params)
    {
        return $this->post('ips/delete', ['form_params' => $params]);
    }
}
