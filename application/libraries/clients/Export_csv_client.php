<?php
class Export_csv_client extends MY_Client
{

    public function __construct()
    {
        parent::__construct();
    }

    public function list($params)
    {
        try {
            $response = $this->_client->get('export_csv/list', ['query' => $params]);
            return json_decode($response->getBody()->getContents(), true) ?? [];
        } catch (Exception $e) {
            return [];
        }
    }

    public function get_staff_kinai_jinjer_core()
    {
        try {
            $response = $this->_client->get('company/get_staff_company_use_kintai', ['base_uri' => getenv('API_CORE_URL')]);
            return json_decode($response->getBody()->getContents(), true) ?? [];
        } catch (Exception $e) {
            return [];
        }
    }

    public function retire_staff($params)
    {
        return $this->post('export_csv/retire_staff', ['form_params' => $params]);
    }

    public function tokyo_marine_staff($params)
    {
        return $this->post('export_csv/tokyo_marine_staff', ['form_params' => $params]);
    }
    
    public function get_company_info_download($params)
    {
        try {
            $response = $this->_client->post('company/data_info_csv', ['base_uri' => getenv('API_BATCH_URL'),  'query' => $params, 'form_params' => $params]);
            return json_decode($response->getBody()->getContents()??[], true) ?? [];
        } catch (Exception $e) {
            if (!empty($e->hasResponse())) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }

    public function get_tokyo_marine_staff_download($params)
    {
        try {
            $response = $this->_client->post('company/tokyo_marine_staff', ['base_uri' => getenv('API_BATCH_URL'), 'query' => $params, 'form_params' => $params ]);
            return json_decode($response->getBody()->getContents(), true) ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }

    public function get_company_download($params)
    {
        try {
            $response = $this->_client->post('company/data_csv', ['base_uri' => getenv('API_BATCH_URL'),  'query' => $params, 'form_params' => $params]);
            return json_decode($response->getBody()->getContents()??[], true) ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }
    
    public function get_count_timecards($params)
    {
        $base_uri = getenv('JINER_K_API_URL');
        $headers = array(
            'Authorization' => 'Bearer ' . getenv('JINER_KINTAI_AUTH_BEARER_TOKEN'),
        );
        try {
            $response = $this->_client->get('v1/connection/export/time_card_count_by_company', ['base_uri' => $base_uri, 'query' => $params, 'headers' => $headers]);
            return $response->getBody()->getContents() ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }

    public function get_count_tightens($params)
    {
        $base_uri = getenv('JINER_K_API_URL');
        $headers = array(
            'Authorization' => 'Bearer ' . getenv('JINER_KINTAI_AUTH_BEARER_TOKEN'),
        );
        try {
            $response = $this->_client->get('v1/connection/export/tighten_count_by_company', ['base_uri' => $base_uri, 'query' => $params, 'headers' => $headers]);
            return $response->getBody()->getContents() ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }

    public function get_health_keihi_download($params)
    {
        try {
            $response = $this->_client->post('company/keihi_health_data', ['base_uri' => getenv('API_BATCH_URL'),  'query' => $params, 'form_params' => $params]);
            return json_decode($response->getBody()->getContents()??[], true) ?? [];
        } catch (Exception $e) {
            if (!empty($e->hasResponse())) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }

    public function get_health_salary_download($params)
    {
        try {
            $response = $this->_client->post('company/post/salary_health_data', ['base_uri' => getenv('API_BATCH_URL'),  'query' => $params, 'form_params' => $params]);
            return json_decode($response->getBody()->getContents()??[], true) ?? [];
        } catch (Exception $e) {
            if (!empty($e->hasResponse())) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }

    public function get_health_signing_download($params)
    {
        try {
            $response = $this->_client->post('company/signing_health_data', ['base_uri' => getenv('API_BATCH_URL'),  'query' => $params, 'form_params' => $params]);
            return json_decode($response->getBody()->getContents()??[], true) ?? [];
        } catch (Exception $e) {
            if (!empty($e->hasResponse())) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }
}
