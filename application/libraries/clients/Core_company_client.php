<?php

require_once(APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'clients' . DIRECTORY_SEPARATOR . 'Core_client.php');

class Core_company_client extends Core_client
{
    public function __construct()
    {
        parent::__construct();
    }

    public function update_contract_type($params)
    {
        return $this->post('company/update_contract_info', $params);
    }
}
