<?php
class Employee_client extends MY_Client
{

    public function __construct()
    {
        parent::__construct();
    }

    public function list($params)
    {
        return $this->get('employees/list', ['query' => $params]);
    }

    public function enable_admin($params)
    {
        return $this->post('employees/enable_admin', ['form_params' => $params]);
    }

    public function get_file($file_id, $core_company_id)
    {
        try {
            $response = $this->_client->get('avatar/get/' . $file_id . '/' . $core_company_id, ['base_uri' => $this->_api_core_url]);
            return json_decode($response->getBody()->getContents(), true) ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }
}
