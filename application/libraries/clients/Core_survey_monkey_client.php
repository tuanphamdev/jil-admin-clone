<?php

require_once(APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'clients' . DIRECTORY_SEPARATOR . 'Core_client.php');

class Core_survey_monkey_client extends Core_client
{
    public function __construct()
    {
        parent::__construct();
    }

    public function reset_all_employees_status()
    {
        return $this->post('survey_monkey/reset_all_employees_status', []);
    }
}
