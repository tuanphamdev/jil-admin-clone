<?php

require_once(APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'clients' . DIRECTORY_SEPARATOR . 'Core_client.php');

class Bank_master_client extends Core_client
{
    public function __construct()
    {
        parent::__construct();
    }

    public function create_bank_master($param)
    {
        return $this->post('banks/bank_masters/admin_create', $param);
    }

    public function update_bank_master($id, $param)
    {
        return $this->post('banks/bank_masters/admin_update/' . $id, $param);
    }
}
