<?php
class Role_client extends MY_Client
{
    
    public function __construct()
    {
        parent::__construct();
    }

    public function list($params = [])
    {
        return $this->get('roles/list', ['query' => $params]);
    }

    public function create($params)
    {
        return $this->post('roles/create', ['form_params' => $params]);
    }

    public function update($params)
    {
        return $this->post('roles/update', ['form_params' => $params]);
    }

    public function detail($params)
    {
        return $this->get('roles/detail', ['query' => $params]);
    }

    public function delete($params)
    {
        return $this->post('roles/delete', ['form_params' => $params]);
    }
}
