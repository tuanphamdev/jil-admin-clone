<?php
class Qa_engine_client extends MY_Client
{
    
    public function __construct()
    {
        parent::__construct();
    }

    public function list($params = [])
    {
        return $this->get('qa_engines/list', ['query' => $params]);
    }

    public function create($params)
    {
        return $this->post('qa_engines/create', ['form_params' => $params]);
    }
}
