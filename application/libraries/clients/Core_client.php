<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Core_client extends MY_Client
{

    public function __construct()
    {
        parent::__construct();
    }

    public function post($url, $params)
    {
        try {
            $response = $this->_client->post($url, ['base_uri' => $this->_api_core_url, 'form_params' => $params]);
            $data = $response->getBody()->getContents();
            return json_decode($data, true) ?? [];
        } catch (\Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];

                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }
}
