<?php
class Import_csv_client extends MY_Client
{
    public function __construct()
    {
        parent::__construct();
    }

    public function list($params)
    {
        return $this->get('import_csv/list', ['query' => $params]);
    }

    public function import_csv_bank_info($params)
    {
        return $this->post('import_csv/import_csv_bank_info', ['form_params' => $params]);
    }

    public function detail($id)
    {
        return $this->get('import_csv/detail/' . $id, []);
    }
}
