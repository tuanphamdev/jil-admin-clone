<?php
class Ts_password_ip_client extends MY_Client
{
    
    public function __construct()
    {
        parent::__construct();
    }

    public function list($params)
    {
        return $this->get('ts_password_ips/list', ['query' => $params]);
    }

    public function create($params)
    {
        return $this->post('ts_password_ips/create', ['form_params' => $params]);
    }

    public function update($params)
    {
        return $this->post('ts_password_ips/update', ['form_params' => $params]);
    }

    public function detail($params)
    {
        return $this->get('ts_password_ips/detail', ['query' => $params]);
    }

    public function delete($params)
    {
        return $this->post('ts_password_ips/delete', ['form_params' => $params]);
    }
}
