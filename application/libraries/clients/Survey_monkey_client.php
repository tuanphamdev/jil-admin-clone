<?php
class Survey_monkey_client extends MY_Client
{
    
    public function __construct()
    {
        parent::__construct();
    }

    public function last_survey_monkey($params = [])
    {
        return $this->get('survey_monkeys/last_survey_monkey', ['query' => $params]);
    }

    public function save($params)
    {
        return $this->post('survey_monkeys/save', ['form_params' => $params]);
    }
}
