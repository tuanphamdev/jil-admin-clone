<?php
class Company_client extends MY_Client
{
    
    public function __construct()
    {
        parent::__construct();
    }

    public function list($params)
    {
        return $this->get('companies/list', ['query' => $params]);
    }

    public function create($params)
    {
        return $this->post('companies/create', ['form_params' => $params]);
    }

    public function update($params)
    {
        return $this->post('companies/update', ['form_params' => $params]);
    }

    public function detail($params)
    {
        return $this->get('companies/detail', ['query' => $params]);
    }
    
    public function prefecture()
    {
        return $this->get('address/prefectures', ['base_uri' => $this->_api_core_url]);
    }
    
    public function get_cities_by_pref_id($params)
    {
        return $this->get('address/cities', ['base_uri' => $this->_api_core_url, 'query' => $params]);
    }

    public function stop_service($params)
    {
        return $this->post('companies/stop_service', ['form_params' => $params]);
    }

    public function start_service($params)
    {
        return $this->post('companies/start_service', ['form_params' => $params]);
    }

    public function core_update_company($params)
    {
        try {
            $company_id = $params['company_id'] ?? 0;
            $response = $this->_client->post('company/update/' . $company_id, ['base_uri' => $this->_api_core_url, 'form_params' => $params]);
            return json_decode($response->getBody()->getContents(), true) ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }
    
    public function core_create_company($params)
    {
        try {
            $response = $this->_client->post('company/register', ['base_uri' => $this->_api_core_url, 'form_params' => $params]);
            return json_decode($response->getBody()->getContents(), true) ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }

    public function core_resend_account_info($params)
    {
        try {
            $response = $this->_client->post('company/resend_account_info', ['base_uri' => $this->_api_core_url, 'form_params' => $params]);
            return json_decode($response->getBody()->getContents(), true) ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }

    public function resend_account_info($params)
    {
        return $this->post('companies/request_resend_account_info', ['form_params' => $params]);
    }

    public function core_change_type_contract_batch($params, $filters)
    {
        return $this->post('companies/request_change_type_contract_batch?' . http_build_query($filters), ['form_params' => $params]);
    }
}
