<?php
class Ts_password_log_client extends MY_Client
{
    
    public function __construct()
    {
        parent::__construct();
    }

    public function list($params)
    {
        return $this->get('ts_password_logs/list', ['query' => $params]);
    }
}
