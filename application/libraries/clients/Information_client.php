<?php

class Information_client extends MY_Client
{

    public function __construct()
    {
        parent::__construct();
    }

    public function list($params)
    {
        return $this->get('informations/list', ['query' => $params]);
    }

    public function on($params)
    {
        return $this->post('informations/on', ['form_params' => $params]);
    }

    public function off($params)
    {
        return $this->post('informations/off', ['form_params' => $params]);
    }

    public function core_off_alert($params)
    {
        try {
            $information_id = $params['information_id'] ?? 0;
            $response = $this->_client->post('information/close/' . $information_id, ['base_uri' => $this->_api_core_url, 'form_params' => $params]);
            return json_decode($response->getBody()->getContents(), true) ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }

    public function core_on_alert($params)
    {
        try {
            $information_id = $params['information_id'] ?? 0;
            $response = $this->_client->post('information/open/' . $information_id, ['base_uri' => $this->_api_core_url, 'form_params' => $params]);
            return json_decode($response->getBody()->getContents(), true) ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }

    public function core_create_information($params)
    {
        try {
            $response = $this->_client->post('information/create', ['base_uri' => $this->_api_core_url, 'form_params' => $params]);
            return json_decode($response->getBody()->getContents(), true) ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }


    public function core_update_information($params)
    {
        try {
            $admin_information_id = $params['admin_information_id'] ?? 0;
            $response = $this->_client->post('information/update/' . $admin_information_id, ['base_uri' => $this->_api_core_url, 'form_params' => $params]);
            return json_decode($response->getBody()->getContents(), true) ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }
}
