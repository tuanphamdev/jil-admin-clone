<?php
class Ts_password_change_client extends MY_Client
{

    public function __construct()
    {
        parent::__construct();
    }

    public function list_log($params)
    {
        return $this->get('ts_password_change/list_log', ['query' => $params]);
    }

    public function list($params)
    {
        return $this->get('ts_password_change/list', ['query' => $params]);
    }

    public function detail_company_core($params)
    {

        return $this->get('ts_password_change/detail_company_core', ['query' => $params]);
    }

    public function change_password($params)
    {
        return $this->post('ts_password_change/change_password', ['form_params' => $params]);
    }

    public function change_password_core($params)
    {
        try {
            $company_id = $params['company_id'] ?? 0;
            $response = $this->_client->post('password/create_ts_password', ['base_uri' => $this->_api_core_url, 'form_params' => $params]);
            return json_decode($response->getBody()->getContents(), true) ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }

    public function get_file($file_id, $core_company_id)
    {
        try {
            $response = $this->_client->get('avatar/get/' . $file_id . '/' . $core_company_id, ['base_uri' => $this->_api_core_url]);
            return json_decode($response->getBody()->getContents(), true) ?? [];
        } catch (Exception $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $content = json_decode($response->getBody()->getContents(), true) ?? [];
                if (!empty($content)) {
                    if (isset($content['status']) && $content['status'] === false) {
                        $error = (array)$content['error'];
                        $error = array_values($error);
                        $content['error'] = $error;
                        unset($content['content']);
                        $content['code'] = $response->getStatusCode();
                        return $content;
                    }
                } else {
                    return [
                        'status' => false,
                        'error' => [
                            $response->getReasonPhrase()
                        ],
                        'code' => $response->getStatusCode()
                    ];
                }
            }

            return [
                'status' => false,
                'error' => [
                    'Jinjer Core: Connection refused.'
                ],
                'code' => $e->getCode()
            ];
        }
    }
}
