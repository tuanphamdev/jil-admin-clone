<?php
class Tokyo_marine_client extends MY_Client
{
    
    public function __construct()
    {
        parent::__construct();
    }

    public function list($params)
    {
        return $this->get('companies/list', ['query' => $params]);
    }

    public function create($params)
    {
        return $this->post('companies/create', ['form_params' => $params]);
    }

    public function update($params)
    {
        return $this->post('companies/update', ['form_params' => $params]);
    }

    public function detail($params)
    {
        return $this->get('companies/detail', ['query' => $params]);
    }
    
    public function prefecture()
    {
        return $this->get('address/prefectures', ['base_uri' => $this->_api_core_url]);
    }
    
    public function get_cities_by_pref_id($params)
    {
        return $this->get('address/cities', ['base_uri' => $this->_api_core_url, 'query' => $params]);
    }

    public function use_tokyo_marine($params)
    {
        return $this->post('companies/use_tokyo_marine', ['form_params' => $params]);
    }

    public function no_use_tokyo_marine($params)
    {
        return $this->post('companies/no_use_tokyo_marine', ['form_params' => $params]);
    }
}
