<?php
class User_client extends MY_Client
{
    
    public function __construct()
    {
        parent::__construct();
    }

    public function list($params)
    {
        return $this->get('users/list', ['query' => $params]);
    }

    public function list_all()
    {
        return $this->get('users/list_all', ['query' => []]);
    }

    public function create($params)
    {
        return $this->post('users/create', ['form_params' => $params]);
    }

    public function update($params)
    {
        return $this->post('users/update', ['form_params' => $params]);
    }

    public function detail($params)
    {
        return $this->get('users/detail', ['query' => $params]);
    }

    public function auth($params)
    {
        return $this->post('users/auth', ['form_params' => $params]);
    }

    public function forgot_password($params)
    {
        return $this->post('users/forgot_password', ['form_params' => $params]);
    }
    
    public function reissue_password($params)
    {
        return $this->post('users/reissue_password', ['form_params' => $params]);
    }
}
