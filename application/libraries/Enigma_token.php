<?php
class Enigma_token
{
    public function __construct()
    {
        $this->key = getenv('ENIGMA_API_KEY_V2'); //enigmaから発行したシークレットキー
        $this->alg = "HS256";
        $this->hs_alg = "sha256";
    }

    /*
    * token生成サンプル
    * String $company_code // 企業コード
    * return String token
    */
    public function get_token_test($company_code, $iss)
    {
        $time = time();
// ペイロード必須項目
        $payload = array(
            'aud' => 'hrtech', //固定値
            'sub' => $company_code,
            'iss' => $iss, //固定値
            'iat' => $time,
            'exp' => strtotime("5 minute"),
        );
        $token = $this->_encode($payload, $this->key);
        return $token;
    }

    /*
    * token encode
    * array $payload //tokenペイロード
    * String $key //秘密鍵
    * return String token
    */
    private function _encode($payload, $key)
    {
        $header = array('typ' => 'JWT', 'alg' => $this->alg);
        $segments = array();
        $segments[] = $this->_base64_encode(json_encode($header, JSON_UNESCAPED_SLASHES));
        $segments[] = $this->_base64_encode(json_encode($payload, JSON_UNESCAPED_SLASHES));
        $signing_input = implode('.', $segments);
        $signature = hash_hmac($this->hs_alg, $signing_input, $key, true);
        $segments[] = $this->_base64_encode($signature);
        return implode('.', $segments);
    }

    /*
    * safety base 64 encode
    * String $input //ヘッダー or ペイロード or 署名
    * return String base64 encoded text
    */
    private function _base64_encode($input)
    {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }
}
