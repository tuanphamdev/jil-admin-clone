<?php
class LanguageLoader
{
    public function initialize()
    {
        $ci = & get_instance();
        $ci->load->helper('language');
        $ci->load->config('settings');
        $support_languages = $ci->config->item('support_languages');
        $rq_header = $ci->input->request_headers();
        if (isset($rq_header['Lg']) && in_array($rq_header['Lg'], $support_languages)) {
            $ci->lang->load($ci->config->item('language_files'), $rq_header['Lg']);
        } else {
            $ci->lang->load('rest_controller', 'english');
        }
    }
}
