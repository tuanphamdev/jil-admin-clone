<?php
use Jinjer\middleware\BaseMiddleware;
use Luthier\MiddlewareInterface;

class AuthMiddleware extends BaseMiddleware implements MiddlewareInterface
{
    public function __construct()
    {
        parent::__construct();
        $this->ci->load->model(['user_model']);
    }

    public function run($args)
    {
        $user_data = $this->ci->token->get_payload($this->api_token);
        $email = $user_data['email'];
        $password = $user_data['password'];
        $user = $this->ci->user_model->auth($email, $password);
        if (empty($user)) {
            $this->response_failure(['Unauthorized'], 401);
        }
    }
}
