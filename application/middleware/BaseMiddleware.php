<?php
namespace  Jinjer\middleware;

abstract class BaseMiddleware
{
    protected $ci;
    protected $api_token;
    public function __construct()
    {
        $this->ci = & get_instance();
        $this->api_token = $this->ci->input->get_request_header('Api-Token', true);
        if (empty($this->api_token)) {
            $this->response_failure(['Unauthorized'], 401);
        }
    }

    protected function response($data, $http_code)
    {
        $this->ci->output->set_content_type('application/json');
        $this->ci->output->set_status_header($http_code);
        $output = $this->ci->format->factory($data)->{'to_json'}();
        $this->ci->output->set_output($output)->_display();
        exit;
    }

    /**
     * Response failure
     *
     * @param array $error
     * @param string $http_code
     */
    protected function response_failure(array $error, $http_code)
    {
        return $this->response([
            $this->ci->config->item('rest_status_field_name') => false,
            $this->ci->config->item('rest_message_field_name') => $error
        ], $http_code);
    }

    /**
     * Response success
     *
     * @param $data
     * @param string $http_code
     */
    protected function response_success($data, $http_code)
    {
        return $this->response([
            $this->ci->config->item('rest_status_field_name') => true, 'data' => $data
        ], $http_code);
    }
}
