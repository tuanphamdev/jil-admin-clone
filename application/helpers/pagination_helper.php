<?php
if (!function_exists("pagination")) {
    function pagination($base_url, $total_rows, $per_page, $uri_segment = 3)
    {
        $ci = & get_instance();
        $ci->load->library('pagination');

        $config = array();
        $config['reuse_query_string'] = true;
        $config["base_url"] = $base_url;
        $config["total_rows"] = $total_rows;
        $config["per_page"] = $per_page;
        $config["uri_segment"] = $uri_segment;

        $config['use_page_numbers']  = true;

        $config['full_tag_open'] = '<ul class="pagination justify-content-end">';
        $config['full_tag_close'] = '</ul>';

        // opening tag for current page link
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="">';
        $config['cur_tag_close'] = '</a></li>';

        // customizing first link
        $config['first_link'] = $ci->lang->line('pagination_first_link');
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';

        // customize last link
        $config['last_link'] = $ci->lang->line('pagination_last_link');
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';

        // customizing previous link
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['prev_link'] = '<';
        // customizing next link
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['next_link'] = '>';

        // tag for all pages link
        $config['num_tag_open'] = '<li class="page-item">';
        ;
        $config['num_tag_close'] = '</li>';

        $config['attributes'] = array('class' => 'page-link');

        $ci->pagination->initialize($config);
        $pagination =  $ci->pagination->create_links();
        // get statistic pagination

        $fromPage = ($ci->pagination->cur_page - 1) * $ci->pagination->per_page +1 ;
        $toPage =  $ci->pagination->cur_page * $ci->pagination->per_page;
        if ($toPage > $total_rows) {
            $toPage = $total_rows;
        }
        $statistic =  "";
        if ($total_rows > $ci->pagination->per_page) {
            //$statistic =  "Showing from ".$fromPage." to ".$toPage." of ". $total_rows." entries";
            $statistic = sprintf($ci->lang->line('pagination_show_info'), $total_rows, $fromPage, $toPage);
        }

        return array("pagination" => $pagination, "statistic" => $statistic);
    }
}
