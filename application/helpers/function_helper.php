<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * function dump and break.
 */
if (!function_exists('dd')) {
    function dd($data)
    {
        dump($data);
        exit;
    }
}

// --------------------------------------------------------------------
if (!function_exists('remove_empty_elements')) {
    function remove_empty_elements(array $data)
    {
        return array_filter($data, function ($element) {
            return trim($element) !== '' && !is_null($element);
        });
    }
}

/**
 * Convert time server to local
 */
if (!function_exists("convertToLocalTimezone")) {
    function convertToLocalTimezone($source, $format = "Y-m-d H:i:s")
    {
        $localTimezone = $_COOKIE['jinjer_local_timezone'] ?? 0;
        $target = new DateTime($source);
        $target->modify("$localTimezone hours");
        $date = $target->format($format);
        return $date;
    }
}

/**
 * CSV Helpers
 * Array to CSV
 *
 * download == "" -> return CSV string
 * download == "toto.csv" -> download file toto.csv
 */
if (!function_exists('array_to_csv')) {
    function array_to_csv($array, $download = "")
    {
        if ($download != "") {
            // echo "\xEF\xBB\xBF";
            header('Content-Encoding: SHIFT-JIS');
            header('Content-type: text/csv; charset=SHIFT-JIS');
            header('Content-Disposition: attachement; filename="' . $download . '"');
        }

        ob_start();
        $f = fopen('php://output', 'w') or show_error("Can't open php://output");
        $n = 0;
        foreach ($array as $line) {
            $n++;
            if (!fputcsv($f, $line)) {
                show_error("Can't write line $n: $line");
            }
        }
        fclose($f) or show_error("Can't close php://output");
        $str = ob_get_contents();
        ob_end_clean();

        if ($download == "") {
            return $str;
        } else {
            $encoding = getenv('CSV_ENCODING') === false ? 'SJIS-win' : getenv('CSV_ENCODING');
            echo mb_convert_encoding($str, $encoding, array('UTF-8'));
        }
    }
}

if (!function_exists('jp_current_date')) {
    function jp_current_date()
    {
        $date = new DateTime(date('Y-m-d H:i:s'), new DateTimeZone('UTC'));
        $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
        return $date->format('Y-m-d');
    }
}

if (!function_exists('jp_current_date_from_utc')) {
    function jp_current_date_from_utc($date, $format = 'Y-m-d H:i:s')
    {
        $date = new DateTime(date('Y-m-d H:i:s', strtotime($date)), new DateTimeZone('UTC'));
        $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
        return $date->format($format);
    }
}

if (!function_exists('get_access_ip')) {
    function get_access_ip()
    {
        $ip_address = '0.0.0.0';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        }
        return $ip_address;
    }

}

/**
 * Check jinjer core connection
 * @return bool
 */
if (!function_exists('core_connected')) {
    function core_connected()
    {
        return getenv('DEV_ENV') == 'production';
    }
}


/**
 * Set ini memory_limit and max_execution_time
 * @return bool
 */
if (!function_exists('ini_set_csv')) {
    function ini_set_csv()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '512M');
    }
}

if (!function_exists('array_only')) {
    /**
     * Get only values with keys
     *
     * @param array $array
     * @param mixed $keys
     *
     * @return array
     */
    function array_only($array, $keys) : array
    {
        return array_intersect_key($array, array_flip((array) $keys));
    }
}

if (!function_exists('html_escape_custom')) {
    function html_escape_custom($var, $excepts = [])
    {
        if (empty($var)) {
            return $var;
        }

        if (is_array($var)) {
            foreach (array_keys($var) as $key) {
                if (in_array($key, $excepts, true)) {
                    continue;
                }
                if (in_array($key, FIELD_JSON_FORCE_ESCAPE_XSS, true) && !empty($var[$key]) && (is_string($var[$key]) || !is_array($var[$key]))) {
                    $decode = json_decode($var[$key], true);
                    $escape = html_escape_custom($decode, $excepts);
                    $var[$key] = json_encode($escape);
                    continue;
                }
                $var[$key] = html_escape_custom($var[$key], $excepts);
            }

            return $var;
        }

        return html_escape($var);
    }
}

if (!function_exists('html_entity_decode_custom')) {
    function html_entity_decode_custom($var, $excepts = [])
    {
        if (empty($var)) {
            return $var;
        }

        if (is_array($var)) {
            foreach (array_keys($var) as $key) {
                if (in_array($key, $excepts, true)) {
                    continue;
                }
                $var[$key] = html_entity_decode_custom($var[$key], $excepts);
            }

            return $var;
        }

        return html_entity_decode($var);
    }
}

if (!function_exists('is_format_date')) {
    function is_format_date($date_str)
    {
        $str = str_replace(array('.', '-', '\\\\'), '/', $date_str);
        $regex = '/^[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}$/';
        $date_arr  = explode('/', $str);
        if (!preg_match($regex, $str) || !checkdate($date_arr[1], $date_arr[2], $date_arr[0])) { // month, day, year
            return false;
        }
        return true;
    }
}
