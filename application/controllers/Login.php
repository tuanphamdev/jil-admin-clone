<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('clients/user_client');
    }


    protected function set_role_filter()
    {
    }

    public function index()
    {

        if ($this->session->userdata('is_logged') === true) {
            $this->callback();
        }

        $data['input']  = $this->get_flashdata('user_login') ?? [];

        if (empty($data['input'])) {
            $remember_me = json_decode(get_cookie('admin_remember_me'), true) ?? [];

            if (!empty($remember_me['email'])) {
                $data['input']['login_email'] = $remember_me['email'];
            }
            if (!empty($remember_me['password'])) {
                $data['input']['login_password'] = $remember_me['password'];
                $data['input']['remember'] = 'on';
            }
        }

        $this->load->view('login/login', $data);
    }


    public function forgot_password()
    {
        $input = $this->get_flashdata('forgot_account');
        $this->load->view('login/forgot_password', $input);
    }

    public function do_login()
    {
        $input = $this->input->post();
        $email = $input['login_email'] ?? '';
        $password = $input['login_password'] ?? '';

        $params['email'] = $email;
        $params['password'] = $password;
        $data = $this->user_client->auth($params);
        $user_name = $data['data']['name'] ?? '';

        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            $input_fill_back = $input;
            $this->set_flashdata('user_login', $input_fill_back);
            redirect('login');
        } else {
            $user_login = ['user_id' => $data['data']['id'], 'email' => $email, 'password' => $password, 'user_name' => $user_name];
            $api_token = $this->token->get_token($user_login);

            $this->session->set_userdata(['user_login' => $user_login,'api_token' => $api_token, 'is_logged' => true, 'roles' => $data['data']['roles']]);


            if (isset($input['remember']) && $input['remember'] == 'on') {
                set_cookie(
                    array(
                        'name' => 'admin_remember_me',
                        'value' => json_encode([
                            'email' => $email,
                            'password' => $password
                        ]),
                        'expire' => time() + 60 * 60 * 24 * 30
                    )
                );
            } else {
                delete_cookie('admin_remember_me');
            }
            $this->callback();
        }
    }

    public function reissue_password($token)
    {
        $data['token'] = $token;
        $data['input'] = $this->get_flashdata('reissue_password');
        $this->load->view('users/reissue_password', $data);
    }

    public function do_reissue_password($token)
    {
        $input = $this->input->post();
        $params['reset_password_token'] = $token;
        $params['password'] = $input['password'] ?? '';
        $params['password_confirm'] = $input['password_confirm'] ?? '';
        $data_token = $this->token->get_payload($token);

        $data = $this->user_client->reissue_password($params);
        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            $this->set_flashdata('reissue_password', $input);
            redirect('/reissue_password/'.$token);
        } else {
            $this->show_alert_success($this->lang->line('auth_send_forgot_success'));
            //send mail to customer
            $message = $this->load->view('template_mail/confirm_password', [], true);
            $this->send_email($data_token['email'], $this->lang->line('user_reissue_password_mail_title'), $message);
            redirect('/login');
        }
    }

    public function do_forgot_password()
    {
        $input = $this->input->post();
        $email = $input['forgot_email'] ?? '';

        $params['email'] = $email;
        $params['reset_password_token'] = $this->token->get_token(['email' => $email]);
        $data = $this->user_client->forgot_password($params);

        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            $this->set_flashdata('forgot_account', $input);
            redirect('/forgot_password');
        } else {
            $this->show_alert_success($this->lang->line('auth_send_forgot_success'));
            //send mail to customer
            $url = getenv('BASE_URL').'/reissue_password/'.$params['reset_password_token'];
            $data_send_mail = [
                'url' => $url,
            ];

            $message = $this->load->view('template_mail/forgot_password', ['data' => $data_send_mail], true);
            $this->send_email($params['email'], $this->lang->line('user_forgot_password_mail_title'), $message);

            redirect('/forgot_password');
        }
    }

    public function callback()
    {
        if ($this->session->has_userdata('url_callback') == true) {
            $url_callback = $this->session->userdata('url_callback');
            $this->session->unset_userdata('url_callback');
            redirect($url_callback);
        }
        redirect('/companies');
    }
}
