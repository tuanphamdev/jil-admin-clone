<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tokyo_Marine extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();

        parent::__construct();
        $this->load->library('clients/tokyo_marine_client');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'tokyo_marine' => READ_PERMISSION,
            ],
            'create' => [
                'tokyo_marine' => READ_PERMISSION,
            ],
            'do_create' => [
                'tokyo_marine' => FULL_PERMISSION,
            ],
            'update' => [
                'tokyo_marine' => READ_PERMISSION,
            ],
            'do_update' => [
                'tokyo_marine' => FULL_PERMISSION,
            ],
            'company_use_tokyo_marine' => [
                'tokyo_marine' => FULL_PERMISSION,
            ],
            'company_no_use_tokyo_marine' => [
                'tokyo_marine' => FULL_PERMISSION,
            ]
        );
    }

    public function index($page = 1)
    {
        $this->load->library('pagination');
        
        $params = $this->input->get();
        $query_string = null;
        if (isset($params['item_page'])) {
            $query_string['per_page'] = $params['item_page'];
        }

        $query_string['curr_page'] = $page;

        if (isset($params['code'])) {
            $query_string['code'] = $params['code'];
        }
        if (isset($params['name'])) {
            $query_string['name'] = $params['name'];
        }

        if (isset($params['type'])) {
            $query_string['type'] = $params['type'];
        }
        if (isset($params['plan'])) {
            $query_string['plan'] = $params['plan'];
        }

        if (isset($params['status'])) {
            $query_string['status'] = $params['status'];
        }
        if (isset($params['start_date_from'])) {
            $query_string['start_date_from'] = $params['start_date_from'];
        }
        if (isset($params['start_date_to'])) {
            $query_string['start_date_to'] = $params['start_date_to'];
        }
        if (isset($params['end_date_from'])) {
            $query_string['end_date_from'] = $params['end_date_from'];
        }
        if (isset($params['end_date_to'])) {
            $query_string['end_date_to'] = $params['end_date_to'];
        }
        if (isset($params['is_tokyo_marine'])) {
            $query_string['is_tokyo_marine'] = $params['is_tokyo_marine'];
        }


        $data = $this->tokyo_marine_client->list($query_string);
        if ($this->uri->segment(2) == 'search') {
            $page = 'tokyo_marines';
            if ($data['status'] === false) {
                $this->show_alert_danger($data['error']);
                redirect($page.'?'.http_build_query($query_string));
            }
        }

        $data['content_title'] = $this->lang->line('tokyo_marine_list_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('tokyo_marine_list_title'), 'link' => 'tokyo_marines']
        ];

        $data['input'] = $params;
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'tokyo_marines/index', $data);
    }

    public function create()
    {
        $data['content_title'] = $this->lang->line('tokyo_marine_create_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('tokyo_marine_list_title'), 'link' => 'tokyo_marines'],
            ['text' => $this->lang->line('tokyo_marine_create_title'), 'link' => 'tokyo_marines/create']
        ];

        $data['input']  = $this->get_flashdata('company_create') ?? [];
        $data['prefecture'] = $this->tokyo_marine_client->prefecture()['content'] ?? [];
        if (!empty($data['input']['prefecture']) && !empty($data['input']['city'])) {
            $params = ['iso_pref' => $data['input']['prefecture']];
            $data['city'] = $this->tokyo_marine_client->get_cities_by_pref_id($params)['content'] ?? [];
        }
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'tokyo_marines/create', $data);
    }

    public function do_create()
    {
        $input = $this->input->post();
        $params = $input;
        $params['tel'] = $this->get_tel_number_pattern($input['tel']);
        $params['fax'] = $this->get_tel_number_pattern($input['fax']);
        $params['zip'] = $this->get_tel_number_pattern($input['zip']);
        $params['created_by_id'] = $this->user_info->user_id;
        $params['status'] = 0;
        if ($params['start_date'] <= jp_current_date()) {
            $params['status'] = 1;
        }

        $data = $this->tokyo_marine_client->create($params);
        if ($data['status'] === false) {
            $this->set_flashdata('company_create', $input);
            $this->show_alert_danger($data['error']);
            redirect('tokyo_marines/create');
        } else {
            $this->show_alert_success($this->lang->line('tokyo_marine_create_success'));
            redirect('tokyo_marines');
        }
    }

    public function update($id)
    {

        $data['content_title'] = $this->lang->line('tokyo_marine_update_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('tokyo_marine_list_title'), 'link' => 'tokyo_marines'],
            ['text' => $this->lang->line('tokyo_marine_update_title'), 'link' => 'tokyo_marines/update/'. $id]
        ];

        $data['input']  = $this->get_flashdata('company_update') ?? [];

        if (empty($data['input'])) {
            $detail = $this->tokyo_marine_client->detail(['id' => $id]);

            if (empty($detail)) {
                $this->show_alert_danger($this->lang->line('company_no_result_found'));
                redirect('tokyo_marines');
            }

            if (isset($detail['status']) === true) {
                $data['input'] = $detail['data'];
                $data['input']['tel'] = $this->tel_number_pattern_to_array($detail['data']['tel']);
                $data['input']['fax'] = $this->tel_number_pattern_to_array($detail['data']['fax']);
                $data['input']['zip'] = $this->tel_number_pattern_to_array($detail['data']['zip']);
                $agreement = $detail['data']['agreement'];
                $agreement_id = $agreement['id'];
                unset($agreement['id']);
                $plan = $detail['data']['plan'];
                $plan_id = $plan['id'];
                unset($plan['id']);
                unset($data['input']['agreement']);
                unset($data['input']['plan']);
                $data['input']['plan_id'] = $plan_id;
                $data['input']['agreement_id'] = $agreement_id;
                $data['input'] = array_merge($data['input'], $agreement, $plan);
            }
        }
        $data['prefecture'] = $this->tokyo_marine_client->prefecture()['content'] ?? [];
        if (!empty($data['input']['city']) && !empty($data['input']['prefecture'])) {
            $params = ['iso_pref' => $data['input']['prefecture']];
            $data['city'] = $this->tokyo_marine_client->get_cities_by_pref_id($params)['content'] ?? [];
        }
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'tokyo_marines/update', $data);
    }

    public function do_update($id)
    {
        $input = $this->input->post();
        $params = $input;
        $params['tel'] = $this->get_tel_number_pattern($input['tel']);
        $params['fax'] = $this->get_tel_number_pattern($input['fax']);
        $params['zip'] = $this->get_tel_number_pattern($input['zip']);
        $params['updated_by_id'] = $this->user_info->user_id;
        $params['status'] = 0;
        if ($params['start_date'] <= jp_current_date() && jp_current_date() <= $params['end_date']) {
            $params['status'] = 1;
            $params['stop_date'] = null;
        }
        
        $data = $this->tokyo_marine_client->update($params);
        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            $this->set_flashdata('company_update', $input);
            redirect('tokyo_marines/update/'.$id);
        } else {
            $this->show_alert_success($this->lang->line('tokyo_marine_update_success'));
            redirect('tokyo_marines');
        }
    }

    public function company_use_tokyo_marine()
    {
        $input = $this->input->post();
        $params = $input;
        $data = $this->tokyo_marine_client->use_tokyo_marine($params);

        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            redirect('tokyo_marines');
        } else {
            $message = sprintf($this->lang->line('tokyo_marine_use_success'), $input['company_name'] ?? 'N/A');
            $this->show_alert_success($message);
            redirect('tokyo_marines');
        }
    }
    
    public function company_no_use_tokyo_marine()
    {
        $input = $this->input->post();
        $params = $input;
        $data = $this->tokyo_marine_client->no_use_tokyo_marine($params);

        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            redirect('tokyo_marines');
        } else {
            $message = sprintf($this->lang->line('tokyo_marine_no_use_success'), $input['company_name'] ?? 'N/A');
            $this->show_alert_success($message);
            redirect('tokyo_marines');
        }
    }
}
