<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TSPasswordIp extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();
        parent::__construct();
        $this->load->library('clients/ts_password_ip_client');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'ts_password_ips' => READ_PERMISSION,
            ],
            'create' => [
                'ts_password_ips' => READ_PERMISSION,
            ],
            'do_create' => [
                'ts_password_ips' => FULL_PERMISSION,
            ],
            'update' => [
                'ts_password_ips' => READ_PERMISSION,
            ],
            'do_update' => [
                'ts_password_ips' => FULL_PERMISSION,
            ],
        );
    }

    public function index()
    {
        $params = $this->input->get();
        $query_string = null;
        $params['item_page'] = 20;
        if (isset($params['item_page'])) {
            $query_string['per_page'] = $params['item_page'];
        }

        if (isset($params['curr_page'])) {
            $query_string['curr_page'] = $params['curr_page'];
        }

        $data = $this->ts_password_ip_client->list($query_string);
        $data['content_title'] = $this->lang->line('ts_password_ip_list_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('ts_password_ip_list_title'), 'link' => '/ts_password_ips']
        ];
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'ts_password_ips/index', $data);
    }

    public function create()
    {
        $data['content_title'] = $this->lang->line('ts_password_ip_create_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('ts_password_ip_list_title'), 'link' => '/ts_password_ips'],
            ['text' => $this->lang->line('ts_password_ip_create_title'), 'link' => '/ts_password_ips/create']
        ];
        $data['input']  = $this->get_flashdata('ip_create') ?? [];
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'ts_password_ips/create', $data);
    }

    public function do_create()
    {
        $input = $this->input->post();
        $params['ip_address'] = $this->get_ip_number_pattern($input['ip_address']);
        $params['name'] = $input['name']??'';
        $data = $this->ts_password_ip_client->create($params);
        if ($data['status'] === false) {
            $input_fill_back = $input;
            $this->set_flashdata('ip_create', $input_fill_back);
            $this->show_alert_danger($data['error']);
            redirect('ts_password_ips/create');
        } else {
            $message = $this->lang->line('ts_password_ip_create_success');
            $this->show_alert_success($message);
            redirect('ts_password_ips');
        }
    }

    public function update($ip_id)
    {
        $data['content_title'] = $this->lang->line('ts_password_ip_update_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('ts_password_ip_list_title'), 'link' => '/ts_password_ips'],
            ['text' => $this->lang->line('ts_password_ip_update_title'), 'link' => '/ts_password_ips/update/'.$ip_id]
        ];

        $data['input']  = $this->get_flashdata('ip_update') ?? [];
        if (empty($data['input'])) {
            $ip = $this->ts_password_ip_client->detail(['id' => $ip_id]);
            if (empty($ip)) {
                $message = $this->lang->line('ts_password_ip_no_result_found');
                $this->show_alert_danger($message);
                redirect('ts_password_ips');
            }
            if ($ip['status'] === false) {
                $this->show_alert_danger($ip['error']);
                redirect('ts_password_ips');
            }
            $data['input'] = $ip['data'];
            $data['input']['ip_address'] = $this->ip_number_pattern_to_array($ip['data']['ip_address']);
        }

        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'ts_password_ips/update', $data);
    }

    public function do_update()
    {
        $input = $this->input->post();
        $params = $input;
        $params['ip_address'] = $this->get_ip_number_pattern($input['ip_address']);
        $data = $this->ts_password_ip_client->update($params);
        if ($data['status'] === false) {
            $input_fill_back = $input;
            $this->set_flashdata('ip_update', $input_fill_back);
            $this->show_alert_danger($data['error']);
            redirect('ts_password_ips/update/'.$input['id']);
        } else {
            $message = $this->lang->line('ts_password_ip_update_success');
            $this->show_alert_success($message);
            redirect('ts_password_ips');
        }
    }
    
    public function delete()
    {
        $input = $this->input->post();
        $params['id'] = $input['ip_id'] ?? '';
        $data = $this->ts_password_ip_client->delete($params);

        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            redirect('ts_password_ips');
        } else {
            $message = $this->lang->line('ts_password_ip_delete_success');
            $this->show_alert_success($message);
            redirect('ts_password_ips');
        }
    }
}
