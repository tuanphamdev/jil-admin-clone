<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Employee extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();

        parent::__construct();
        $this->load->library('clients/employee_client');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'employee' => READ_PERMISSION,
            ],
            'enable_admin' => [
                'employee' => FULL_PERMISSION,
            ]
        );
    }

    public function index($page = 1)
    {
        $this->load->library('pagination');

        $params = $this->input->get();
        $query_string = null;
        if (isset($params['item_page'])) {
            $query_string['per_page'] = $params['item_page'];
        }

        $query_string['curr_page'] = $page;

        if (isset($params['company_code'])) {
            $query_string['company_code'] = trim($params['company_code']);
        }

        if (isset($params['employee_code'])) {
            $query_string['employee_code'] = trim($params['employee_code']);
        }

        if (isset($params['employee_name'])) {
            $query_string['employee_name'] = trim($params['employee_name']);
        }


        if (!empty($params)) {
            $data = $this->employee_client->list($query_string);
            if ($this->uri->segment(2) == 'search') {
                $page = 'admin_employees';
                if ($data['status'] === false) {
                    $this->show_alert_danger($data['error']);
                    redirect($page.'?'.http_build_query($query_string));
                }
            }
        }
        $data['content_title'] = $this->lang->line('employee_admin_list_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('employee_admin_list_title'), 'link' => 'admin_employees']
        ];

        $data['input'] = $params;
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'employees/index', $data);
    }


    public function enable_admin()
    {
        $input = $this->input->post();
        $params = $input;
        $data = $this->employee_client->enable_admin($params);

        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            redirect('admin_employees');
        } else {
            $message = sprintf($this->lang->line('employee_change_role_success'), $input['employee_code'] ?? 'N/A');
            $this->show_alert_success($message);
            $page = 'admin_employees/search';
            $query['company_code'] = $params['core_company_code'] ?? '';
            $query['employee_code'] = $params['admin_code'] ?? '';
            redirect($page.'?'.http_build_query($query));
        }
    }

    /**
     * Get avatar
     * @param integer $face_photo_uploads_id
     * @param integer $core_company_id
     */
    public function get_avatar($face_photo_uploads_id = null, $core_company_id = null)
    {
        $src = '';
        if (!empty($face_photo_uploads_id) && !empty($core_company_id)) {
            $response = $this->employee_client->get_file($face_photo_uploads_id, $core_company_id);
            if (!empty($response['content']['base64_file'])) {
                $src = "data:image/png;base64, " . $response['content']['base64_file'];
            }
        }
        die($src);
    }
}
