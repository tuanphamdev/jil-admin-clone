<?php


class BankMaster extends MY_Controller
{
    public function __construct()
    {
        $this->set_role_filter();
        parent::__construct();
        $this->load->library('clients/bank_master_client');
        $this->load->library('clients/import_csv_client');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'create' => [
                'import_bank' => READ_PERMISSION,
            ],
            'edit' => [
                'import_bank' => FULL_PERMISSION,
            ],
        );
    }

    public function create()
    {
        $data['content_title'] = $this->lang->line('bank_create_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('import_csv_list_title'), 'link' => 'import_banks'],
            ['text' => $this->lang->line('bank_create_title'), 'link' => 'import_banks/create']
        ];
        $data['input']  = $this->get_flashdata('bank_create') ?? [];

        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'banks/create', html_escape_custom($data));
    }

    public function do_create()
    {
        $input = $bank_create =  $this->input->post();
        $input['branch_post_code'] = $this->get_tel_number_pattern($input['branch_post_code']);
        $input['branch_phone_number'] = $this->get_tel_number_pattern($input['branch_phone_number']);

        $data = $this->bank_master_client->create_bank_master($input);
        if ($data['status'] === false) {
            $this->set_flashdata('bank_create', $bank_create);
            $this->show_alert_danger($data['error']);
            redirect('import_banks/create');
        } else {
            $this->show_alert_success($this->lang->line('bank_store_success'));
            redirect('import_banks');
        }
    }

    public function edit($id)
    {
        $data['content_title'] = $this->lang->line('company_list_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('import_csv_list_title'), 'link' => 'import_banks'],
            ['text' => $this->lang->line('bank_edit_title'), 'link' => 'import_banks/edit']
        ];

        $detail = $this->import_csv_client->detail($id);

        if (!$detail['status'] || empty($detail['data'])) {
            $this->show_alert_danger($this->lang->line('bank_master_no_result_found'));
            redirect('import_banks');
        }
        if ($detail['status'] === true) {
            $detail['data']['branch_post_code'] = $this->tel_number_pattern_to_array($detail['data']['branch_post_code']);
            $detail['data']['branch_phone_number'] = $this->tel_number_pattern_to_array($detail['data']['branch_phone_number']);
        }

        $data['input']  = $this->get_flashdata('bank_update') ?? $detail['data'];
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'banks/edit', html_escape_custom($data));
    }

    public function do_update($id)
    {
        $input = $bank_update = $this->input->post();

        $input['branch_post_code'] = $this->get_tel_number_pattern($input['branch_post_code']);
        $input['branch_phone_number'] = $this->get_tel_number_pattern($input['branch_phone_number']);
        $data = $this->bank_master_client->update_bank_master($id, $input);
        if ($data['status'] === false) {
            $bank_update['id'] = $id;
            $this->set_flashdata('bank_update', $bank_update);
            $this->show_alert_danger($data['error']);
            redirect('import_banks/edit/' . $id);
        } else {
            $this->show_alert_success($this->lang->line('bank_store_success'));
            redirect('import_banks');
        }
    }
}
