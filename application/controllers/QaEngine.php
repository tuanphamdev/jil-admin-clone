<?php
defined('BASEPATH') or exit('No direct script access allowed');

class QaEngine extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();
        parent::__construct();
        $this->load->library('clients/qa_engine_client');
        $this->load->driver('cache');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'qa_engine' => READ_PERMISSION,
            ],
            'create' => [
                'qa_engine' => READ_PERMISSION,
            ]
        );
    }

    public function index()
    {
        $data = $this->qa_engine_client->list();
        $data['content_title'] = $this->lang->line('menu_qa_engine');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('menu_qa_engine'), 'link' => '/qa_engine']
        ];
        if (!empty($this->get_flashdata('qa_engine_create'))) {
            $data['data'] = $this->get_flashdata('qa_engine_create');
        }
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'qa_engine/index', $data);
    }

    public function do_create()
    {
        $input = $this->input->post();
        $input['status'] = isset($input['status']) && $input['status'] == 'on' ? 1 : 0;
        $data = $this->qa_engine_client->create($input);
        if ($data['status'] === false) {
            $this->set_flashdata('qa_engine_create', $input);
            $this->show_alert_danger($data['error']);
            redirect('qa_engine');
        } else {
            if ($this->cache->redis->get(CACHE_ADMIN_QA_ENGINE)) {
                $this->cache->redis->delete(CACHE_ADMIN_QA_ENGINE);
            }
            $this->cache->redis->save(CACHE_ADMIN_QA_ENGINE, $input, CACHE_TIME_TO_LIVE);
            $message = $this->lang->line('qa_engine_create_success');
            $this->show_alert_success($message);
            redirect('qa_engine');
        }
    }
}
