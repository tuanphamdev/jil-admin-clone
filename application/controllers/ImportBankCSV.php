<?php
defined('BASEPATH') or exit('No direct script access allowed');
use League\Csv\Reader;

class ImportBankCSV extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();
        parent::__construct();
        $this->load->library('clients/import_csv_client');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'import_bank' => READ_PERMISSION,
            ],
            'import_banks' => [
                'import_bank' => FULL_PERMISSION,
            ],
        );
    }

    public function index($page = 1)
    {
        $this->load->library('pagination');
        
        $params = $this->input->get();
        $query_string = null;
        if (isset($params['item_page'])) {
            $query_string['per_page'] = $params['item_page'];
        }

        $query_string['curr_page'] = $page;
        
        if (isset($params['bank_code'])) {
            $query_string['bank_code'] = $params['bank_code'];
        }

        if (isset($params['branch_code'])) {
            $query_string['branch_code'] = $params['branch_code'];
        }
        if (isset($params['bank_name'])) {
            $query_string['bank_name'] = $params['bank_name'];
        }

        if (isset($params['branch_name'])) {
            $query_string['branch_name'] = $params['branch_name'];
        }

//        if (isset($params['status_filter']) && $params['status_filter'] == 0) {
//            $query_string['status'] = BANK_STATUS_INVALID;
//        } else {
//            $query_string['status'] = BANK_STATUS_VALID;
//        }

        $data = $this->import_csv_client->list($query_string);

        $data['content_title'] = $this->lang->line('import_csv_list_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('import_csv_list_title'), 'link' => 'import_csv']
        ];
        $data['input'] = $params;
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'import_bank_csv/index', $data);
    }


    /**
     * Import banks
     *
     */
    public function import_banks()
    {
        ini_set_csv();

        // upload file config
        $config['upload_path']          = '/tmp/';
        $config['allowed_types']        = 'csv';
        $config['max_size']             = 5*1024;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('bank_info')) {
            $this->show_alert_danger($this->upload->display_errors('', ''));
            redirect('import_banks');
        }
        $upload = $this->upload->data();
        $content = mb_convert_encoding(file_get_contents($upload['full_path']), 'UTF-8', 'SJIS-win');
        if (! write_file($upload['full_path'], $content)) {
            $this->show_alert_danger($this->lang->line('import_bank_failed'));
            redirect('import_banks');
        }

        // load model and create reader
        $this->load->model('bank_master_model');
        // init reader
        $reader = Reader::createFromPath($upload['full_path'], 'r');
        $reader->setHeaderOffset(0);

        if ($this->bank_master_model->valid_headers($reader->getHeader()) === false) {
            $this->show_alert_danger($this->lang->line('import_bank_failed'));
            redirect('import_banks');
        }

        $records = $reader->getRecords();
        $csv_data = $this->bank_master_model->process_import_csv_bank_data($records);

        if ($csv_data === false) {
            $this->show_alert_danger($this->validation_errors());
            redirect('import_banks');
        }

        if ($this->bank_master_model->save_banks($csv_data) === false) {
            $this->show_alert_danger($this->lang->line('import_bank_failed'));
            redirect('import_banks');
        }

        $this->show_alert_success($this->lang->line('import_bank_success'));
        redirect('import_banks');
    }
}
