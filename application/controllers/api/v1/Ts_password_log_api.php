<?php
defined('BASEPATH') or exit('No direct script access allowed');
//
class Ts_password_log_api extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model(['ts_password_log_model']);
    }


    /**
     * List ts password logs
     *
     */
    public function list_get()
    {
        $paginate = $this->paginate();
        $per_page = $paginate['per_page'];
        $curr_page = $paginate['curr_page'];
        $query_search = $this->get();
        $data['total'] = $this->ts_password_log_model->list($query_search, $per_page, (($curr_page-1)*$per_page), true);
        $data['ts_password_log'] = $this->ts_password_log_model->list($query_search, $per_page, (($curr_page-1)*$per_page));
        $data['per_page'] = $per_page;
        if (empty($data['ts_password_log'])) {
            $this->response_failure([$this->lang->line('ts_password_log_no_result_found')], self::HTTP_NOT_FOUND);
        }
        $this->response_success($data, self::HTTP_OK);
    }
}
