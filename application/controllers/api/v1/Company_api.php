<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Jinjer\services\CompanyService;
use Jinjer\services\AgreementService;

class Company_api extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model(['company_model']);
        $this->load->model(['agreement_model']);
        $this->load->model(['plan_model', 'plan_signing_model']);
    }


    /**
     * List Companies
     *
     */
    public function list_get()
    {
        $this->agreement_model->from_form($this->agreement_model->rules['search_date'], null, null, $this->input->get());
        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_BAD_REQUEST);
        }

        $company_fields = array (
            'id',
            'code',
            'name',
            'application_name',
            'application_surname',
            'application_furigana',
            'application_furigana_surname',
            'status',
            'agreements.type',
            'agreements.num_account',
            'agreements.start_date',
            'agreements.end_date',
            'plans.jinjer_db',
            'plans.kintai',
            'plans.roumu',
            'plans.jinji',
            'plans.keihi',
            'plans.my_number',
            'plans.work_vital',
            'plans.salary',
            'plans.workflow',
            'plans.employee_contract',
            'plans.year_end_tax_adjustment',
            'plans.signing',
            'is_tokyo_marine',
        );
        $query_company_field = implode(',', $company_fields);

        $paginate = $this->paginate();
        $per_page = $paginate['per_page'];
        $curr_page = $paginate['curr_page'];

        $query_search = $this->query_search(self::COMPANY_SEARCH);

        $data['total'] = $this->company_model->as_array()
            ->fields($query_company_field)
            ->where($query_search)
            ->join_agreement()
            ->join_plan()
            ->count_rows();
        $data['company'] = $this->company_model->as_array()
            ->fields($query_company_field)->where($query_search)
            ->join_agreement()
            ->join_plan()->order_by('companies.created_at', 'DESC')
            ->paginate($per_page, $data['total'], $curr_page);
        $data['per_page'] = $per_page;

        if (empty($data['company'])) {
            $this->response_failure([$this->lang->line('company_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }


    public function create_post()
    {
        $input = $this->post();
        //Validate post data
        $this->session->set_flashdata('data_validate', $input);
        $company = $this->company_model->from_form();
        $agreement = $this->agreement_model->from_form();
        $plan = $this->plan_model->from_form(null, $input);
        $plan_signing = $this->plan_signing_model->from_form(null, $input);

        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_OK);
            return;
        }
        $input = $this->post();
        $jinjer_db = CompanyService::getValueJinjerDB($input);
        if ($jinjer_db === false && (!isset($input['jinjer_db']) || $input['jinjer_db'] == 0)) {
            $this->response_failure([$this->lang->line('company_stop_all_service')], self::HTTP_OK);
            return;
        }

        //Create company, agreement, plan
        $this->db->trans_begin();
        $company_id = $company->insert();
        $agreement_id = $agreement->insert(null, ['company_id' => $company_id]);
        $plan_id = $plan->insert(null, ['company_id' => $company_id, 'agreement_id' => $agreement_id]);
        $plan_signing->insert(null, ['company_id' => $company_id, 'plan_id' => $plan_id]);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('company_create_failed')], self::HTTP_OK);
            return;
        } else {
            //connect to Core
            if (core_connected()) {
                $this->load->library('clients/company_client');
                $contract_info = [
                    'jinjer_db' =>  [
                        'type' => $input['jinjer_db'] ?? $jinjer_db,
                    ],
                    'kintai' =>  [
                        'type' => $input['kintai'] ?? 0,
                    ],
                    'roumu' =>  [
                        'type' => $input['roumu'] ?? 0,
                    ],
                    'jinji' =>  [
                        'type' => $input['jinji'] ?? 0,
                    ],
                    'keihi' =>  [
                        'type' => $input['keihi'] ?? 0,
                    ],
                    'my_number' =>  [
                        'type' => $input['my_number'] ?? 0,
                    ],
                    'work_vital' =>  [
                        'type' => $input['work_vital'] ?? 0,
                    ],
                    'salary' =>  [
                        'type' => $input['salary'] ?? 0,
                    ],
                    'workflow' =>  [
                        'type' => $input['workflow'] ?? 0,
                    ],
                    'employee_contract' =>  [
                        'type' => $input['employee_contract'] ?? 0,
                    ],
                    'year_end_tax_adjustment' =>  [
                        'type' => $input['year_end_tax_adjustment'] ?? 0,
                    ],
                    'signing' => [
                        'type' => $input['signing'] ?? 0,
                    ],
                ];
                if (!empty($input['kintai'])) {
                    $is_paid_management = $input['is_paid_management'] ?? 0;
                    $is_shift_management = $input['is_shift_management'] ?? 0;
                    $is_overtime_management = $input['is_overtime_management'] ?? 0;
                }

                if (!empty($input['keihi'])) {
                    $is_using_ai_ocr = $input['is_using_ai_ocr']??0;
                    $is_using_timestamp = $input['timestamp']??0;
                    $is_ebook_store = $input['is_ebook_store'] ?? 0;
                    $is_credit_card = $input['is_credit_card'] ?? 0;
                }
                if (!empty($input['salary'])) {
                    $is_payslip = $input['is_payslip'] ?? 0;
                }
                $contract_info = json_encode($contract_info);
                $core_data = [
                    'admin_company_id' => $company_id,
                    'code' => $input['code'],
                    'name' => $input['name'],
                    'phone_number' => $input['tel'],
                    'postcode' => $input['zip'],
                    'prefecture_id' => $input['prefecture'],
                    'city_id' => $input['city'],
                    'street' => $input['address'],
                    'representative_name' => $input['representative_name'],
                    'email' => $input['application_email'],
                    'last_name' => $input['application_surname'],
                    'first_name' => $input['application_name'],
                    'last_name_kana' => $input['application_furigana_surname'],
                    'first_name_kana' => $input['application_furigana'],
                    'status' => $input['status'],
                    'start_date' => $input['start_date'],
                    'end_date' => $input['end_date'],
                    'contract_type' => $input['type'],
                    'contract_info' => $contract_info,
                    'capacity' => $input['num_account'],
                    'is_admin_connect' => getenv('API_KEY'),
                    'name_phonetic' => $input['name_furigana'],
                    'representative_name_kana' => $input['representative_name_furigana'],
                    'representative_title' => $input['representative_title'],
                    'fax' => $input['fax'],
                    'address_kana' => $input['address_furigana'],
                    'enigma_code' => $input['enigma_code'],
                    'enigma_version' => $input['enigma_version'],
                    'is_using_ai' => $input['is_using_ai'],
                    'is_using_ultra' => $input['is_using_ultra'],
                    'is_docomo' => $input['is_docomo'],
                    'd_account_id' => $input['d_account_id'] ?? '',
                    'is_using_ai_ocr' => $is_using_ai_ocr ?? 0,
                    'is_ebook_store' => $is_ebook_store ?? 0,
                    'is_credit_card' => $is_credit_card ?? 0,
                    'is_using_timestamp' => $is_using_timestamp??0,
                    'maximum_signing' => $input['maximum_signing'],
                    'signing_folder' => $input['signing_folder'],
                    'signing_folder_range' => $input['signing_folder_range'],
                    'signing_internal_workflow' => $input['signing_internal_workflow'],
                    'signing_batch_import' => $input['signing_batch_import'],
                    'import_signed_file' => $input['import_signed_file'],
                    'maximum_import_signed_file' => $input['maximum_import_signed_file'],
                    'import_signed_file_timestamp' => $input['import_signed_file_timestamp'],
                    'ip_limit' => $input['ip_limit'],
                    'send_sms' => $input['send_sms'],
                    'electronic_signature_long_term' => $input['electronic_signature_long_term'],
                    'electronic_signature_timestamp' => $input['electronic_signature_timestamp'],
                    'electronic_signature_authentication' => $input['electronic_signature_authentication'],
                    'send_function_digital_signature' => $input['send_function_digital_signature'],
                    'imprint_registration' => $input['imprint_registration'],
                    'attach_file_receiver' => $input['attach_file_receiver'],
                    'attach_file_sender' => $input['attach_file_sender'],
                    'alert_function' => $input['alert_function'],
                    'language_support' => $input['language_support'],

                    'is_paid_management' => $is_paid_management ?? 0,
                    'is_shift_management' => $is_shift_management ?? 0,
                    'is_overtime_management' => $is_overtime_management ?? 0,
                    'is_payslip' => $is_payslip ?? 0,
                    'kintai_sense_link' => $input['kintai_sense_link'] ?? 0,
                    'kintai_alligate' => $input['kintai_alligate'] ?? 0,
                ];

                $data = $this->company_client->core_create_company($core_data);
                if ($data['status'] === false) {
                    $this->response_failure($data['error'], $data['code']);
                    return;
                }
            }
            $this->db->trans_commit();
        }
        $this->response_success($company_id, self::HTTP_CREATED);
    }
    public function detail_get()
    {
        $id = $this->get('id');
        $data = $this->company_model
            ->as_array()
            ->fields('*')
            ->with_agreement('agreements:*')
            ->with_plan('plans:*')
            ->with_plan_signing('plans_signing:*')
            ->where('id', $id)
            ->get();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('company_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function update_post()
    {
        $input = $this->post();
        $company_id = $this->post('id');
        $agreement_id = $this->post('agreement_id');
        $plan_id = $this->post('plan_id');
        $plan_signing_id = $this->post('plan_signing_id');
        $this->session->set_flashdata('data_validate', $input);

        //Validate post data
        $company = $this->company_model->from_form(null, null, array('id' => $company_id));
        $agreement = $this->agreement_model->from_form(null, null, array('id' => $agreement_id));
        $plan = $this->plan_model->from_form(null, $input, array('id' => $plan_id));
        $plan_signing = $this->plan_signing_model->from_form(null, $input, array('id' => $plan_signing_id));

        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_OK);
            return;
        }

        //Validate company data
        $data  = $this->company_model->fields('id')
            ->where(array('companies.id' => $company_id, 'agreements.id' => $agreement_id,  'plans.id' => $plan_id, 'plans_signing.id' => $plan_signing_id))
            ->join_agreement()
            ->join_plan()
            ->join_plan_signing()
            ->get();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('company_no_result_found')], self::HTTP_OK);
            return;
        }
        $jinjer_db = CompanyService::getValueJinjerDB($input);
        if ($jinjer_db === false && (!isset($input['jinjer_db']) || $input['jinjer_db'] == 0)) {
            $this->response_failure([$this->lang->line('company_stop_all_service')], self::HTTP_OK);
            return;
        }

        //Update company, agreement, plan
        $this->db->trans_begin();
        $company->where('id', $company_id)->update();
        $agreement->where('id', $agreement_id)->update(null, ['company_id' => $company_id]);
        $plan->where('id', $plan_id)->update(null, ['company_id' => $company_id, 'agreement_id' => $agreement_id]);
        $plan_signing->where('id', $plan_signing_id)->update(null, ['company_id' => $company_id, 'plan_id' => $plan_id]);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('company_update_failed')], self::HTTP_OK);
            return;
        } else {
            //connect to Core
            if (core_connected()) {
                $this->load->library('clients/company_client');
                $input = $this->post();
                $contract_info = [
                    'jinjer_db' =>  [
                        'type' => $input['jinjer_db'] ?? $jinjer_db,
                    ],
                    'kintai' =>  [
                        'type' => $input['kintai'] ?? 0,
                    ],
                    'roumu' =>  [
                        'type' => $input['roumu'] ?? 0,
                    ],
                    'jinji' =>  [
                        'type' => $input['jinji'] ?? 0,
                    ],
                    'keihi' =>  [
                        'type' => $input['keihi'] ?? 0,
                    ],
                    'my_number' =>  [
                        'type' => $input['my_number'] ?? 0,
                    ],
                    'work_vital' =>  [
                        'type' => $input['work_vital'] ?? 0,
                    ],
                    'salary' =>  [
                        'type' => $input['salary'] ?? 0,
                    ],
                    'workflow' =>  [
                        'type' => $input['workflow'] ?? 0,
                    ],
                    'employee_contract' =>  [
                        'type' => $input['employee_contract'] ?? 0,
                    ],
                    'year_end_tax_adjustment' =>  [
                        'type' => $input['year_end_tax_adjustment'] ?? 0,
                    ],
                    'signing' =>  [
                        'type' => $input['signing'] ?? 0,
                    ],
                ];
                if (!empty($input['kintai'])) {
                    $is_paid_management = $input['is_paid_management'] ?? 0;
                    $is_shift_management = $input['is_shift_management'] ?? 0;
                    $is_overtime_management = $input['is_overtime_management'] ?? 0;
                }

                if (!empty($input['keihi'])) {
                    $is_using_ai_ocr = $input['is_using_ai_ocr']??0;
                    $is_using_timestamp = $input['timestamp']??0;
                    $is_ebook_store = $input['is_ebook_store'] ?? 0;
                    $is_credit_card = $input['is_credit_card'] ?? 0;
                }
                if (!empty($input['salary'])) {
                    $is_payslip = $input['is_payslip'] ?? 0;
                }
                $contract_info = json_encode($contract_info);
                $core_data = [
                    'code' => $input['code'],
                    'start_date' => $input['start_date'],
                    'end_date' => $input['end_date'],
                    'contract_type' => $input['type'],
                    'contract_info' => $contract_info,
                    'capacity' => $input['num_account'],
                    'company_id' => $company_id,
                    'status' => $input['status'],
                    'email' => $input['application_email'],
                    'enigma_code' => $input['enigma_code'],
                    'enigma_version' => $input['enigma_version'],
                    'is_using_ai' => $input['is_using_ai'],
                    'is_using_ultra' => $input['is_using_ultra'],
                    'is_docomo' => $input['is_docomo'],
                    'd_account_id' => $input['d_account_id'] ?? '',
                    'is_using_ai_ocr' => $is_using_ai_ocr ?? 0,
                    'is_ebook_store' => $is_ebook_store ?? 0,
                    'is_credit_card' => $is_credit_card ?? 0,
                    'is_using_timestamp' => $is_using_timestamp ?? 0,
                    'maximum_signing' => $input['maximum_signing'],

                    'signing_folder' => $input['signing_folder'],
                    'signing_folder_range' => $input['signing_folder_range'],
                    'signing_internal_workflow' => $input['signing_internal_workflow'],
                    'signing_batch_import' => $input['signing_batch_import'],
                    'import_signed_file' => $input['import_signed_file'],
                    'maximum_import_signed_file' => $input['maximum_import_signed_file'],
                    'import_signed_file_timestamp' => $input['import_signed_file_timestamp'],
                    'ip_limit' => $input['ip_limit'],
                    'send_sms' => $input['send_sms'],
                    'electronic_signature_long_term' => $input['electronic_signature_long_term'],
                    'electronic_signature_timestamp' => $input['electronic_signature_timestamp'],
                    'electronic_signature_authentication' => $input['electronic_signature_authentication'],
                    'send_function_digital_signature' => $input['send_function_digital_signature'],
                    'imprint_registration' => $input['imprint_registration'],
                    'attach_file_receiver' => $input['attach_file_receiver'],
                    'attach_file_sender' => $input['attach_file_sender'],
                    'alert_function' => $input['alert_function'],
                    'language_support' => $input['language_support'],

                    'is_paid_management' => $is_paid_management ?? 0,
                    'is_shift_management' => $is_shift_management ?? 0,
                    'is_overtime_management' => $is_overtime_management ?? 0,
                    'is_payslip' => $is_payslip ?? 0,
                    'kintai_sense_link' => $input['kintai_sense_link'] ?? 0,
                    'kintai_alligate' => $input['kintai_alligate'] ?? 0,
                ];

                $data = $this->company_client->core_update_company($core_data);

                if ($data['status'] === false) {
                    $this->response_failure($data['error'], $data['code']);
                    return;
                }
            }

            $this->db->trans_commit();
        }
        $this->response_success($company_id, self::HTTP_OK);
    }

    public function stop_service_post()
    {
        $company_id = $this->post('company_id');

        //Validate post data
        $rule = array(
            'company_id' => [
            'field'=>'company_id',
            'label'=>'company_id',
            'rules'=>'trim|required|is_natural_greater_than_zero'
            ]);
        $company = $this->company_model->from_form($rule, array('status' =>  0), array('id' => $company_id));

        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_BAD_REQUEST);
            return;
        }

        //Validate company data
        $data  = $this->company_model->fields('id, code, name, status')
            ->where(array('companies.id' => $company_id))
            ->get();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('company_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }

        //connect to Core
        if (core_connected()) {
            $this->load->library('clients/company_client');
            $data = $this->company_client->core_update_company(['status' => 0, 'company_id' => $company_id]);
            if ($data['status'] === false) {
                $this->response_failure($data['error'], $data['code']);
                return;
            }
        }
        $this->db->trans_begin();
        $affected = $company->where('id', $company_id)->update();
        $this->agreement_model->where('company_id', $company_id)->update(['stop_date' => date('Y-m-d H:i:s')]);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('company_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        } else {
            $this->db->trans_commit();
        }
        $this->response_success($affected, self::HTTP_OK);
    }

    public function start_service_post()
    {
        $company_id = $this->post('company_id');

        //Validate post data
        $rule = array(
            'company_id' => [
                'field'=>'company_id',
                'label'=>'company_id',
                'rules'=>'trim|required|is_natural_greater_than_zero'
            ],
        );

        $company = $this->company_model->from_form($rule, array('status' =>  1), array('id' => $company_id));
        $this->agreement_model->from_form($this->agreement_model->rules['start_service'], null, array('company_id' => $company_id));

        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_BAD_REQUEST);
            return;
        }

        //Validate company data
        $data  = $this->company_model->fields('id, code, name, status')
            ->where(array('companies.id' => $company_id))
            ->get();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('company_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }

        //connect to Core
        if (core_connected()) {
            $this->load->library('clients/company_client');
            $data = $this->company_client->core_update_company(['status' => 1, 'company_id' => $company_id]);
            if ($data['status'] === false) {
                $this->response_failure($data['error'], $data['code']);
                return;
            }
        }
        $start_date = $this->post('start_date');
        $end_date = $this->post('end_date');
        $this->db->trans_begin();
        $affected = $company->where('id', $company_id)->update();
        $this->agreement_model->where('company_id', $company_id)
            ->update(['stop_date' => null, 'start_date' => $start_date, 'end_date' => $end_date]);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('company_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        } else {
            $this->db->trans_commit();
        }
        $this->response_success($affected, self::HTTP_OK);
    }

    public function use_tokyo_marine_post()
    {
        $company_id = $this->post('company_id');

        //Validate post data
        $rule = array(
            'company_id' => [
                'field'=>'company_id',
                'label'=>'company_id',
                'rules'=>'trim|required|is_natural_greater_than_zero'
            ],
        );
        $company = $this->company_model->from_form($rule, array('is_tokyo_marine' =>  1), array('id' => $company_id));
        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_BAD_REQUEST);
            return;
        }

        //Validate company data
        $data  = $this->company_model->fields('id, code, name, is_tokyo_marine')
            ->where(array('companies.id' => $company_id))
            ->get();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('tokyo_marine_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }

        $this->db->trans_begin();
        $affected = $company->where('id', $company_id)->update();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('tokyo_marine_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        } else {
            $this->db->trans_commit();
        }
        $this->response_success($affected, self::HTTP_OK);
    }

    public function no_use_tokyo_marine_post()
    {
        $company_id = $this->post('company_id');

        //Validate post data
        $rule = array(
            'company_id' => [
                'field'=>'company_id',
                'label'=>'company_id',
                'rules'=>'trim|required|is_natural_greater_than_zero'
            ],
        );
        $company = $this->company_model->from_form($rule, array('is_tokyo_marine' =>  0), array('id' => $company_id));
        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_BAD_REQUEST);
            return;
        }

        //Validate company data
        $data  = $this->company_model->fields('id, code, name, is_tokyo_marine')
            ->where(array('companies.id' => $company_id))
            ->get();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('tokyo_marine_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }

        $this->db->trans_begin();
        $affected = $company->where('id', $company_id)->update();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('tokyo_marine_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        } else {
            $this->db->trans_commit();
        }
        $this->response_success($affected, self::HTTP_OK);
    }

    public function request_resend_account_info_post()
    {
        $input = $this->post();
        $company = $this->company_model->as_array()
            ->fields('id, application_email, application_surname, application_name')
            ->where('id', $input['admin_company_id'])
            ->where('deleted_at', null)
            ->get();

        if (!empty($company)) {
            $params = [
                'admin_company_id' => $input['admin_company_id'],
                'email' => $company['application_email'],
                'last_name' => $company['application_surname'],
                'first_name' => $company['application_name'],
            ];
            $this->load->library('clients/company_client');
            $data = $this->company_client->core_resend_account_info($params);
        } else {
            $data['status'] = false;
            $data['error'] = $this->lang->line('company_no_result_found');
        }
        if ($data['status'] === false) {
            $this->response_failure($data['error'], $data['code']);
        } else {
            $this->response_success($data, self::HTTP_OK);
        }
    }

    public function request_change_type_contract_batch_post()
    {
        $input = $this->post();
        $agreementService = new AgreementService();
        $errors = $agreementService->validate($input);
        if (!empty($errors)) {
            $this->response_failure($errors, self::HTTP_OK);
        }

        $companies = $this->company_model->as_array()
            ->fields(['id', 'deleted_at'])
            ->join_agreement()
            ->join_plan();

        if (in_array('all', $input['company_ids'])) {
            $filters = $this->input->get();
            $query_search = $this->query_search(self::COMPANY_SEARCH, $filters);
            $companies->where($query_search);
            if (isset($input['company_ids_unchecked']) && !empty($input['company_ids_unchecked'])) {
                $this->db->where_not_in('companies.id', $input['company_ids_unchecked']);
            }
        } else {
            $this->db->where_in('companies.id', $input['company_ids']);
        }
        $companies = $companies->get_all();
        $input['company_ids'] = array_column($companies, 'id');

        list($totalRows, $errors) = $agreementService->handleSaveChangeTypeBatch($input);

        $this->response_success(['success' => $totalRows, 'errors' => $errors], self::HTTP_OK);
    }

    /**
     * Valid required with
     *
     * @param string $value valua field check
     * @param string $field field name to check with
     *
     * @return bool
     */
    public function required_with($value, $field)
    {
        $data_validate = $this->session->flashdata('data_validate');
        if (trim($value) == '' && isset($data_validate[$field]) && $data_validate[$field] == 1) {
            $this->form_validation->set_message('required_with', $this->lang->line('form_validation_required'));
            return false;
        }
        return true;
    }
}
