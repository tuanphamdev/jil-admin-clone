<?php
class Ts_password_change_api extends Api_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model(['ts_password_change_model', 'admin_ts_password_log_model']);
    }

    /**
     * List log company use ts password
     *
     */
    public function list_log_get()
    {
        $paginate = $this->paginate();
        $per_page = $paginate['per_page'];
        $curr_page = $paginate['curr_page'];
        $query_search = $this->get();
        $data['total'] = $this->ts_password_change_model->list_log($query_search, $per_page, (($curr_page-1)*$per_page), true);
        $data['ts_password_log'] = $this->ts_password_change_model->list_log($query_search, $per_page, (($curr_page-1)*$per_page));
        $data['per_page'] = $per_page;
        if (empty($data['ts_password_log'])) {
            $this->response_failure([$this->lang->line('ts_password_log_no_result_found')], self::HTTP_NOT_FOUND);
        }
        $this->response_success($data, self::HTTP_OK);
    }

    /**
     * List employee ts password
     *
     */
    public function list_get()
    {
        $paginate = $this->paginate();
        $per_page = $paginate['per_page'];
        $curr_page = $paginate['curr_page'];
        $query_search = $this->get();
        $data['total'] = $this->ts_password_change_model->list($query_search, $per_page, (($curr_page-1)*$per_page), true);
        $data['ts_password_change'] = $this->ts_password_change_model->list($query_search, $per_page, (($curr_page-1)*$per_page));
        $data['per_page'] = $per_page;
        if (empty($data['ts_password_change'])) {
            $this->response_failure([$this->lang->line('ts_password_log_no_result_found')], self::HTTP_NOT_FOUND);
        }
        $this->response_success($data, self::HTTP_OK);
    }


    /**
     * List employee ts password
     *
     */
    public function detail_company_core_get()
    {
        $params = $this->get();
        $data = $this->ts_password_change_model->detail_company_core($params);
        if (empty($data)) {
            $this->response_failure([$this->lang->line('ts_password_log_no_result_found')], self::HTTP_NOT_FOUND);
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function change_password_post()
    {
        $data = $this->post();
        $lenght_pass = strlen($data['ts_password']);
        if ((!empty($data['ts_password']) && ($lenght_pass < 8 || $lenght_pass >30)) || empty($data['core_company_id']) || empty($data['employee_id'])) {
            $this->response_failure(['パスワードは8文字以上で入力してください。'], self::HTTP_BAD_REQUEST);
            return;
        }
        //connect to Core
        if (core_connected()) {
            $this->load->library('clients/ts_password_change_client');
            $data_core = $data;
            $data_core['company_id'] = $data_core['core_company_id'];
            unset($data_core['core_company_id']);
            $result = $this->ts_password_change_client->change_password_core($data_core);
            if ($result['status'] === false) {
                $this->response_failure($result['error'], self::HTTP_BAD_REQUEST);
            }
            $data = [
                'user_id' => $data['user_id'],
                'company_id' => $data['core_company_id'],
                'employee_id' => $data['employee_id'],
                'action_type' => $lenght_pass > 0 ? 0 : 1,
            ];
            $insert_id = $this->admin_ts_password_log_model->insert($data);
            if (empty($insert_id)) {
                $this->response_failure(['TS_PASSWORD_LOGSの更新が失敗しました。'], self::HTTP_BAD_REQUEST);
                return;
            }
            $this->response_success('TSパスワードを更新しました。', self::HTTP_OK);
        } else {
            $this->response_failure('Cannot connect Admin to Core.', self::HTTP_BAD_REQUEST);
        }
        $this->response_failure('Update to data core fail.', self::HTTP_BAD_REQUEST);
    }
}
