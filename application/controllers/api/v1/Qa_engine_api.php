<?php
defined('BASEPATH') or exit('No direct script access allowed');
//
class Qa_engine_api extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['qa_engine_model']);
    }

    /**
     * List QA Engine
     *
     */
    public function list_get()
    {
        $data = $this->qa_engine_model->as_array()->fields('*')->get();
        if (empty($data)) {
            $this->response_failure([$this->lang->line('ip_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function create_post()
    {
        //Validate post data
        $update_id = $this->post('id');
        $qa_engine = $this->qa_engine_model->from_form();
        if (!empty($this->validation_errors()) === true) {
            $errors = $this->validation_errors();
            $this->response_failure(array_unique($errors), self::HTTP_OK);
            return;
        }
        if (!empty($update_id)) {
            $data = $this->qa_engine_model->from_form(null, null, array('id' => $update_id))
                ->where('id', $update_id)
                ->update();
            if (empty($data)) {
                $this->response_failure([$this->lang->line('qa_engine_create_failed')], self::HTTP_OK);
                return;
            }
            $this->response_success($data, self::HTTP_OK);
        } else {
            $this->db->trans_begin();
            $qa_id = $qa_engine->insert();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                $this->response_failure([$this->lang->line('qa_engine_create_failed')], self::HTTP_OK);
                return;
            } else {
                $this->db->trans_commit();
            }
            $this->response_success($qa_id, self::HTTP_CREATED);
        }
    }
}
