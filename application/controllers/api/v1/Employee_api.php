<?php
/**
 * Created by PhpStorm.
 * User: mrnghia
 * Date: 5/7/19
 * Time: 11:14 AM
 */
class Employee_api extends Api_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model(['employee_model', 'company_model']);
    }

    /**
     * List Employees
     *
     */
    public function list_get()
    {
        $paginate = $this->paginate();
        $per_page = $paginate['per_page'];
        $curr_page = $paginate['curr_page'];

        $query_search = $this->query_search(self::EMPLOYEE_SEARCH);
        //validate search employee
        $this->employee_model->from_form($this->employee_model->rules['search_employee'], null, null, $query_search);
        if (!empty($this->validation_errors())) {
            $this->response_failure($this->validation_errors(), self::HTTP_BAD_REQUEST);
            return;
        }
        $data['total'] = $this->employee_model->list($query_search, $per_page, (($curr_page-1)*$per_page), true);
        $data['employee'] = $this->employee_model->list($query_search, $per_page, (($curr_page-1)*$per_page));
        $data['per_page'] = $per_page;

        if (empty($data['employee'])) {
            $this->response_failure([$this->lang->line('employee_no_result_found')], self::HTTP_NOT_FOUND);
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function enable_admin_role_post()
    {
        $data = $this->post();
        $employee_code = $this->post('employee_code');
        $core_company_id = $this->post('core_company_id');
        $admin_company_id = $this->post('admin_company_id');

        //validate data change role
        $this->employee_model->from_form($this->employee_model->rules['enable_admin_role'], null, null, $data??['search']);
        if (!empty($this->validation_errors())) {
            $this->response_failure($this->validation_errors(), self::HTTP_BAD_REQUEST);
            return;
        }

        //Validate isset employee code
        $data  = $this->employee_model->fields('id')->as_array()
            ->where(array('employees.code' => $employee_code, 'company_id' => $core_company_id))
            ->get();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('employee_no_result_found')], self::HTTP_NOT_FOUND);
        }

        $this->db->trans_begin();

        $old_admin_updated = $this->employee_model->where(array('is_admin' => '1', 'company_id' => $core_company_id))->update(['is_admin' => '0']);
        $this->employee_model->where(array('id' => $data['id'], 'company_id' => $core_company_id))->update(['is_admin' => 1]);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('employee_no_result_found')], self::HTTP_NOT_FOUND);
        } else {
            $this->db->trans_commit();
        }

        //connect to Core
        if (core_connected()) {
            $this->load->library('clients/company_client');
            $data = $this->company_client->core_update_company(['company_id' => $admin_company_id]);
            if ($data['status'] === false) {
                $this->response_failure($data['error'], $data['code']);
            }
        }

        $this->response_success($old_admin_updated, self::HTTP_OK);
    }
}
