<?php
defined('BASEPATH') or exit('No direct script access allowed');
//
class Information_api extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model(['information_model']);
    }


    /**
     * List ip
     *
     */
    public function list_get()
    {
        $infor_fields = array (
            'id',
            'title',
            'link_url',
            'expiration_date',
            'created_at',
            'is_open'
        );
        $query_infor_field = implode(',', $infor_fields);
        $paginate = $this->paginate();
        $per_page = $paginate['per_page'];
        $curr_page = $paginate['curr_page'];

        $query_search = $this->query_search(self::INFORMATION_SEARCH);

        $data['total'] = $this->information_model->as_array()
            ->fields($query_infor_field)->where($query_search)
            ->count_rows();
        $data['information'] = $this->information_model->as_array()
            ->fields($query_infor_field)->order_by('informations.updated_at', 'DESC')
            ->where($query_search)
            ->paginate($per_page, $data['total'], $curr_page);
        $data['per_page'] = $per_page;

        if (empty($data)) {
            $this->response_failure([$this->lang->line('information_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function create_post()
    {
        //Validate post data
        $information = $this->information_model->from_form();
        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_OK);
            return;
        }
        $this->db->trans_begin();
        $information_id = $information->insert();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('information_create_failed')], self::HTTP_OK);
            return;
        } else {
            //connect to Core
            if (core_connected()) {
                $this->load->library('clients/information_client');
                $input = $this->post();
                $core_data = [
                    'admin_information_id' => $information_id,
                    'title' => $input['title'],
                    'detail' => $input['detail'],
                    'link_url' => $input['link_url'],
                    'expiration_date' => $input['expiration_date'],
                ];
                $data = $this->information_client->core_create_information($core_data);
                if ($data['status'] === false) {
                    $this->response_failure($data['error'], $data['code']);
                    return;
                }
            }
            $this->db->trans_commit();
        }
         $this->response_success(['id' => $information_id], self::HTTP_CREATED);
    }

    
    public function update_post()
    {
        $information_id = $this->post('id');
        $information = $this->information_model->from_form(null, null, array('id' => $information_id));
        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_OK);
            return;
        }

        //Validate information data
        $data  = $this->information_model->fields('id')
            ->where(array('informations.id' => $information_id))
            ->get();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('information_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }

        $this->db->trans_begin();
        $affected = $information->where('id', $information_id)->update();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('information_update_failed')], self::HTTP_OK);
            return;
        } else {
            //connect to Core
            if (core_connected()) {
                $this->load->library('clients/information_client');
                $input = $this->post();
                $core_data = [
                    'admin_information_id' => $information_id,
                    'title' => $input['title'],
                    'detail' => $input['detail'],
                    'link_url' => $input['link_url']
                ];
                $data = $this->information_client->core_update_information($core_data);
                if ($data['status'] === false) {
                    $this->response_failure($data['error'], $data['code']);
                    return;
                }
            }
            $this->db->trans_commit();
        }
        $this->response_success($affected, self::HTTP_OK);
    }


    public function off_post()
    {
        $information_id = $this->post('information_id');

        //Validate post data
        $rule = array(
            'information_id' => [
                'field'=>'information_id',
                'label'=>'information_id',
                'rules'=>'trim|required|is_natural_greater_than_zero'
            ]);
        $information = $this->information_model->from_form($rule, array('is_open' =>  0), array('id' => $information_id));

        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_BAD_REQUEST);
            return;
        }

        //Validate information data
        $data  = $this->information_model->fields('id')
            ->where(array('informations.id' => $information_id))
            ->get();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('information_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }

        //connect to Core
        if (core_connected()) {
            $this->load->library('clients/information_client');
            $data = $this->information_client->core_off_alert(['is_open' => 0, 'information_id' => $information_id]);
            if ($data['status'] === false) {
                $this->response_failure($data['error'], $data['code']);
                return;
            }
        }
        $this->db->trans_begin();
        $affected = $information->where('id', $information_id)->update();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('information_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        } else {
            $this->db->trans_commit();
        }
        $this->response_success($affected, self::HTTP_OK);
    }

    public function on_post()
    {
        $information_id = $this->post('information_id');

        //Validate post data
        $rule = array(
            'information_id' => [
                'field'=>'information_id',
                'label'=>'information_id',
                'rules'=>'trim|required|is_natural_greater_than_zero'
            ]);
        $information = $this->information_model->from_form($rule, array('is_open' =>  1), array('id' => $information_id));

        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_BAD_REQUEST);
            return;
        }

        //Validate information data
        $data  = $this->information_model->fields('id')
            ->where(array('informations.id' => $information_id))
            ->get();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('information_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }

        //connect to Core
        if (core_connected()) {
            $this->load->library('clients/information_client');
            $data = $this->information_client->core_on_alert(['is_open' => 1, 'information_id' => $information_id]);
            if ($data['status'] === false) {
                $this->response_failure($data['error'], $data['code']);
                return;
            }
        }
        $this->db->trans_begin();
        $affected = $information->where('id', $information_id)->update();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('information_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        } else {
            $this->db->trans_commit();
        }
        $this->response_success($affected, self::HTTP_OK);
    }
}
