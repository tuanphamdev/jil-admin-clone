<?php
defined('BASEPATH') or exit('No direct script access allowed');
//
class Ts_password_ip_api extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model(['ts_password_ip_model']);
    }


    /**
     * List ip
     *
     */
    public function list_get()
    {
        $ip_fields = array (
            'id',
            'name',
            'ip_address',
        );
        $query_ip_field = implode(',', $ip_fields);
        $data['total'] = $this->ts_password_ip_model->as_array()
            ->fields($query_ip_field)
            ->count_rows();
        $data['ip'] = $this->ts_password_ip_model->as_array()
            ->fields($query_ip_field)->order_by('ts_password_ips.updated_at', 'DESC')
            ->get_all();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('ts_password_ip_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function create_post()
    {
        //Validate post data
        $ip = $this->ts_password_ip_model->from_form();

        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_OK);
            return;
        }

        $this->db->trans_begin();
        $ip_id = $ip->insert();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('ts_password_ip_create_failed')], self::HTTP_OK);
            return;
        } else {
            $this->db->trans_commit();
        }
        $this->response_success($ip_id, self::HTTP_CREATED);
    }
    public function detail_get()
    {
        $id = $this->get('id');
        $data = $this->ts_password_ip_model->as_array()->fields('*')
            ->with_agreement('fields:*')
            ->with_plan('fields:*')->where('id', $id)->get();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('ts_password_ip_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }


    public function update_post()
    {
        $ip_id = $this->post('id');
        $data = $this->ts_password_ip_model->from_form(null, null, array('id' => $ip_id))->where('id', $ip_id)->update();
        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_OK);
            return;
        }
        if (empty($data)) {
            $this->response_failure([$this->lang->line('ts_password_ip_update_failed')], self::HTTP_OK);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function delete_post()
    {
        $ip_id = $this->post('id') ?? '';
        $data = $this->ts_password_ip_model->delete($ip_id);
        if (empty($data)) {
            $this->response_failure([$this->lang->line('ts_password_ip_delete_failed')], self::HTTP_OK);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }
}
