<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Jinjer\services\SurveyMonkeyService;

class Survey_mokey_api extends Api_Controller
{
    protected $surveyMonkeyService;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['survey_monkey_model']);
        $this->load->library(['clients/core_survey_monkey_client']);
        $this->surveyMonkeyService = new SurveyMonkeyService();
    }

    /**
     * Last Survey monkey
     *
     */
    public function last_survey_monkey_get()
    {
        $data = $this->surveyMonkeyService->getLastSurveyMonkey();
        if (empty($data)) {
            $this->response_failure([$this->lang->line('survey_monkey_create_failed')], self::HTTP_NOT_FOUND);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function save_post()
    {
        $input = $this->post();
        $old_survey_monkey = $this->surveyMonkeyService->getLastSurveyMonkey();
        list($isChangeScriptContent, $isUpdate) = $this->surveyMonkeyService->checkStatusUpdateSurveyMonkey($input, $old_survey_monkey);
        $input['services'] = implode(',', $input['services'] ?? []);
        $survey_monkey = $this->survey_monkey_model->from_form(null, $input, [], $input);
        if (!empty($this->validation_errors()) === true) {
            $errors = $this->validation_errors();
            $this->response_failure(array_unique($errors), self::HTTP_OK);
            return;
        }
        if ($isUpdate && !$isChangeScriptContent) {
            $survey_monkey->update($input, ['id' => $old_survey_monkey['id']]);
            $this->response_success($survey_monkey->get(), self::HTTP_OK);
        } elseif ($isChangeScriptContent) {
            try {
                $this->db->trans_begin();
                $this->survey_monkey_model->delete(['id' => $old_survey_monkey['id']]);
                $survey_monkey->insert();
                $response = $this->core_survey_monkey_client->reset_all_employees_status();
                if (!$response['status']) {
                    throw new \Exception("Can't reset employees status", 500);
                }
                $this->surveyMonkeyService->resetSurveyMonkeyEmployeesStatusCache();
                $this->db->trans_commit();
                $this->response_success($survey_monkey->get(), self::HTTP_OK);
            } catch (\Exception $e) {
                $this->db->trans_rollback();
                $this->monolog->logger(Monolog::EXCEPTION)->info($e, ['request' => 'survey monkey: save_post']);
                $this->response_failure([$this->lang->line('survey_monkey_create_failed')], self::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        $this->response_success($old_survey_monkey, self::HTTP_OK);
    }
}
