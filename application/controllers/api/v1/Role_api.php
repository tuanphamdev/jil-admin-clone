<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Role_api extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['role_model', 'user_model']);
        $this->load->model(['permission_model']);
    }

    public function list_get()
    {
        $role_fields = array(
            Role_model::$field_id,
            Role_model::$field_name
        );
        $query_role_field = implode(',', $role_fields);
        $data['total'] = $this->role_model->as_array()->fields($query_role_field)->count_rows();
        $data['role'] = $this->role_model->as_array()
            ->fields($query_role_field)->order_by('roles.updated_at', 'DESC')
            ->get_all();

        $this->role_model->as_array()->fields($query_role_field)->count_rows();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('role_no_result_found')], self::HTTP_OK);
            return;
        } else {
            foreach ($data['role'] as &$item) {
                $user_counter = $this->user_model->as_array()->fields('id')->where(['role_id' => $item['id']])->count_rows();
                $item['user_counter'] = $user_counter;
            }
            $this->response_success($data, self::HTTP_OK);
        }
    }

    public function create_post()
    {

        $role = $this->role_model->from_form();
        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_OK);
            return;
        }

        $permissions = $this->post();
        $this->db->trans_begin();
        $role_id = $role->insert();
        $this->permission_model->insert_permission($role_id, $permissions);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('role_create_failed')], self::HTTP_OK);
            return;
        } else {
            $this->db->trans_commit();
        }

        $this->response_success($role_id, self::HTTP_CREATED);
    }

    public function update_post()
    {
        $role_id = $this->post('id');
        $role = $this->role_model->from_form(null, null, array('id' => $role_id));
        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_OK);
            return;
        }

        $permissions = $this->post();
        $this->db->trans_begin();
        $data = $role->where('id', $role_id)->update();
        $this->permission_model->update_permission($role_id, $permissions);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('role_update_failed')], self::HTTP_OK);
            return;
        } else {
            $this->db->trans_commit();
        }

        $this->response_success($data, self::HTTP_OK);
    }

    public function delete_post()
    {
        $role_id = $this->post('id') ?? '';
        $data = $this->role_model->as_array()
            ->fields('id')
            ->get($role_id);
        if (empty($data)) {
            $this->response_failure([$this->lang->line('role_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }

        $user_counter = $this->user_model->as_array()->fields('id')->where(['role_id' => $role_id])->count_rows();
        if ($user_counter > 0) {
            $this->response_failure([$this->lang->line('role_delete_failed')], self::HTTP_NOT_ACCEPTABLE);
            return;
        }

        $this->db->trans_begin();
        $data = $this->role_model->delete($role_id);
        $this->permission_model->where(['role_id' => $role_id])->delete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $this->response_failure([$this->lang->line('role_delete_failed')], self::HTTP_OK);
            return;
        } else {
            $this->db->trans_commit();
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function detail_get()
    {
        $id = $this->get('id');
        $role_fields = array(
            Role_model::$field_id,
            Role_model::$field_name
        );
        $query_role_field = implode(',', $role_fields);

        $data = $this->role_model->as_array()
            ->fields($query_role_field)
            ->with_permissions()
            ->get($id);

        if (empty($data)) {
            $this->response_failure([$this->lang->line('role_no_result_found')], self::HTTP_OK);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }
}
