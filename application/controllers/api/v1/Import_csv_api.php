<?php
defined('BASEPATH') or exit('No direct script access allowed');
//
class Import_csv_api extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model(['bank_master_model']);
    }

    /**
     * List bank info
     *
     */
    public function list_get()
    {
        $bank_fields = array (
            'id',
            'bank_code',
            'branch_code',
            'bank_name',
            'branch_name',
            'bank_name_kana',
            'branch_name_kana',
            'branch_post_code',
            'branch_address',
            'branch_phone_number',
            'draft_clearing_place',
            'sort',
            'status'
        );
        $query_bank_field = implode(',', $bank_fields);

        $paginate = $this->paginate();
        $per_page = $paginate['per_page'];
        $curr_page = $paginate['curr_page'];
        
        $query_search = $this->query_search(self::BANK_SEARCH);

        $data['total'] = $this->bank_master_model->as_array()
            ->fields($query_bank_field)
            ->where($query_search)
            ->count_rows();
        $data['bank'] = $this->bank_master_model->as_array()
            ->fields($query_bank_field)
            ->where($query_search)
            ->paginate($per_page, $data['total'], $curr_page);
        $data['per_page'] = $per_page;
        if (empty($data)) {
            $this->response_failure([$this->lang->line('import_csv_result_found')], self::HTTP_NOT_FOUND);
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function import_csv_bank_master_post()
    {
        $csv_file = $this->post('data');

        if (file_exists($csv_file['tmp_name'])) {
            $read_handle = fopen($csv_file['tmp_name'], 'r');
            while ($line = fgetcsv($read_handle, 0, ";", "\"")) {
                foreach ($line as $key => $value) {
                    $data = explode(",", mb_convert_encoding($value, 'UTF-8', 'Shift-JIS'));
                    if (!is_numeric($data[0])) {
                        continue;
                    }
                    $row['bank_code'] = sprintf("%04d", trim($data[0], '"'));
                    $row['branch_code'] = sprintf("%03d", trim($data[1], '"'));
                    $row['bank_name_kana'] = trim($data[2], '"');
                    $row['bank_name'] = trim($data[3], '"');
                    $row['branch_name_kana'] = trim($data[4], '"');
                    $row['branch_name'] = trim($data[5], '"');
                    $row['branch_post_code'] = trim($data[6], '"');
                    $row['branch_address'] = trim($data[7], '"');
                    $row['branch_phone_number'] = trim($data[8], '"');
                    $row['draft_clearing_place'] = trim($data[9], '"');
                    $row['sort'] = trim($data[10], '"');
                }
                if (!empty($row)) {
                    $csv_data[] = $row;
                }
            }
            
            fclose($read_handle);
        }

        if (empty($csv_data)) {
            $this->response_failure([$this->lang->line('export_csv_no_result')], self::HTTP_NOT_FOUND);
            return;
        }
        $data = $this->bank_master_model->insert_or_update_batch_bank_master($csv_data);
        if (empty($data)) {
            $this->response_failure([$this->lang->line('company_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function detail_get($id)
    {
        $data = $this->bank_master_model->detail($id);
        $this->response_success($data[0] ?? [], self::HTTP_OK);
    }
}
