<?php
defined('BASEPATH') or exit('No direct script access allowed');
//
class User_api extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['user_model']);
        $this->load->model(['role_model', 'permission_model']);
    }

    public function auth_post()
    {
        $rule = array(
            'email' => [
                'field'=>'email',
                'label'=>'lang:auth_login_email',
                'rules'=>'trim|required|valid_email'
            ],
            'password' => [
                'field'=>'password',
                'label'=>'lang:auth_login_password',
                'rules'=>'trim|required'
            ]
        );
        $this->user_model->from_form($rule);
        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_OK);
        }
        $email = $this->post('email');
        $password = $this->post('password');
        $data = $this->user_model->auth($email, $password);
        $role = $this->role_model->as_array()->where(['id' => $data['role_id']])->with_permissions('fields: item, permission')->get();
        if ($role && is_array($role['permissions'])) {
            $data['roles'] = [];
            foreach ($role['permissions'] as $permission) {
                $data['roles'][$permission->item] = $permission->permission;
            }
        }

        if (empty($data)) {
            $this->response_failure([$this->lang->line('auth_login_failed')], self::HTTP_OK);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function list_get()
    {
        $user_fields = array (
            User_model::$field_id,
            User_model::$field_role_id,
            User_model::$field_name,
            User_model::$field_name_furigana,
            User_model::$field_email,
            User_model::$field_status,
            User_model::$field_is_send_mail,
        );
        $query_user_field = implode(',', $user_fields);
        $role_fields = array (
            Role_model::$field_id,
            Role_model::$field_name
        );
        $query_role_field = "fields:" . implode(',', $role_fields);
        $query_search = $this->query_search(self::USER_SEARCH);


        $paginate = $this->paginate();
        $per_page = $paginate['per_page'];
        $curr_page = $paginate['curr_page'];
        
        $data['total'] = $this->user_model->as_array()
            ->fields($query_user_field)
            ->with_role($query_role_field)
            ->where($query_search)->count_rows();

        $data['user'] = $this->user_model->as_array()
            ->fields($query_user_field)
            ->where($query_search)
            ->with_role($query_role_field)->order_by('users.updated_at', 'DESC')
            ->paginate($per_page, $data['total'], $curr_page);
        $data['per_page'] = $per_page;

        if (empty($data)) {
            $this->response_failure([$this->lang->line('user_no_result_found')], self::HTTP_OK);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function list_all_get()
    {
        $user_fields = array (
            User_model::$field_id,
            User_model::$field_role_id,
            User_model::$field_name,
            User_model::$field_name_furigana,
            User_model::$field_email,
            User_model::$field_status,
            User_model::$field_is_send_mail,
        );
        $query_user_field = implode(',', $user_fields);
        $data['user'] = $this->user_model->as_array()
        ->fields($query_user_field)->order_by('users.updated_at', 'DESC')
        ->get_all();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('user_no_result_found')], self::HTTP_OK);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function create_post()
    {

        $this->user_model->from_form();
        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_OK);
            return;
        }

        $password = $this->post('password');
        $hash_password = password_hash($password, PASSWORD_BCRYPT);
        $data = [
            'role_id' => $this->post('role_id'),
            'name' => $this->post('name'),
            'email' => $this->post('email'),
            'name_furigana' => $this->post('name_furigana'),
            'password' => $hash_password,
            'status' => $this->post('status'),
            'is_send_mail' => $this->post('is_send_mail'),
        ];
        $user_id = $this->user_model->insert($data);
        if (empty($user_id)) {
            $this->response_failure([$this->lang->line('user_update_failed')], self::HTTP_OK);
            return;
        }
        $this->response_success($user_id, self::HTTP_CREATED);
    }

    public function update_post()
    {
        $user_id = $this->post('id');
        $data = $this->user_model->from_form(null, null, array('id' => $user_id))->where('id', $user_id)->update();
        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_OK);
            return;
        }
        if (empty($data)) {
            $this->response_failure([$this->lang->line('user_update_failed')], self::HTTP_OK);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function delete_post()
    {
        $id = $this->post('id') ?? '';
        $data = $this->user_model->delete($id);
        if (empty($data)) {
            if (!empty($this->user_model->form_validation->error_array()) === true) {
                $this->response_failure($this->validation_errors(), self::HTTP_OK);
                return;
            }
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function detail_get()
    {
        $id = $this->get('id');
        $user_fields = array (
            User_model::$field_id,
            User_model::$field_role_id,
            User_model::$field_name,
            User_model::$field_name_furigana,
            User_model::$field_email,
            User_model::$field_status,
            User_model::$field_is_send_mail,
        );
        $query_user_field = implode(',', $user_fields);
        $role_fields = array (
            Role_model::$field_id,
            Role_model::$field_name
        );
        $query_role_field = "fields:" . implode(',', $role_fields);

        $data = $this->user_model->as_array()
            ->fields($query_user_field)
            ->with_role($query_role_field)
            ->get($id);

        if (empty($data)) {
            $this->response_failure([$this->lang->line('company_no_result_found')], self::HTTP_OK);
            return;
        }
        $data['user'] = $data;
        $this->response_success($data, self::HTTP_OK);
    }
    
    public function detail_by_email_get()
    {
        $email = $this->post('email');
        $user_fields = array (
            User_model::$field_id,
            User_model::$field_role_id,
            User_model::$field_name,
            User_model::$field_name_furigana,
            User_model::$field_email,
        );
        $query_user_field = implode(',', $user_fields);

        $data = $this->user_model->as_array()
            ->fields($query_user_field)
            ->where(['email' => $email])
            ->get();

        if (empty($data)) {
            $this->response_failure([$this->lang->line('company_no_result_found')], self::HTTP_OK);
        } else {
            $data['user'] = $data;
            $this->response_success($data, self::HTTP_OK);
        }
    }

    public function forgot_password_post()
    {
        $email = $this->post('email') ?? '';
        $token = $this->post('reset_password_token') ?? '';
        $data = $this->user_model->where('email', $email)->update(['reset_password_token' => $token]);
        if (empty($data)) {
            $this->response_failure([$this->lang->line('user_forgot_failed')], self::HTTP_NOT_FOUND);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function reissue_password_post()
    {
        $reset_password_token = $this->post('reset_password_token');
        $user = $this->user_model->as_array()
            ->fields('id')
            ->where(['reset_password_token' => $reset_password_token])
            ->get();

        if (empty($user)) {
            $this->response_failure([$this->lang->line('user_token_invalid')], self::HTTP_OK);
            return;
        }

        $rule = array(
            'password' => [
                'field'=>'password',
                'label'=>'lang:auth_login_password',
                'rules'=>'trim|required'
            ],
            'password_confirm' => [
                'field'=>'password_confirm',
                'label'=>'lang:auth_login_password_confirm',
                'rules'=>'trim|required|matches[password]'
            ]
        );
        $user = $this->user_model->from_form($rule);
        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_OK);
            return;
        }
        $password = $this->post('password');
        $data = $this->user_model->where('reset_password_token', $reset_password_token)->update(['password' => password_hash($password, PASSWORD_BCRYPT), 'reset_password_token' => null]);
        if (empty($data)) {
            $this->response_failure([$this->lang->line('user_reissue_password_failed')], self::HTTP_NOT_FOUND);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }
}
