<?php
defined('BASEPATH') or exit('No direct script access allowed');
//
class Export_csv_api extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model(['company_model']);
        $this->load->model(['agreement_model']);
    }


    /**
     * List Companies
     *
     */
    public function list_get()
    {
        $company_fields = array (
            'id',
            'code',
            'name',
            'registered_date',
            'tel',
            'agreements.type'
        );
        $query_company_field = implode(',', $company_fields);

        $paginate = $this->paginate();
        $per_page = $paginate['per_page'];
        $curr_page = $paginate['curr_page'];
        
        $query_search = $this->query_search(self::COMPANY_SEARCH);

        $data['total'] = $this->company_model->as_array()
            ->fields($query_company_field)->where($query_search)
            ->join_agreement()
            ->count_rows();
        $data['company'] = $this->company_model->as_array()
            ->fields($query_company_field)->where($query_search)
            ->join_agreement()
            ->paginate($per_page, $data['total'], $curr_page);
        $data['per_page'] = $per_page;

        if (empty($data)) {
            $this->response_failure([$this->lang->line('company_no_result_found')], self::HTTP_NOT_FOUND);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }

    public function retire_staff_post()
    {
        $start_date_retire = $this->post('start_date_retire');
        $end_date_retire = $this->post('end_date_retire');

        $this->company_model->from_form($this->company_model->rules['retire_staff'], null, null);

        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_BAD_REQUEST);
            return;
        }

        $data = $this->company_model->get_retire_staff($start_date_retire, $end_date_retire);

        if (empty($data)) {
            $this->response_failure([$this->lang->line('export_csv_no_result')], self::HTTP_NOT_FOUND);
            return;
        }
        $this->response_success($data, self::HTTP_OK);
    }
    
    public function tokyo_marine_staff_post()
    {
        $num_account = $this->post('num_account');
        $this->company_model->from_form($this->company_model->rules['tokyo_marine_staff'], null, null);
        if (!empty($this->validation_errors()) === true) {
            $this->response_failure($this->validation_errors(), self::HTTP_BAD_REQUEST);
        }
        ini_set_csv();
        $data = $this->company_model->get_tokyo_marine_staff($num_account);
    
        if (empty($data)) {
            $this->response_failure([$this->lang->line('export_csv_no_result')], self::HTTP_NOT_FOUND);
        }
        $this->response_success($data, self::HTTP_OK);
    }
}
