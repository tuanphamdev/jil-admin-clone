<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Top extends CI_Controller
{
    public function index()
    {
        $data['content_title'] = $this->lang->line('top_title');
        $data['no_permission'] = $this->input->get('no_permission')??'';
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'dashboard/index', $data);
    }

    protected function get_permissions()
    {
        $default = [
            'company' => '2',
            'user' => '2',
            'role' => '2',
            'csv' => '2',
            'ip' => '2',
            'information' => '1',
            'import_bank' => '2',
            'tokyo_marine' => '2',
            'employee' => '2',
            'ts_password_change' => '2',
            'ts_password_ips' => '2',
            'ts_password_logs' => '2',
        ];
        $roles = $this->session->userdata('roles');
        foreach ($roles as $role => $permission) {
            $default[$role] = $permission;
        }
        return $default;
    }
}
