<?php
class Batch extends CI_Controller
{

    const APPLY_CONTRACT = 'apply_contract';
    const EXPIRATION_CONTRACT = 'expiration_contract';
    const EXPIRATION_INFORMATION = 'expiration_information';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('clients/company_client');
    }

    public function rake_data_ip()
    {
        $old_db = $this->load->database('old_db', true);
        $new_db = $this->load->database('default', true);

        $old_data = $old_db->select('*')->from('access_ips')->get()->result_array();
        foreach ($old_data as $data) {
            $insert_data = array(
                'id' => $data['id'],
                'ip_address' => $data['ip_address'],
                'created_at' => $data['created'],
                'updated_at' => $data['updated'],
                'deleted_at' => $data['deleted'],
            );

            $new_db->insert('access_ips', $insert_data);
        }
    }

    public function rake_data_role()
    {
        echo "start...\n";
        $old_db = $this->load->database('old_db', true);
        $new_db = $this->load->database('default', true);

        $old_data = $old_db->select('*')->from('roles')->get()->result_array();
        $counter = count($old_data);
        foreach ($old_data as $data) {
            $insert_data = array(
                'id' => $data['id'],
                'name' => $data['name'],
                'created_at' => $data['created'],
                'updated_at' => $data['updated'],
                'deleted_at' => $data['deleted'],
                'created_by_id' => 1,
            );

            $insert_data_permission = array(
                array(
                    'item' => 'company',
                    'permission' => 2,
                    'created_at' => date('Y-m-d H:i:s'),
                    'role_id' => $data['id']
                ),
                array(
                    'item' => 'user',
                    'permission' => 2,
                    'created_at' => date('Y-m-d H:i:s'),
                    'role_id' => $data['id']
                ),
                array(
                    'item' => 'role',
                    'permission' => 2,
                    'created_at' => date('Y-m-d H:i:s'),
                    'role_id' => $data['id']
                ),
                array(
                    'item' => 'ip',
                    'permission' => 2,
                    'created_at' => date('Y-m-d H:i:s'),
                    'role_id' => $data['id']
                ),
                array(
                    'item' => 'csv',
                    'permission' => 2,
                    'created_at' => date('Y-m-d H:i:s'),
                    'role_id' => $data['id']
                )

            );

            $new_db->query('set foreign_key_checks = 0;');
            $new_db->insert('roles', $insert_data);
            $new_db->insert_batch('role_permissions', $insert_data_permission);
        }
        echo "done {$counter} rows.\n";
    }

    public function rake_data_user()
    {
        echo "start...\n";
        $old_db = $this->load->database('old_db', true);
        $new_db = $this->load->database('default', true);

        $old_data = $old_db->select('*')->from('users')->get()->result_array();
        $counter = count($old_data);
        foreach ($old_data as $data) {
            $insert_data = array(
                'id' => $data['id'],
                'name' => $data['name'],
                'name_furigana' => $data['furigana'] ?? ' ',
                'email' => $data['mail_address'],
                'password' => $data['password'],
                'reset_password_token' => $data['reset_token'],
                'role_id' => $data['role_id'],
                'created_at' => $data['created'],
                'updated_at' => $data['updated'],
                'deleted_at' => $data['deleted'],
                'status' => 1
            );
            $new_db->query('set foreign_key_checks = 0;');
            $new_db->insert('users', $insert_data);
        }
        echo "done {$counter} rows.\n";
    }

    public function rake_data_company()
    {
        echo "start...\n";
        $old_db = $this->load->database('old_db', true);
        $new_db = $this->load->database('default', true);

        $old_data = $old_db->select('companies.*,agreements.parent_id, agreements.company_id, agreements.type, agreements.start_date
        , agreements.end_date, agreements.number, plans.jinjer_db, plans.kintai, plans.roumu, plans.jinji, plans.my_number, plans.work_vital, plans.agreement_id')
            ->from('companies')
            ->join('agreements', 'agreements.company_id = companies.id AND agreements.parent_id IS NULL 
            AND current_date() BETWEEN agreements.start_date AND agreements.end_date AND agreements.cancel_status <> 2 AND agreements.deleted IS NULL')
            ->join('plans', 'agreements.id = plans.id AND plans.deleted IS NULL')
            ->where(['companies.deleted' => null])
            ->get()->result_array();

        $active_ids =  [];
        foreach ($old_data as $data) {
            $active_ids[] = $data['id'];
        }

        $old_data_inactive = $old_db->select('companies.*, agreements.parent_id, agreements.company_id, agreements.type, agreements.start_date,
         agreements.end_date, agreements.number, plans.jinjer_db, plans.kintai, plans.roumu, plans.jinji, plans.my_number, plans.work_vital, plans.agreement_id')
            ->from('companies')
            ->join('agreements', 'agreements.company_id = companies.id AND agreements.parent_id IS NULL 
            AND agreements.deleted IS NULL')
            ->join('plans', 'agreements.id = plans.id AND plans.deleted IS NULL')
            ->where(['companies.deleted' => null])
            ->where_not_in('companies.id', $active_ids)->get()->result_array();
        
        $old_data = array_merge($old_data, $old_data_inactive);
        $counter = count($old_data);

        foreach ($old_data as $data) {
            $user = $old_db->select('name,furigana')->from('users')->where(['id' => $data['user_id']])->limit(1)->get()->row_array();
            $data['user_name'] = $user['name'];
            $data['user_furigana'] = $user['furigana'];
            $insert_data = $this->get_insert_company_data($data);
            $insert_data_agreement = $this->get_insert_agreement_data($data);
            $insert_data_plan = $this->get_insert_plan_data($data);

            //-------------------------------------------
            $new_db->query('set foreign_key_checks = 0;');
            $new_db->insert('companies', $insert_data);
            $new_db->insert('agreements', $insert_data_agreement);
            $new_db->insert('plans', $insert_data_plan);
        }
        echo "done {$counter} rows.\n";
    }

    public function rake_data_company_map_missing()
    {
        echo "start...\n";
        $new_db = $this->load->database('default', true);
        $jinji_db = $this->load->database('jinji_db', true);
        $company_codes = $new_db->select('code')->from('companies')->get()->result_array();
        $counter = count($company_codes);
        foreach ($company_codes as $data) {
            $code = $data['code'];
            $jinji = $jinji_db->select('code, name_phonetic, representative_name, representative_name_kana, representative_title, address_kana')
                ->from('companies')->where(['code' => $code])->limit(1)->get()->row_array();
            if (!empty($jinji)) {
                $data_jinji = array(
                    'name_furigana' => $jinji['name_phonetic'],
                    'representative_name' => $jinji['representative_name'],
                    'representative_name_furigana' => $jinji['representative_name_kana'],
                    'representative_title' => $jinji['representative_title'],
                    'address_furigana' => $jinji['address_kana'],
                );

                $new_db->where('code', $code);
                $new_db->update('companies', $data_jinji);
            }
        }
        echo "done {$counter} rows.\n";
    }

    public function rake_data_company_map_admin_core()
    {
        echo "start...\n";
        $new_db = $this->load->database('default', true);
        $jinji_db = $this->load->database('jinji_db', true);
        $company_codes = $jinji_db->select('code')->from('companies')->where(['deleted_at' => null])->get()->result_array();
        $counter = count($company_codes);
        foreach ($company_codes as $data) {
            $code = $data['code'];
            $admin = $new_db->select('id')
                ->from('companies')->where(['code' => $code])->limit(1)->get()->row_array();
            if (!empty($admin)) {
                $data_admin = array(
                    'admin_company_id' => $admin['id'],
                );

                $jinji_db->where('code', $code);
                $jinji_db->update('companies', $data_admin);
            }
        }
        echo "done {$counter} rows.\n";
    }

    private function get_insert_agreement_data($data)
    {
        $insert_data = array(
            'id' => $data['id'],
            'company_id' => $data['company_id'],
            'type' => $data['type'],
            'start_date' => $data['start_date'],
            'end_date' => $data['end_date'],
            'num_account' => $data['number'],
            'created_at' => $data['created'],
            'updated_at' => $data['updated'],
            'deleted_at' => $data['deleted']
        );
        return $insert_data;
    }

    private function get_insert_plan_data($data)
    {

        $insert_data = array(
            'id' => $data['id'],
            'company_id' => $data['company_id'],
            'jinjer_db' => $data['jinjer_db'],
            'kintai' => $data['kintai'],
            'roumu' => $data['roumu'],
            'jinji' => $data['jinji'],
            'my_number' => $data['my_number'],
            'work_vital' => $data['work_vital'],
            'agreement_id' => $data['agreement_id'],
            'created_at' => $data['created'],
            'updated_at' => $data['updated'],
            'deleted_at' => $data['deleted']
        );
        return $insert_data;
    }

    private function get_insert_company_data($data)
    {
        $insert_data = array(
            'id' => $data['id'],
            'created_by_id' => 1,
            'code' => $data['code'],
            'name' => $data['name'],
            'name_furigana' => ' ',
            'representative_name' => $data['user_name'],
            'representative_name_furigana' => $data['user_furigana'] ?? ' ',
            'representative_title' => ' ',
            'tel' => $data['tel'],
            'fax' => $data['tel'],
            'zip' => $data['zip'],
            'prefecture' => json_decode($data['address1'], true)['id'],
            'city' => json_decode($data['address2'], true)['id'],
            'address' => $data['address3'],
            'address_furigana' => ' ',
            'application_name' => $data['application_name'],
            'application_surname' => $data['application_surname'],
            'application_furigana' => $data['application_furigana'],
            'application_furigana_surname' => $data['application_furigana_surname'],
            'application_email' => $data['application_mail_address'],
            'registered_date' => $data['register_date'],
            'status' => $data['status'],
            'created_at' => $data['created'],
            'updated_at' => $data['updated'],
            'deleted_at' => $data['deleted']
        );
        return $insert_data;
    }
    public function rake_data_company_contract_map_core_to_admin()
    {
        echo "start...\n";
        $new_db = $this->load->database('default', true);
        $jinji_db = $this->load->database('jinji_db', true);
        $company_codes = $jinji_db->select('code, status, contract_type, contract_info, start_date, end_date')
            ->from('companies')
            ->where(['deleted_at' => null])
            ->get()->result_array();
        $counter = 0;
        foreach ($company_codes as $jinji) {
            $code = $jinji['code'];
            $admin = $new_db->select('c.id, c.status, a.start_date, a.end_date, a.type, p.*')
                ->from('companies c')
                ->join('agreements a', 'a.company_id = c.id')
                ->join('plans p', 'p.company_id = a.id')
                ->where(['c.code' => $code, 'c.deleted_at' => null])->limit(1)->get()->row_array();
            if (empty($admin)) {
                continue;
            }
            $company_admin = null;
            $agreement_admin = null;
            $plan_admin = null;
            $company_id = $admin['id'];
            //company
            echo "code {$code}...\n";
            if ($admin['status'] != $jinji['status']) {
                $company_admin['status'] = $jinji['status'];
                echo "status {$admin['status']} -> {$jinji['status']} ...\n";
            }

            //agreement
            if ($admin['type'] != $jinji['contract_type']) {
                $agreement_admin['type'] = $jinji['contract_type'];
                echo "contract_type {$agreement_admin['type']} -> {$jinji['contract_type']} ...\n";
            }

            if ($admin['end_date'] != $jinji['end_date']) {
                $agreement_admin['end_date'] = $jinji['end_date'];
                echo "end_date {$agreement_admin['end_date']} -> {$jinji['end_date']} ...\n";
            }

            //plan
            $contract_info = json_decode($jinji['contract_info'], true);
            if ($admin['jinjer_db'] != $contract_info['jinjer_db']['type']) {
                $plan_admin['jinjer_db'] = $contract_info['jinjer_db']['type'];
                echo "jinjer_db {$plan_admin['jinjer_db']} -> {$contract_info['jinjer_db']['type']} ...\n";
            }
            if ($admin['kintai'] != $contract_info['kintai']['type']) {
                $plan_admin['kintai'] = $contract_info['kintai']['type'];
                echo "kintai {$admin['kintai']} -> {$contract_info['kintai']['type']} ...\n";
            }
            if ($admin['roumu'] != $contract_info['roumu']['type']) {
                $plan_admin['roumu'] = $contract_info['roumu']['type'];
                echo "roumu {$admin['roumu']} -> {$contract_info['roumu']['type']} ...\n";
            }
            if ($admin['jinji'] != $contract_info['jinji']['type']) {
                $plan_admin['jinji'] = $contract_info['jinji']['type'];
                echo "jinji {$admin['jinji']} -> {$contract_info['jinji']['type']} ...\n";
            }
            if ($admin['my_number'] != $contract_info['my_number']['type']) {
                $plan_admin['my_number'] = $contract_info['my_number']['type'];
                echo "my_number {$admin['my_number']} -> {$contract_info['my_number']['type']} ...\n";
            }
            if ($admin['work_vital'] != $contract_info['work_vital']['type']) {
                $plan_admin['work_vital'] = $contract_info['work_vital']['type'];
                echo "work_vital {$admin['work_vital']} -> {$contract_info['work_vital']['type']} ...\n";
            }
            if ($admin['keihi'] != $contract_info['keihi']['type']) {
                $plan_admin['keihi'] = $contract_info['keihi']['type'];
                echo "keihi {$admin['keihi']} -> {$contract_info['keihi']['type']} ...\n";
            }

            $is_changed = false;
            if (!empty($company_admin)) {
                $new_db->where('code', $code);
                $new_db->where('deleted_at', null);
                $new_db->update('companies', $company_admin);
                $is_changed = true;
            }
            if (!empty($agreement_admin)) {
                $new_db->where('company_id', $company_id);
                $new_db->where('deleted_at', null);
                $new_db->update('agreements', $agreement_admin);
                $is_changed = true;
            }
            if (!empty($plan_admin)) {
                $new_db->where('company_id', $company_id);
                $new_db->where('deleted_at', null);
                $new_db->update('plans', $plan_admin);
                $is_changed = true;
            }
            if ($is_changed) {
                $counter += 1;
            }
        }
        echo "done {$counter} rows.\n";
    }

    /**
     * Apply Contract
     *
     * Run everyday, at 00:30 Tokyo time zone
     */
    public function apply_contract()
    {
        while (true) {
            $start_time = strtotime(date('Y-m-d 15:30:00'));
            $end_time = strtotime(date('Y-m-d 15:50:00'));

            if (getenv('CI_ENV') !== 'production') {
                $start_time = strtotime(date('Y-m-d 01:30:00'));
                $end_time = strtotime(date('Y-m-d 01:50:00'));
                echo "running in testing environment...\n";
            }
            $at = date('Y-m-d H:i:s');
            echo "at: {$at}\n";

            if (isset($first_time) === false || ($start_time <= time() && time() <= $end_time)) {
                $first_time = true;

                echo "start apply contract...\n";
                $this->monolog->logger(Monolog::CRONJOB)->info('start apply contract...');

                $current_date = jp_current_date();

                // get all contracts will be applied
                $db = $this->load->database('default', true);
                $apply_contracts = $db->select('companies.*, agreements.type, agreements.start_date,
                    agreements.end_date, agreements.stop_date, agreements.num_account, plans.jinjer_db, plans.kintai, 
                    plans.roumu, plans.jinji, plans.my_number, plans.work_vital, plans.agreement_id')
                    ->from('companies')
                    ->join('agreements', 'agreements.company_id = companies.id AND agreements.deleted_at IS NULL')
                    ->join('plans', 'plans.agreement_id = agreements.id AND plans.deleted_at IS NULL')
                    ->where('agreements.start_date', $current_date)
                    ->where('companies.status', 0)
                    ->where('agreements.stop_date IS NULL')
                    ->where('companies.deleted_at IS NULL')
                    ->get()->result_array();

                $total_contracts = count($apply_contracts);
                echo "total contracts: {$total_contracts}\n";
                $this->monolog->logger(Monolog::CRONJOB)->info("total contracts: {$total_contracts}");
                $affected_contracts = 0;
                if (!empty($apply_contracts)) {
                    foreach ($apply_contracts as $contract) {
                        $current_time = date('Y-m-d H:i:s');
                        echo "process contract: #{$contract['id']}, at {$current_time}\n";
                        $this->monolog->logger(Monolog::CRONJOB)->info("process contract: #{$contract['id']}, at {$current_time}");

                        $connected = true;
                        //connect to Core
                        if (core_connected()) {
                            echo "connected core...\n";
                            $this->monolog->logger(Monolog::CRONJOB)->info("connected core...");
                            $core_data = [
                                'status' => 1,
                                'company_id' => $contract['id'],
                            ];
                            $data = $this->company_client->core_update_company($core_data);
                            if ($data['status'] === false) {
                                $connected = false;
                                // save batch logs
                                $batch_log = [
                                    'name' => self::APPLY_CONTRACT,
                                    'content' => json_encode($core_data),
                                    'response' => json_encode($data['error']),
                                    'created_at' => date('Y-m-d H:i:s')
                                ];
                                $db->insert('batch_logs', $batch_log);
                            } else {
                                echo "update core --> success\n";
                                $this->monolog->logger(Monolog::CRONJOB)->info("update core --> success");
                            }
                        }
                        if ($connected === false) {
                            //handle next contract because can not connect to Core
                            echo "connect core --> failed\n";
                            $this->monolog->logger(Monolog::CRONJOB)->info("connect core --> failed");
                            continue;
                        }

                        // save batch logs
                        $batch_log = [
                            'name' => self::APPLY_CONTRACT,
                            'content' => json_encode($contract),
                            'response' => 'success',
                            'created_at' => date('Y-m-d H:i:s')
                        ];
                        $db->insert('batch_logs', $batch_log);

                        $db->trans_begin();

                        // update company status (status = 1 --> running; status = 0 --> stopping)
                        $db->where('id', $contract['id']);
                        $db->update('companies', ['status' => 1]);

                        if ($db->trans_status() === false) {
                            $db->trans_rollback();
                            echo "#{$contract['id']} --> failed\n";
                            $this->monolog->logger(Monolog::CRONJOB)->info("#{$contract['id']} --> failed");
                        } else {
                            echo "#{$contract['id']} --> success\n";
                            $this->monolog->logger(Monolog::CRONJOB)->info("#{$contract['id']} --> success");
                            $affected_contracts += 1;
                            $db->trans_commit();
                        }
                    }

                    // sleep 20 minutes after processed all contracts
                    // after process get up, it will get out IF clause,
                    // so the next process will handle next day
                    echo "sleep 20 minutes...\n";
                    $this->monolog->logger(Monolog::CRONJOB)->info("sleep 20 minutes...");
                    sleep(20 * 60);
                }
                echo "done apply contract ---> {$affected_contracts}\n";
                echo "----------------------------------------------\n";
                $this->monolog->logger(Monolog::CRONJOB)->info("done apply contract ---> {$affected_contracts}");
                $db->close(); // close database connection to release memory
            }
            // sleep 5 minutes for a cycle
            // this way will help create new connection and never appear error Mysql: Has Gone Away
            echo "sleep 5 minutes...\n";
            $this->monolog->logger(Monolog::CRONJOB)->info("sleep 5 minutes...");
            sleep(5 * 60);
        }
    }

    /**
     * Expiration Contract
     *
     * Run everyday, at 00:30 Tokyo time zone
     */
    public function expiration_contract()
    {
        while (true) {
            $start_time = strtotime(date('Y-m-d 15:30:00'));
            $end_time = strtotime(date('Y-m-d 15:50:00'));

            if (getenv('CI_ENV') !== 'production') {
                $start_time = strtotime(date('Y-m-d 01:30:00'));
                $end_time = strtotime(date('Y-m-d 01:50:00'));
                echo "running in testing environment...\n";
            }
            $at = date('Y-m-d H:i:s');
            echo "at: {$at}\n";

            if (isset($first_time) === false || ($start_time <= time() && time() <= $end_time)) {
                $first_time = true;

                echo "start expiration contract...\n";
                $this->monolog->logger(Monolog::CRONJOB)->info('start expiration contract...');

                $current_date = jp_current_date();

                // get all contracts will be applied
                $db = $this->load->database('default', true);
                $apply_contracts = $db->select('companies.*, agreements.type, agreements.start_date,
                    agreements.end_date, agreements.stop_date, agreements.num_account, plans.jinjer_db, plans.kintai, 
                    plans.roumu, plans.jinji, plans.my_number, plans.work_vital, plans.agreement_id')
                    ->from('companies')
                    ->join('agreements', 'agreements.company_id = companies.id AND agreements.deleted_at IS NULL')
                    ->join('plans', 'plans.agreement_id = agreements.id AND plans.deleted_at IS NULL')
                    ->where('agreements.end_date <', $current_date)
                    ->where('companies.status', 1)
                    ->where('agreements.stop_date IS NULL')
                    ->where('companies.deleted_at IS NULL')
                    ->get()->result_array();

                $total_contracts = count($apply_contracts);
                echo "total contracts: {$total_contracts}\n";
                $this->monolog->logger(Monolog::CRONJOB)->info("total contracts: {$total_contracts}");
                $affected_contracts = 0;
                if (!empty($apply_contracts)) {
                    foreach ($apply_contracts as $contract) {
                        $current_time = date('Y-m-d H:i:s');
                        echo "process contract: #{$contract['id']}, at {$current_time}\n";
                        $this->monolog->logger(Monolog::CRONJOB)->info("process contract: #{$contract['id']}, at {$current_time}");

                        $connected = true;
                        //connect to Core
                        if (core_connected()) {
                            echo "connected core...\n";
                            $this->monolog->logger(Monolog::CRONJOB)->info("connected core...");
                            $core_data = [
                                'status' => 0,
                                'company_id' => $contract['id'],
                            ];
                            $data = $this->company_client->core_update_company($core_data);
                            if ($data['status'] === false) {
                                $connected = false;
                                // save batch logs
                                $batch_log = [
                                    'name' => self::EXPIRATION_CONTRACT,
                                    'content' => json_encode($core_data),
                                    'response' => json_encode($data['error']),
                                    'created_at' => date('Y-m-d H:i:s')
                                ];
                                $db->insert('batch_logs', $batch_log);
                            } else {
                                echo "update core --> success\n";
                                $this->monolog->logger(Monolog::CRONJOB)->info("update core --> success");
                            }
                        }
                        if ($connected === false) {
                            //handle next contract because can not connect to Core
                            echo "connect core --> failed\n";
                            $this->monolog->logger(Monolog::CRONJOB)->info("connect core --> failed");
                            continue;
                        }

                        // save batch logs
                        $batch_log = [
                            'name' => self::EXPIRATION_CONTRACT,
                            'content' => json_encode($contract),
                            'response' => 'success',
                            'created_at' => date('Y-m-d H:i:s')
                        ];
                        $db->insert('batch_logs', $batch_log);

                        $db->trans_begin();
                        // update company status (status = 1 --> running; status = 0 --> stopping)
                        $db->where('id', $contract['id']);
                        $db->update('companies', ['status' => 0]);

                        //update agreements (stop_date = now())
                        $db->where('id', $contract['agreement_id']);
                        $db->update('agreements', ['stop_date' => date('Y-m-d H:i:s')]);

                        if ($db->trans_status() === false) {
                            $db->trans_rollback();
                            echo "#{$contract['id']} --> failed\n";
                            $this->monolog->logger(Monolog::CRONJOB)->info("#{$contract['id']} --> failed");
                        } else {
                            echo "#{$contract['id']} --> success\n";
                            $this->monolog->logger(Monolog::CRONJOB)->info("#{$contract['id']} --> success");
                            $affected_contracts += 1;
                            $db->trans_commit();
                        }
                    }

                    // sleep 20 minutes after processed all contracts
                    // after process get up, it will get out IF clause,
                    // so the next process will handle next day
                    echo "sleep 20 minutes...\n";
                    $this->monolog->logger(Monolog::CRONJOB)->info("sleep 20 minutes...");
                    sleep(20 * 60);
                }
                echo "done expiration contract ---> {$affected_contracts}\n";
                echo "----------------------------------------------\n";
                $this->monolog->logger(Monolog::CRONJOB)->info("done expiration contract ---> {$affected_contracts}");
                $db->close(); // close database connection to release memory
            }
            // sleep 5 minutes for a cycle
            // this way will help create new connection and never appear error Mysql: Has Gone Away
            echo "sleep 5 minutes...\n";
            $this->monolog->logger(Monolog::CRONJOB)->info("sleep 5 minutes...");
            sleep(5 * 60);
        }
    }

     /**
     * Expiration Information
     *
     * Run everyday, at 00:30 Tokyo time zone
     */
    public function expiration_information()
    {
        while (true) {
            $start_time = strtotime(date('Y-m-d 15:30:00'));
            $end_time = strtotime(date('Y-m-d 15:50:00'));

            if (isset($first_time) === false || ($start_time <= time() && time() <= $end_time)) {
                $first_time = true;

                echo "start expiration information...\n";
                $this->monolog->logger(Monolog::CRONJOB)->info('start expiration information...');

                $current_date = jp_current_date();

                // get all informations will be applied
                $db = $this->load->database('default', true);
                $apply_informations = $db->select('*')
                    ->from('informations')
                    ->where('informations.expiration_date <', $current_date)
                    ->where('informations.is_open', 1)
                    ->where('informations.deleted_at IS NULL')
                    ->get()->result_array();

                $total_informations = count($apply_informations);
                echo "total informations: {$total_informations}\n";
                $this->monolog->logger(Monolog::CRONJOB)->info("total informations: {$total_informations}");
                $affected_informations = 0;
                if (!empty($apply_informations)) {
                    foreach ($apply_informations as $information) {
                        $current_time = date('Y-m-d H:i:s');
                        echo "process information: #{$information['id']}, at {$current_time}\n";
                        $this->monolog->logger(Monolog::CRONJOB)->info("process information: #{$information['id']}, at {$current_time}");

                        $connected = true;
                        //connect to Core
                        if (core_connected()) {
                            echo "connected core...\n";
                            $this->monolog->logger(Monolog::CRONJOB)->info("connected core...");
                            $core_data = [
                                'is_open' => 0,
                                'information_id' => $information['id']
                            ];
                            $this->load->library('clients/information_client');
                            $data = $this->information_client->core_off_alert($core_data);
                            if ($data['status'] === false) {
                                $connected = false;
                                // save batch logs
                                $batch_log = [
                                    'name' => self::EXPIRATION_INFORMATION,
                                    'content' => json_encode($core_data),
                                    'response' => json_encode($data['error']),
                                    'created_at' => date('Y-m-d H:i:s')
                                ];
                                $db->insert('batch_logs', $batch_log);
                            } else {
                                echo "update core --> success\n";
                                $this->monolog->logger(Monolog::CRONJOB)->info("update core --> success");
                            }
                        }
                        if ($connected === false) {
                            //handle next information because can not connect to Core
                            echo "connect core --> failed\n";
                            $this->monolog->logger(Monolog::CRONJOB)->info("connect core --> failed");
                            continue;
                        }

                        // save batch logs
                        $batch_log = [
                            'name' => self::EXPIRATION_INFORMATION,
                            'content' => json_encode($information),
                            'response' => 'success',
                            'created_at' => date('Y-m-d H:i:s')
                        ];
                        $db->insert('batch_logs', $batch_log);

                        $db->trans_begin();
                        // update information status (is_open = 1 --> on; is_open = 0 --> off)
                        $db->where('id', $information['id']);
                        $db->update('informations', ['is_open' => 0]);

                        if ($db->trans_status() === false) {
                            $db->trans_rollback();
                            echo "#{$information['id']} --> failed\n";
                            $this->monolog->logger(Monolog::CRONJOB)->info("#{$information['id']} --> failed");
                        } else {
                            echo "#{$information['id']} --> success\n";
                            $this->monolog->logger(Monolog::CRONJOB)->info("#{$information['id']} --> success");
                            $affected_informations += 1;
                            $db->trans_commit();
                        }
                    }

                    // sleep 20 minutes after processed all informations
                    // after process get up, it will get out IF clause,
                    // so the next process will handle next day
                    echo "sleep 20 minutes...\n";
                    $this->monolog->logger(Monolog::CRONJOB)->info("sleep 20 minutes...");
                    sleep(20 * 60);
                }
                echo "done expiration information ---> {$affected_informations}\n";
                echo "----------------------------------------------\n";
                $this->monolog->logger(Monolog::CRONJOB)->info("done expiration information ---> {$affected_informations}");
                $db->close(); // close database connection to release memory
            }
            // sleep 5 minutes for a cycle
            // this way will help create new connection and never appear error Mysql: Has Gone Away
            echo "sleep 5 minutes...\n";
            $this->monolog->logger(Monolog::CRONJOB)->info("sleep 5 minutes...");
            sleep(5 * 60);
        }
    }
}
