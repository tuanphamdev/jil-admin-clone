<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Information extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();
        parent::__construct();
        $this->load->library('clients/information_client');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'information' => READ_PERMISSION,
            ],
            'do_on' => [
                'information' => FULL_PERMISSION,
            ],
            'do_off' => [
                'information' => FULL_PERMISSION,
            ],
        );
    }

    public function index($page = 1)
    {
        $this->load->library('pagination');
        $params = $this->input->get();
        $query_string = null;
        if (isset($params['item_page'])) {
            $query_string['per_page'] = $params['item_page'];
        }

        $query_string['curr_page'] = $page;

        if (isset($params['title'])) {
            $query_string['title'] = $params['title'];
        }

        if (isset($params['created_at_from'])) {
            $query_string['created_at_from'] = $params['created_at_from'];
        }
        if (isset($params['created_at_to'])) {
            $query_string['created_at_to'] = $params['created_at_to'];
        }

        if (isset($params['is_open'])) {
            $query_string['is_open'] = $params['is_open'];
        }

        $data = $this->information_client->list($query_string);
        $data['content_title'] = $this->lang->line('information_list_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('information_list_title'), 'link' => '/informations']
        ];
        $data['input'] = $params;
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'informations/index', $data);
    }

    public function do_on()
    {
        $input = $this->input->post();
        $params = $input;
        $data = $this->information_client->on($params);

        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            redirect('informations');
        } else {
            $message = sprintf($this->lang->line('information_on_success'), $input['information_title'] ?? '');
            $this->show_alert_success($message);
            redirect('informations');
        }
    }

    public function do_off()
    {
        $input = $this->input->post();
        $params = $input;
        $data = $this->information_client->off($params);

        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            redirect('informations');
        } else {
            $message = sprintf($this->lang->line('information_off_success'), $input['information_title'] ?? '');
            $this->show_alert_success($message);
            redirect('informations');
        }
    }

    // public function do_create()
    // {
    //     $input = $this->input->post();
    //     $params = $input;
    //     $data = $this->ip_client->create($params);

    //     if ($data['status'] === false) {
    //         $this->show_alert_danger($data['error']);
    //         redirect('ips/create');
    //     } else {
    //         $message = $this->lang->line('information_create_success');
    //         $this->show_alert_success($message);
    //         redirect('ips');
    //     }
    // }

    // public function do_update()
    // {
    //     $input = $this->input->post();
    //     $params = $input;
    //     $params['ip_address'] = $this->get_ip_number_pattern($input['ip_address']);
    //     $data = $this->ip_client->update($params);
    //     if ($data['status'] === false) {
    //         $input_fill_back = $input;
    //         $this->set_flashdata('ip_update', $input_fill_back);
    //         $this->show_alert_danger($data['error']);
    //         redirect('ips/update/'.$input['id']);
    //     } else {
    //         $message = $this->lang->line('ip_update_success');
    //         $this->show_alert_success($message);
    //         redirect('ips');
    //     }
    // }
}
