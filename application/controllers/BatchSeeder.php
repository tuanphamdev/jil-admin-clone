<?php
class BatchSeeder extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function seed_permission_tokyo_marine()
    {
        $roles = $this->db->select('id')->from('roles')->get()->result_array();
        foreach ($roles as $data) {
            $insert_data = array(
                'item' => 'tokyo_marine',
                'permission' => '2',
                'role_id' => $data['id'],
                'created_at' => date('Y-m-d'),
            );
            $this->db->insert('role_permissions', $insert_data);
        }
    }

    public function seed_permission_import_bank()
    {
        $roles = $this->db->select('id')->from('roles')->get()->result_array();
        foreach ($roles as $data) {
            $insert_data = array(
                'item' => 'import_bank',
                'permission' => '2',
                'role_id' => $data['id'],
                'created_at' => date('Y-m-d'),
            );
            $this->db->insert('role_permissions', $insert_data);
        }
    }

    public function seed_permission_employee()
    {
        $roles = $this->db->select('id')->from('roles')->get()->result_array();
        foreach ($roles as $data) {
            $insert_data = array(
                'item' => 'employee',
                'permission' => '2',
                'role_id' => $data['id'],
                'created_at' => date('Y-m-d'),
            );
            $this->db->insert('role_permissions', $insert_data);
        }
    }

    public function seed_permission_ts_password()
    {
        $roles = $this->db->select('id')->from('roles')->get()->result_array();
        foreach ($roles as $data) {
            $insert_data = array(
                array(
                    'item' => 'ts_password_change',
                    'permission' => '2',
                    'role_id' => $data['id'],
                    'created_at' => date('Y-m-d H:i:s'),
                ),
                array(
                    'item' => 'ts_password_ips',
                    'permission' => '2',
                    'role_id' => $data['id'],
                    'created_at' => date('Y-m-d H:i:s'),
                ),
                array(
                    'item' => 'ts_password_logs',
                    'permission' => '2',
                    'role_id' => $data['id'],
                    'created_at' => date('Y-m-d H:i:s'),
                )
            );
            $this->db->insert_batch('role_permissions', $insert_data);
        }
    }

    public function seed_permission_qa_engine()
    {
        $roles = $this->db->select('id')->from('roles')->get()->result_array();
        foreach ($roles as $data) {
            $insert_data = array(
                array(
                    'item' => 'qa_engine',
                    'permission' => '1',
                    'role_id' => $data['id'],
                    'created_at' => date('Y-m-d H:i:s'),
                ),
            );
            $this->db->insert_batch('role_permissions', $insert_data);
        }
    }

    public function seed_permission_survey_monkey()
    {
        $roles = $this->db->select('id')->from('roles')->get()->result_array();
        $insert_data = [];
        foreach ($roles as $data) {
            $insert_data[] = [
                'item' => 'survey_monkey',
                'permission' => '1',
                'role_id' => $data['id'],
                'created_at' => date('Y-m-d H:i:s'),
            ];
        }
        $this->db->insert_batch('role_permissions', $insert_data);
        echo 'Number affect: ' . $this->db->affected_rows() . PHP_EOL;
    }

    public function seed_enable_service_ultra_and_ocr()
    {
        $companies_services_info = $this->db->select('companies.id, plans.id plan_id , companies.is_using_ai_ocr, companies.is_using_ultra, ')
                                        ->from('companies')
                                        ->join('plans', 'plans.company_id = companies.id', 'left')
                                        ->get()
                                        ->result_array();
        $data_update = [];
        foreach ($companies_services_info as $company_info) {
            $data_update[] = [
              'id' => $company_info['plan_id'],
              'ai_ocr' => $company_info['is_using_ai_ocr'],
              'ultra' => $company_info['is_using_ultra'],
            ];
        }
        $total_updated = 0;
        $data_update_chunk = array_chunk($data_update, 100);
        foreach ($data_update_chunk as $item_chunk) {
            $number_updated = $this->db->update_batch('plans', $item_chunk, 'id');
            $total_updated = $total_updated + $number_updated;
        }
        echo 'Number updated: ' . $total_updated . PHP_EOL;
    }

    public function add_date_default_to_service_enable()
    {
        $plans = $this->db->select('id, kintai, roumu, jinji, keihi, my_number, work_vital, workflow, employee_contract, year_end_tax_adjustment, signing, salary, ai_ocr, ultra')
            ->from('plans')
            ->get()
            ->result_array();

        $total_update = 0;
        $tokyoTimezone = new DateTimeZone('Asia/Tokyo');
        $myDateTime = new DateTime('now', $tokyoTimezone);
        $date_default = $myDateTime->format('Y-m-d 00:00:00');
        foreach (array_chunk($plans, 100) as $plan_chunk) {
            $data_update = [];
            foreach ($plan_chunk as $plan) {
                $data_update[] = [
                    'id' => $plan['id'],
                    'date_enable_kintai' => $plan['kintai'] ? $date_default : null,
                    'date_enable_roumu' => $plan['roumu'] ? $date_default : null,
                    'date_enable_jinji' => $plan['jinji'] ? $date_default : null,
                    'date_enable_keihi' => $plan['keihi'] ? $date_default : null,
                    'date_enable_ai_ocr' => $plan['ai_ocr'] ? $date_default : null,
                    'date_enable_my_number' => $plan['my_number'] ? $date_default : null,
                    'date_enable_work_vital' => $plan['work_vital'] ? $date_default : null,
                    'date_enable_salary' => $plan['salary'] ? $date_default : null,
                    'date_enable_workflow' => $plan['workflow'] ? $date_default : null,
                    'date_enable_employee_contract' => $plan['employee_contract'] ? $date_default : null,
                    'date_enable_signing' => $plan['signing'] ? $date_default : null,
                    'date_enable_year_end_tax_adjustment' => $plan['year_end_tax_adjustment'] ? $date_default : null,
                    'date_enable_ultra' => $plan['ultra'] ? $date_default : null,
                ];
            }
            $number_updated = $this->db->update_batch('plans', $data_update, 'id');
            $total_update = $total_update + $number_updated;
        }

        echo "Total records updated: {$total_update}" . PHP_EOL;
    }

    public function seed_enable_setting_kintai_keihi_salary()
    {
        $companies_services_info = $this->db->select('companies.id, plans.id plan_id , plans.kintai, plans.keihi, plans.salary')
            ->from('companies')
            ->join('plans', 'plans.company_id = companies.id', 'left')
            ->where('plans.kintai', 1)
            ->or_where('plans.salary', 1)
            ->get()
            ->result_array();
        $companies_update = $plan_update = [];
        foreach ($companies_services_info as $company_info) {
            if ($company_info['kintai']) {
                $companies_update[] = [
                    'id' => $company_info['id'],
                    'is_paid_management' => 1,
                    'is_shift_management' => 1,
                    'is_overtime_management' => 1,
                ];
                $plan_update[] = [
                    'id' => $company_info['plan_id'],
                    'paid_management' => 1,
                    'shift_management' => 1,
                    'overtime_management' => 1,
                ];
            }

            if ($company_info['salary']) {
                $companies_update[] = [
                    'id' => $company_info['id'],
                    'is_payslip' => 1,
                ];
                $plan_update[] = [
                    'id' => $company_info['plan_id'],
                    'payslip' => 1,
                ];
            }
        }
        $total_company_updated = 0;
        $data_update_chunk = array_chunk($companies_update, 100);
        foreach ($data_update_chunk as $item_chunk) {
            $number_updated = $this->db->update_batch('companies', $item_chunk, 'id');
            $total_company_updated = $total_company_updated + $number_updated;
        }
        echo 'Number Company updated: ' . $total_company_updated . PHP_EOL;

        $total_plan_updated = 0;
        $data_update_chunk = array_chunk($plan_update, 100);
        foreach ($data_update_chunk as $item_chunk) {
            $number_updated = $this->db->update_batch('plans', $item_chunk, 'id');
            $total_plan_updated = $total_plan_updated + $number_updated;
        }
        echo 'Number Plan updated: ' . $total_plan_updated . PHP_EOL;
    }

    public function update_jinjer_db_with_only_signing()
    {
        $plans = $this->db->select('id')
            ->from('plans')
            ->where('kintai', 0)
            ->where('roumu', 0)
            ->where('jinji', 0)
            ->where('keihi', 0)
            ->where('my_number', 0)
            ->where('work_vital', 0)
            ->where('workflow', 0)
            ->where('employee_contract', 0)
            ->where('year_end_tax_adjustment', 0)
            ->where('salary', 0)
            ->where('bi', 0)
            ->where('signing', 1)
            ->where('jinjer_db', 1)
            ->where('deleted_at', null)
            ->get()
            ->result_array();
        echo "========== START UPDATE =============".PHP_EOL;
        if (!empty($plans)) {
            $sql = "UPDATE plans set jinjer_db = 0 where id IN (". implode(',', array_column($plans, 'id')).")";
            $this->db->query($sql);
            echo $this->db->affected_rows() . PHP_EOL;
        }
        echo "========== FINISHED UPDATE =============".PHP_EOL;
    }

    public function move_data_signing_from_plans_to_plans_signing()
    {
        echo "========== START COPY DATA =============".PHP_EOL;
        $new_columns = "company_id, plan_id, signing_folder, signing_folder_range, signing_internal_workflow, signing_batch_import, date_enable_signing_folder, date_enable_signing_folder_range, date_enable_signing_internal_workflow, date_enable_signing_batch_import, created_at, updated_at, deleted_at";
        $old_columns = "company_id, id, signing_folder, signing_folder_range, signing_internal_workflow, signing_batch_import, date_enable_signing_folder, date_enable_signing_folder_range, date_enable_signing_internal_workflow, date_enable_signing_batch_import, created_at, updated_at, deleted_at";
        $sql = "INSERT INTO plans_signing ({$new_columns}) SELECT {$old_columns} FROM plans;";
        $this->db->query($sql);

        echo $this->db->affected_rows() . PHP_EOL;
        echo "========== END COPY DATA =============".PHP_EOL;
    }

    public function remove_singing_columns_from_plans_table()
    {
        echo "============= List fields before remove ===========" .PHP_EOL;
        $fields = $this->db->list_fields('plans');
        var_dump($fields);
        $columns_remove = [
            'signing_folder',
            'signing_folder_range',
            'signing_internal_workflow',
            'signing_batch_import',
            'date_enable_signing_folder',
            'date_enable_signing_folder_range',
            'date_enable_signing_internal_workflow',
            'date_enable_signing_batch_import'
        ];
        try {
            $this->load->dbforge();
            foreach ($columns_remove as $column) {
                if (in_array($column, $fields)) {
                    $this->dbforge->drop_column('plans', $column);
                }
            }
        } catch (\Exception $e) {
            printf($e->getTraceAsString());
        }
        echo "============= List fields after remove ===========" .PHP_EOL;
        $fields = $this->db->list_fields('plans');
        var_dump($fields);
    }

    public function update_electronic_signature_long_term_signing_on()
    {
        $plans_signing_ids = $this->db->select('plans_signing.id')
            ->from('plans')
            ->join('plans_signing', 'plans.id = plans_signing.plan_id')
            ->where('plans.signing', 1)
            ->where('plans_signing.electronic_signature_long_term', 0)
            ->where('plans_signing.electronic_signature_timestamp', 0)
            ->where('plans_signing.electronic_signature_authentication', 0)
            ->where('plans.deleted_at', null)
            ->where('plans_signing.deleted_at', null)
            ->get()
            ->result_array();
        echo "========== START UPDATE =============" . PHP_EOL;
        if (!empty($plans_signing_ids)) {
            $sql = "UPDATE plans_signing set electronic_signature_long_term = 1 where id IN (". implode(',', array_column($plans_signing_ids, 'id')).")";
            $this->db->query($sql);
            echo 'Number affected rows: ' . $this->db->affected_rows() . PHP_EOL;
        }
        echo "========== FINISHED UPDATE =============" . PHP_EOL;
    }
}
