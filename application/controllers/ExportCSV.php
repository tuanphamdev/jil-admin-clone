<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ExportCSV extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();
        parent::__construct();
        $this->load->library('clients/export_csv_client');
        $this->load->model('Company_model');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'csv' => READ_PERMISSION,
            ],
            'get_staff_use_kintai' => [
                'csv' => FULL_PERMISSION,
            ],
            'retire_staff' => [
                'csv' => FULL_PERMISSION,
            ],
            'tokyo_marine_staff' => [
                'csv' => FULL_PERMISSION,
            ],
        );
    }

    public function index($page = 1)
    {
        $this->load->library('pagination');

        $params = $this->input->get();
        $query_string = null;
        if (isset($params['item_page'])) {
            $query_string['per_page'] = $params['item_page'];
        }

        $query_string['curr_page'] = $page;

        if (isset($params['type'])) {
            $query_string['type'] = $params['type'];
        }

        if (isset($params['code'])) {
            $query_string['code'] = $params['code'];
        }
        if (isset($params['name'])) {
            $query_string['name'] = $params['name'];
        }

        if (isset($params['registered_date_from'])) {
            $query_string['registered_date_from'] = $params['registered_date_from'];
        }
        if (isset($params['registered_date_to'])) {
            $query_string['registered_date_to'] = $params['registered_date_to'];
        }

        if (isset($params['code_from'])) {
            $query_string['code_from'] = $params['code_from'];
        }
        if (isset($params['code_to'])) {
            $query_string['code_to'] = $params['code_to'];
        }

        $data = $this->export_csv_client->list($query_string);

        $data['content_title'] = $this->lang->line('export_csv_list_title');
        $data['breadcrumb'] = [
                    ['text' => $this->lang->line('export_csv_list_title'), 'link' => 'export_csv']
            ];

        $data['input'] = $params;
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'export_csv/index', $data);
    }

    public function retire_staff()
    {
        ini_set_csv();
        $params = $this->input->get();
        $start_date_retire = str_replace('-', '', $this->input->get('start_date_retire'));
        $end_date_retire = str_replace('-', '', $this->input->get('end_date_retire'));

        $data = $this->export_csv_client->retire_staff($params);
        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            redirect('export_csv');
        }
        $title = [
            $this->lang->line('csv_header_retire_staff_company_code'),
            $this->lang->line('csv_header_retire_staff_employee_code'),
            $this->lang->line('csv_header_retire_staff_retire_day'),
        ];
        $dataCSV = [];
        if (!empty($data['data'])) {
            foreach ($data['data'] as $item) {
                $dataCSV[] = [
                    $item['company_code'],
                    $item['employee_code'],
                    $item['retirement_date'],
                ];
            }
        }
        $dataCSV = array_merge([$title], $dataCSV);
        $fileName = $this->lang->line('export_csv_get_retire_staff') . " ({$start_date_retire}〜{$end_date_retire})".'.csv';
        array_to_csv($dataCSV, $fileName);
        exit();
    }

    /**
     * Get Tokyo Marine Staffs
     */
    public function tokyo_marine_staff()
    {
        $email_login = $this->session->userdata('user_login')['email']??'';
        $api_token = $this->token->get_token(['admin_token' => getenv('API_KEY')], getenv('JWT_CORE_SECRET'));
        $reponse = $this->export_csv_client->get_tokyo_marine_staff_download(['token' => $api_token, 'email' => $email_login]);
        if (isset($reponse['status']) && $reponse['status'] === true) {
            $this->show_alert_success($reponse['message']??'CSV出力が正常に生成されました。');
        } else {
            $this->show_alert_danger($reponse['message']??'CSV出力の作成が失敗しました。');
        }
        redirect('export_csv');
    }

    /**
     * Get CSV company from Jinjer core
     */
    public function company_download()
    {
        $email_login = $this->session->userdata('user_login')['email']??'';
        $api_token = $this->token->get_token(['admin_token' => getenv('API_KEY')], getenv('JWT_CORE_SECRET'));
        $reponse = $this->export_csv_client->get_company_download(['token' => $api_token, 'email' => $email_login]);
        if (isset($reponse['status']) && $reponse['status'] === true) {
            $this->show_alert_success($reponse['message']??'CSV出力が正常に生成されました。');
        } else {
            $this->show_alert_danger($reponse['message']??'CSV出力の作成が失敗しました。');
        }
        redirect('export_csv');
    }

    /**
     * Get CSV count_timecards
     */
    public function count_timecards()
    {
        ini_set_csv();
        $fileName = '打刻実績_' .$this->input->get('start_time').'_'.$this->input->get('end_time').'.csv';
        $params = $this->input->get();
        $data_csv = $this->export_csv_client->get_count_timecards($params);
        if (isset($data_csv['status']) && $data_csv['status'] === false) {
            redirect('export_csv');
        }
        $this->load->helper('download');
        force_download($fileName, $data_csv);
    }

    /**
     * Get CSV count_tightens
     */
    public function count_tightens()
    {
        ini_set_csv();
        $fileName = '締め実績_' .$this->input->get('start_time').'_'.$this->input->get('end_time').'.csv';
        $params = $this->input->get();
        $data_csv = $this->export_csv_client->get_count_tightens($params);
        if (isset($data_csv['status']) && $data_csv['status'] === false) {
            redirect('export_csv');
        }
        $this->load->helper('download');
        force_download($fileName, $data_csv);
    }

    /**
     * Get CSV company info from Jinjer core
     */
    public function company_info()
    {
        $email_login = $this->session->userdata('user_login')['email']??'';
        $api_token = $this->token->get_token(['admin_token' => getenv('API_KEY')], getenv('JWT_CORE_SECRET'));
        $reponse = $this->export_csv_client->get_company_info_download(['token' => $api_token, 'email' => $email_login]);
        if (isset($reponse['status']) && $reponse['status'] === true) {
            $this->show_alert_success($reponse['message']??'CSV出力が正常に生成されました。');
        } else {
            $this->show_alert_danger($reponse['message']??'CSV出力の作成が失敗しました。');
        }
        redirect('export_csv');
    }
    
    public function csv_health_keihi()
    {
        $email_login = $this->session->userdata('user_login')['email']??'';
        $api_token = $this->token->get_token(['admin_token' => getenv('API_KEY')], getenv('JWT_CORE_SECRET'));
        $params = [
            'email' => $email_login,
            'start_date' => $this->input->get('start_date'),
            'end_date' => $this->input->get('end_date'),
            'token' => $api_token,
            'time_zone' => $_COOKIE['jinjer_local_timezone']??9,
        ];
        $reponse = $this->export_csv_client->get_health_keihi_download($params);
        if (isset($reponse['status']) && $reponse['status'] === true) {
            $this->show_alert_success('CSV出力に成功しました。出力処理が完了したらメールにCSVファイルを添付し送信します。');
        } else {
            $this->show_alert_danger($reponse['error'][0]??'CSV出力の作成が失敗しました。');
        }
        redirect('export_csv');
    }


    public function csv_health_salary()
    {
        $email_login = $this->session->userdata('user_login')['email']??'';
        $api_token = $this->token->get_token(['admin_token' => getenv('API_KEY')], getenv('JWT_CORE_SECRET'));
        $params = [
            'email' => $email_login,
            'start_date' => $this->input->get('start_date'),
            'end_date' => $this->input->get('end_date'),
            'token' => $api_token,
            'time_zone' => $_COOKIE['jinjer_local_timezone']??9,
            'request_time' => date('Y-m-d H:i:s'),
        ];
        $response = $this->export_csv_client->get_health_salary_download($params);
        if (isset($response['status']) && $response['status'] === true) {
            $this->show_alert_success('CSV出力に成功しました。出力処理が完了したらメールにCSVファイルを添付し送信します。');
        } else {
            $this->show_alert_danger($response['error'][0]??'CSV出力の作成が失敗しました。');
        }
        redirect('export_csv');
    }

    public function csv_health_signing()
    {
        $email_login = $this->session->userdata('user_login')['email']??'';
        $api_token = $this->token->get_token(['admin_token' => getenv('API_KEY')], getenv('JWT_CORE_SECRET'));
        $params = [
            'email' => $email_login,
            'start_date' => $this->input->get('start_date'),
            'end_date' => $this->input->get('end_date'),
            'token' => $api_token,
            'time_zone' => $_COOKIE['jinjer_local_timezone']??9,
        ];
        $reponse = $this->export_csv_client->get_health_signing_download($params);
        if (isset($reponse['status']) && $reponse['status'] === true) {
            $this->show_alert_success('CSV出力に成功しました。出力処理が完了したらメールにCSVファイルを添付し送信します。');
        } else {
            $this->show_alert_danger($reponse['error'][0]??'CSV出力の作成が失敗しました。');
        }
        redirect('export_csv');
    }
}
