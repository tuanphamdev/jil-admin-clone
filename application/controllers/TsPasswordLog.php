<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TsPasswordLog extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();

        parent::__construct();
        $this->load->library('clients/ts_password_log_client');
        $this->load->library('clients/user_client');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'ts_password_logs' => READ_PERMISSION,
            ],
        );
    }

    public function index($page = 1)
    {
        $this->load->library('pagination');

        $params = $this->input->get();
        $query_string = null;
        if (isset($params['item_page'])) {
            $query_string['per_page'] = $params['item_page'];
        }

        $query_string['curr_page'] = $page;

        if (isset($params['company_code'])) {
            $query_string['company_code'] = $params['company_code'];
        }
        if (isset($params['employee_code'])) {
            $query_string['employee_code'] = $params['employee_code'];
        }
        if (isset($params['user_id'])) {
            $query_string['user_id'] = $params['user_id'];
        }
        if (isset($params['employee_name'])) {
            $query_string['employee_name'] = $params['employee_name'];
        }
        if (isset($params['action_type'])) {
            $query_string['action_type'] = $params['action_type'];
        }
        if (isset($params['created_date_start'])) {
            $query_string['created_date_start'] = $params['created_date_start'];
        }
        if (isset($params['created_date_end'])) {
            $query_string['created_date_end'] = $params['created_date_end'];
        }
        $data = $this->ts_password_log_client->list($query_string);
        if ($this->uri->segment(2) == 'search') {
            $page = 'ts_password_logs';
            if ($data['status'] === false) {
                $this->show_alert_danger($data['error']);
                redirect($page.'?'.http_build_query($query_string));
            }
        }

        $data['content_title'] = '操作ログ一覧';
        $data['breadcrumb'] = [
            ['text' => '操作ログ一覧', 'link' => 'ts_password_logs']
        ];
        $data['input'] = $params;
        $data['permissions'] = $this->get_permissions();
        $data['users'] = $this->user_client->list_all()['data']['user']??[];
        $this->template->load('default', 'ts_password_logs/index', $data);
    }
}
