<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SurveyMonkey extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();
        parent::__construct();
        $this->load->library('clients/survey_monkey_client');
        $this->load->driver('cache', ['adapter' => 'redis', 'backup' => 'file']);
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'survey_monkey' => READ_PERMISSION,
            ],
            'create' => [
                'survey_monkey' => READ_PERMISSION,
            ]
        );
    }

    public function index()
    {
        $data = $this->survey_monkey_client->last_survey_monkey();
        $data['content_title'] = $this->lang->line('menu_survey_monkey');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('menu_survey_monkey'), 'link' => '/survey_monkey']
        ];
        if (!empty($this->get_flashdata('survey_monkey_create'))) {
            $data['data'] = $this->get_flashdata('survey_monkey_create');
        }
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'survey_monkey/index', $data);
    }

    public function do_create()
    {
        $input = $this->input->post();
        $input['creator_id'] = $this->user_info->user_id;
        $data = $this->survey_monkey_client->save($input);
        if ($data['status'] === false) {
            $input['services'] = $input['services'] ?? [];
            $this->set_flashdata('survey_monkey_create', $input);
            $this->show_alert_danger($data['error']);
            redirect('survey_monkey');
        } else {
            if ($this->cache->get(CACHE_ADMIN_SURVEY_MONKEY)) {
                $this->cache->delete(CACHE_ADMIN_SURVEY_MONKEY);
            }
            $this->cache->save(CACHE_ADMIN_SURVEY_MONKEY, $input, CACHE_TIME_TO_LIVE);
            $message = $this->lang->line('survey_monkey_create_success');
            $this->show_alert_success($message);
            redirect('survey_monkey');
        }
    }
}
