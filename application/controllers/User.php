<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();

        parent::__construct();
        $this->load->library('clients/user_client');
        $this->load->library('clients/role_client');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'user' => READ_PERMISSION,
            ],
            'create' => [
                'user' => READ_PERMISSION,
            ],
            'do_create' => [
                'user' => FULL_PERMISSION,
            ],
            'update' => [
                'user' => READ_PERMISSION,
            ],
            'do_update' => [
                'user' => FULL_PERMISSION,
            ],
        );
    }

    public function index($page = 1)
    {
        $this->load->library('pagination');
        
        $params = $this->input->get();
        $query_string = null;
        if (isset($params['item_page'])) {
            $query_string['per_page'] = $params['item_page'];
        }

        $query_string['curr_page'] = $page;

        if (isset($params['name'])) {
            $query_string['name'] = $params['name'];
        }
        if (isset($params['status'])) {
            $query_string['status'] = $params['status'];
        }

        $data = $this->user_client->list($query_string);

        $data['content_title'] = $this->lang->line('user_list_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('user_list_title'), 'link' => '/users']
        ];
        $data['input'] = $params;
        $data['permissions'] = $this->get_permissions();

        $this->template->load('default', 'users/index', $data);
    }
    
    public function create()
    {
        $data['content_title'] = $this->lang->line('user_create_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('user_list_title'), 'link' => '/users'],
            ['text' => $this->lang->line('user_create_title'), 'link' => '/users/create']
        ];
        $roles = $this->role_client->list(null);
        if (!empty($roles) && $roles['status'] == true) {
            $data['roles'] = $roles['data']['role'];
        }
        $data['input']  = $this->get_flashdata('user_create') ?? [];
        $data['permissions'] = $this->get_permissions();

        $this->template->load('default', 'users/create', $data);
    }
    
    public function do_create()
    {
        $input = $this->input->post();
        $data = $this->user_client->create($input);
        if ($data['status'] === false) {
            $input_fill_back = $input;
            $this->set_flashdata('user_create', $input_fill_back);
            $this->show_alert_danger($data['error']);
            redirect('users/create');
        } else {
            $is_send_mail = $input['is_send_mail'] ?? '';
            //Send mail to customer
            if ($is_send_mail == 1) {
                $data_send_mail = [
                    'url' => getenv('BASE_URL'),
                ];
                $message = $this->load->view('template_mail/create_update_user', ['data' => $data_send_mail], true);
                $this->send_email($input['email'], $this->lang->line('user_email_title'), $message);
            }

            $message = $this->lang->line('user_create_success');
            $this->show_alert_success($message);
            redirect('users');
        }
    }
    
    public function update($user_id)
    {
        $data['content_title'] = $this->lang->line('user_update_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('user_list_title'), 'link' => '/users'],
            ['text' => $this->lang->line('user_update_title'), 'link' => '/users/update/'.$user_id]
        ];
        $data['input']  = $this->get_flashdata('user_update') ?? [];
        if (empty($data['input'])) {
            $user = $this->user_client->detail(['id' => $user_id]);
            if (empty($user)) {
                $this->show_alert_danger($this->lang->line('user_no_result_found'));
                redirect('users');
            }
            if ($user['status'] === false) {
                $this->show_alert_danger($user['error']);
                redirect('users');
            }
            $data['input'] = $user['data']['user'];
        }
        $roles = $this->role_client->list();
        if ($roles['status'] === true) {
            $data['roles'] = $roles['data']['role'];
        }
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'users/update', $data);
    }

    public function do_update()
    {
        $input = $this->input->post();
        $data = $this->user_client->update($input);
        if (isset($data['status']) && $data['status'] === false) {
            $alert_type = self::ALERT_DANGER;
            $message = $data['error'];
            $input_fill_back = $input;
        }
        $is_send_mail = $input['is_send_mail'] ?? '';
        if (isset($data['status']) &&  $data['status'] === true) {
            //Send mail to customer
            if ($is_send_mail == 1) {
                $data_send_mail = [
                    'url' => getenv('BASE_URL'),
                ];
                $message = $this->load->view('template_mail/create_update_user', ['data' => $data_send_mail], true);
                $this->send_email($input['email'], $this->lang->line('user_email_title'), $message);
            }
            $alert_type = self::ALERT_SUCCESS;
            $message = $this->lang->line('user_update_success');
            ;
            $this->show_alert($alert_type, $message);
            redirect('users');
        }
        if (isset($alert_type) && isset($message)) {
            $this->show_alert($alert_type, $message);
            if (isset($input_fill_back)) {
                $this->set_flashdata('user_update', $input_fill_back);
            }
            redirect('users/update/'.$input['id']);
        }
    }

    public function logout()
    {
        $this->session->unset_userdata(array('api_token', 'is_logged'));
        redirect('login');
    }
}
