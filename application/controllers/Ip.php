<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ip extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();
        parent::__construct();
        $this->load->library('clients/ip_client');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'ip' => READ_PERMISSION,
            ],
            'create' => [
                'ip' => READ_PERMISSION,
            ],
            'do_create' => [
                'ip' => FULL_PERMISSION,
            ],
            'update' => [
                'ip' => READ_PERMISSION,
            ],
            'do_update' => [
                'ip' => FULL_PERMISSION,
            ],
        );
    }

    public function index()
    {
        $params = $this->input->get();
        $query_string = null;
        if (isset($params['item_page'])) {
            $query_string['per_page'] = $params['item_page'];
        }

        if (isset($params['curr_page'])) {
            $query_string['curr_page'] = $params['curr_page'];
        }

        $data = $this->ip_client->list($query_string);
        $data['content_title'] = $this->lang->line('ip_list_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('ip_list_title'), 'link' => '/ips']
        ];
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'ips/index', $data);
    }

    public function create()
    {
        $data['content_title'] = $this->lang->line('ip_create_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('ip_list_title'), 'link' => '/ips'],
            ['text' => $this->lang->line('ip_create_title'), 'link' => '/ips/create']
        ];
        $data['input']  = $this->get_flashdata('ip_create') ?? [];
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'ips/create', $data);
    }

    public function do_create()
    {
        $input = $this->input->post();
        $params['ip_address'] = $this->get_ip_number_pattern($input['ip_address']);
        $data = $this->ip_client->create($params);

        if ($data['status'] === false) {
            $input_fill_back = $input;
            $this->set_flashdata('ip_create', $input_fill_back);
            $this->show_alert_danger($data['error']);
            redirect('ips/create');
        } else {
            $message = $this->lang->line('ip_create_success');
            $this->show_alert_success($message);
            redirect('ips');
        }
    }

    public function update($ip_id)
    {
        $data['content_title'] = $this->lang->line('ip_update_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('ip_list_title'), 'link' => '/ips'],
            ['text' => $this->lang->line('ip_update_title'), 'link' => '/ips/update/'.$ip_id]
        ];

        $data['input']  = $this->get_flashdata('ip_update') ?? [];
        if (empty($data['input'])) {
            $ip = $this->ip_client->detail(['id' => $ip_id]);
            if (empty($ip)) {
                $message = $this->lang->line('ip_no_result_found');
                $this->show_alert_danger($message);
                redirect('ips');
            }
            if ($ip['status'] === false) {
                $this->show_alert_danger($ip['error']);
                redirect('ips');
            }
            $data['input'] = $ip['data'];
            $data['input']['ip_address'] = $this->ip_number_pattern_to_array($ip['data']['ip_address']);
        }

        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'ips/update', $data);
    }

    public function do_update()
    {
        $input = $this->input->post();
        $params = $input;
        $params['ip_address'] = $this->get_ip_number_pattern($input['ip_address']);
        $data = $this->ip_client->update($params);
        if ($data['status'] === false) {
            $input_fill_back = $input;
            $this->set_flashdata('ip_update', $input_fill_back);
            $this->show_alert_danger($data['error']);
            redirect('ips/update/'.$input['id']);
        } else {
            $message = $this->lang->line('ip_update_success');
            $this->show_alert_success($message);
            redirect('ips');
        }
    }
    
    public function delete()
    {
        $input = $this->input->post();
        $params['id'] = $input['ip_id'] ?? '';
        $data = $this->ip_client->delete($params);

        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            redirect('ips');
        } else {
            $message = $this->lang->line('ip_delete_success');
            $this->show_alert_success($message);
            redirect('ips');
        }
    }
}
