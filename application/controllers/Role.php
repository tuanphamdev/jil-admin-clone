<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Role extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();
        parent::__construct();
        $this->load->library('clients/role_client');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'role' => READ_PERMISSION,
            ],
            'create' => [
                'role' => READ_PERMISSION,
            ],
            'do_create' => [
                'role' => FULL_PERMISSION,
            ],
            'update' => [
                'role' => READ_PERMISSION,
            ],
            'do_update' => [
                'role' => FULL_PERMISSION,
            ],
        );
    }

    public function index()
    {
        $data['content_title'] = $this->lang->line('role_list_title');
        $data['breadcrumb'] = [
                    ['text' => $this->lang->line('role_list_title'), 'link' => '/roles']
            ];
        $data['role'] = $this->role_client->list(null);
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'roles/index', $data);
    }

    public function create()
    {
        $data['content_title'] = $this->lang->line('role_create_title');
        $data['breadcrumb'] = [
                ['text' => $this->lang->line('role_list_title'), 'link' => '/roles'],
                ['text' => $this->lang->line('role_create_title'), 'link' => '/roles/create']
            ];
        $data['input']  = $this->get_flashdata('role_create') ?? [];
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'roles/create', $data);
    }

    public function do_create()
    {
        $input = $this->input->post();
        $data = $this->role_client->create($input);
        if (isset($data['status']) && $data['status'] === false) {
            $alert_type = self::ALERT_DANGER;
            $message = $data['error'];
            $input_fill_back = $input;
        }
        if (isset($data['status']) &&  $data['status'] === true) {
            $alert_type = self::ALERT_SUCCESS;
            $message = $this->lang->line('role_create_success');
            $this->show_alert($alert_type, $message);
            redirect('roles');
        }
        if (isset($alert_type) && isset($message)) {
            $this->show_alert($alert_type, $message);
            if (isset($input_fill_back)) {
                $this->set_flashdata('role_create', $input_fill_back);
            }
            redirect('roles/create');
        }
    }

    public function update($role_id)
    {
        $data['content_title'] = $this->lang->line('role_update_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('role_list_title'), 'link' => '/roles'],
            ['text' => $this->lang->line('role_update_title'), 'link' => '/roles/update/'.$role_id]
        ];
        $data['input']  = $this->get_flashdata('role_update') ?? [];
        if (empty($data['input'])) {
            $detail = $this->role_client->detail(['id' => $role_id]);
            if (empty($detail)) {
                $this->show_alert_danger($this->lang->line('role_no_result_found'));
                redirect('roles');
            }

            if ($detail['status'] === false) {
                $this->show_alert_danger($detail['error']);
                redirect('roles');
            }

            $permissions = $detail['data']['permissions'];
            $data['input']['id'] = $detail['data']['id'];
            $data['input']['name'] = $detail['data']['name'];
            unset($detail['data']);
            if (!empty($permissions) && is_array($permissions)) {
                foreach ($permissions as $item) {
                    $data['input'][$item['item']] = $item['permission'];
                }
            }
        }

        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'roles/update', $data);
    }

    public function do_update()
    {
        $input = $this->input->post();
        $params = $input;
        $data = $this->role_client->update($params);
        if (isset($data['status']) && $data['status'] === false) {
            $alert_type = self::ALERT_DANGER;
            $message = $data['error'];
            $input_fill_back = $input;
        }
        if (isset($data['status']) &&  $data['status'] === true) {
            $alert_type = self::ALERT_SUCCESS;
            $message = $this->lang->line('role_update_success');
            $this->show_alert($alert_type, $message);
            redirect('roles');
        }
        if (isset($alert_type) && isset($message)) {
            $this->show_alert($alert_type, $message);
            if (isset($input_fill_back)) {
                $this->set_flashdata('role_update', $input_fill_back);
            }
            redirect('roles/update/'.$input['id'] ?? '');
        }
    }

    public function delete()
    {
        $input = $this->input->post();
        $params['id'] = $input['role_id'] ?? '';
        $data = $this->role_client->delete($params);

        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            redirect('roles');
        } else {
            $message = sprintf($this->lang->line('role_delete_success'), $input['role_name'] ?? 'N/A');
            $this->show_alert_success($message);
            redirect('roles');
        }
    }
}
