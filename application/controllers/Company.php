<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Jinjer\services\CompanyService;

class Company extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();

        parent::__construct();
        $this->load->library('clients/company_client');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'company' => READ_PERMISSION,
            ],
            'create' => [
                'company' => READ_PERMISSION,
            ],
            'do_create' => [
                'company' => FULL_PERMISSION,
            ],
            'update' => [
                'company' => READ_PERMISSION,
            ],
            'do_update' => [
                'company' => FULL_PERMISSION,
            ],
            'stop_service' => [
                'company' => FULL_PERMISSION,
            ],
            'start_service' => [
                'company' => FULL_PERMISSION,
            ],
            'resend_account_info' => [
                'company' => FULL_PERMISSION,
            ],
            'change_type_contract_batch' => [
                'company' => FULL_PERMISSION,
            ]
        );
    }

    public function index($page = 1)
    {
        $this->load->library('pagination');

        $params = $this->input->get();
        $query_string = null;
        if (isset($params['item_page'])) {
            $query_string['per_page'] = $params['item_page'];
        }

        $query_string['curr_page'] = $page;

        if (isset($params['code'])) {
            $query_string['code'] = $params['code'];
        }
        if (isset($params['name'])) {
            $query_string['name'] = $params['name'];
        }

        if (isset($params['type'])) {
            $query_string['type'] = $params['type'];
        }
        if (isset($params['plan'])) {
            $query_string['plan'] = $params['plan'];
        }

        if (isset($params['status'])) {
            $query_string['status'] = $params['status'];
        }
        if (isset($params['start_date_from'])) {
            $query_string['start_date_from'] = $params['start_date_from'];
        }
        if (isset($params['start_date_to'])) {
            $query_string['start_date_to'] = $params['start_date_to'];
        }
        if (isset($params['end_date_from'])) {
            $query_string['end_date_from'] = $params['end_date_from'];
        }
        if (isset($params['end_date_to'])) {
            $query_string['end_date_to'] = $params['end_date_to'];
        }

        $data = $this->company_client->list($query_string);
        if ($this->uri->segment(2) == 'search') {
            $page = 'companies';
            if ($data['status'] === false) {
                $this->show_alert_danger($data['error']);
                redirect($page.'?'.http_build_query($query_string));
            }
        }

        $data['content_title'] = $this->lang->line('company_list_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('company_list_title'), 'link' => 'companies']
        ];

        $data['input'] = $params;
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'companies/index', $data);
    }

    public function create()
    {
        $data['content_title'] = $this->lang->line('company_create_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('company_list_title'), 'link' => 'companies'],
            ['text' => $this->lang->line('company_create_title'), 'link' => 'companies/create']
        ];

        $data['input']  = $this->get_flashdata('company_create') ?? [];
        if (!empty($data['input']['enigma_code']) && $data['input']['enigma_version'] != 0) {
            $enigma_check = $this->check_enigma_company($data['input']['enigma_code'], $data['input']['enigma_version']);
            $data['input']['enigma_status'] = $enigma_check['enigma_status'];
            $data['input']['enigma_name'] = $enigma_check['enigma_name'];
        }

        $data['prefecture'] = $this->company_client->prefecture()['content'] ?? [];
        if (!empty($data['input']['prefecture']) && !empty($data['input']['city'])) {
            $params = ['iso_pref' => $data['input']['prefecture']];
            $data['city'] = $this->company_client->get_cities_by_pref_id($params)['content'] ?? [];
        }
        $data['permissions'] = $this->get_permissions();
        $data['jinjer_DB_status'] = 0;
        $this->template->load('default', 'companies/create', $data);
    }

    public function do_create()
    {
        $input = $this->input->post();
        $params = $input;
        $params['tel'] = $this->get_tel_number_pattern($input['tel']);
        $params['fax'] = $this->get_tel_number_pattern($input['fax']);
        $params['zip'] = $this->get_tel_number_pattern($input['zip']);
        $params['created_by_id'] = $this->user_info->user_id;
        $params['status'] = 0;
        if ($params['start_date'] <= jp_current_date()) {
            $params['status'] = 1;
        }
        $params['ultra'] = $input['is_using_ultra'] ?? 0;
        $params['ai_ocr'] = $input['is_using_ai_ocr'] ?? 0;
        $params['paid_management'] = $input['is_paid_management'] ?? 0;
        $params['shift_management'] = $input['is_shift_management'] ?? 0;
        $params['overtime_management'] = $input['is_overtime_management'] ?? 0;
        $params['ebook_store'] = $input['is_ebook_store'] ?? 0;
        $params['credit_card'] = $input['is_credit_card'] ?? 0;
        $params['payslip'] = $input['is_payslip'] ?? 0;
        $params['docomo'] = $input['is_docomo'] ?? 0;

        if (isset($params['docomo']) && $params['docomo'] == 0 && isset($params['d_account_id'])) {
            $params['d_account_id'] = '';
        }

        $params = CompanyService::setOptionSigning($params);
        $params = CompanyService::setOptionKintai($params);
        $params = CompanyService::setDateEnableServiceForCreate($params);

        $data = $this->company_client->create($params);
        if ($data['status'] === false) {
            $this->set_flashdata('company_create', $input);
            $this->show_alert_danger($data['error']);
            redirect('companies/create');
        } else {
            $this->show_alert_success($this->lang->line('company_create_success'));
            redirect('companies');
        }
    }

    public function update($id)
    {
        $data['content_title'] = $this->lang->line('company_update_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('company_list_title'), 'link' => 'companies'],
            ['text' => $this->lang->line('company_update_title'), 'link' => 'companies/update/' . $id]
        ];

        $data['input']  = $this->get_flashdata('company_update') ?? [];

        if (empty($data['input'])) {
            $detail = $this->company_client->detail(['id' => $id]);

            if (empty($detail)) {
                $this->show_alert_danger($this->lang->line('company_no_result_found'));
                redirect('companies');
            }

            if (isset($detail['status']) === true) {
                $data['input'] = $detail['data'];
                $data['input']['tel'] = $this->tel_number_pattern_to_array($detail['data']['tel']);
                $data['input']['fax'] = $this->tel_number_pattern_to_array($detail['data']['fax']);
                $data['input']['zip'] = $this->tel_number_pattern_to_array($detail['data']['zip']);
                $agreement = $detail['data']['agreement'];
                $agreement_id = $agreement['id'];
                unset($agreement['id']);
                $plan = $detail['data']['plan'];
                $plan_id = $plan['id'];
                unset($plan['id']);
                $plan_signing = $detail['data']['plan_signing'];
                $plan_signing_id = $plan_signing['id'];
                unset($plan_signing['id']);
                unset($data['input']['agreement']);
                unset($data['input']['plan']);
                $data['input']['plan_id'] = $plan_id;
                $data['input']['agreement_id'] = $agreement_id;
                $data['input']['plan_signing_id'] = $plan_signing_id;
                $data['input'] = array_merge($data['input'], $agreement, $plan, $plan_signing);
            }
        }

        if (!empty($data['input']['enigma_code']) && $data['input']['enigma_version'] != 0) {
            $enigma_check = $this->check_enigma_company($data['input']['enigma_code'], $data['input']['enigma_version']);
            $data['input']['enigma_status'] = $enigma_check['enigma_status'];
            $data['input']['enigma_name'] = $enigma_check['enigma_name'];
        }


        $data['prefecture'] = $this->company_client->prefecture()['content'] ?? [];
        if (!empty($data['input']['city']) && !empty($data['input']['prefecture'])) {
            $params = ['iso_pref' => $data['input']['prefecture']];
            $data['city'] = $this->company_client->get_cities_by_pref_id($params)['content'] ?? [];
        }
        $data['permissions'] = $this->get_permissions();
        $jinjerDBStatus = CompanyService::getValueJinjerDB($data['input']);
        $data['jinjer_DB_status'] = $jinjerDBStatus;
        $this->template->load('default', 'companies/update', $data);
    }

    public function do_update($id)
    {
        $input = $this->input->post();
        $params = $input;
        $params['tel'] = $this->get_tel_number_pattern($input['tel']);
        $params['fax'] = $this->get_tel_number_pattern($input['fax']);
        $params['zip'] = $this->get_tel_number_pattern($input['zip']);

        $params['ultra'] = $input['is_using_ultra'] ?? 0;
        $params['ai_ocr'] = $input['is_using_ai_ocr'] ?? 0;
        $params['paid_management'] = $input['is_paid_management'] ?? 0;
        $params['shift_management'] = $input['is_shift_management'] ?? 0;
        $params['overtime_management'] = $input['is_overtime_management'] ?? 0;
        $params['ebook_store'] = $input['is_ebook_store'] ?? 0;
        $params['credit_card'] = $input['is_credit_card'] ?? 0;
        $params['payslip'] = $input['is_payslip'] ?? 0;
        $params['docomo'] = $input['is_docomo'] ?? 0;

        if (isset($input['is_docomo']) && $input['is_docomo'] == 0) {
            $params['d_account_id'] = '';
        }

        // set date enable of service
        $services = CompanyService::$services;

        $old_data = $this->company_client->detail(['id' => $id]);
        $data_filter = array_merge($old_data['data']['plan'], $old_data['data']['plan_signing']);
        $old_plan = array_filter($data_filter, function ($value, $key) use ($services) {
            return in_array($key, $services) && $value == 1;
        }, ARRAY_FILTER_USE_BOTH);
        $new_data =  array_filter($params, function ($value, $key) use ($services) {
            return in_array($key, $services) && $value == 1;
        }, ARRAY_FILTER_USE_BOTH);
        $date_default = jp_current_date_from_utc('now');

        $data_update_enable = array_diff_assoc($new_data, $old_plan);
        $data_update_disable = array_diff_assoc($old_plan, $new_data);
        foreach ($data_update_enable as $key => $value) {
            $params['date_enable_' . $key] = $date_default;
        }
        foreach ($data_update_disable as $key => $value) {
            $params['date_enable_' . $key] = null;
        }

        $params['updated_by_id'] = $this->user_info->user_id;
        $params['status'] = 0;
        if ($params['start_date'] <= jp_current_date() && jp_current_date() <= $params['end_date']) {
            $params['status'] = 1;
            $params['stop_date'] = null;
        }
        $params = CompanyService::setOptionSigning($params);
        $params = CompanyService::setOptionKintai($params);

        $data = $this->company_client->update($params);
        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            $this->set_flashdata('company_update', $input);
            redirect('companies/update/'.$id);
        } else {
            $this->show_alert_success($this->lang->line('company_update_success'));
            redirect('companies');
        }
    }

    public function cities($pref_id)
    {
        $params = ['iso_pref' => $pref_id];
        $cities = $this->company_client->get_cities_by_pref_id($params);
        die(json_encode($cities['content'] ?? []));
    }

    public function stop_service()
    {
        $input = $this->input->post();
        $params = $input;
        $data = $this->company_client->stop_service($params);

        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            redirect('companies');
        } else {
            $message = sprintf($this->lang->line('company_stop_service_success'), $input['company_name'] ?? 'N/A');
            $this->show_alert_success($message);
            redirect('companies');
        }
    }

    public function start_service()
    {
        $input = $this->input->post();
        $params = $input;
        if ($params['start_date'] > jp_current_date()) {
            $params['start_date'] = jp_current_date();
        }
        $data = $this->company_client->start_service($params);

        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            redirect('companies');
        } else {
            $message = sprintf($this->lang->line('company_start_service_success'), $input['company_name'] ?? 'N/A');
            $this->show_alert_success($message);
            redirect('companies');
        }
    }
    public function ajax_check_enigma_company()
    {
        $company_code = $this->input->post()['company_code'];
        $version = $this->input->post()['version'];
        die(json_encode($this->check_enigma_company($company_code, $version)));
    }

    private function check_enigma_company($company_code, $version)
    {
        $this->load->library('clients/enigma_company_client');
        $enigma_check = $this->enigma_company_client->check(['enigma_code' => $company_code, 'enigma_version' => $version]);
        $data = [];
        if ($enigma_check['status'] === false) {
            $data['enigma_status'] = '0';
            $data['enigma_name'] = '';
        } else {
            $data['enigma_status'] = '1';
            $data['enigma_name'] = $enigma_check['content']['company_name'];
        }
        return $data;
    }

    public function resend_account_info()
    {
        $input = $this->input->post();
        $params = [
            'admin_company_id' => $input['company_id']
        ];
        $data = $this->company_client->resend_account_info($params);

        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            redirect('companies');
        } else {
            $message = sprintf($this->lang->line('company_resend_account_info_success'), $input['company_name'] ?? 'N/A');
            $this->show_alert_success($message);
            redirect('companies');
        }
    }

    public function change_type_contract_batch()
    {
        $input = $this->input->post();
        $filters = $this->input->get();
        $data = $this->company_client->core_change_type_contract_batch($input, $filters);
        if ($data['status']) {
            if (empty($data['data']['errors'])) {
                $this->show_alert_success($this->lang->line('company_modal_type_change_success'));
            } else {
                $this->show_alert_danger($data['data']['errors']);
            }
            header("Content-type: application/json; charset=utf-8");
            echo json_encode([
                'status' => true,
                'messages' => $this->lang->line('company_modal_type_change_success')
                             ]);
            die();
        } else {
            $this->show_alert_danger($data['error']);
            header("Content-type: application/json; charset=utf-8");
            echo json_encode([
                                 'status' => false,
                                 'messages' => $data['error']
                             ]);
            die();
        }
    }
}
