<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TSPasswordChange extends MY_Controller
{

    public function __construct()
    {
        $this->set_role_filter();

        parent::__construct();
        $this->load->library('clients/ts_password_change_client');
        $this->load->library('clients/company_client');
    }

    protected function set_role_filter()
    {
        $this->role_filter = array(
            'index' => [
                'ts_password_change' => READ_PERMISSION,
            ],
            'index_change' => [
                'ts_password_change' => READ_PERMISSION,
            ],
            'change_password' => [
                'ts_password_change' => FULL_PERMISSION,
            ]
        );
    }

    public function index($page = 1)
    {
        $this->load->library('pagination');

        $params = $this->input->get();
        $query_string = null;
        if (isset($params['item_page'])) {
            $query_string['per_page'] = $params['item_page'];
        }

        $query_string['curr_page'] = $page;

        if (isset($params['code'])) {
            $query_string['code'] = $params['code'];
        }
        if (isset($params['name'])) {
            $query_string['name'] = $params['name'];
        }
        if (isset($params['use_start'])) {
            $query_string['use_start'] = $params['use_start'];
        }
        if (isset($params['use_end'])) {
            $query_string['use_end'] = $params['use_end'];
        }
        $data = $this->ts_password_change_client->list_log($query_string);
        if ($this->uri->segment(2) == 'search') {
            $page = 'ts_password_change';
            if ($data['status'] === false) {
                $this->show_alert_danger($data['error']);
                redirect($page.'?'.http_build_query($query_string));
            }
        }

        $data['content_title'] = $this->lang->line('ts_password_log_list_title');
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('ts_password_log_list_title'), 'link' => 'ts_password_change']
        ];

        $data['input'] = $params;
        $data['permissions'] = $this->get_permissions();
        $this->template->load('default', 'ts_password_change/index_log', $data);
    }

    public function index_change($page = 1)
    {
        $this->load->library('pagination');
        $params = $this->input->get();
        $company_id = $this->input->get('company_id');
        if (empty($company_id) || !is_numeric($company_id)) {
            redirect('/ts_password_change');
        }
        $query_string = null;
        if (isset($params['item_page'])) {
            $query_string['per_page'] = $params['item_page'];
        }
        $query_string['curr_page'] = $page;
        if (isset($params['employee_code'])) {
            $query_string['employee_code'] = $params['employee_code'];
        }
        if (isset($params['employee_name'])) {
            $query_string['employee_name'] = $params['employee_name'];
        }
        if (isset($params['flag_setting'])) {
            $query_string['flag_setting'] = $params['flag_setting'];
        }
        $query_string['company_id'] = $company_id;
        $data = $this->ts_password_change_client->list($query_string);
        if ($this->uri->segment(2) == 'search') {
            $page = 'ts_password_change/change';
            if ($data['status'] === false) {
                $this->show_alert_danger($data['error']);
                redirect($page.'?'.http_build_query($query_string));
            }
        }
        $data['content_title'] = $this->ts_password_change_client->detail_company_core(['id' => $company_id])['data']['name']??'';
        $data['breadcrumb'] = [
            ['text' => $this->lang->line('ts_password_log_list_title'), 'link' => 'ts_password_change']
        ];
        $data['input'] = $params;
        $data['permissions'] = $this->get_permissions();
        $data['company_id'] = $company_id;
        $this->template->load('default', 'ts_password_change/index', $data);
    }


    public function change_password($company_id)
    {
        $input = $this->input->post();
        $params = $input;
        $params['ts_password'] = trim($params['ts_password']);
        $data = $this->ts_password_change_client->change_password($params);
        if ($data['status'] === false) {
            $this->show_alert_danger($data['error']);
            redirect('ts_password_change/change?company_id='.$company_id);
        } else {
            $message = sprintf($this->lang->line('ts_pw_change_change_role_success'), $input['employee_code'] ?? 'N/A');
            $this->show_alert_success($message);
            $page = "ts_password_change/change/search?company_id=".$company_id;
            $query['employee_code'] = $params['employee_code'] ?? '';
            $query['employee_name'] = $params['employee_name'] ?? '';
            $query['flag_setting'] = $params['flag_setting'] ?? '';
            redirect($page.'&'.http_build_query($query));
        }
    }

    /**
     * Get avatar
     * @param integer $face_photo_uploads_id
     * @param integer $core_company_id
     */
    public function get_avatar($face_photo_uploads_id = null, $core_company_id = null)
    {
        $src = '';
        if (!empty($face_photo_uploads_id) && !empty($core_company_id)) {
            $response = $this->ts_password_change_client->get_file($face_photo_uploads_id, $core_company_id);
            if (!empty($response['content']['base64_file'])) {
                $src = "data:image/png;base64, " . $response['content']['base64_file'];
            }
        }
        die($src);
    }
}
