<?php

class TestCase extends CIPHPUnitTestCase
{
    protected $CI;
    public function __construct()
    {
        parent::__construct();
        $this->CI = &get_instance();
    }

    protected function createStartDate(){
        return date( "Y-m-d", strtotime( date("Y-m-d")) );
    }

    protected function createEndDate(){
        return date( "Y-m-d", strtotime( date("Y-m-d") . " +100 day" ) );
    }

    /**
     * This method is called before each test.
     */
    protected function setUp(){


        // set default session
        $user_login = ['user_id' => 1, 'email' => 'admin@gmail.com', 'password' => '12345678', 'user_name' => 'master'];
        $api_token = $this->CI->token->get_token($user_login);
        $this->CI->session->set_userdata([
            'user_login' => $user_login,
            'api_token' => $api_token,
            'is_logged' => true,
            'roles' => [
                'company' => 3,
                'csv' => 3,
                'user' => 3,
                'ip' => 3,
                'csv' => 3,
                'role' => 3
            ]]);
    }

}
