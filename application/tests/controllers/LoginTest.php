<?php

class LoginTest extends TestCase
{

    /**
     * This method is called before each test.
     */
    protected function setUp(){
        if($this->CI->session->has_userdata('is_logged')){
            $this->CI->session->unset_userdata('is_logged');
        }
    }

    /**
     * This method is called after each test.
     */
    protected function tearDown()
    {
    }

    /**
     * This method is called before the first test of this test class is run.
     */
    public static function setUpBeforeClass()
    {

    }

    /**
     * This method is called after the last test of this test class is run.
     */
    public static function tearDownAfterClass()
    {

    }

    public function testUserWillAbleToLoginWithAValidEmailAndAValidPassword(){
        $params = [
            'email' => 'tester@jinjer.biz',
            'password' => 'neo12345'
        ];
        $response = $this->request('POST', ['user_api', 'auth_post'], $params);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);
    }

   public function testUserCanNotLoginWithAValidEmailAndAnInvalidPassword(){
        $params = [
           'email' => 'master@jinjer.biz',
           'password' => 'neo12345!'
        ];
        $response = $this->request('POST', ['user_api', 'auth_post'], $params);
        $inspected = json_decode($response, true);
        $this->assertContains($this->CI->lang->line('auth_login_failed'), $inspected['error']);
        $this->assertNotTrue($inspected['status']);
   }

    public function testAlertErrorWhenSubmitLoginWithBlankEmailAndPassword(){
        $response = $this->request('POST', ['user_api', 'auth_post'], []);
        $inspected = json_decode($response, true);
        $this->assertContains($this->CI->lang->line('auth_login_email') , $inspected['error'][0]);
        $this->assertNotTrue($inspected['status']);
    }

    public function testSendRequestForgotPasswordWithAnInvalidEmail(){
        $params = [
            'email' => 'abc@jinjer.biz',
            'reset_password_token' => 'reset_password_token_failed'
        ];
        $response = $this->request('POST', ['user_api', 'forgot_password_post'], $params);
        $inspected = json_decode($response, true);
        $this->assertContains($this->CI->lang->line('user_forgot_failed'), $inspected['error']);
        $this->assertNotTrue($inspected['status']);
    }

    public function testSendRequestForgotPasswordWithValidEmail(){
        $params = [
          'email' => 'tester@jinjer.biz'
        ];
        $response = $this->request('POST', ['user_api', 'forgot_password_post'], $params);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);
    }

}
