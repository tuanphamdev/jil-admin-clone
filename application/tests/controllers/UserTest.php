<?php

class UserTest extends TestCase
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * This method is called before each test.
     */
    protected function setUp(){
        parent::setUp();
    }

    /**
     * This method is called after each test.
     */
    protected function tearDown()
    {
    }

    /**
     * This method is called before the first test of this test class is run.
     */
    public static function setUpBeforeClass()
    {
        $CI = &get_instance();
        $CI->db->query("SET FOREIGN_KEY_CHECKS = 0");
        $CI->db->truncate('users');
        $CI->db->query("SET FOREIGN_KEY_CHECKS = 1");
    }

    /**
     * This method is called after the last test of this test class is run.
     */
    public static function tearDownAfterClass()
    {
    }


    public function testCreateAnUserWithValidAllField(){
        $params = [
            "name" => "test1",
            "name_furigana" => "フリガナ",
            "email" => "vu.np1@neo-lab.vn",
            "password" => "12345678",
            "role_id" => "1",
            "status" => "1",
            "is_send_mail" => "1",
        ];
        $response = $this->request('POST', ['user_api', 'create_post'], $params);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);

    }

    public function testUpdateAnUserWithValidAllField(){
        $data = [
            "name" => "test1",
            "name_furigana" => "フリガナ",
            "email" => "vu.np11@neo-lab.vn",
            "password" => "12345678",
            "role_id" => "1",
            "status" => "1",
            "is_send_mail" => "1",
        ];
        $this->CI->db->insert('users', $data);
        $id = $this->CI->db->insert_id();
        $param = [
            'id' => $id,
            "name" => "test11",
            "name_furigana" => "フリガナフリガナ",
            "email" => "vu.np2@neo-lab.vn",
            "role_id" => "1",
            "status" => "2",
            "is_send_mail" => "2",
        ];
        $response = $this->request('POST', ['user_api', 'update_post'], $param);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);

    }

    public function testDeleteAnUserWithValidAllField(){
        $param = [
            "name" => "test2",
            "name_furigana" => "フリガナ",
            "email" => "vu.np3@neo-lab.vn",
            "password" => "12345678",
            "role_id" => "1",
            "status" => "1",
            "is_send_mail" => "1",
        ];
        $this->CI->db->insert('users', $param);
        $id = $this->CI->db->insert_id();
        $response = $this->request('POST', ['user_api', 'delete_post'], ['id' => $id]);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);

    }

    public function testGetDetailAnUserWithValidFieldId(){
        $data = [
            "name" => "test4",
            "name_furigana" => "フリガナ",
            "email" => "vu.np4@neo-lab.vn",
            "password" => "12345678",
            "role_id" => "1",
            "status" => "1",
            "is_send_mail" => "1",
        ];
        $this->CI->db->insert('users', $data);
        $id = $this->CI->db->insert_id();
        $param = [
            'id' => $id,
        ];
        $inspected = $this->request('GET', ['user_api', 'detail_get'], $param);
        $actual = 'vu.np4@neo-lab.vn';
        $this->assertContains($actual, $inspected);

    }

    public function testGetListIpAddressSuccess(){
        $this->CI->db->insert_batch('users',
            [
                [
                    "name" => "test5",
                    "name_furigana" => "フリガナ",
                    "email" => "vu.np5@neo-lab.vn",
                    "password" => "12345678",
                    "role_id" => "1",
                    "status" => "1",
                    "is_send_mail" => "1",
                ],
                [
                    "name" => "test6",
                    "name_furigana" => "フリガナ",
                    "email" => "vu.np6@neo-lab.vn",
                    "password" => "12345678",
                    "role_id" => "1",
                    "status" => "1",
                    "is_send_mail" => "1",
                ]
            ]
        );
        $inspected = $this->request('GET', ['user_api', 'list_get']);
        $this->assertContains('vu.np5@neo-lab.vn', $inspected);
        $this->assertContains('vu.np6@neo-lab.vn', $inspected);
    }
}
