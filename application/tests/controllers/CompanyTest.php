<?php

class CompanyTest extends TestCase
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * This method is called before each test.
     */
    protected function setUp(){
        parent::setUp();
    }

    /**
     * This method is called after each test.
     */
    protected function tearDown()
    {
    }

    /**
     * This method is called before the first test of this test class is run.
     */
    public static function setUpBeforeClass()
    {
        $CI = &get_instance();
        $CI->db->query("SET FOREIGN_KEY_CHECKS = 0");
        $CI->db->truncate('companies');
        $CI->db->truncate('agreements');
        $CI->db->truncate('plans');
        $CI->db->query("SET FOREIGN_KEY_CHECKS = 1");
    }

    /**
     * This method is called after the last test of this test class is run.
     */
    public static function tearDownAfterClass()
    {
    }


    public function testCreateAnCompanyWithValidAllField(){
        $param = [
            "code" => "1001",
            "name" => "test01",
            "name_furigana" => "フリガナ",
            "representative_name" => "フリガナ",
            "representative_name_furigana" => "フリガナ",
            "representative_title" => "フリガナ",
            "tel" => "05-5687-5687",
            "fax" => "05-5687-5678",
            "zip" => "567-5687",
            "prefecture" => "5",
            "city" => "305",
            "address" => "フリガナ",
            "address_furigana" => "フリガナ",
            "application_surname" => "フリガナ",
            "application_name" => "フリガナ",
            "application_furigana_surname" => "フリガナ",
            "application_furigana" => "フリガナ",
            "application_email" => "vu1@neo-lab.vn",
            "registered_date" => "2019-01-18",
            "type" => "2",
            "start_date" => $this->createStartDate(),
            "end_date" => $this->createEndDate(),
            "num_account" => "1000",
            "jinjer_db" => "1",
            "kintai" => "1",
            "roumu" => "1",
            "jinji" => "1",
            "keihi" => "1",
            "my_number" => "1",
            "work_vital" => "1",
            "salary" => 1,
            "workflow" => "1",
            "employee_contract" => "1",
            "year_end_tax_adjustment" => "1",
            "created_by_id" => "1",
            "status" => 1,
            "is_tokyo_marine" => 1,
            "enigma_code" => '123abc',
            "enigma_version" => 1,
            "is_using_ai" => 0,
            "bi" => 0,
            "is_using_ai_ocr" => 0
        ];
        $response = $this->request('POST', ['company_api', 'create_post'], $param);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);
    }

    public function testUpdateAnCompanyWithValidAllField(){
        $param_company = [
            "code" => "10325",
            "name" => "test01",
            "name_furigana" => "フリガナ",
            "representative_name" => "フリガナ",
            "representative_name_furigana" => "フリガナ",
            "representative_title" => "フリガナ",
            "tel" => "05-5687-5687",
            "fax" => "05-5687-5678",
            "zip" => "567-5687",
            "prefecture" => "5",
            "city" => "305",
            "address" => "フリガナ",
            "address_furigana" => "フリガナ",
            "application_surname" => "フリガナ",
            "application_name" => "フリガナ",
            "application_furigana_surname" => "フリガナ",
            "application_furigana" => "フリガナ",
            "application_email" => "vu32@neo-lab.vn",
            "registered_date" => "2019-01-18",
            "type" => "2",
            "start_date" => $this->createStartDate(),
            "end_date" => $this->createEndDate(),
            "num_account" => "1000",
            "jinjer_db" => "1",
            "kintai" => "1",
            "roumu" => "1",
            "jinji" => "1",
            "keihi" => "1",
            "my_number" => "1",
            "work_vital" => "1",
            "salary" => 1,
            "workflow" => "1",
            "employee_contract" => "1",
            "year_end_tax_adjustment" => "1",
            "created_by_id" => "1",
            "status" => 1,
            "is_tokyo_marine" => 1,
            "enigma_code" => '123abc',
            "enigma_version" => 0,
            "is_using_ai" => 0,
            "bi" => 0,
            "is_using_ai_ocr" => 0
        ];

        $create_response = $this->request('POST', ['company_api', 'create_post'], $param_company);
        $data = json_decode($create_response, true);
        $id = $data['data'];
        $param_update = [
            "id" => $id,
            "company_id" => $id,
            "code" => "1035",
            "name" => "test01",
            "name_furigana" => "フリガナ",
            "representative_name" => "フリガナ",
            "representative_name_furigana" => "フリガナ",
            "representative_title" => "フリガナ",
            "tel" => "05-5687-5687",
            "fax" => "05-5687-5678",
            "zip" => "567-5687",
            "prefecture" => "5",
            "city" => "305",
            "address" => "フリガナ",
            "address_furigana" => "フリガナ",
            "application_surname" => "フリガナ",
            "application_name" => "フリガナ",
            "application_furigana_surname" => "フリガナ",
            "application_furigana" => "フリガナ",
            "application_email" => "vu16@neo-lab.vn",
            "registered_date" => "2019-01-18",
            "type" => "1",
            "start_date" => $this->createStartDate(),
            "end_date" => $this->createEndDate(),
            "num_account" => "1000",
            "jinjer_db" => "1",
            "kintai" => "1",
            "roumu" => "1",
            "jinji" => "1",
            "keihi" => "1",
            "my_number" => "1",
            "work_vital" => "1",
            "salary" => 1,
            "workflow" => "1",
            "employee_contract" => "1",
            "year_end_tax_adjustment" => "1",
            "updated_by_id" => "1",
            "status" => 1,
            "agreement_id" => "2",
            "plan_id" => "2",
            "is_tokyo_marine" => 1,
            "enigma_code" => null,
            "enigma_version" => 0,
            "is_using_ai" => 0,
            "bi" => 0,
            "is_using_ai_ocr" => 0
        ];
        $response = $this->request('POST', ['company_api', 'update_post'], $param_update);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);
    }

    public function testGetDetailAnCompanyWithValidFieldId(){
        $param = [
            "code" => "21238",
            "name" => "test01",
            "name_furigana" => "フリガナ",
            "representative_name" => "フリガナ",
            "representative_name_furigana" => "フリガナ",
            "representative_title" => "フリガナ",
            "tel" => "05-5687-5687",
            "fax" => "05-5687-5678",
            "zip" => "567-5687",
            "prefecture" => "5",
            "city" => "305",
            "address" => "フリガナ",
            "address_furigana" => "フリガナ",
            "application_surname" => "フリガナ",
            "application_name" => "フリガナ",
            "application_furigana_surname" => "フリガナ",
            "application_furigana" => "フリガナ",
            "application_email" => "vu2@neo-lab.vn",
            "registered_date" => "2019-01-18",
            "type" => "2",
            "start_date" => $this->createStartDate(),
            "end_date" => $this->createEndDate(),
            "num_account" => "1000",
            "jinjer_db" => "1",
            "kintai" => "1",
            "roumu" => "1",
            "jinji" => "1",
            "keihi" => "1",
            "my_number" => "1",
            "work_vital" => "1",
            "salary" => 1,
            "workflow" => "1",
            "employee_contract" => "1",
            "year_end_tax_adjustment" => "1",
            "created_by_id" => "1",
            "status" => 1,
            "is_tokyo_marine" => 1,
            "enigma_code" => null,
            "enigma_version" => 0,
            "is_using_ai" => 0,
            "bi" => 0,
            "is_using_ai_ocr" => 0
        ];
        $create_response = $this->request('POST', ['company_api', 'create_post'], $param);
        $data = json_decode($create_response, true);
        $id = $data['data'];
        $response = $this->request('GET', ['company_api', 'detail_get'], ['id' => $id]);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);

    }


    public function testGetListCompanyWithValidAllField(){
        $param = [
            "code" => "212232",
            "name" => "testlistget",
            "name_furigana" => "フリガナ",
            "representative_name" => "フリガナ",
            "representative_name_furigana" => "フリガナ",
            "representative_title" => "フリガナ",
            "tel" => "05-5687-5687",
            "fax" => "05-5687-5678",
            "zip" => "567-5687",
            "prefecture" => "5",
            "city" => "305",
            "address" => "フリガナ",
            "address_furigana" => "フリガナ",
            "application_surname" => "フリガナ",
            "application_name" => "フリガナ",
            "application_furigana_surname" => "フリガナ",
            "application_furigana" => "フリガナ",
            "application_email" => "vu2@neo-lab.vn",
            "registered_date" => "2019-01-18",
            "type" => "2",
            "start_date" => $this->createStartDate(),
            "end_date" => $this->createEndDate(),
            "num_account" => "1000",
            "jinjer_db" => "1",
            "kintai" => "1",
            "roumu" => "1",
            "jinji" => "1",
            "keihi" => "1",
            "my_number" => "1",
            "work_vital" => "1",
            "salary" => 1,
            "workflow" => "1",
            "employee_contract" => "1",
            "year_end_tax_adjustment" => "1",
            "created_by_id" => "1",
            "status" => 1,
            "is_tokyo_marine" => 1,
            "enigma_code" => null,
            "enigma_version" => 0,
            "is_using_ai" => 0,
            "bi" => 0,
            "is_using_ai_ocr" => 0
        ];
        $this->request('POST', ['company_api', 'create_post'], $param);
        $inspected = $this->request('GET', ['company_api', 'list_get']);
        $actual = 'testlistget';
        $this->assertContains($actual, $inspected);
    }

    public function testStopServiceAnCompanyWithValidAllField(){
        $param = [
            "code" => "2132232",
            "name" => "teststopservice",
            "name_furigana" => "フリガナ",
            "representative_name" => "フリガナ",
            "representative_name_furigana" => "フリガナ",
            "representative_title" => "フリガナ",
            "tel" => "05-5687-5687",
            "fax" => "05-5687-5678",
            "zip" => "567-5687",
            "prefecture" => "5",
            "city" => "305",
            "address" => "フリガナ",
            "address_furigana" => "フリガナ",
            "application_surname" => "フリガナ",
            "application_name" => "フリガナ",
            "application_furigana_surname" => "フリガナ",
            "application_furigana" => "フリガナ",
            "application_email" => "vu2@neo-lab.vn",
            "registered_date" => "2019-01-18",
            "type" => "2",
            "start_date" => $this->createStartDate(),
            "end_date" => $this->createEndDate(),
            "num_account" => "1000",
            "jinjer_db" => "1",
            "kintai" => "1",
            "roumu" => "1",
            "jinji" => "1",
            "keihi" => "1",
            "my_number" => "1",
            "work_vital" => "1",
            "salary" => 1,
            "workflow" => "1",
            "employee_contract" => "1",
            "year_end_tax_adjustment" => "1",
            "created_by_id" => "1",
            "status" => 1,
            "is_tokyo_marine" => 1,
            "enigma_code" => null,
            "enigma_version" => 0,
            "is_using_ai" => 0,
            "bi" => 0,
            "is_using_ai_ocr" => 0
        ];
        $create_response = $this->request('POST', ['company_api', 'create_post'], $param);
        $data = json_decode($create_response, true);
        $id = $data['data'];
        $stop_param = [
            "company_id" => $id,
            "company_name" => "teststopservice"
        ];

        $response = $this->request('POST', ['company_api', 'stop_service_post'], $stop_param);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);
    }

    public function testStartServiceAnCompanyWithValidAllField(){
        $param = [
            "code" => "2134232",
            "name" => "teststartservice",
            "name_furigana" => "フリガナ",
            "representative_name" => "フリガナ",
            "representative_name_furigana" => "フリガナ",
            "representative_title" => "フリガナ",
            "tel" => "05-5687-5687",
            "fax" => "05-5687-5678",
            "zip" => "567-5687",
            "prefecture" => "5",
            "city" => "305",
            "address" => "フリガナ",
            "address_furigana" => "フリガナ",
            "application_surname" => "フリガナ",
            "application_name" => "フリガナ",
            "application_furigana_surname" => "フリガナ",
            "application_furigana" => "フリガナ",
            "application_email" => "vu2@neo-lab.vn",
            "registered_date" => "2019-01-18",
            "type" => "2",
            "start_date" => $this->createStartDate(),
            "end_date" => $this->createEndDate(),
            "num_account" => "1000",
            "jinjer_db" => "1",
            "kintai" => "1",
            "roumu" => "1",
            "jinji" => "1",
            "keihi" => "1",
            "my_number" => "1",
            "work_vital" => "1",
            "salary" => 1,
            "workflow" => "1",
            "employee_contract" => "1",
            "year_end_tax_adjustment" => "1",
            "created_by_id" => "1",
            "status" => 1,
            "is_tokyo_marine" => 1,
            "enigma_code" => null,
            "enigma_version" => 0,
            "is_using_ai" => 0,
            "bi" => 0,
            "is_using_ai_ocr" => 0
        ];
        $create_response = $this->request('POST', ['company_api', 'create_post'], $param);
        $data = json_decode($create_response, true);
        $id = $data['data'];
        $stop_param = [
            "company_id" => $id,
            "company_name" => "teststartservice",
            "start_date" => $this->createStartDate(),
            "end_date" => $this->createEndDate(),
        ];
        $response = $this->request('POST', ['company_api', 'start_service_post'], $stop_param);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);
    }
}
