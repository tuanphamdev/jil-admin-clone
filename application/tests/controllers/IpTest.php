<?php

class IpTest extends TestCase
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * This method is called before each test.
     */
    protected function setUp(){
        parent::setUp();
    }

    /**
     * This method is called after each test.
     */
    protected function tearDown()
    {
    }

    /**
     * This method is called before the first test of this test class is run.
     */
    public static function setUpBeforeClass()
    {
        $CI = &get_instance();
        $CI->db->query("SET FOREIGN_KEY_CHECKS = 0");
        $CI->db->truncate('access_ips');
        $CI->db->query("SET FOREIGN_KEY_CHECKS = 1");
    }

    /**
     * This method is called after the last test of this test class is run.
     */
    public static function tearDownAfterClass()
    {
    }


    public function testCreateAnIpAddressWithValidAllField(){
        $params['ip_address'] = '192.169.2.3';
        $response = $this->request('POST', ['ip_api', 'create_post'], $params);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);

    }

    public function testUpdateAnIpAddressWithValidAllField(){
        $this->CI->db->insert('access_ips', ['ip_address' => '192.169.22.3']);
        $id = $this->CI->db->insert_id();
        $params = [
            'id' => $id,
            'ip_address' => '192.169.22.33'
        ];
        $response = $this->request('POST', ['ip_api', 'update_post'], $params);
        $inspected = json_decode($response, true);

        $this->assertTrue($inspected['status']);

    }

    public function testDeleteAnIpAddressWithValidAllField(){
        $this->CI->db->insert('access_ips', ['ip_address' => '192.169.22.35']);
        $id = $this->CI->db->insert_id();
        $params = [
            'id' => $id,
        ];
        $response = $this->request('POST', ['ip_api', 'delete_post'], $params);
        $inspected = json_decode($response, true);

        $this->assertTrue($inspected['status']);

    }

    public function testGetDetailAnIpAddressWithValidFieldId(){
        $this->CI->db->insert('access_ips', ['ip_address' => '192.169.99.99']);
        $id = $this->CI->db->insert_id();
        $params = [
            'id' => $id,
        ];
        $response = $this->request('GET', ['ip_api', 'detail_get'], $params);
        $inspected = json_decode($response, true);
        $this->assertEquals('192.169.99.99', $inspected['data']['ip_address']);

    }

    public function testGetListIpAddressWithValidAllField(){
        $this->CI->db->insert_batch('access_ips',
            [
                ['ip_address' => '192.169.99.1'],
                ['ip_address' => '192.169.99.2']
            ]
        );
        $response = $this->request('GET', ['ip_api', 'list_get']);
        $this->assertContains('192.169.99.1', $response);
        $this->assertContains('192.169.99.2', $response);
    }
}
