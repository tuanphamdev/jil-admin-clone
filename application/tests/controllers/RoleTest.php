<?php

class RoleTest extends TestCase
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * This method is called before each test.
     */
    protected function setUp(){
        parent::setUp();
    }

    /**
     * This method is called after each test.
     */
    protected function tearDown()
    {
    }

    /**
     * This method is called before the first test of this test class is run.
     */
    public static function setUpBeforeClass()
    {
        $CI = &get_instance();
        $CI->db->query("SET FOREIGN_KEY_CHECKS = 0");
        $CI->db->truncate('roles');
        $CI->db->truncate('role_permissions');
        $CI->db->query("SET FOREIGN_KEY_CHECKS = 1");
    }

    /**
     * This method is called after the last test of this test class is run.
     */
    public static function tearDownAfterClass()
    {
    }


    public function testCreateAnRoleWithValidAllField(){
        $params = [
            "name" => "role1",
            "company" => "1",
            "csv" => "1",
            "user" => "1",
            "role" => "1",
            "ip" => "1",
        ];
        $response = $this->request('POST', ['role_api', 'create_post'], $params);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);

    }

    public function testUpdateAnRoleWithValidAllField(){
        $param = [
            "name" => "role2",
            "company" => "1",
            "csv" => "1",
            "user" => "1",
            "role" => "1",
            "ip" => "1",
        ];
        $create_response = $this->request('POST', ['role_api', 'create_post'], $param);
        $data = json_decode($create_response, true);
        $id = $data['data'];
        $param = [
            'id' => $id,
            "name" => "role122",
            "company" => "3",
            "csv" => "3",
            "user" => "3",
            "role" => "3",
            "ip" => "3",
        ];
        $response = $this->request('POST', ['role_api', 'update_post'], $param);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);
    }

    public function testDeleteAnRoleWithValidAllField(){
        $param = [
            "name" => "role3",
            "company" => "1",
            "csv" => "1",
            "user" => "1",
            "role" => "1",
            "ip" => "1",
        ];
        $create_response = $this->request('POST', ['role_api', 'create_post'], $param);
        $data = json_decode($create_response, true);
        $id = $data['data'];
        $response = $this->request('POST', ['role_api', 'delete_post'], ['id' => $id]);
        $inspected = json_decode($response, true);
        $this->assertTrue($inspected['status']);

    }

    public function testGetDetailAnRoleWithValidId(){
        $param = [
            "name" => "role5",
            "company" => "1",
            "csv" => "1",
            "user" => "1",
            "role" => "1",
            "ip" => "1",
        ];
        $create_response = $this->request('POST', ['role_api', 'create_post'], $param);
        $data = json_decode($create_response, true);
        $id = $data['data'];
        $param = [
            'id' => $id,
        ];
        $response = $this->request('GET', ['role_api', 'detail_get'], $param);
        $actual = 'role5';
        $this->assertContains($actual, $response);

    }
}
