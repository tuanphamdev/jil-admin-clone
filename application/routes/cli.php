<?php

/**
 * CLI Routes
 *
 * This routes only will be available under a CLI environment
 */

// To enable Luthier-CI built-in cli commands
// uncomment the followings lines:

Luthier\Cli::maker();
// Luthier\Cli::migrations();

Route::cli('rake_data_ip', 'Batch@rake_data_ip');
Route::cli('rake_data_role', 'Batch@rake_data_role');
Route::cli('rake_data_user', 'Batch@rake_data_user');
Route::cli('rake_data_company', 'Batch@rake_data_company');
Route::cli('rake_data_company_map_missing', 'Batch@rake_data_company_map_missing');
Route::cli('rake_data_company_map_admin_core', 'Batch@rake_data_company_map_admin_core');
Route::cli('rake_data_company_contract_map_core_to_admin', 'Batch@rake_data_company_contract_map_core_to_admin');


Route::cli('apply_contract', 'Batch@apply_contract');
Route::cli('expiration_contract', 'Batch@expiration_contract');
Route::cli('expiration_information', 'Batch@expiration_information');

// seed batch
Route::cli('seed_permission_tokyo_marine', 'BatchSeeder@seed_permission_tokyo_marine');
Route::cli('seed_permission_import_bank', 'BatchSeeder@seed_permission_import_bank');
Route::cli('seed_permission_employee', 'BatchSeeder@seed_permission_employee');
Route::cli('seed_permission_ts_password', 'BatchSeeder@seed_permission_ts_password');
Route::cli('seed_permission_qa_engine', 'BatchSeeder@seed_permission_qa_engine');
Route::cli('seed_permission_survey_monkey', 'BatchSeeder@seed_permission_survey_monkey');

Route::cli('seed_enable_service_ultra_and_ocr', 'BatchSeeder@seed_enable_service_ultra_and_ocr');
Route::cli('add_date_default_to_service_enable', 'BatchSeeder@add_date_default_to_service_enable');

Route::cli('seed_enable_setting_kintai_keihi_salary', 'BatchSeeder@seed_enable_setting_kintai_keihi_salary');
Route::cli('update_jinjer_db_with_only_signing', 'BatchSeeder@update_jinjer_db_with_only_signing');
Route::cli('move_data_signing_from_plans_to_plans_signing', 'BatchSeeder@move_data_signing_from_plans_to_plans_signing');
Route::cli('remove_singing_columns_from_plans_table', 'BatchSeeder@remove_singing_columns_from_plans_table');

Route::cli('update_electronic_signature_long_term_signing_on', 'BatchSeeder@update_electronic_signature_long_term_signing_on');
