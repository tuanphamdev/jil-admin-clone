<?php

/**
 * Welcome to Luthier-CI!
 *
 * This is your main route file. Put all your HTTP-Based routes here using the static
 * Route class methods
 *
 * Examples:
 *
 *    Route::get('foo', 'bar@baz');
 *      -> $route['foo']['GET'] = 'bar/baz';
 *
 *    Route::post('bar', 'baz@fobie', [ 'namespace' => 'cats' ]);
 *      -> $route['bar']['POST'] = 'cats/baz/foobie';
 *
 *    Route::get('blog/{slug}', 'blog@post');
 *      -> $route['blog/(:any)'] = 'blog/post/$1'
 */

Route::get('/', 'Login@index');
Route::get('/top', 'Top@index');

//Route::set('404_override', function(){
//    show_404();
//});

Route::set('translate_uri_dashes', false);

//Login
Route::group('', [], function () {
    Route::get('login', 'Login@index');
    Route::get('forgot_password', 'Login@forgot_password');
});

//User
Route::group('', [], function () {
    Route::post('/do_login', 'Login@do_login');
    Route::get('/reissue_password/{any:token}', 'Login@reissue_password');
    Route::post('/do_reissue_password/{any:token}', 'Login@do_reissue_password');
    Route::post('/do_forgot_password', 'Login@do_forgot_password');
    Route::get('/logout', 'User@logout');
});

Route::group('users', [], function () {
    Route::get('/', 'User@index');
    Route::get('/{num:page}', 'User@index');
    Route::get('/search', 'User@index');
    Route::get('/search/{num:page}', 'User@index');
    Route::get('/create', 'User@create');
    Route::get('/update/{num:id}', 'User@update');
    Route::post('/do_create', 'User@do_create');
    Route::post('/do_update', 'User@do_update');
});

//Company
Route::group('companies', [], function () {
    Route::get('/', 'Company@index');
    Route::get('/{num:page}', 'Company@index');
    Route::get('/search', 'Company@index');
    Route::get('/search/{num:page}', 'Company@index');
    Route::get('/create', 'Company@create');
    Route::get('/update/{num:id}', 'Company@update');
    Route::get('/cities/{num:id}', 'Company@cities');
    Route::post('/start_service', 'Company@start_service');
    Route::post('/stop_service', 'Company@stop_service');
    Route::post('/do_create', 'Company@do_create');
    Route::post('/do_update/{num:id}', 'Company@do_update');
    Route::post('/check_enigma_company', 'Company@ajax_check_enigma_company');
    Route::post('/resend_account_info', 'Company@resend_account_info');
    Route::post('/change_type_contract_batch', 'Company@change_type_contract_batch');
});

//Tokyo marine
Route::group('tokyo_marines', [], function () {
    Route::get('/', 'Tokyo_Marine@index');
    Route::get('/{num:page}', 'Tokyo_Marine@index');
    Route::get('/search', 'Tokyo_Marine@index');
    Route::get('/search/{num:page}', 'Tokyo_Marine@index');
    Route::get('/create', 'Tokyo_Marine@create');
    Route::get('/update/{num:id}', 'Tokyo_Marine@update');
    Route::get('/cities/{num:id}', 'Company@cities');
    Route::post('/do_create', 'Tokyo_Marine@do_create');
    Route::post('/do_update/{num:id}', 'Tokyo_Marine@do_update');
    Route::post('/use_tokyo_marine', 'Tokyo_Marine@company_use_tokyo_marine');
    Route::post('/no_use_tokyo_marine', 'Tokyo_Marine@company_no_use_tokyo_marine');
});

Route::group('ips', [], function () {
    Route::get('/', 'Ip@index');
    Route::get('/create', 'Ip@create');
    Route::get('/update/{num:id}', 'Ip@update');
    Route::post('/do_create', 'Ip@do_create');
    Route::post('/do_update', 'Ip@do_update');
    Route::post('/delete', 'Ip@delete');
});

//Roles
Route::group('roles', [], function () {
    Route::get('/', 'Role@index');
    Route::get('/create', 'Role@create');
    Route::get('/update/{num:id}', 'Role@update');
    Route::post('/do_create', 'Role@do_create');
    Route::post('/do_update', 'Role@do_update');
    Route::post('/delete', 'Role@delete');
});

//Export Csv
Route::group('export_csv', [], function () {
    Route::get('/{num:page}', 'ExportCSV@index');
    Route::get('/search', 'ExportCSV@index');
    Route::get('/search/{num:page}', 'ExportCSV@index');
    Route::get('/', 'ExportCSV@index');
    Route::get('/retire_staff', 'ExportCSV@retire_staff');
    Route::get('/tokyo_marine_staff', 'ExportCSV@tokyo_marine_staff');
    Route::get('/company_download', 'ExportCSV@company_download');
    Route::get('/count_timecards', 'ExportCSV@count_timecards');
    Route::get('/count_tightens', 'ExportCSV@count_tightens');
    Route::get('/company_info', 'ExportCSV@company_info');
    Route::get('/csv_health_keihi', 'ExportCSV@csv_health_keihi');
    Route::get('/csv_health_salary', 'ExportCSV@csv_health_salary');
    Route::get('/csv_health_signing', 'ExportCSV@csv_health_signing');
});

//Import Csv
Route::group('import_banks', [], function () {
    Route::get('/', 'ImportBankCSV@index');
    Route::get('/{num:page}', 'ImportBankCSV@index');
    Route::post('/import', 'ImportBankCSV@import_banks');
    Route::get('/search', 'ImportBankCSV@index');
    Route::get('/search/{num:page}', 'ImportBankCSV@index');

    Route::get('/create', 'BankMaster@create');
    Route::post('/do_create', 'BankMaster@do_create');
    Route::get('/edit/{num:id}', 'BankMaster@edit');
    Route::post('/update/{num:id}', 'BankMaster@do_update');
});

//Information
Route::group('informations', [], function () {
    Route::get('/{num:page}', 'Information@index');
    Route::get('/search', 'Information@index');
    Route::get('/search/{num:page}', 'Information@index');
    Route::get('/', 'Information@index');
    Route::post('/do_off', 'Information@do_off');
    Route::post('/do_on', 'Information@do_on');
});

//Admin employees
Route::group('admin_employees', [], function () {
    Route::get('/{num:page}', 'Employee@index');
    Route::get('/search', 'Employee@index');
    Route::get('/search/{num:page}', 'Employee@index');
    Route::get('/', 'Employee@index');
    Route::post('/enable_admin', 'Employee@enable_admin');
    Route::get('/avatar/{num:id}/{num:core_company_id}', 'Employee@get_avatar');
});

//TS password change
Route::group('ts_password_change', [], function () {
    Route::get('/', 'TSPasswordChange@index');
    Route::get('/{num:page}', 'TSPasswordChange@index');
    Route::get('/search', 'TSPasswordChange@index');
    Route::get('/search/{num:page}', 'TSPasswordChange@index');
    Route::get('/change/', 'TSPasswordChange@index_change');
    Route::get('/change/{num:page}', 'TSPasswordChange@index_change');
    Route::get('/change/search', 'TSPasswordChange@index_change');
    Route::get('/change/search/{num:page}', 'TSPasswordChange@index_change');
    Route::post('/change_password/{num:id}', 'TSPasswordChange@change_password');
    Route::get('/avatar/{num:id}/{num:core_company_id}', 'TSPasswordChange@get_avatar');
});

//TS password ips
Route::group('ts_password_ips', [], function () {
    Route::get('/', 'TSPasswordIp@index');
    Route::get('/change/{num:id}', 'TSPasswordIp@index_change');
    Route::get('/create', 'TSPasswordIp@create');
    Route::get('/update/{num:id}', 'TSPasswordIp@update');
    Route::post('/do_create', 'TSPasswordIp@do_create');
    Route::post('/do_update', 'TSPasswordIp@do_update');
    Route::post('/delete', 'TSPasswordIp@delete');
});

//TS password logs
Route::group('ts_password_logs', [], function () {
    Route::get('/', 'TsPasswordLog@index');
    Route::get('/{num:page}', 'TsPasswordLog@index');
    Route::get('/search', 'TsPasswordLog@index');
    Route::get('/search/{num:page}', 'TsPasswordLog@index');
});

Route::group('qa_engine', [], function () {
    Route::get('/', 'QaEngine@index');
    Route::post('/do_create', 'QaEngine@do_create');
});

Route::group('survey_monkey', [], function () {
    Route::get('/', 'SurveyMonkey@index');
    Route::post('/do_create', 'SurveyMonkey@do_create');
});

/*
| -------------------------------------------------------------------------
| API ROUTING
| -------------------------------------------------------------------------
*/
Route::group('api/v1', [], function () {
    //Users routes
    Route::post('/users/auth', 'api/v1/user_api@auth');
    Route::post('/users/forgot_password', 'api/v1/user_api@forgot_password');
    Route::post('/users/reissue_password', 'api/v1/user_api@reissue_password');
    Route::group('users', ['middleware' => ['AuthMiddleware']], function () {
        Route::get('/list', 'api/v1/user_api@list');
        Route::get('/list_all', 'api/v1/user_api@list_all');
        Route::get('/detail', 'api/v1/user_api@detail');
        Route::post('/create', 'api/v1/user_api@create');
        Route::post('/update', 'api/v1/user_api@update');
        Route::post('/delete', 'api/v1/user_api@delete');
    });

    //Companies routes
    Route::group('companies', ['middleware' => ['AuthMiddleware']], function () {
        Route::get('/list', 'api/v1/company_api@list');
        Route::get('/detail', 'api/v1/company_api@detail');
        Route::post('/create', 'api/v1/company_api@create');
        Route::post('/update', 'api/v1/company_api@update');
        Route::post('/delete', 'api/v1/company_api@delete');
        Route::post('/stop_service', 'api/v1/company_api@stop_service');
        Route::post('/start_service', 'api/v1/company_api@start_service');
        Route::post('/use_tokyo_marine', 'api/v1/company_api@use_tokyo_marine');
        Route::post('/no_use_tokyo_marine', 'api/v1/company_api@no_use_tokyo_marine');
        Route::post('/request_resend_account_info', 'api/v1/company_api@request_resend_account_info');
        Route::post('/request_change_type_contract_batch', 'api/v1/company_api@request_change_type_contract_batch');
    });


    //Roles routes
    Route::group('roles', ['middleware' => ['AuthMiddleware']], function () {
        Route::get('/list', 'api/v1/role_api@list');
        Route::post('/create', 'api/v1/role_api@create');
        Route::get('/detail', 'api/v1/role_api@detail');
        Route::post('/update', 'api/v1/role_api@update');
        Route::post('/delete', 'api/v1/role_api@delete');
    });


    //Export CSV routes
    Route::group('export_csv', ['middleware' => ['AuthMiddleware']], function () {
        Route::get('/list', 'api/v1/export_csv_api@list');
        Route::post('/retire_staff', 'api/v1/export_csv_api@retire_staff');
        Route::post('/tokyo_marine_staff', 'api/v1/export_csv_api@tokyo_marine_staff');
    });

    //Import CSV routes
    Route::group('import_csv', ['middleware' => ['AuthMiddleware']], function () {
        Route::post('/import_csv_bank_info', 'api/v1/import_csv_api@import_csv_bank_master');
        Route::get('/list', 'api/v1/import_csv_api@list');
        Route::get('/detail/{num:id}', 'api/v1/import_csv_api@detail');
    });

    //Import CSV routes
    Route::group('banks', ['middleware' => ['AuthMiddleware']], function () {
        Route::post('/create', 'api/v1/bank@import_csv_bank_master');
        Route::get('/list', 'api/v1/import_csv_api@list');
    });

    //Ip routes
    Route::group('ips', ['middleware' => ['AuthMiddleware']], function () {
        Route::get('/list', 'api/v1/ip_api@list');
        Route::post('/create', 'api/v1/ip_api@create');
        Route::get('/detail', 'api/v1/ip_api@detail');
        Route::post('/update', 'api/v1/ip_api@update');
        Route::post('/delete', 'api/v1/ip_api@delete');
    });

    //Information routes
    Route::group('informations', [], function () {
        Route::get('/list', 'api/v1/information_api@list');
        Route::post('/on', 'api/v1/information_api@on');
        Route::post('/off', 'api/v1/information_api@off');
    });

    Route::group('informations', [], function () {
        Route::post('/create', 'api/v1/information_api@create');
        Route::post('/update', 'api/v1/information_api@update');
    });

    Route::group('employees', ['middleware' => ['AuthMiddleware']], function () {
        Route::get('/list', 'api/v1/employee_api@list');
        Route::post('/enable_admin', 'api/v1/employee_api@enable_admin_role');
    });

    //TS password change
    Route::group('ts_password_change', ['middleware' => ['AuthMiddleware']], function () {
        Route::get('/list_log', 'api/v1/ts_password_change_api@list_log');
        Route::get('/list', 'api/v1/ts_password_change_api@list');
        Route::post('/change_password', 'api/v1/ts_password_change_api@change_password');
        Route::get('/detail_company_core', 'api/v1/ts_password_change_api@detail_company_core');
    });

    //TS password ips routes
    Route::group('ts_password_ips', ['middleware' => ['AuthMiddleware']], function () {
        Route::get('/list', 'api/v1/ts_password_ip_api@list');
        Route::post('/create', 'api/v1/ts_password_ip_api@create');
        Route::get('/detail', 'api/v1/ts_password_ip_api@detail');
        Route::post('/update', 'api/v1/ts_password_ip_api@update');
        Route::post('/delete', 'api/v1/ts_password_ip_api@delete');
    });

    //TS password logs routes
    Route::group('ts_password_logs', ['middleware' => ['AuthMiddleware']], function () {
        Route::get('/list', 'api/v1/ts_password_log_api@list');
    });

    //QA Engine
    Route::group('qa_engines', ['middleware' => ['AuthMiddleware']], function () {
        Route::get('/list', 'api/v1/qa_engine_api@list');
        Route::post('/create', 'api/v1/qa_engine_api@create');
    });

    //Survey monkey
    Route::group('survey_monkeys', ['middleware' => ['AuthMiddleware']], function () {
        Route::get('/last_survey_monkey', 'api/v1/Survey_mokey_api@last_survey_monkey');
        Route::post('/save', 'api/v1/Survey_mokey_api@save');
    });
});
