<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Role_model extends MY_Model
{

    /**
     * Fields
     */
    public static $field_id = 'id';
    public static $field_name = 'name';
    public static $field_created_by_id = 'created_by_id';
    public static $field_updated_by_id = 'updated_by_id';

    public $table = 'roles';
    public $primary_key = 'id';
    public $protected = array('id');

    public $rules =  array(
        'insert' => array(
            'name' => array(
                'field'=>'name',
                'label'=>'lang:role_name',
                'rules'=>'required'
            ),

        ),
        'update' => array(
            'name' => array(
                'field'=>'name',
                'label'=>'lang:role_name',
                'rules'=>'required'
            ),
            'id' => [
                'field'=>'id',
                'label'=>'id',
                'rules'=>'trim|required|is_natural_greater_than_zero',
            ],

        ),
        'delete' => array(
            'id' => [
                'field'=>'id',
                'label'=>'id',
                'rules'=>'trim|required|is_natural_greater_than_zero',
            ],

        )
    );

    /**
     * Sound_model constructor.
     */
    public function __construct()
    {
        $this->timestamps = true;
        $this->soft_deletes = true;
        $this->has_many['permissions'] = array('Permission_model','role_id','id');

        parent::__construct();
    }
}
