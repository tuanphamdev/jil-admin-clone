<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Plan_signing_model extends MY_Model
{

    /**
     * Fields
     */
    public static $field_id = 'id';
    public static $field_company_id = 'company_id';
    public static $field_plan_id = 'plan_id';
    public static $field_signing_folder = 'signing_folder';
    public static $field_signing_folder_range = 'signing_folder_range';
    public static $field_signing_internal_workflow = 'signing_internal_workflow';
    public static $field_signing_batch_import = 'signing_batch_import';
    public static $field_import_signed_file = 'import_signed_file';
    public static $field_maximum_import_signed_file = 'maximum_import_signed_file';
    public static $field_import_signed_file_timestamp = 'import_signed_file_timestamp';
    public static $field_ip_limit = 'ip_limit';
    public static $field_send_sms = 'send_sms';
    public static $field_electronic_signature_long_term = 'electronic_signature_long_term';
    public static $field_electronic_signature_timestamp = 'electronic_signature_timestamp';
    public static $field_electronic_signature_authentication = 'electronic_signature_authentication';
    public static $field_send_function_digital_signature = 'send_function_digital_signature';
    public static $field_imprint_registration = 'imprint_registration';
    public static $field_attach_file_receiver = 'attach_file_receiver';
    public static $field_attach_file_sender = 'attach_file_sender';
    public static $field_alert_function = 'alert_function';
    public static $field_language_support = 'language_support';
    public static $field_date_enable_signing_folder = 'date_enable_signing_folder';
    public static $field_date_enable_signing_folder_range = 'date_enable_signing_folder_range';
    public static $field_date_enable_signing_internal_workflow = 'date_enable_signing_internal_workflow';
    public static $field_date_enable_signing_batch_import = 'date_enable_signing_batch_import';
    public static $field_date_enable_import_signed_file = 'date_enable_import_signed_file';
    public static $field_date_enable_ip_limit = 'date_enable_ip_limit';
    public static $field_date_enable_send_sms = 'date_enable_send_sms';
    public static $field_date_enable_electronic_signature_long_term = 'date_enable_electronic_signature_long_term';
    public static $field_date_enable_electronic_signature_timestamp = 'date_enable_electronic_signature_timestamp';
    public static $field_date_enable_electronic_signature_authentication = 'date_enable_electronic_signature_authentication';
    public static $field_date_enable_send_function_digital_signature = 'date_enable_send_function_digital_signature';
    public static $field_date_enable_imprint_registration = 'date_enable_imprint_registration';
    public static $field_date_enable_attach_file_receiver = 'date_enable_attach_file_receiver';
    public static $field_date_enable_attach_file_sender = 'date_enable_attach_file_sender';
    public static $field_date_enable_alert_function = 'date_enable_alert_function';
    public static $field_date_enable_language_support = 'date_enable_language_support';
    public static $field_created_at = 'created_at';
    public static $field_updated_at = 'updated_at';
    public static $field_deleted_at = 'deleted_at';


    public $table = 'plans_signing';
    public $primary_key = 'id';
    public $protected = array('id');

    public $rules =  array(
        'insert' => array(
            'signing_folder' => array(
                'field' => 'signing_folder',
                'label' => 'lang:company_plan_signing_folder',
                'rules' => 'required|is_natural',
            ),
            'signing_folder_range' => array(
                'field' => 'signing',
                'label' => 'lang:company_plan_signing_folder_range',
                'rules' => 'required|is_natural',
            ),
            'signing_internal_workflow' => array(
                'field'=>'signing_internal_workflow',
                'label'=>'lang:company_plan_signing_internal_workflow',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'signing_batch_import' => array(
                'field'=>'signing_batch_import',
                'label'=>'lang:company_plan_signing_batch_import',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'import_signed_file' => array(
                'field'=>'import_signed_file',
                'label'=>'lang:company_plan_import_signed_file',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'maximum_import_signed_file' => array(
                'field'=>'maximum_import_signed_file',
                'label'=>'lang:company_plan_maximum_import_signed_file',
                'rules'=>'trim|numeric|max_length[100]',
                'errors' => [
                    'max_length' => '%sは100桁以内で入力してください。',
                    'numeric' => '%sは半角数字で入力してください。'
                ]
            ),
            'import_signed_file_timestamp' => array(
                'field'=>'import_signed_file_timestamp',
                'label'=>'lang:company_plan_import_signed_file_timestamp',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'ip_limit' => array(
                'field'=>'ip_limit',
                'label'=>'lang:company_plan_ip_limit',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'send_sms' => array(
                'field'=>'send_sms',
                'label'=>'lang:company_plan_send_sms',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'electronic_signature_long_term' => array(
                'field'=>'electronic_signature_long_term',
                'label'=>'lang:company_plan_electronic_signature_long_term',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'electronic_signature_timestamp' => array(
                'field'=>'electronic_signature_timestamp',
                'label'=>'lang:company_plan_electronic_signature_timestamp',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'electronic_signature_authentication' => array(
                'field'=>'electronic_signature_authentication',
                'label'=>'lang:company_plan_electronic_signature_authentication',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'send_function_digital_signature' => array(
                'field'=>'send_function_digital_signature',
                'label'=>'lang:company_plan_send_function_digital_signature',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'imprint_registration' => array(
                'field'=>'import_signed_file',
                'label'=>'lang:company_plan_imprint_registration',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'attach_file_receiver' => array(
                'field'=>'import_signed_file',
                'label'=>'lang:company_plan_attach_file_receiver',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'attach_file_sender' => array(
                'field'=>'import_signed_file',
                'label'=>'lang:company_plan_attach_file_sender',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'alert_function' => array(
                'field'=>'import_signed_file',
                'label'=>'lang:company_plan_alert_function',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'language_support' => array(
                'field'=>'import_signed_file',
                'label'=>'lang:company_plan_language_support',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
        ),
        'update' => array(
            'signing_folder' => array(
                'field' => 'signing_folder',
                'label' => 'lang:company_plan_signing_folder',
                'rules' => 'required|is_natural',
            ),
            'signing_folder_range' => array(
                'field' => 'signing',
                'label' => 'lang:company_plan_signing_folder_range',
                'rules' => 'required|is_natural',
            ),
            'signing_internal_workflow' => array(
                'field'=>'signing_internal_workflow',
                'label'=>'lang:company_plan_signing_internal_workflow',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'signing_batch_import' => array(
                'field'=>'signing_batch_import',
                'label'=>'lang:company_plan_signing_batch_import',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'import_signed_file' => array(
                'field'=>'import_signed_file',
                'label'=>'lang:company_plan_import_signed_file',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'maximum_import_signed_file' => array(
                'field'=>'maximum_import_signed_file',
                'label'=>'lang:company_plan_maximum_import_signed_file',
                'rules'=>'trim|numeric|max_length[100]',
                'errors' => [
                    'max_length' => '%sは100桁以内で入力してください。',
                    'numeric' => '%sは半角数字で入力してください。'
                ]
            ),
            'import_signed_file_timestamp' => array(
                'field'=>'import_signed_file_timestamp',
                'label'=>'lang:company_plan_import_signed_file_timestamp',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'ip_limit' => array(
                'field'=>'ip_limit',
                'label'=>'lang:company_plan_ip_limit',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'send_sms' => array(
                'field'=>'send_sms',
                'label'=>'lang:company_plan_send_sms',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'electronic_signature_long_term' => array(
                'field'=>'electronic_signature_long_term',
                'label'=>'lang:company_plan_electronic_signature_long_term',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'electronic_signature_timestamp' => array(
                'field'=>'electronic_signature_timestamp',
                'label'=>'lang:company_plan_electronic_signature_timestamp',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'electronic_signature_authentication' => array(
                'field'=>'electronic_signature_authentication',
                'label'=>'lang:company_plan_electronic_signature_authentication',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'send_function_digital_signature' => array(
                'field'=>'send_function_digital_signature',
                'label'=>'lang:company_plan_send_function_digital_signature',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'imprint_registration' => array(
                'field'=>'import_signed_file',
                'label'=>'lang:company_plan_imprint_registration',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'attach_file_receiver' => array(
                'field'=>'import_signed_file',
                'label'=>'lang:company_plan_attach_file_receiver',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'attach_file_sender' => array(
                'field'=>'import_signed_file',
                'label'=>'lang:company_plan_attach_file_sender',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'alert_function' => array(
                'field'=>'import_signed_file',
                'label'=>'lang:company_plan_alert_function',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'language_support' => array(
                'field'=>'import_signed_file',
                'label'=>'lang:company_plan_language_support',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
        ),
    );

    /**
     * Plan_model constructor.
     */
    public function __construct()
    {
        $this->timestamps = true;
        $this->soft_deletes = true;
        parent::__construct();
    }
}
