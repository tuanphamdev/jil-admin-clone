<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Company_model extends MY_Model
{

    /**
     * Fields
     */
    public static $field_id = 'id';
    public static $field_created_by_id = 'created_by_id';
    public static $field_updated_by_id = 'updated_by_id';
    public static $field_code = 'code';
    public static $field_name = 'name';
    public static $field_name_furigana = 'name_furigana';
    public static $field_representative_name = 'representative_name';
    public static $field_representative_name_furigana = 'representative_name_furigana';
    public static $field_representative_title = 'representative_title';
    public static $field_tel = 'tel';
    public static $field_fax = 'fax';
    public static $field_zip = 'zip';
    public static $field_prefecture = 'prefecture';
    public static $field_city = 'city';
    public static $field_address = 'address';
    public static $field_address_furigana = 'address_furigana';
    public static $field_application_name = 'application_name';
    public static $field_application_surname = 'application_surname';
    public static $field_application_furigana = 'application_furigana';
    public static $field_application_furigana_surname = 'application_furigana_surname';
    public static $field_application_email = 'application_email';
    public static $field_registered_date = 'registered_date';
    public static $field_status = 'status';
    public static $field_deleted_at = 'deleted_at';
    public static $field_updated_at = 'updated_at';
    public static $field_created_at = 'created_at';


    public $table = 'companies';
    public $primary_key = 'id';


    public $rules = [
        'insert' => [
            'created_by_id' => [
                'field'=>'created_by_id',
                'label'=>'created_by_id',
                'rules'=>'trim|required|is_natural_greater_than_zero',
            ],
            'status' => [
                'field'=>'status',
                'label'=>'status',
                'rules'=>'trim|required|is_natural',
            ],
            'code' => [
                'field'=>'code',
                'label'=>'lang:company_code',
                'rules'=>'required|max_length[10]|numeric|is_natural_greater_than_zero|company_validate[code]',
            ],
            'name' => [
                'field'=>'name',
                'label'=>'lang:company_name',
                'rules'=>'trim|required',
            ],
            'name_furigana' => [
                'field'=>'name_furigana',
                'label'=>'lang:company_name_furigana',
                'rules'=>'trim|valid_furigana',
            ],
            'representative_name' => [
                'field'=>'representative_name',
                'label'=>'lang:company_representative_name',
                'rules'=>'trim',
            ],
            'representative_name_furigana' => [
                'field'=>'representative_name_furigana',
                'label'=>'lang:company_representative_name_furigana',
                'rules'=>'trim|valid_furigana',
            ],
            'representative_title' => [
                'field'=>'representative_title',
                'label'=>'lang:company_representative_title',
                'rules'=>'trim',
            ],
            'tel' => [
                'field'=>'tel',
                'label'=>'lang:company_tel',
                'rules'=>'trim|valid_tel',
            ],
            'fax' => [
                'field'=>'fax',
                'label'=>'lang:company_fax',
                'rules'=>'trim|valid_tel',
            ],
            'zip' => [
                'field'=>'zip',
                'label'=>'lang:company_zip',
                'rules'=>'trim|valid_zip',
            ],
            'prefecture' => [
                'field'=>'prefecture',
                'label'=>'lang:company_prefecture',
                'rules'=>'trim|is_natural_greater_than_zero',
            ],
            'city' => [
                'field'=>'city',
                'label'=>'lang:company_city',
                'rules'=>'trim|is_natural_greater_than_zero',
            ],
            'address' => [
                'field'=>'address',
                'label'=>'lang:company_address',
                'rules'=>'trim',
            ],
            'address_furigana' => [
                'field'=>'address_furigana',
                'label'=>'lang:company_address_furigana',
                'rules'=>'trim|valid_furigana_address',
            ],
            'application_name' => [
                'field'=>'application_name',
                'label'=>'lang:company_application_name',
                'rules'=>'trim|required',
            ],
            'application_surname' => [
                'field'=>'application_surname',
                'label'=>'lang:company_application_surname',
                'rules'=>'trim|required',
            ],
            'application_furigana' => [
                'field'=>'application_furigana',
                'label'=>'lang:company_application_furigana',
                'rules'=>'trim|required|valid_furigana',
            ],
            'application_email' => [
                'field'=>'application_email',
                'label'=>'lang:company_application_email',
                'rules'=>'trim|required|valid_email',
            ],
            'registered_date' => [
                'field'=>'registered_date',
                'label'=>'lang:company_registered_date',
                'rules'=>'trim|required',
            ],
            'application_furigana_surname' => [
                'field'=>'application_furigana_surname',
                'label'=>'lang:company_application_furigana_surname',
                'rules'=>'trim|required|valid_furigana',
            ],
            'is_tokyo_marine' => [
                'field'=>'is_tokyo_marine',
                'label'=>'lang:company_is_tokyo_marine',
                'rules'=>'trim|required|in_list[0,1]',
            ],
            'enigma_code' => [
                'field'=>'enigma_code',
                'label'=>'lang:company_enigma_code',
                'rules'=>'trim|max_length[20]|is_enigma_code',
            ],
            'enigma_version' => [
                'field'=>'enigma_version',
                'label'=>'lang:company_enigma_version',
                'rules'=>'trim|required|in_list[0,1,2]|company_validate[enigma_version]',
            ],
            'is_using_ai' => [
                'field'=>'is_using_ai',
                'label'=>'lang:company_ai_option',
                'rules'=>'trim|required|in_list[0,1]',
            ],
            'is_using_ai_ocr' => [
                'field'=>'is_using_ai_ocr',
                'label'=>'lang:company_option_AI_OCR',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_ebook_store' => [
                'field'=>'is_ebook_store',
                'label'=>'lang:company_option_ebook_store',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_credit_card' => [
                'field'=>'is_credit_card',
                'label'=>'lang:company_option_credit_card',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_paid_management' => [
                'field'=>'is_paid_management',
                'label'=>'lang:company_option_paid_management',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_shift_management' => [
                'field'=>'is_shift_management',
                'label'=>'lang:company_option_shift_management',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_overtime_management' => [
                'field'=>'is_overtime_management',
                'label'=>'lang:company_option_overtime_management',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_payslip' => [
                'field'=>'is_payslip',
                'label'=>'lang:company_option_payslip',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_using_timestamp' => [
                'field'=>'is_using_timestamp',
                'label'=>'lang:company_option_timestamp',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_using_ultra' => [
                'field'=>'is_using_ultra',
                'label'=>'lang:company_ultra_option',
                'rules'=>'trim|required|in_list[0,1]',
            ],
            'is_docomo' => [
                'field'=>'is_docomo',
                'label'=>'lang:company_docomo_option',
                'rules'=>'trim|required|in_list[0,1]',
            ],
            'maximum_signing' => [
                'field'=>'maximum_signing',
                'label'=>'lang:company_maximum_signing_option',
                'rules'=>'trim|numeric|max_length[100]',
                'errors' => [
                    'max_length' => '%sは100桁以内で入力してください。',
                    'numeric' => '%sは半角数字で入力してください。'
                ]
            ]
        ],
        'update' => [
            'id' => [
                'field'=>'id',
                'label'=>'id',
                'rules'=>'trim|required|is_natural_greater_than_zero',
            ],
            'status' => [
                'field'=>'status',
                'label'=>'status',
                'rules'=>'trim|required|is_natural',
            ],
            'plan_id' => array(
                'field'=>'plan_id',
                'label'=>'plan_id',
                'rules'=>'required|is_natural_greater_than_zero'
            ),
            'agreement_id' => array(
                'field'=>'agreement_id',
                'label'=>'agreement_id',
                'rules'=>'required|is_natural_greater_than_zero'
            ),
            'updated_by_id' => [
                'field'=>'updated_by_id',
                'label'=>'updated_by_id',
                'rules'=>'trim|required|is_natural_greater_than_zero',
            ],
            'code' => [
                'field'=>'code',
                'label'=>'lang:company_code',
                'rules'=>'required|max_length[10]|numeric|is_natural_greater_than_zero|company_validate[code]',
            ],
            'name' => [
                'field'=>'name',
                'label'=>'lang:company_name',
                'rules'=>'trim|required',
            ],
            'name_furigana' => [
                'field'=>'name_furigana',
                'label'=>'lang:company_name_furigana',
                'rules'=>'trim|valid_furigana',
            ],
            'representative_name' => [
                'field'=>'representative_name',
                'label'=>'lang:company_representative_name',
                'rules'=>'trim',
            ],
            'representative_name_furigana' => [
                'field'=>'representative_name_furigana',
                'label'=>'lang:company_representative_name_furigana',
                'rules'=>'trim|valid_furigana',
            ],
            'representative_title' => [
                'field'=>'representative_title',
                'label'=>'lang:company_representative_title',
                'rules'=>'trim',
            ],
            'tel' => [
                'field'=>'tel',
                'label'=>'lang:company_tel',
                'rules'=>'trim|valid_tel',
            ],
            'fax' => [
                'field'=>'fax',
                'label'=>'lang:company_fax',
                'rules'=>'trim|valid_tel',
            ],
            'zip' => [
                'field'=>'zip',
                'label'=>'lang:company_zip',
                'rules'=>'trim|valid_zip',
            ],
            'prefecture' => [
                'field'=>'prefecture',
                'label'=>'lang:company_prefecture',
                'rules'=>'trim|is_natural_greater_than_zero',
            ],
            'city' => [
                'field'=>'city',
                'label'=>'lang:company_city',
                'rules'=>'trim|is_natural_greater_than_zero',
            ],
            'address' => [
                'field'=>'address',
                'label'=>'lang:company_address',
                'rules'=>'trim',
            ],
            'address_furigana' => [
                'field'=>'address_furigana',
                'label'=>'lang:company_address_furigana',
                'rules'=>'trim|valid_furigana_address',
            ],
            'application_name' => [
                'field'=>'application_name',
                'label'=>'lang:company_application_name',
                'rules'=>'trim|required',
            ],
            'application_surname' => [
                'field'=>'application_surname',
                'label'=>'lang:company_application_surname',
                'rules'=>'trim|required',
            ],
            'application_furigana' => [
                'field'=>'application_furigana',
                'label'=>'lang:company_application_furigana',
                'rules'=>'trim|required|valid_furigana',
            ],
            'application_email' => [
                'field'=>'application_email',
                'label'=>'lang:company_application_email',
                'rules'=>'trim|required|valid_email',
            ],
            'registered_date' => [
                'field'=>'registered_date',
                'label'=>'lang:company_registered_date',
                'rules'=>'trim|required',
            ],
            'application_furigana_surname' => [
                'field'=>'application_furigana_surname',
                'label'=>'lang:company_application_furigana_surname',
                'rules'=>'trim|required|valid_furigana',
            ],
            'is_tokyo_marine' => [
                'field'=>'is_tokyo_marine',
                'label'=>'lang:company_is_tokyo_marine',
                'rules'=>'trim|required|in_list[0,1]',
            ],
            'enigma_code' => [
                'field'=>'enigma_code',
                'label'=>'lang:company_enigma_code',
                'rules'=>'trim|max_length[20]|is_enigma_code',
            ],
            'enigma_version' => [
                'field'=>'enigma_version',
                'label'=>'lang:company_enigma_version',
                'rules'=>'trim|required|in_list[0,1,2]|company_validate[enigma_version]',
            ],
            'is_using_ai' => [
                'field'=>'is_using_ai',
                'label'=>'lang:company_ai_option',
                'rules'=>'trim|required|in_list[0,1]',
            ],
            'is_using_ai_ocr' => [
                'field'=>'is_using_ai_ocr',
                'label'=>'lang:company_option_AI_OCR',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_ebook_store' => [
                'field'=>'is_ebook_store',
                'label'=>'lang:company_option_ebook_store',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_credit_card' => [
                'field'=>'is_credit_card',
                'label'=>'lang:company_option_credit_card',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_paid_management' => [
                'field'=>'is_paid_management',
                'label'=>'lang:company_option_paid_management',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_shift_management' => [
                'field'=>'is_shift_management',
                'label'=>'lang:company_option_shift_management',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_overtime_management' => [
                'field'=>'is_overtime_management',
                'label'=>'lang:company_option_overtime_management',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_payslip' => [
                'field'=>'is_payslip',
                'label'=>'lang:company_option_payslip',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_using_timestamp' => [
                'field'=>'is_using_timestamp',
                'label'=>'lang:company_option_timestamp',
                'rules'=>'trim|in_list[0,1]',
            ],
            'is_using_ultra' => [
                'field'=>'is_using_ultra',
                'label'=>'lang:company_ultra_option',
                'rules'=>'trim|required|in_list[0,1]',
            ],
            'is_docomo' => [
                'field'=>'is_docomo',
                'label'=>'lang:company_docomo_option',
                'rules'=>'trim|required|in_list[0,1]',
            ],
            'maximum_signing' => [
                'field'=>'maximum_signing',
                'label'=>'lang:company_maximum_signing_option',
                'rules'=>'trim|numeric|max_length[100]',
                'errors' => [
                    'max_length' => '%sは100桁以内で入力してください。',
                    'numeric' => '%sは半角数字で入力してください。'
                ]
            ]
        ],
        'retire_staff' => [
            'end_date_retire' => [
                'field'=>'end_date_retire',
                'label'=>'end_date_retire',
                'rules'=>'trim|company_validate[end_date_retire]',
            ]
        ],
        'tokyo_marine_staff' => [
            'num_account' => [
                'field'=>'num_account',
                'label'=>'lang:export_csv_num_account',
                'rules'=>'trim|is_natural_no_zero',
            ]
        ],
    ];

    const ENABLE_SERVICE = 1;
    const DISABLE_SERVICE = 0;

    /**
     * Company_model constructor.
     */
    public function __construct()
    {
        $this->timestamps = true;
        $this->soft_deletes = true;
        $this->has_one['agreement'] = array('Agreement_model','company_id','id','join');
        $this->has_one['plan'] = array('Plan_model','company_id','id');
        $this->has_one['plan_signing'] = array('Plan_signing_model','company_id','id');
        parent::__construct();
    }

    public function join_agreement($join = null)
    {
        $this->_database
            ->join('agreements', 'agreements.company_id = companies.id', $join);
        return $this;
    }

    public function join_plan($join = null)
    {
        $this->_database
            ->join('plans', 'plans.company_id = companies.id', $join);
        return $this;
    }

    public function join_plan_signing($join = null)
    {
        $this->_database
            ->join('plans_signing', 'plans.id = plans_signing.plan_id', $join);
        return $this;
    }

    public function get_retire_staff($start_date_retire = null, $end_date_retire = null)
    {
        $jinji_db = $this->load->database('jinji_db', true);
        $retire_staff = $jinji_db->select('companies.code as company_code, employees.code as employee_code, employees.retirement_date')
            ->from('employees')
            ->join('companies', 'employees.company_id = companies.id ')
            ->where('employees.retirement_date IS NOT NULL')
            ->where('employees.deleted_at IS NULL')
            ->where('companies.deleted_at IS NULL');
        if (!empty($start_date_retire)) {
            $retire_staff->where('employees.retirement_date >=', $start_date_retire);
        }
        if (!empty($end_date_retire)) {
            $retire_staff->where('employees.retirement_date <=', $end_date_retire);
        }

        if ($retire_staff) {
            return $retire_staff->get()->result_array();
        }
        return [];
    }

    public function get_tokyo_marine_staff($num_account = 50)
    {
        $jinji_db = $this->load->database('jinji_db', true);
        $jp_current_date = jp_current_date();
        $tokyo_marine_staff = $jinji_db->query("
                    SELECT 
                    e.id AS id,
                    c.code AS company_code,
                    e.code AS employee_code,
                    affs_max.name AS dept_name,
                    e.last_name,
                    e.first_name,
                    e.last_name_kana,
                    e.first_name_kana,
                    p.date_of_birth,
                    e.gender,
                    p.mobile,
                    e.email,
                    adr.postcode,
                    adr.pref_name,
                    adr.city_name,
                    adr.chrome_street,
                    adr.building_name,
                    e.employment_classification,
                    e.enrollment_status,
                    e.retirement_date,
                    e.retirement_classification,
                    e.retirement_reason
                FROM
                    employees e
                        JOIN
                    companies c ON e.company_id = c.id AND c.status = 1
                        AND c.capacity >= 50
                        AND c.deleted_at IS NULL
                        LEFT JOIN
                    privacies p ON p.employee_id = e.id
                        AND e.deleted_at IS NULL
                        LEFT JOIN
                    (SELECT 
                        adr1.postcode,
                            adr1.employee_id,
                            adr1.chrome_street,
                            adr1.building_name,
                            prefectures.name AS pref_name,
                            cities.name AS city_name
                    FROM
                        addresses adr1
                    LEFT JOIN prefectures ON prefectures.id = adr1.prefecture_id
                    LEFT JOIN cities ON cities.id = adr1.municipality_id
                    WHERE
                        adr1.date_of_moving = (SELECT 
                                MAX(adr2.date_of_moving)
                            FROM
                                addresses adr2
                            WHERE
                                deleted_at IS NULL
                                    AND adr2.employee_id = adr1.employee_id
                                    AND adr2.date_of_moving <= '{$jp_current_date}')) adr ON e.id = adr.employee_id
                        LEFT JOIN
                    (SELECT 
                        aff1.dept_id, aff1.employee_id, depts.name
                    FROM
                        affiliations aff1
                    LEFT JOIN depts ON aff1.dept_id = depts.id
                    WHERE
                        aff1.deleted_at IS NULL
                            AND aff1.issued_on = (SELECT 
                                MAX(aff2.issued_on)
                            FROM
                                affiliations aff2
                            WHERE
                                aff2.deleted_at IS NULL
                                    AND aff1.employee_id = aff2.employee_id
                                    AND aff2.issued_on <= '{$jp_current_date}')) AS affs_max ON affs_max.employee_id = e.id
                WHERE
                    e.deleted_at IS NULL
                        AND c.id IN (SELECT 
                            company_id
                        FROM
                            employees
                        GROUP BY company_id
                        HAVING COUNT(id) >= 50)");

        $jinji_db = null;
        if ($tokyo_marine_staff) {
            return $tokyo_marine_staff->result_array();
        }
        return [];
    }


    /**
     * Company validate
     *
     * validation for company model
     * belong to business of model
     *
     * @param string $str
     * @param string $field
     * @return bool
     */
    public function validate($str, $field)
    {
        switch ($field) {
            case 'code':
                if (!empty($this->input->post())) {
                    $id = $this->input->post('id');
                }
                $this->form_validation->set_message('company_validate', $this->lang->line('company_duplicate'));
                if (!empty($id)) {
                    return ($this->as_array()->where(array($field => $str, self::$field_id.' <> '=> $id))->count_rows() === 0);
                } else {
                    return ($this->as_array()->where(array($field => $str))->count_rows() === 0);
                }

                break;
            case 'end_date_retire':
                $start_date = $this->input->post('start_date_retire');
                $end_date = $this->input->post('end_date_retire');
                if ($start_date > $end_date) {
                    $this->form_validation->set_message('company_validate', $this->lang->line('company_agreement_start_date_less_than_end_date'));
                    return false;
                }
                break;
            case 'enigma_version':
                $enigma_code = $this->input->post('enigma_code');
                $enigma_version = $this->input->post('enigma_version');

                if ($enigma_version != '0' && empty($enigma_code)) {
                    $this->form_validation->set_message('company_validate', $this->lang->line('company_enigma_code_version_required'));
                    return false;
                }
                if (!empty($enigma_code)) {
                    $id = $this->input->post('id');
                    if (!empty($id)) {
                        $this->form_validation->set_message('company_validate', $this->lang->line('company_enigma_code_duplicate'));
                        return ($this->as_array()->where(array('enigma_code' => $enigma_code, self::$field_id.' <> '=> $id))->count_rows() === 0);
                    }
                }
                break;
            default:
                break;
        }
        return true;
    }

    public function get_info()
    {
        $fields = [
            'code',
            'enigma_code',
            '(case when enigma_version = 0 then "'.$this->lang->line('company_enigma_version_no_use').'" else enigma_version end) as enigma_version',
            'companies.name',
            'companies.name_furigana',
            'representative_name',
            'representative_name_furigana',
            'representative_title',
            'tel',
            'fax',
            'zip',
            'p.name as prefecture_name',
            'c.name as city_name',
            'address',
            'address_furigana',
            'application_name',
            'application_surname',
            'application_furigana',
            'application_furigana_surname',
            'application_email',
            'DATE_FORMAT(registered_date, "%Y/%m/%d")',
            '(case when is_tokyo_marine = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as is_tokyo_marine',
            '(case when is_using_ai = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as is_using_ai',
            '(case when agreements.type = 0 then "'.$this->lang->line('company_agree_trial').'" when agreements.type = 1 then "'.$this->lang->line('company_agree_free').'" else  "'.$this->lang->line('csv_header_company_agree_fee').'" end) as type',
            'DATE_FORMAT(agreements.start_date, "%Y/%m/%d")',
            'DATE_FORMAT(agreements.end_date, "%Y/%m/%d")',
            'agreements.num_account',
            'DATE_FORMAT(convert_tz(companies.created_at, "+00:00", "+09:00"), "%Y/%m/%d %H:%i:%s")',
            '(case when plans.jinjer_db = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as jinjer_db',
            '(case when plans.kintai = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as kintai',
            '(case when plans.roumu = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as roumu',
            '(case when plans.jinji = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as jinji',
            '(case when plans.keihi = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as keihi',
            '(case when plans.ai_ocr = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as ai_ocr',
            '(case when plans.timestamp = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as timestamp',
            '(case when plans.my_number = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as my_number',
            '(case when plans.work_vital = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as work_vital',
            '(case when plans.salary = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as salary',
            '(case when plans.workflow = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as workflow',
            '(case when plans.employee_contract = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as employee_contract',
            '(case when plans.year_end_tax_adjustment = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as year_end_tax_adjustment',
            '(case when plans.signing = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as signing',
            '(case when plans.signing = 1 then companies.maximum_signing else "" end) as maximum_signing',
            '(case when plans.signing_folder = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as signing_folder',
            '(case when plans.signing_folder_range = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as signing_folder_range',
            '(case when plans.ultra = 1 then "'.$this->lang->line('company_agree_use').'" else "'.$this->lang->line('company_agree_no_use').'" end) as ultra',
            'DATE_FORMAT(plans.date_enable_kintai, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_roumu, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_jinji, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_keihi, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_ai_ocr, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_timestamp, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_my_number, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_work_vital, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_salary, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_workflow, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_employee_contract, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_year_end_tax_adjustment, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_signing, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_signing_folder, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_signing_folder_range, "%Y/%m/%d %H:%i:%s")',
            'DATE_FORMAT(plans.date_enable_ultra, "%Y/%m/%d %H:%i:%s")'
        ];
        $joins = [
            ['tbl' => 'agreements', 'on' => 'agreements.company_id = companies.id'],
            ['tbl' => 'plans', 'on' => 'plans.company_id = companies.id'],
            ['tbl' => getenv('DB_NAME_HR').'.prefectures as p', 'on' => 'p.id = companies.prefecture'],
            ['tbl' => getenv('DB_NAME_HR').'.cities as c', 'on' => 'c.id = companies.city'],
        ];
        $join_sql = '';
        foreach ($joins as $v) {
            $join_sql .= ' left outer join ' . $v['tbl'] . ' on ' . $v['on'] . ' ';
        }
        $sql = 'select ' . implode(',', $fields) . ' from ' . $this->table . $join_sql;
        $data = $this->db->query($sql)->result_array();

        return $data;
    }
}
