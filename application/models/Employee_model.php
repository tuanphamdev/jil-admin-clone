<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Employee_model extends MY_Model
{

    public $table = null;
    public $primary_key = 'id';
    public $protected = array('id');

    public $rules =  array(
        'search_employee' => array(
            'company_code' => [
                'field'=>'company_code',
                'label'=>'企業コード',
                'rules'=>'trim|required|is_natural_greater_than_zero'
            ],
            'employee_code' => [
                'field'=>'employee_code',
                'label'=>'社員番号',
                'rules'=>'trim|required'
            ]
        ),
        'enable_admin_role' => array(
            'core_company_id' => [
                'field'=>'core_company_id',
                'label'=>'企業ID',
                'rules'=>'trim|required|is_natural_greater_than_zero'
            ],
            'admin_company_id' => [
                'field'=>'admin_company_id',
                'label'=>'admin_company_id',
                'rules'=>'trim|required|is_natural_greater_than_zero'
            ],
            'employee_code' => [
                'field'=>'employee_code',
                'label'=>'社員番号',
                'rules'=>'trim|required'
            ]
        ),
    );

    /**
     * Employee_model constructor.
     */
    public function __construct()
    {
        $this->table = getenv('DB_NAME_HR').".employees";
        $this->timestamps = true;
        $this->soft_deletes = true;
        parent::__construct();
    }



    /**
     * Employee validate
     *
     * validation for employee model
     *
     * @param string $str
     * @param string $field
     * @return bool
     */
    public function validate($str, $field)
    {
        switch ($field) {
            default:
                break;
        }
        return true;
    }

    public function list($where = [], $limit, $offset, $count = false)
    {
        $jinji_db = $this->load->database('jinji_db', true);
        $jp_current_date = jp_current_date();
        $query = "SELECT DISTINCT
            e.face_photo_uploads_id AS photo_id,
            c.name AS company_name,
            c.code AS company_code,
            e.code AS employee_code,
            c.id AS core_company_id,
            c.admin_company_id,
            CONCAT(e.last_name, ' ', e.first_name) AS full_name,
            affs_max.name AS dept_name,
            affs_max.job_title_name AS job_title,
            e.enrollment_status,
            e.is_admin
        FROM
            employees e
                JOIN
            companies c ON e.company_id = c.id AND c.status = 1
                AND c.deleted_at IS NULL
                LEFT JOIN
            (SELECT 
                aff1.dept_id, aff1.employee_id, depts.name, job_titles.name as job_title_name
            FROM
                affiliations aff1
            LEFT JOIN depts ON aff1.dept_id = depts.id AND depts.deleted_at IS NULL
            LEFT JOIN job_titles ON aff1.job_title_id = job_titles.id  AND job_titles.deleted_at IS NULL
            WHERE
                aff1.deleted_at IS NULL
                    AND aff1.is_primary = 1 AND aff1.is_using = 1
                    AND aff1.issued_on = (SELECT 
                        MAX(aff2.issued_on)
                    FROM
                        affiliations aff2
                    WHERE
                        aff2.deleted_at IS NULL
                        AND aff2.is_primary = 1 AND aff2.is_using = 1
                            AND aff1.employee_id = aff2.employee_id
                            AND aff2.issued_on <= '{$jp_current_date}')) AS affs_max ON affs_max.employee_id = e.id
        WHERE
            e.deleted_at IS NULL";

        $query .= " AND ( ( e.code = '{$where['employee_code']}'";

        if (!empty($where['employee_name'])) {
            $query .= " AND CONCAT_WS(' ', e.last_name, e.first_name) LIKE '%{$where['employee_name']}%' ) ";
        } else {
            $query .= " ) ";
        }

        $query .= " OR e.is_admin = 1 )";

        $query .= " AND c.code = '{$where['company_code']}'";
        $query .= " ORDER BY e.is_admin DESC";

        if ($count === false) {
            $query .= " LIMIT {$offset}, {$limit}";
        }

        $employees = $jinji_db->query($query);
        if (!$employees) {
            return false;
        }

        $jinji_db = null;
        if ($count) {
            return $employees->num_rows();
        }
        return $employees->result_array();
    }
}
