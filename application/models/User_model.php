<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends MY_Model
{

    /**
     * Fields
     */
    public static $field_id = 'id';
    public static $field_role_id = 'role_id';
    public static $field_name = 'name';
    public static $field_name_furigana = 'name_furigana';
    public static $field_email = 'email';
    public static $field_password = 'password';
    public static $field_status = 'status';
    public static $field_is_send_mail = 'is_send_mail';
    public static $field_deleted_at = 'deleted_at';
    public static $field_updated_at = 'updated_at';
    public static $field_created_at = 'created_at';

    public $table = 'users';
    public $primary_key = 'id';
    public $protected = array('id');

    public $rules =  array(
        'insert' => array(
            'role_id' => array(
                'field'=>'role_id',
                'label'=>'lang:user_role_id',
                'rules'=>'required'
            ),
            'name' => array(
                'field'=>'name',
                'label'=>'lang:user_name',
                'rules'=>'required'
            ),
            'name_furigana' => array(
                'field'=>'name_furigana',
                'label'=>'lang:user_name_furigana',
                'rules'=>'required|valid_furigana'
            ),
            'email' => array(
                'field'=>'email',
                'label'=>'lang:user_email',
                'rules'=>'required|valid_email|user_validate[email]'
            ),
            'password' => array(
                'field'=>'password',
                'label'=>'lang:user_password',
                'rules'=>'required'
            ),
            'status' => array(
                'field'=>'status',
                'label'=>'lang:user_status',
                'rules'=>'required'
            ),
            'is_send_mail' => array(
                'field'=>'is_send_mail',
                'label'=>'lang:user_is_send_mail',
                'rules'=>'required'
            ),
        ),
        'update' => array(
            'role_id' => array(
                'field'=>'role_id',
                'label'=>'lang:user_role',
                'rules'=>'required'
            ),
            'name' => array(
                'field'=>'name',
                'label'=>'lang:user_name',
                'rules'=>'required'
            ),
            'name_furigana' => array(
                'field'=>'name_furigana',
                'label'=>'lang:user_name_furigana',
                'rules'=>'required|valid_furigana'
            ),
            'email' => array(
                'field'=>'email',
                'label'=>'lang:user_email',
                'rules'=>'required|valid_email|user_validate[email]'
            ),
            'status' => array(
                'field'=>'status',
                'label'=>'lang:user_status',
                'rules'=>'required'
            ),
            'is_send_mail' => array(
                'field'=>'is_send_mail',
                'label'=>'lang:user_is_send_mail',
                'rules'=>'required'
            ),
        )
    );

    /**
     * Sound_model constructor.
     */
    public function __construct()
    {
        $this->timestamps = true;
        $this->soft_deletes = true;
        $this->has_one['role'] = array('Role_model','id','role_id');
        parent::__construct();
    }

    /**
     * User validate
     *
     * validation for user model
     * belong to business of model
     *
     * @param string $str
     * @param string $field
     * @return bool
     */
    public function validate($str, $field)
    {
        switch ($field) {
            case 'email':
                if (!empty($this->input->post())) {
                    $id = $this->input->post('id');
                }
                $this->form_validation->set_message('user_validate', $this->lang->line('user_duplicate'));
                if (!empty($id)) {
                    return ($this->as_array()->where(array($field => $str, self::$field_id.' <> '=> $id))->count_rows() === 0);
                } else {
                    return ($this->as_array()->where(array($field => $str))->count_rows() === 0);
                }

                break;
            default:
                break;
        }
        return true;
    }

    /**
     * Authentication
     *
     * @param $email
     * @param $password
     * @return bool|mixed
     */
    public function auth($email, $password)
    {
        $data = $this->as_array()->where(['email' => $email, 'status' => 1])
            ->fields('id, role_id, name, name_furigana, email, password, status')->get();
        if (!empty($data)) {
            $password_verify = password_verify($password, $data['password']);
            if ($password_verify !== false) {
                $data['access_token'] = $this->token->get_token(['user_id' => $data['id'], 'email' => $data['email'], 'password' => $password]);
                unset($data['password']);
                return $data;
            }
        }

        return false;
    }
}
