<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ts_password_change_model extends MY_Model
{

    public $table = null;
    public $primary_key = 'id';
    public $protected = array('id');

    public $rules =  array(
    );

    /**
     * Employee_model constructor.
     */
    public function __construct()
    {
        $this->table = getenv('DB_NAME_HR').".employees";
        $this->timestamps = true;
        $this->soft_deletes = true;
        parent::__construct();
    }



    /**
     *  validate
     *
     * @param string $str
     * @param string $field
     * @return bool
     */
    public function validate($str, $field)
    {
        switch ($field) {
            default:
                break;
        }
        return true;
    }

    public function list_log($where = [], $limit, $offset, $count = false)
    {
        $db_jinji = getenv('DB_NAME_HR');
        $query = "SELECT 
            p.*,
            a.type,
            c_core.name,
            c.code,
            c.id,
            c.application_surname,
            c.application_name, 
            c.application_furigana_surname,
            c.application_furigana,
            c_core.ts_password_use_start,
            c_core.ts_password_use_end,
            c_core.ts_password_use_period_type
        FROM
            companies c
                JOIN
            {$db_jinji}.companies c_core ON c_core.admin_company_id = c.id AND c_core.code = c.code AND c_core.ts_password_use_flag = 1 
            JOIN agreements a ON a.company_id = c.id  AND a.deleted_at IS NULL
            JOIN plans p ON p.company_id = c.id  AND p.deleted_at IS NULL
                AND c_core.deleted_at IS NULL
                WHERE c.deleted_at IS NULL";
        $query_search = [];
        if (!empty($where['name']) || !empty($where['code'])) {
            if (!empty($where['name'])) {
                $query_search[] = " c_core.name LIKE '%{$where['name']}%' ";
            }
            if (!empty($where['code'])) {
                $query_search[] = " c.code LIKE '%{$where['code']}%' ";
            }
        }
        if (!empty($query_search)) {
            $query .= ' AND ( ' . implode(' AND ', $query_search) . ' ) ';
        }

        if (!empty($where['use_start']) || !empty($where['use_end'])) {
            if (!empty($where['use_start']) && !empty($where['use_end'])) {
                $query .= " AND DATE(c_core.ts_password_use_end) >= '{$where['use_start']}' AND DATE(c_core.ts_password_use_end) <= '{$where['use_end']}'";
            } elseif (!empty($where['use_start'])) {
                $query .= " AND DATE(c_core.ts_password_use_end) >= '{$where['use_start']}'";
            } elseif (!empty($where['use_end'])) {
                $query .= " AND DATE(c_core.ts_password_use_end) <= '{$where['use_end']}'";
            }
        }

        $query .= " ORDER BY c.id DESC";
        if ($count === false) {
            $query .= " LIMIT {$offset}, {$limit}";
        }

        $companies = $this->db->query($query);
        if (!$companies) {
            return false;
        }

        $jinji_db = null;
        if ($count) {
            return $companies->num_rows();
        }
        return $companies->result_array();
    }

    public function list($where = [], $limit, $offset, $count = false)
    {
        $jinji_db = $this->load->database('jinji_db', true);

        $jp_current_date = jp_current_date();
        $query = "SELECT DISTINCT
            e.face_photo_uploads_id AS photo_id,
            c.name AS company_name,
            c.code AS company_code,
            e.code AS employee_code,
            e.id as employee_id,
            c.id AS core_company_id,
            c.admin_company_id,
            CONCAT(e.last_name, ' ', e.first_name) AS full_name,
            affs_max.name AS dept_name,
            affs_max.job_title_name AS job_title,
            e.enrollment_status,
            e.ts_password
        FROM
            employees e
                JOIN
            companies c ON e.company_id = c.id AND c.status = 1 AND c.ts_password_use_flag = 1 
                AND c.deleted_at IS NULL
                LEFT JOIN
            (SELECT 
                aff1.dept_id, aff1.employee_id, depts.name, job_titles.name as job_title_name
            FROM
                affiliations aff1
            LEFT JOIN depts ON aff1.dept_id = depts.id AND depts.deleted_at IS NULL
            LEFT JOIN job_titles ON aff1.job_title_id = job_titles.id AND job_titles.deleted_at IS NULL
            WHERE
                aff1.deleted_at IS NULL
                    AND aff1.issued_on = (SELECT 
                        MAX(aff2.issued_on)
                    FROM
                        affiliations aff2
                    WHERE
                        aff2.deleted_at IS NULL
                        AND aff2.is_primary = 1 AND aff2.is_using = 1
                            AND aff1.employee_id = aff2.employee_id
                            AND aff2.issued_on <= '{$jp_current_date}')) AS affs_max ON affs_max.employee_id = e.id
        WHERE
            e.deleted_at IS NULL";

        if (!empty($where['employee_name']) || !empty($where['employee_code'])) {
            if (!empty($where['employee_name'])) {
                $query_search[] = " CONCAT_WS(' ', e.last_name, e.first_name) LIKE '%{$where['employee_name']}%' ";
            }
            if (!empty($where['employee_code'])) {
                $query_search[] = " e.code LIKE '%{$where['employee_code']}%' ";
            }
        }
        if (!empty($query_search)) {
            $query .= ' AND ( ' . implode(' AND ', $query_search) . ' ) ';
        }

        if (!empty($where['flag_setting'])) {
            if ($where['flag_setting'] == 3) {
                $query .= " AND (e.ts_password IS NULL OR e.ts_password = '') ";
            }
            if ($where['flag_setting'] == 4) {
                $query .= " AND ( e.ts_password IS NOT NULL AND e.ts_password != '')";
            }
        }

        $query .= " AND c.admin_company_id = '{$where['company_id']}'";
        $query .= " ORDER BY e.id DESC";

        if ($count === false) {
            $query .= " LIMIT {$offset}, {$limit}";
        }

        $employees = $jinji_db->query($query);
        if (!$employees) {
            return false;
        }

        $jinji_db = null;
        if ($count) {
            return $employees->num_rows();
        }
        return $employees->result_array();
    }


    public function detail_company_core($params)
    {
        $jinji_db = $this->load->database('jinji_db', true);
        $company_id = $params['id'] ?? '';
        if (empty($company_id)) {
            return false;
        }

        $query = "SELECT c.name FROM companies c WHERE c.admin_company_id = {$company_id} AND c.status = 1 AND c.deleted_at IS NULL;";
        $company = $jinji_db->query($query);
        if (!$company) {
            return false;
        }
        $jinji_db = null;
        return $company->row_array();
    }
}
