<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agreement_model extends MY_Model
{

    /**
     * Fields
     */
    public static $field_id = 'id';
    public static $field_company_id = 'company_id';
    public static $field_type = 'type';
    public static $field_start_date = 'start_date';
    public static $field_end_date = 'end_date';
    public static $field_num_account = 'num_account';

    public $table = 'agreements';
    public $primary_key = 'id';
    public $protected = array('id');

    public $rules =  array(
        'insert' => array(
            'type' => array(
                'field'=>'type',
                'label'=>'lang:company_agreement_type',
                'rules'=>'required'
            ),
            'start_date' => array(
                'field'=>'start_date',
                'label'=>'lang:company_agreement_start_date',
                'rules'=>'required'
            ),
            'end_date' => array(
                'field'=>'end_date',
                'label'=>'lang:company_agreement_end_date',
                'rules'=>'required|agreement_validate[end_date]'
            ),
            'num_account' => array(
                'field'=>'num_account',
                'label'=>'lang:company_agreement_num_account',
                'rules'=>'trim|required|is_natural'
            )

        ),
        'update' => array(
            'type' => array(
                'field'=>'type',
                'label'=>'lang:company_agreement_type',
                'rules'=>'required|is_natural'
            ),
            'start_date' => array(
                'field'=>'start_date',
                'label'=>'lang:company_agreement_start_date',
                'rules'=>'required'
            ),
            'end_date' => array(
                'field'=>'end_date',
                'label'=>'lang:company_agreement_end_date',
                'rules'=>'required|agreement_validate[end_date]'
            ),
            'num_account' => array(
                'field'=>'num_account',
                'label'=>'lang:company_agreement_num_account',
                'rules'=>'trim|required|is_natural'
            ),
            'company_id' => array(
                'field'=>'company_id',
                'label'=>'company_id',
                'rules'=>'required|is_natural_greater_than_zero'
            ),
            'stop_date' => [
                'field'=>'stop_date',
                'label'=>'stop_date',
                'rules'=>'trim',
            ],

        ),
        'start_service' => array(
            'start_date' => array(
                'field'=>'start_date',
                'label'=>'lang:company_agreement_start_date',
                'rules'=>'required'
            ),
            'end_date' => array(
                'field'=>'end_date',
                'label'=>'lang:company_agreement_end_date',
                'rules'=>'required|agreement_validate[end_date]'
            ),
            'company_id' => array(
                'field'=>'company_id',
                'label'=>'company_id',
                'rules'=>'required|is_natural_greater_than_zero'
            )
            
        ),
        'search_date' => array(
            'start_date_to' => array(
                'field'=>'start_date_to',
                'label'=>'lang:start_date_to',
                'rules'=>'agreement_validate[start_date_to]'
            ),
            'end_date_to' => array(
                'field'=>'end_date_to',
                'label'=>'lang:end_date_to',
                'rules'=>'agreement_validate[end_date_to]'
            ),

        )
    );

    /**
     * Agreement_model constructor.
     */
    public function __construct()
    {
        $this->timestamps = true;
        $this->soft_deletes = true;
        parent::__construct();
    }


    /**
     * Agreement validate
     *
     * validation for agreement model
     * belong to business of model
     *
     * @param string $str
     * @param string $field
     * @return bool
     */
    public function validate($str, $field)
    {
        switch ($field) {
            case 'end_date':
                $start_date = $this->input->post('start_date');
                $end_date = $this->input->post('end_date');
                $current_date = jp_current_date();
                if ($end_date < $current_date) {
                    $this->form_validation->set_message('agreement_validate', $this->lang->line('company_agreement_current_date_less_than_end_date'));
                    return false;
                }
                if ($start_date >= $end_date) {
                    $this->form_validation->set_message('agreement_validate', $this->lang->line('company_agreement_start_date_less_than_end_date'));
                    return false;
                }

                break;
            case 'start_date_to':
                $start_date_from = $this->input->get('start_date_from');
                $start_date_to = $this->input->get('start_date_to');
                if ($start_date_from > $start_date_to) {
                    $this->form_validation->set_message('agreement_validate', $this->lang->line('company_agreement_start_from_less_than_start_to'));
                    return false;
                }

                break;
            case 'end_date_to':
                $end_date_from = $this->input->get('end_date_from');
                $end_date_to = $this->input->get('end_date_to');
                if ($end_date_from > $end_date_to) {
                    $this->form_validation->set_message('agreement_validate', $this->lang->line('company_agreement_end_from_less_than_end_to'));
                    return false;
                }
                break;

            default:
                break;
        }
        return true;
    }

    public function test($data)
    {
    }
}
