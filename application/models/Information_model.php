<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Information_model extends MY_Model
{

    /**
     * Fields
     */
    public static $field_id = 'id';
    public static $field_title = 'title';
    public static $field_detail = 'detail';
    public static $field_link_url = 'link_url';
    public static $field_expiration_date = 'expiration_date';
    public static $field_is_open = 'is_open';
    public static $field_deleted_at = 'deleted_at';
    public static $field_updated_at = 'updated_at';
    public static $field_created_at = 'created_at';

    public $table = 'informations';
    public $primary_key = 'id';
    public $protected = array('id');

    public $rules =  array(
        'insert' => array(
            'title' => array(
                'field'=>'title',
                'label'=>'lang:information_title',
                'rules'=>'required|max_length[128]|information_validate[title]'
            ),
            'detail' => array(
                'field'=>'detail',
                'label'=>'lang:information_detail',
                'rules'=>'required|information_validate[detail]'
            ),
            'link_url' => array(
                'field'=>'link_url',
                'label'=>'lang:information_link_url',
                'rules'=>'required|information_validate[link_url]'
            ),
            'expiration_date' => array(
                'field'=>'expiration_date',
                'label'=>'lang:information_expiration_date',
                'rules'=>'required|information_validate[expiration_date]'
            )
        ),
        'update' => array(
            'title' => array(
                'field'=>'title',
                'label'=>'lang:information_title',
                'rules'=>'required|information_validate[title]'
            ),
            'detail' => array(
                'field'=>'detail',
                'label'=>'lang:information_detail',
                'rules'=>'required|information_validate[detail]'
            ),
            'link_url' => array(
                'field'=>'link_url',
                'label'=>'lang:information_link_url',
                'rules'=>'required|information_validate[link_url]'
            )
        ),
    );

    /**
     * Information_model constructor.
     */
    public function __construct()
    {
        $this->timestamps = true;
        $this->soft_deletes = true;
        parent::__construct();
    }


    /**
     * Information validate
     *
     * validation for information model
     *
     * @param string $str
     * @param string $field
     * @return bool
     */
    public function validate($str, $field)
    {
        switch ($field) {
            case 'title':
//                if (!empty($this->input->post())) {
//                    $id = $this->input->post('id');
//                }
//
//                $this->form_validation->set_message('information_validate', $this->lang->line('information_validate'));
//                if (!empty($id)) {
//                    return ($this->as_array()->where(array($field => $str, self::$field_id.' <> '=> $id))->count_rows() === 0);
//                } else {
//                    return ($this->as_array()->where(array($field => $str))->count_rows() === 0);
//                }

                break;
            default:
                break;
        }
        return true;
    }
}
