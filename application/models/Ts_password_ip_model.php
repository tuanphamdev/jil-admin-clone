<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ts_password_ip_model extends MY_Model
{

    /**
     * Fields
     */
    public static $field_id = 'id';
    public static $field_name = 'name';
    public static $field_ip_address = 'ip_address';
    public static $field_deleted_at = 'deleted_at';
    public static $field_updated_at = 'updated_at';
    public static $field_created_at = 'created_at';

    public $table = 'ts_password_ips';
    public $primary_key = 'id';
    public $protected = array('id');

    public $rules =  array(
        'insert' => array(
            'ip_address' => array(
                'field'=>'ip_address',
                'label'=>'lang:ts_password_ip_address',
                'rules'=>'required|valid_ip|ts_password_ip_validate[ip_address]',
                'errors' => [
                    'required' => '許可するIPはIPの形式で入力してください。',
                ]
            ),
            'name' => array(
                'field'=>'name',
                'label'=>'lang:ts_password_ip_field_name',
                'rules'=>'required'
            ),
        ),
        'update' => array(
            'ip_address' => array(
                'field'=>'ip_address',
                'label'=>'lang:ts_password_ip_address',
                'rules'=>'required|valid_ip|ts_password_ip_validate[ip_address]',
                'errors' => [
                    'required' => '許可するIPはIPの形式で入力してください。',
                ]
            ),
            'name' => array(
                'field'=>'name',
                'label'=>'lang:ts_password_ip_field_name',
                'rules'=>'required'
            ),
        )
    );

    /**
     * TS password ip model constructor.
     */
    public function __construct()
    {
        $this->timestamps = true;
        $this->soft_deletes = true;
        parent::__construct();
    }

    public function allow($ip)
    {
        return ($this->as_array()->where([self::$field_ip_address => $ip])->count_rows() > 0);
    }

    /**
     * Ip validate
     *
     * validation for ip model
     *
     * @param string $str
     * @param string $field
     * @return bool
     */
    public function validate($str, $field)
    {
        switch ($field) {
            case 'ip_address':
                if (!empty($this->input->post())) {
                    $id = $this->input->post('id');
                }
                
                $this->form_validation->set_message('ts_password_ip_validate', $this->lang->line('ip_duplicate'));
                if (!empty($id)) {
                    return ($this->as_array()->where(array($field => $str, self::$field_id.' <> '=> $id))->count_rows() === 0);
                } else {
                    return ($this->as_array()->where(array($field => $str))->count_rows() === 0);
                }

                break;
            default:
                break;
        }
        return true;
    }
}
