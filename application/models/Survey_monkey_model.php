<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Survey_monkey_model extends MY_Model
{
    /**
     * Fields
     */
    public static $field_id = 'id';
    public static $field_start_date = 'start_date';
    public static $field_end_date = 'end_date';
    public static $field_services = 'services';
    public static $field_creator_id = 'creator_id';
    public static $field_script_content = 'script_content';
    public static $field_deleted_at = 'deleted_at';
    public static $field_updated_at = 'updated_at';
    public static $field_created_at = 'created_at';

    public $table = 'survey_monkey';
    public $primary_key = 'id';
    public $protected = array('id');

    public $rules =  [
        'insert' => [
            'services' => [
                'field'=>'services',
                'label'=>'lang:services',
                'rules'=>'trim|required',
                'errors' => [
                    'required' => '表示サービスを1つ以上選択してください。'
                ]
            ],
            'start_date' => [
                'field'=>'start_date',
                'label'=>'lang:survey_monkey_start_date',
                'rules'=>'required|valid_date_format',
                'errors' => [
                    'valid_date_format' => '%sを正しい日付フォーマットで入力してください。',
                    'required' => '%sを入力してください。'
                ]
            ],
            'end_date' => [
                'field'=>'end_date',
                'label'=>'lang:survey_monkey_end_date',
                'rules'=>'trim|valid_date_format|greater_than_date[start_date]',
                'errors' => [
                    'valid_date_format' => '%sを正しい日付フォーマットで入力してください。',
                    'greater_than_date' => '%sを正しく入力してください。',
                ]
            ],
            'script_content' => [
                'field'=>'script_content',
                'label'=>'lang:survey_monkey_script_content',
                'rules'=>'trim'
            ],
            'creator_id' => [
                'field'=>'creator_id',
                'label'=>'lang:creator_id',
                'rules'=>'trim|numeric'
            ]
        ],
    ];

    public function __construct()
    {
        parent::__construct();
        $this->soft_deletes = true;
        $this->after_get = ['convert_services', 'convert_end_date_is_null'];
    }

    public function convert_services($row)
    {
        $row['services'] = explode(',', $row['services']);
        return $row;
    }

    public function convert_end_date_is_null($row)
    {
        $row['end_date'] = $row['end_date'] !== '0000-00-00' ? $row['end_date'] : null;
        return $row;
    }
}
