<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bank_master_model extends MY_Model
{
    public $table = null;
    public $primary_key = 'id';

    public $rules =  array(
        'import' => array(
            'bank_code' => array(
                'rules'=>'required'
            ),
            'branch_code' => array(
                'rules'=>'required'
            ),
            'sort' => array(
                'rules'=>'required'
            ),
            'bank_name' => array(
                'rules'=>'required'
            ),
            'branch_name' => array(
                'rules'=>'required'
            ),
        )
    );

    public function __construct()
    {
        $this->table = getenv('DB_NAME_HR').".bank_master";
        $this->timestamps = true;
        $this->soft_deletes = true;
        parent::__construct();
    }

    private function set_import_validate_label($line, $field)
    {
        $label = '';
        $line_label =  $this->lang->line('import_bank_csv_line')." $line: ";
        switch ($field) {
            case 'bank_code':
                $label = $line_label.$this->lang->line('csv_header_import_bank_code');
                break;
            case 'branch_code':
                $label = $line_label.$this->lang->line('csv_header_import_bank_branch_code');
                break;
            case 'sort':
                $label = $line_label.$this->lang->line('import_csv_sort');
                break;
            case 'bank_name':
                $label = $line_label.$this->lang->line('import_csv_bank_name');
                break;
            case 'branch_name':
                $label = $line_label.$this->lang->line('import_csv_branch_name');
                break;
            default;
        }
        return $label;
    }

    /**
     * Save bank data into database
     *
     * @param $data_csv
     * @return bool
     */
    public function save_banks($data_csv)
    {
        $jinji_db = $this->load->database('jinji_db', true);
        $bank_master = $jinji_db->select('id, bank_code, bank_name, bank_name_kana, 
        branch_code, branch_name, branch_name_kana, branch_post_code, 
        branch_address, branch_phone_number, branch_phone_number, sort')
            ->from('bank_master')
            ->where('deleted_at', null)
            ->get()->result_array();
        $insert_batch = [];
        $update_batch = [];
        $delete_ids = [];

        foreach ($bank_master as $bank_k => $bank_v) {
            $new_key = $bank_v['bank_code'].'_'.$bank_v['branch_code'].'_'.$bank_v['sort'];
            $bank_master[$new_key] = $bank_v;
            unset($bank_master[$bank_k]);
        }

        $csv_current_banks = [];
        foreach ($data_csv as $csv_key => $csv_value) {
            $csv_key = $csv_value['bank_code'].'_'.$csv_value['branch_code'].'_'.$csv_value['sort'];
            $csv_current_banks[] = $csv_key;

            if (!empty($bank_master[$csv_key])) {
                $csv_value['id'] = $bank_master[$csv_key]['id'];
                $update_batch[] = $csv_value;
                $delete_ids[] = $csv_value['id'];
            } else {
                $insert_batch[] = $csv_value;
            }
        }

        $will_delete_banks = [];
        if (!empty($csv_current_banks)) {
            $will_delete_bank_code = array_diff(array_keys($bank_master), $csv_current_banks);
            foreach ($will_delete_bank_code as $bank_k) {
                $will_delete_banks[] = $bank_master[$bank_k]['id'];
            }
        }
        $jinji_db->trans_start();
        $jinji_db->query("SET FOREIGN_KEY_CHECKS = 0;");
        if (!empty($will_delete_banks)) {
            $jinji_db->set('deleted_at', date('Y-m-d H:i:s'));
            $jinji_db->where_in('id', $will_delete_banks);
            $jinji_db->update('bank_master');
        }
        if (!empty($update_batch)) {
            $jinji_db->where_in('id', $delete_ids)->delete('bank_master');
            $jinji_db->insert_batch('bank_master', $update_batch);
        }
        if (!empty($insert_batch)) {
            $jinji_db->insert_batch('bank_master', $insert_batch);
        }

        $jinji_db->query("SET FOREIGN_KEY_CHECKS = 1;");

        $jinji_db->trans_complete();

        if ($jinji_db->trans_status() === false) {
            return false;
        }
        return true;
    }


    /**
     * Validate import csv headers
     *
     * @param $headers
     * @return bool
     */
    public function valid_headers($headers)
    {
        $expected_headers = $this->import_bank_expected_headers();
        if (!empty(array_diff(array_keys($expected_headers), $headers))) {
            return false;
        }
        return true;
    }


    /**
     * Import csv headers expected
     *
     * @return array
     */
    private function import_bank_expected_headers()
    {
        return [
            $this->lang->line('csv_header_import_bank_code') => 'bank_code',
            $this->lang->line('csv_header_import_bank_branch_code') => 'branch_code',
            $this->lang->line('csv_header_import_bank_name_kana') => 'bank_name_kana',
            $this->lang->line('csv_header_import_bank_name') => 'bank_name',
            $this->lang->line('csv_header_import_bank_branch_name_kana') => 'branch_name_kana',
            $this->lang->line('csv_header_import_bank_branch_name') => 'branch_name',
            $this->lang->line('csv_header_import_bank_zip_code') => 'branch_post_code',
            $this->lang->line('csv_header_import_bank_address') => 'branch_address',
            $this->lang->line('csv_header_import_bank_phone') => 'branch_phone_number',
            $this->lang->line('csv_header_import_bank_draft_clearing_place') => 'draft_clearing_place',
            $this->lang->line('csv_header_import_bank_sort_by') => 'sort',
        ];
    }


    /**
     * Process import csv data
     *
     * @param $records
     * @return array
     */
    public function process_import_csv_bank_data($records)
    {
        $headers = $this->import_bank_expected_headers();
        $csv_data = [];
        foreach ($records as $offset => $record) {
            $row_data = [];
            foreach ($record as $column => $column_value) {
                if (isset($headers[$column])) {
                    $field = $headers[$column];
                    if (in_array($field, ['bank_code', 'branch_code', 'sort','bank_name', 'branch_name'])) {
                        $this->form_validation->set_rules($field, $this->set_import_validate_label($offset+1, $field), $this->rules['import'][$field]);
                    }
                    $row_data[$field] = $column_value;
                }
            }
            if (!empty($row_data)) {
                $this->form_validation->set_data($row_data);
                if ($this->form_validation->run() == false) {
                    return false;
                }

                $row_data['bank_code'] = sprintf("%04d", $row_data['bank_code']);
                $row_data['branch_code'] = sprintf("%03d", $row_data['branch_code']);
                $csv_data[] = $row_data;
            }
        }
        return $csv_data;
    }

    public function detail($id)
    {
        $jinji_db = $this->load->database('jinji_db', true);
        return  $jinji_db->select('*')
            ->from('bank_master')
            ->where('id', $id)
            ->get()
            ->result_array();
    }
}
