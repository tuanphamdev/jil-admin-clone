<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Qa_engine_model extends MY_Model
{

    /**
     * Fields
     */
    public static $field_id = 'id';
    public $table = 'qa_engines';
    public $primary_key = 'id';
    public $protected = array('id');

    public $rules =  array(
        'insert' => array(
            'status' => array(
                'field'=>'status',
                'label'=>'lang:status',
                'rules'=>'trim'
            ),
            'information_1' => array(
                'field'=>'information_1',
                'label'=>'lang:qa_engine_information_1',
                'rules'=>'required|trim',
                'errors' => [
                    'required' => '%sに文字を入力してください。'
                ]
            ),
            'information_2' => array(
                'field'=>'information_2',
                'label'=>'lang:qa_engine_information_2',
                'rules'=>'required|trim',
                'errors' => [
                    'required' => '%sに文字を入力してください。'
                ]
            )
        ),
        'update' => array(
            'status' => array(
                'field'=>'status',
                'label'=>'lang:status',
                'rules'=>'trim'
            ),
            'information_1' => array(
                'field'=>'information_1',
                'label'=>'lang:qa_engine_information_1',
                'rules'=>'required|trim',
                'errors' => [
                    'required' => '%sに文字を入力してください。'
                ]
            ),
            'information_2' => array(
                'field'=>'information_2',
                'label'=>'lang:qa_engine_information_2',
                'rules'=>'required|trim',
                'errors' => [
                    'required' => '%sに文字を入力してください。'
                ]
            )
        )
    );

    /**
     * Ip_model constructor.
     */
    public function __construct()
    {
        $this->timestamps = true;
        $this->soft_deletes = true;
        parent::__construct();
    }
}
