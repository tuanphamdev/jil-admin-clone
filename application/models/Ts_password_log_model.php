<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ts_password_log_model extends MY_Model
{

    public $table = null;
    public $primary_key = 'id';
    public $protected = array('id');

    public $rules =  array();

    /**
     * Employee_model constructor.
     */
    public function __construct()
    {
        $this->table = getenv('DB_NAME_HR').".companies";
        $this->timestamps = true;
        $this->soft_deletes = true;
        parent::__construct();
    }



    /**
     *  validate
     *
     * @param string $str
     * @param string $field
     * @return bool
     */
    public function validate($str, $field)
    {
        switch ($field) {
            default:
                break;
        }
        return true;
    }

    public function list($where = [], $limit, $offset, $count = false)
    {
        $db_jinji = getenv('DB_NAME_HR');
        $query = "SELECT 
            ts_l.*,
            users.name as user_name,
            c_core.name as company_name,
            c_core.code as company_code,
            CONCAT(emp_core.last_name, ' ', emp_core.first_name) AS emp_name,
            emp_core.code
        FROM
            ts_password_logs ts_l
            JOIN {$db_jinji}.companies c_core ON c_core.id = ts_l.company_id AND c_core.deleted_at IS NULL
            JOIN {$db_jinji}.employees emp_core ON emp_core.id = ts_l.employee_id  AND emp_core.deleted_at IS NULL
            JOIN users ON users.id = ts_l.user_id  AND users.deleted_at IS NULL
            WHERE ts_l.deleted_at IS NULL";
        $query_search = [];

        if (!empty($where['company_code'])) {
            $query_search[] = " c_core.code LIKE '%{$where['company_code']}%' ";
        }
        if (!empty($where['employee_code'])) {
            $query_search[] = " emp_core.code LIKE '%{$where['employee_code']}%' ";
        }
        if (!empty($where['user_id'])) {
            $query_search[] = " ts_l.user_id = {$where['user_id']} ";
        }
        if (!empty($where['employee_name'])) {
            $query_search[] = " CONCAT(emp_core.last_name, ' ', emp_core.first_name) LIKE '%{$where['employee_name']}%' ";
        }
        if (isset($where['action_type']) && $where['action_type'] !== '') {
            $query_search[] = " ts_l.action_type = {$where['action_type']}  ";
        }
        if (!empty($query_search)) {
            $query .= ' AND ' . implode(' AND ', $query_search) . ' ';
        }
        if (!empty($where['created_date_start']) || !empty($where['created_date_end'])) {
            if (!empty($where['created_date_start']) && !empty($where['created_date_end'])) {
                $query .= " AND DATE(ts_l.created_at) >= '{$where['created_date_start']}' AND DATE(ts_l.created_at) <= '{$where['created_date_end']}'";
            } elseif (!empty($where['created_date_start'])) {
                $query .= " AND DATE(ts_l.created_at) >= '{$where['created_date_start']}'";
            } elseif (!empty($where['created_date_end'])) {
                $query .= " AND DATE(ts_l.created_at) <= '{$where['created_date_end']}'";
            }
        }
        $query .= " ORDER BY ts_l.id DESC";
        if ($count === false) {
            $query .= " LIMIT {$offset}, {$limit}";
        }
        $companies = $this->db->query($query);
        if (!$companies) {
            return false;
        }

        $jinji_db = null;
        if ($count) {
            return $companies->num_rows();
        }
        return $companies->result_array();
    }
}
