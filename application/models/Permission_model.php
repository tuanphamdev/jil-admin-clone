<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Permission_model extends MY_Model
{

    /**
     * Fields
     */
    public static $field_id = 'id';
    public static $field_item = 'item';


    public $table = 'role_permissions';
    public $primary_key = 'id';
    public $protected = array('id');

    public $rules =  array(
        'insert' => array(
            'item' => array(
                'field'=>'item',
                'label'=>'lang:role_permission_item',
                'rules'=>'required'
            )
        ),
        'update' => array()
    );

    /**
     * Permission_model constructor.
     */
    public function __construct()
    {
        $this->timestamps = true;
        $this->soft_deletes = true;

        parent::__construct();
    }

    public function insert_permission($role_id, $permissions)
    {

        if (isset($permissions['company'])) {
            $this->insert(['item' => 'company', 'permission' => (int)$permissions['company'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'company', 'permission' => 1 , 'role_id' => $role_id]);
        }

        if (isset($permissions['user'])) {
            $this->insert(['item' => 'user', 'permission' => (int)$permissions['user'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'user', 'permission' => 1 , 'role_id' => $role_id]);
        }

        if (isset($permissions['csv'])) {
            $this->insert(['item' => 'csv', 'permission' => (int)$permissions['csv'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'csv', 'permission' => 1 , 'role_id' => $role_id]);
        }
        if (isset($permissions['role'])) {
            $this->insert(['item' => 'role', 'permission' => (int)$permissions['role'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'role', 'permission' => 1 , 'role_id' => $role_id]);
        }
        if (isset($permissions['ip'])) {
            $this->insert(['item' => 'ip', 'permission' => (int)$permissions['ip'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'ip', 'permission' => 1 , 'role_id' => $role_id]);
        }
        if (isset($permissions['information'])) {
            $this->insert(['item' => 'information', 'permission' => (int)$permissions['information'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'information', 'permission' => 1 , 'role_id' => $role_id]);
        }
        if (isset($permissions['tokyo_marine'])) {
            $this->insert(['item' => 'tokyo_marine', 'permission' => (int)$permissions['tokyo_marine'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'tokyo_marine', 'permission' => 1 , 'role_id' => $role_id]);
        }
        if (isset($permissions['import_bank'])) {
            $this->insert(['item' => 'import_bank', 'permission' => (int)$permissions['import_bank'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'import_bank', 'permission' => 1 , 'role_id' => $role_id]);
        }
        if (isset($permissions['employee'])) {
            $this->insert(['item' => 'employee', 'permission' => (int)$permissions['employee'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'employee', 'permission' => 1 , 'role_id' => $role_id]);
        }

        if (isset($permissions['ts_password_change'])) {
            $this->insert(['item' => 'ts_password_change', 'permission' => (int)$permissions['ts_password_change'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'ts_password_change', 'permission' => 1 , 'role_id' => $role_id]);
        }
        if (isset($permissions['ts_password_ips'])) {
            $this->insert(['item' => 'ts_password_ips', 'permission' => (int)$permissions['ts_password_ips'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'ts_password_ips', 'permission' => 1 , 'role_id' => $role_id]);
        }
        if (isset($permissions['ts_password_logs'])) {
            $this->insert(['item' => 'ts_password_logs', 'permission' => (int)$permissions['ts_password_logs'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'ts_password_logs', 'permission' => 1 , 'role_id' => $role_id]);
        }

        if (isset($permissions['qa_engine'])) {
            $this->insert(['item' => 'qa_engine', 'permission' => (int)$permissions['qa_engine'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'qa_engine', 'permission' => 1 , 'role_id' => $role_id]);
        }

        if (isset($permissions['survey_monkey'])) {
            $this->insert(['item' => 'survey_monkey', 'permission' => (int)$permissions['survey_monkey'], 'role_id' => $role_id]);
        } else {
            $this->insert(['item' => 'survey_monkey', 'permission' => 1 , 'role_id' => $role_id]);
        }
    }

    public function update_permission($role_id, $permissions)
    {
        if (isset($permissions['company'])) {
            $this->where(['item' => 'company', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['company']]);
        }

        if (isset($permissions['user'])) {
            $this->where(['item' => 'user', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['user']]);
        }

        if (isset($permissions['csv'])) {
            $this->where(['item' => 'csv', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['csv']]);
        }
        if (isset($permissions['role'])) {
            $this->where(['item' => 'role', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['role']]);
        }
        if (isset($permissions['ip'])) {
            $this->where(['item' => 'ip', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['ip']]);
        }
        if (isset($permissions['information'])) {
            $this->where(['item' => 'information', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['information']]);
        }
        if (isset($permissions['tokyo_marine'])) {
            $this->where(['item' => 'tokyo_marine', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['tokyo_marine']]);
        }
        if (isset($permissions['import_bank'])) {
            $this->where(['item' => 'import_bank', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['import_bank']]);
        }
        if (isset($permissions['employee'])) {
            $this->where(['item' => 'employee', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['employee']]);
        }
        if (isset($permissions['ts_password_change'])) {
            $this->where(['item' => 'ts_password_change', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['ts_password_change']]);
        }
        if (isset($permissions['ts_password_ips'])) {
            $this->where(['item' => 'ts_password_ips', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['ts_password_ips']]);
        }
        if (isset($permissions['ts_password_logs'])) {
            $this->where(['item' => 'ts_password_logs', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['ts_password_logs']]);
        }
        if (isset($permissions['qa_engine'])) {
            $this->where(['item' => 'qa_engine', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['qa_engine']]);
        }
        if (isset($permissions['survey_monkey'])) {
            $this->where(['item' => 'survey_monkey', 'role_id' => $role_id])
                ->update(['permission' => (int)$permissions['survey_monkey']]);
        }
    }

    public function delete_permission($role_id, $permissions)
    {
        if (isset($permissions['company'])) {
            $this->where(['item' => 'company', 'role_id' => $role_id])
                ->delete();
        }

        if (isset($permissions['user'])) {
            $this->where(['item' => 'user', 'role_id' => $role_id])
                ->delete();
        }

        if (isset($permissions['csv'])) {
            $this->where(['item' => 'csv', 'role_id' => $role_id])
                ->delete();
        }
        if (isset($permissions['role'])) {
            $this->where(['item' => 'role', 'role_id' => $role_id])
                ->delete();
        }
        if (isset($permissions['ip'])) {
            $this->where(['item' => 'ip', 'role_id' => $role_id])
                ->delete();
        }
        if (isset($permissions['tokyo_marine'])) {
            $this->where(['item' => 'tokyo_marine', 'role_id' => $role_id])
                ->delete();
        }
        if (isset($permissions['import_bank'])) {
            $this->where(['item' => 'import_bank', 'role_id' => $role_id])
                ->delete();
        }
        if (isset($permissions['employee'])) {
            $this->where(['item' => 'employee', 'role_id' => $role_id])
                ->delete();
        }
        if (isset($permissions['ts_password_change'])) {
            $this->where(['item' => 'ts_password_change', 'role_id' => $role_id])
                ->delete();
        }
        if (isset($permissions['ts_password_ips'])) {
            $this->where(['item' => 'ts_password_ips', 'role_id' => $role_id])
                ->delete();
        }
        if (isset($permissions['ts_password_logs'])) {
            $this->where(['item' => 'ts_password_logs', 'role_id' => $role_id])
                ->delete();
        }

        if (isset($permissions['qa_engine'])) {
            $this->where(['item' => 'qa_engine', 'role_id' => $role_id])
                ->delete();
        }
        if (isset($permissions['survey_monkey'])) {
            $this->where(['item' => 'survey_monkey', 'role_id' => $role_id])
                ->delete();
        }
    }
}
