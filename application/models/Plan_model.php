<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Plan_model extends MY_Model
{

    /**
     * Fields
     */
    public static $field_id = 'id';
    public static $field_company_id = 'company_id';
    public static $field_jinjer_db = 'jinjer_db';
    public static $field_kintai = 'kintai';
    public static $field_roumu = 'roumu';
    public static $field_jinji = 'jinji';
    public static $field_keihi = 'keihi';
    public static $field_my_number = 'my_number';
    public static $field_work_vital = 'work_vital';
    public static $field_salary = 'salary';
    public static $field_workflow = 'workflow';
    public static $field_employee_contract = 'employee_contract';
    public static $field_year_end_tax_adjustment = 'year_end_tax_adjustment';
    public static $field_signing = 'signing';
    public static $field_folder = 'folder';
    public static $field_folder_range = 'folder_range';
    public static $field_ai_ocr = 'ai_ocr';
    public static $field_timestamp = 'timestamp';
    public static $field_ultra = 'ultra';
    public static $field_docomo = 'docomo';
    public static $field_d_account_id = 'd_account_id';
    public static $field_date_enable_kintai = 'date_enable_kintai';
    public static $field_date_enable_roumu = 'date_enable_roumu';
    public static $field_date_enable_jinji = 'date_enable_jinji';
    public static $field_date_enable_keihi = 'date_enable_keihi';
    public static $field_date_enable_ai_ocr = 'date_enable_ai_ocr';
    public static $field_date_enable_ebook_store = 'date_enable_ebook_store';
    public static $field_date_enable_credit_card = 'date_enable_credit_card';
    public static $field_date_enable_timestamp = 'date_enable_timestamp';
    public static $field_date_enable_my_number = 'date_enable_my_number';
    public static $field_date_enable_work_vital = 'date_enable_work_vital';
    public static $field_date_enable_salary = 'date_enable_salary';
    public static $field_date_enable_workflow = 'date_enable_workflow';
    public static $field_date_enable_employee_contract = 'date_enable_employee_contract';
    public static $field_date_enable_signing = 'date_enable_signing';
    public static $field_date_enable_folder = 'date_enable_signing_folder';
    public static $field_date_enable_folder_range = 'date_enable_signing_folder_range';
    public static $field_date_enable_year_end_tax_adjustment = 'date_enable_year_end_tax_adjustment';
    public static $field_date_enable_ultra = 'date_enable_ultra';
    public static $field_created_at = 'created_at';
    public static $field_deleted_at = 'deleted_at';


    public $table = 'plans';
    public $primary_key = 'id';
    public $protected = array('id');

    public $rules =  array(
        'insert' => array(
            'jinjer_db' => array(
                'field'=>'jinjer_db',
                'label'=>'lang:company_plan_jinjer_db',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'kintai' => array(
                'field'=>'kintai',
                'label'=>'lang:company_plan_kintai',
                'rules'=>'required|is_natural'
            ),
            'roumu' => array(
                'field'=>'roumu',
                'label'=>'lang:company_plan_roumu',
                'rules'=>'required|is_natural'
            ),
            'jinji' => array(
                'field'=>'jinji',
                'label'=>'lang:company_plan_jinji',
                'rules'=>'required|is_natural'
            ),
            'keihi' => array(
                'field'=>'keihi',
                'label'=>'lang:company_plan_keihi',
                'rules'=>'required|is_natural'
            ),
            'my_number' => array(
                'field'=>'my_number',
                'label'=>'lang:company_plan_my_number',
                'rules'=>'required|is_natural'
            ),
            'work_vital' => array(
                'field'=>'work_vital',
                'label'=>'lang:company_plan_work_vital',
                'rules'=>'required|is_natural'
            ),
            'salary' => array(
                'field'=>'salary',
                'label'=>'lang:company_plan_salary',
                'rules'=>'required|is_natural'
            ),
            'workflow' => array(
                'field'=>'workflow',
                'label'=>'lang:company_plan_workflow',
                'rules'=>'required|is_natural'
            ),
            'employee_contract' => array(
                'field'=>'employee_contract',
                'label'=>'lang:company_plan_employee_contract',
                'rules'=>'required|is_natural'
            ),
            'bi' => array(
                'field' => 'bi',
                'label' => 'lang:company_plan_bi',
                'rules' => 'required|is_natural',
            ),
            'signing' => array(
                'field' => 'signing',
                'label' => 'lang:company_plan_signing',
                'rules' => 'required|is_natural',
            ),
            'signing_folder' => array(
                'field' => 'signing_folder',
                'label' => 'lang:company_plan_signing_folder',
                'rules' => 'required|is_natural',
            ),
            'signing_folder_range' => array(
                'field' => 'signing',
                'label' => 'lang:company_plan_signing_folder_range',
                'rules' => 'required|is_natural',
            ),
            'year_end_tax_adjustment' => array(
                'field'=>'year_end_tax_adjustment',
                'label'=>'lang:company_plan_year_end_tax_adjustment',
                'rules'=>'required|is_natural'
            ),
            'ultra' => array(
                'field' => 'ultra',
                'label' => 'lang:company_ultra_option',
                'rules' => 'required|is_natural',
            ),
            'ai_ocr' => array(
                'field' => 'ai_ocr',
                'label' => 'lang:company_option_AI_OCR',
                'rules' => 'is_natural',
            ),
            'ebook_store' => array(
                'field' => 'ebook_store',
                'label' => 'lang:company_option_ebook_store',
                'rules' => 'is_natural',
            ),
            'credit_card' => array(
                'field' => 'credit_card',
                'label' => 'lang:company_option_credit_card',
                'rules' => 'is_natural',
            ),
            'timestamp' => array(
                'field' => 'timestamp',
                'label' => 'lang:company_option_timestamp',
                'rules' => 'is_natural',
            ),
            'docomo' => [
                'field'=>'docomo',
                'label'=>'lang:company_docomo_option',
                'rules'=>'required|in_list[0,1]',
            ],
            'd_account_id' => [
                'field'=>'d_account_id',
                'label'=>'lang:company_d_account_id',
                'rules'=>'trim|max_length[100]|callback_required_with[docomo]|plans_validate[d_account_id]',
            ],
            'kintai_sense_link' =>[
                'field'=>'kintai_sense_link',
                'label'=>'lang:company_plan_kintai_sense_link',
                'rules'=>'required|is_natural|in_list[0,1]'
            ],
            'kintai_alligate' => [
                'field'=>'kintai_alligate',
                'label'=>'lang:company_plan_kintai_alligate',
                'rules'=>'required|is_natural|in_list[0,1]'
            ],
        ),

        'update' => array(
            'plan_id' => [
                'field'=>'id',
                'label'=>'id',
                'rules'=>'trim|required|is_natural_no_zero',
            ],
            'jinjer_db' => array(
                'field'=>'jinjer_db',
                'label'=>'lang:company_plan_jinjer_db',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'kintai' => array(
                'field'=>'kintai',
                'label'=>'lang:company_plan_kintai',
                'rules'=>'required|is_natural'
            ),
            'roumu' => array(
                'field'=>'roumu',
                'label'=>'lang:company_plan_roumu',
                'rules'=>'required|is_natural'
            ),
            'jinji' => array(
                'field'=>'jinji',
                'label'=>'lang:company_plan_jinji',
                'rules'=>'required|is_natural'
            ),
            'keihi' => array(
                'field'=>'keihi',
                'label'=>'lang:company_plan_keihi',
                'rules'=>'required|is_natural'
            ),
            'my_number' => array(
                'field'=>'my_number',
                'label'=>'lang:company_plan_my_number',
                'rules'=>'required|is_natural'
            ),
            'work_vital' => array(
                'field'=>'work_vital',
                'label'=>'lang:company_plan_work_vital',
                'rules'=>'required|is_natural'
            ),
            'salary' => array(
                'field'=>'salary',
                'label'=>'lang:company_plan_salary',
                'rules'=>'required|is_natural'
            ),
            'workflow' => array(
                'field'=>'workflow',
                'label'=>'lang:company_plan_workflow',
                'rules'=>'required|is_natural'
            ),
            'employee_contract' => array(
                'field'=>'employee_contract',
                'label'=>'lang:company_plan_employee_contract',
                'rules'=>'required|is_natural'
            ),
            'bi' => array(
                'field' => 'bi',
                'label' => 'lang:company_plan_bi',
                'rules' => 'required|is_natural',
            ),
            'signing' => array(
                'field' => 'signing',
                'label' => 'lang:company_plan_signing',
                'rules' => 'required|is_natural',
            ),
            'signing_folder' => array(
                'field' => 'signing_folder',
                'label' => 'lang:company_plan_signing_folder',
                'rules' => 'required|is_natural',
            ),
            'signing_folder_range' => array(
                'field' => 'signing',
                'label' => 'lang:company_plan_signing_folder_range',
                'rules' => 'required|is_natural',
            ),
            'year_end_tax_adjustment' => array(
                'field'=>'year_end_tax_adjustment',
                'label'=>'lang:company_plan_year_end_tax_adjustment',
                'rules'=>'required|is_natural'
            ),
            'agreement_id' => array(
                 'field'=>'agreement_id',
                 'label'=>'agreement_id',
                 'rules'=>'required|is_natural_greater_than_zero'
            ),
            'company_id' => array(
                'field'=>'company_id',
                'label'=>'plan_company_id',
                'rules'=>'required|is_natural_greater_than_zero'
            ),
            'ultra' => array(
                'field' => 'ultra',
                'label' => 'lang:company_ultra_option',
                'rules' => 'required|is_natural',
            ),
            'ai_ocr' => array(
                'field' => 'ai_ocr',
                'label' => 'lang:company_option_AI_OCR',
                'rules' => 'is_natural',
            ),
            'ebook_store' => array(
                'field' => 'ebook_store',
                'label' => 'lang:company_option_ebook_store',
                'rules' => 'is_natural',
            ),
            'credit_card' => array(
                'field' => 'credit_card',
                'label' => 'lang:company_option_credit_card',
                'rules' => 'is_natural',
            ),
            'timestamp' => array(
                'field' => 'timestamp',
                'label' => 'lang:company_option_timestamp',
                'rules' => 'is_natural',
            ),
            'docomo' => [
                'field'=>'docomo',
                'label'=>'lang:company_docomo_option',
                'rules'=>'required|in_list[0,1]',
            ],
            'd_account_id' => [
                'field'=>'d_account_id',
                'label'=>'lang:company_d_account_id',
                'rules'=>'trim|max_length[100]|callback_required_with[docomo]|plans_validate[d_account_id]',
            ],
            'kintai_sense_link' => array(
                'field'=>'kintai_sense_link',
                'label'=>'lang:company_plan_kintai_sense_link',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
            'kintai_alligate' => array(
                'field'=>'kintai_alligate',
                'label'=>'lang:company_plan_kintai_alligate',
                'rules'=>'required|is_natural|in_list[0,1]'
            ),
        ),
    );

    /**
     * Plan_model constructor.
     */
    public function __construct()
    {
        $this->timestamps = true;
        $this->soft_deletes = true;
        parent::__construct();
    }

    public function validate($str, $field)
    {
        switch ($field) {
            case 'd_account_id':
                if (!empty($this->input->post())) {
                    $id = $this->input->post('plan_id');
                }
                $sql = "SELECT id
                        FROM `plans`
                        WHERE BINARY `d_account_id` = ?
                        AND plans.deleted_at IS NULL";
                $this->form_validation->set_message('plans_validate', $this->lang->line('d_account_id_unique_error'));
                if (!empty($id)) {
                    $sql .=  " AND id != ?";
                    $result = $this->db->query($sql, [$str, $id])->row();
                } else {
                    $result = $this->db->query($sql, [$str])->row();
                }
                return !$result;

            default:
                break;
        }
        return true;
    }
}
