<div class="breadcrumb-holder">
    <h1 class="main-title float-left"><?= $content_title ?? ''; ?> </h1>
    <?php if(isset($breadcrumb)): ?>
    <ol class="breadcrumb float-right">
        <?php foreach ($breadcrumb as $menu): ?>
        <li class="breadcrumb-item <?= isset($menu['active']) ? 'active' : '' ?>">
            <a href="<?= site_url($menu['link'])?>"> <?= $menu['text']?> </a>
        </li>
        <?php endforeach;?>
    </ol>
    <?php endif;?>
    <div class="clearfix"></div>
</div>

