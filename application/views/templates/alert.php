<?php
if($this->session->flashdata('alert')):
    $type = $this->session->flashdata('alert')['type'];
    $message = $this->session->flashdata('alert')['message'];
    ?>

<div class="alert alert-<?= $type ?> alert-dismissible fade show" role="alert">
    <?= $message ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>

<?php endif; ?>