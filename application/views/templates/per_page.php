<label class='d-inline'><?= $this->lang->line('pagination_per_page_title') ?></label>
<select class="form-control d-inline per-page" style="width:80px;">
    <?php

    $min_item = $this->config->item('min_item_per_page');
    $max_item = $this->config->item('max_item_per_page');


    for ($page = $min_item; $page <= $max_item ; $page = $page + 10) {
        $selected = $per_page == $page ? 'selected' : '';
        echo "<option value='{$page}' {$selected}>{$page}{$this->lang->line('pagination_per_page_item')}</option>";
    }
    ?>

</select>

<script>
    $("select.per-page").on("change", function(){

        var location = window.location;
        var url = location.pathname;
        if (url.match(/^search\/([0-9]+)$/)) {
            url = url.replace(/^search\/([0-9]+)$/, '/search/1');
        } else {
            url = url.replace(/([0-9]+)/, '1');
        }
        var base_url = location.origin + url;

        var params = new window.URLSearchParams(location.search);
        params.set("item_page", $(this).val());
        var param_obj = {};

        params.forEach(function (value,key) {
            param_obj[key] = value;

        });

        var url_encoded = $.param( param_obj, true );
        document.location.href = base_url + "?" + url_encoded;

    });
</script>