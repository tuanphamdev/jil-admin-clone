<?php
$menu_manage_company_class = in_array($this->uri->segment(1), ['companies', 'tokyo_marines', 'admin_employees'] ) ? 'active': '';
$company_class = $this->uri->segment(1) === 'companies' ? 'active': '';
$tokyo_marine_class = $this->uri->segment(1) === 'tokyo_marines' ? 'active': '';
$employee_admin_role_class = $this->uri->segment(1) === 'admin_employees' ? 'active': '';

$export_csv_class = $this->uri->segment(1) === 'export_csv' ? 'active': '';
$import_csv_class = $this->uri->segment(1) === 'import_banks' ? 'active': '';

$config_class = in_array($this->uri->segment(1), ['users', 'roles', 'ips', 'informations', 'qa_engine', 'survey_monkey'] ) ? 'active': '';
$ts_password_class = in_array($this->uri->segment(1), ['ts_password_change', 'ts_password_ips', 'ts_password_logs'] ) ? 'active': '';
$user_class = $this->uri->segment(1) === 'users' ? 'active': '';
$role_class = $this->uri->segment(1) === 'roles' ? 'active': '';
$ip_class = $this->uri->segment(1) === 'ips' ? 'active': '';
$ts_password_change = $this->uri->segment(1) === 'ts_password_change' ? 'active': '';
$ts_password_ips = $this->uri->segment(1) === 'ts_password_ips' ? 'active': '';
$ts_password_logs = $this->uri->segment(1) === 'ts_password_logs' ? 'active': '';

$information_class = $this->uri->segment(1) === 'informations' ? 'active': '';
$qa_engine = $this->uri->segment(1) === 'qa_engine' ? 'active': '';
$survey_monkey = $this->uri->segment(1) === 'survey_monkey' ? 'active': '';

?>
<!-- Left Sidebar -->
<div class="left main-sidebar">
    <div class="sidebar-inner leftscroll">
        <div id="sidebar-menu">
            <ul>
                <?php if($permissions['company'] != NON_PERMISSION || $permissions['tokyo_marine'] != NON_PERMISSION || $permissions['employee'] != NON_PERMISSION): ?>
                <li class="submenu">
                    <a href="#" class="<?= $menu_manage_company_class ?>"><i class="fa fa-fw fa-credit-card"></i> <span><?= $this->lang->line('menu_manage_company')?></span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <?php if($permissions['company'] != NON_PERMISSION): ?>
                                <li class="<?= $company_class ?>"><a href="<?= site_url('companies') ?>"><?= $this->lang->line('menu_company')?></a></li>
                            <?php endif;?>
                            <?php if($permissions['tokyo_marine'] != NON_PERMISSION): ?>
                                <li class="<?= $tokyo_marine_class ?>"><a href="<?= site_url('tokyo_marines') ?>"><?= $this->lang->line('menu_tokyo_marine')?></a></li>
                            <?php endif;?>
                            <?php if($permissions['employee'] != NON_PERMISSION): ?>
                                <li class="<?= $employee_admin_role_class ?>"><a href="<?= site_url('admin_employees') ?>"><?= $this->lang->line('menu_admin_employee')?></a></li>
                            <?php endif;?>
                        </ul>
                </li>
                <?php endif;?>

                <?php if($permissions['csv'] != NON_PERMISSION): ?>
                <li class="submenu">
                    <a href="#" class="<?= $export_csv_class ?>"><i class="fa fa-fw fa-file-text-o"></i> <span><?= $this->lang->line('menu_export_csv')?></span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <?php if($permissions['csv'] != NON_PERMISSION): ?>
                                <li class="<?= $export_csv_class ?>"><a href="<?= site_url('export_csv') ?>"><?= $this->lang->line('menu_export_csv_saleforce')?></a></li>
                            <?php endif;?>
                        </ul>
                </li>
                <?php endif;?>

                <?php if($permissions['import_bank'] != NON_PERMISSION): ?>
                <li class="submenu">
                    <a href="#" class="<?= $import_csv_class ?>"><i class="fa fa-fw fa-upload"></i> <span><?= $this->lang->line('menu_upload')?></span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <?php if($permissions['import_bank'] != NON_PERMISSION): ?>
                                <li class="<?= $import_csv_class ?>"><a href="<?= site_url('import_banks') ?>"><?= $this->lang->line('menu_import_bank_csv')?></a></li>
                            <?php endif;?>
                        </ul>
                </li>
                <?php endif;?>

                <?php if($permissions['user'] != NON_PERMISSION || $permissions['role'] != NON_PERMISSION
                            || $permissions['ip'] != NON_PERMISSION || $permissions['information'] != NON_PERMISSION
                            || $permissions['qa_engine'] != NON_PERMISSION || $permissions['survey_monkey'] != NON_PERMISSION):?>
                <li class="submenu">
                    <a href="#" class="<?= $config_class ?>"><i class="fa fa-fw fa-gears"></i> <span><?= $this->lang->line('menu_config')?></span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <?php if($permissions['user'] != NON_PERMISSION): ?>
                            <li class="<?= $user_class ?>" ><a href="<?= site_url('users') ?>"><?= $this->lang->line('menu_user')?></a></li>
                            <?php endif;?>
                            <?php if($permissions['role'] != NON_PERMISSION): ?>
                            <li class="<?= $role_class ?>"><a href="<?= site_url('roles') ?>"><?= $this->lang->line('menu_role')?></a></li>
                            <?php endif;?>
                            <?php if($permissions['ip'] != NON_PERMISSION): ?>
                            <li class="<?= $ip_class ?>"><a href="<?= site_url('ips') ?>"><?= $this->lang->line('menu_ip')?></a></li>
                            <?php endif;?>
                            <?php if($permissions['information'] !== NON_PERMISSION):
                                ?>
                                <li class="<?= $information_class ?>"><a href="<?= site_url('informations') ?>"><?= $this->lang->line('menu_information')?></a></li>
                            <?php endif;?>
                            <?php if($permissions['qa_engine'] != NON_PERMISSION): ?>
                                <li class="<?= $qa_engine ?>"><a href="<?= site_url('qa_engine') ?>"><?= $this->lang->line('menu_qa_engine')?></a></li>
                            <?php endif;?>
                            <?php if($permissions['survey_monkey'] != NON_PERMISSION): ?>
                                <li class="<?= $survey_monkey ?>"><a href="<?= site_url('survey_monkey') ?>"><?= $this->lang->line('menu_survey_monkey')?></a></li>
                            <?php endif;?>
                        </ul>
                </li>
                <?php endif;?>

                <?php if($permissions['ts_password_change'] != NON_PERMISSION || $permissions['ts_password_ips'] != NON_PERMISSION
                    || $permissions['ts_password_logs'] != NON_PERMISSION):
                    ?>
                <li class="submenu">
                    <a href="#" class="<?= $ts_password_class ?>"><i class="fa fa-fw fa-gears"></i> <span><?= $this->lang->line('menu_ts_password')?></span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <?php if($permissions['ts_password_change'] != NON_PERMISSION): ?>
                            <li class="<?= $ts_password_change ?>" ><a href="<?= site_url('ts_password_change') ?>"><?= $this->lang->line('menu_ts_password_change')?></a></li>
                            <?php endif;?>
                            <?php if($permissions['ts_password_ips'] != NON_PERMISSION): ?>
                            <li class="<?= $ts_password_ips ?>"><a href="<?= site_url('ts_password_ips') ?>"><?= $this->lang->line('menu_ts_password_ips')?></a></li>
                            <?php endif;?>
                            <?php if($permissions['ts_password_logs'] != NON_PERMISSION): ?>
                            <li class="<?= $ts_password_logs ?>"><a href="<?= site_url('ts_password_logs').'?item_page=20' ?>"><?= $this->lang->line('menu_ts_password_logs')?></a></li>
                            <?php endif;?>
                        </ul>
                </li>
                <?php endif;?>
            </ul>
        </div>
    </div>
</div>
<!-- End Sidebar -->
