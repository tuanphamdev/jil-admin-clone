<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/top');?>
<?php $this->load->view('templates/left_menu');?>
<div class="content-page">

	<!-- Start content -->
	<div class="content">

		<div class="container-fluid">


			<div class="row">
					<div class="col-xl-12">
                        <?php $this->load->view('templates/breadcrumb');?>
					</div>
			</div>
			<!-- end row -->


			<div class="row">
					<div class="col-xl-12">

                        <?php echo $body;?>
                     </div>
			</div>



		</div>
		<!-- END container-fluid -->

	</div>
	<!-- END content -->

</div>
<!-- END content-page -->
<?php $this->load->view('templates/footer');?>