<?php echo form_open($modal_action ?? '', ['method' => 'POST']); ?>

    <div class="modal fade" id="default_modal" tabindex="-1" role="dialog" aria-labelledby="default_modal_label" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="default_modal_label"><?= $modal_title ?? '' ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><?= $modal_message ?? '' ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('ui_close_btn') ?></button>
                    <button type="button" class="btn btn-primary"><?= $this->lang->line('ui_save_btn') ?></button>
                </div>
            </div>
        </div>
    </div>
</form>