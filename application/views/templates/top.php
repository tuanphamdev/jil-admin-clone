<!-- top bar navigation -->
<div class="headerbar">

<!-- LOGO -->
<div class="headerbar-left">
    <a href="#" class="logo"><img alt="Logo" src="<?= site_url('assets/images/logo.png') ?>" /> <span>Admin</span></a>
</div>

<nav class="navbar-custom">

    <ul class="list-inline float-right mb-0">

        <li class="list-inline-item dropdown notif">
            <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
            <div class="align_middle"><i class="fa fa-fw fa-user-circle-o"></i> <?= $this->session->userdata('user_login')['user_name'] ?></div>
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                <!-- item-->
                <!-- <div class="dropdown-item noti-title">
                    <h5 class="text-overflow"><small>Hello, admin</small> </h5>
                </div> -->

                <!-- item-->
                <!-- <a href="pro-profile.html" class="dropdown-item notify-item">
                    <i class="fa fa-user"></i> <span>Profile</span>
                </a> -->

                <!-- item-->
                <a href="<?= site_url('logout')?>" class="dropdown-item notify-item bg-warning">
                    <i class="fa fa-sign-out"></i> <span><?= $this->lang->line('btn_logout')?></span>
                </a>
                
                <!-- item-->
                <!-- <a target="_blank" href="https://www.pikeadmin.com" class="dropdown-item notify-item">
                    <i class="fa fa-external-link"></i> <span>Pike Admin</span>
                </a> -->
            </div>
        </li>

    </ul>

    <ul class="list-inline menu-left mb-0">
        <li class="float-left">
            <button class="button-menu-mobile open-left">
                <i class="fa fa-fw fa-bars"></i>
            </button>
        </li>                        
    </ul>

</nav>

</div>
<!-- End Navigation -->