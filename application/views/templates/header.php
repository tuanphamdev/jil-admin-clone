<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<title>jinjer Admin | <?= $content_title ?? ''; ?> </title>
		<meta name="description" content="Jinjer Admin, Neolab VN, NeoCareer">
        <meta name="author" content="Neolab VN">
        <meta name="robots" content="noindex">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?= site_url('assets/images/favicon.ico')?>">
		
		<!-- Bootstrap CSS -->
		<link href="<?= site_url('assets/css/bootstrap.min.css' )?>" rel="stylesheet" type="text/css" />

		<!-- Bootstrap datepicker CSS -->
		<link href="<?= site_url('assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css')?>" rel="stylesheet" type="text/css" />
		
		<!-- Font Awesome CSS -->
		<link href="<?= site_url('assets/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
		
		<!-- Custom CSS -->
		<link href="<?= site_url('assets/css/style.css') ?> " rel="stylesheet" type="text/css" />
        <link href="<?= site_url('assets/css/custom.css') ?> " rel="stylesheet" type="text/css" />
		<!-- Loading CSS -->
		<link href="<?= site_url('assets/css/jquery.loadingModal.min.css') ?> " rel="stylesheet" type="text/css" />
		<!-- UI checkbox, radio bootstrap CSS -->
		<link href="<?= site_url('assets/css/bootstrap-checkbox.css') ?> " rel="stylesheet">
		<link href="<?= site_url('assets/css/custom-select2.css') ?> " rel="stylesheet">
		<link href="<?= site_url('/assets/plugins/select2/css/select2.min.css') ?> " rel="stylesheet" type="text/css"/>
		<!-- <link href="assets/plugins/jquery.filer/css/jquery.filer.css" rel="stylesheet" /> -->

		<!-- BEGIN CSS for this page -->
		<link href="<?= site_url('assets/plugins/jquery.filer/css/jquery.filer.css') ?>" rel="stylesheet" />
		<link href="<?= site_url('assets/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css') ?>" rel="stylesheet" />
		<!-- BEGIN CSS for this page -->
		<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/> -->
		<!-- END CSS for this page -->

		<script src="<?= site_url('assets/js/jquery.min.js') ?> "></script>
		<script src="<?= site_url('assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')?> "></script>
		<script src="<?= site_url('assets/bootstrap-datepicker-master/dist/locales/bootstrap-datepicker.ja.min.js') ?> "></script>
		
</head>

<body class="adminbody">
<!-- START main -->
<div id="main">

<script>
	function setCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + (value || "") + expires + "; path=/";
	}

	function getCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}

	function eraseCookie(name) {
		document.cookie = name + '=; Max-Age=-99999999;';
	}

	var d = new Date();
	var dtz = -(d.getTimezoneOffset()) / 60;

	if (getCookie("jinjer_local_timezone") != dtz) {
		setCookie("jinjer_local_timezone", dtz, 30);
	}
</script>