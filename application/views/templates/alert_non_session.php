<div class="non-session alert alert-dismissible fade show" role="alert" style="display: none">
    <div class="message-content"></div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
