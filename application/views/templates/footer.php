<footer class="footer">
		<span class="text-right">
		<strong>Copyright <i class="fa fa-copyright bigfonts" aria-hidden="true"></i> jinjer Admin.</strong> All rights reserved.
		</span>
	</footer>

</div>
<!-- END main -->
<script src="<?= site_url('assets/js/custom.js') ?>"></script>
<script src="<?= site_url('assets/js/modernizr.min.js') ?>"></script>
<script src="<?= site_url('assets/js/moment.min.js') ?>"></script>
		
<script src="<?= site_url('assets/js/popper.min.js') ?>"></script>
<script src="<?= site_url('assets/js/bootstrap.min.js') ?>"></script>

<script src="<?= site_url('assets/js/detect.js') ?>"></script>
<script src="<?= site_url('assets/js/fastclick.js') ?>"></script>
<script src="<?= site_url('assets/js/jquery.blockUI.js') ?>"></script>
<script src="<?= site_url('assets/js/jquery.nicescroll.js') ?>"></script>
<script src="<?= site_url('assets/js/jquery.loadingModal.min.js') ?>"></script>
<!-- BEGIN Java Script for this page -->
<script src="<?= site_url('assets/plugins/jquery.filer/js/jquery.filer.min.js') ?>"></script>

<!-- App js -->
<script src="<?= site_url('assets/js/pikeadmin.js') ?>"></script>
<!-- BEGIN Java Script for this page -->
	<!-- <script src="<?= site_url('') ?>https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src="<?= site_url('') ?>https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script> -->
<!-- END Java Script for this page -->
<script src="<?= site_url('assets/plugins/select2/js/select2.min.js') ?>"></script>
<script src="<?= site_url('assets/js/select2_custom.js') ?>"></script>

</body>
</html>
<script>
