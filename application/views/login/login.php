<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>

    <!-- Core CSS -->
    <link href="<?= site_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= site_url('assets/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?= site_url('assets/images/favicon.ico') ?>">
	<!-- Custom CSS -->
	<link href="<?= site_url('assets/css/login.css') ?>" rel="stylesheet">

    <!-- Checkboxes style -->
    <link href="<?= site_url('assets/css/bootstrap-checkbox.css') ?>" rel="stylesheet">
    <script src="<?= site_url('assets/js/jquery.min.js') ?> "></script>
</head>

<body>
<div class="container h-100">
    <div class="row justify-content-center align-items-center logo_login">
        <img src="<?= site_url('assets/images/logo.png') ?>" class="img-fluid" >
    </div>
    <?php echo form_open("/do_login", ['novalidate' => '']); ?>
        <div class="row justify-content-center align-items-center">
            <div class="card login-form ">
                <h4 class="card-header text-center">Sign In</h4>

                <div class="card-body">
                    <?php $this->load->view('templates/alert');?>
                            <div class="row">
                                <div class="col-md-12">
                                <div class="form-group">
                                <label>メールアドレス</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope-open" aria-hidden="true"></i></span>
                                    <input value="<?= $input['login_email'] ?? '' ?>" type="email" class="form-control" name="login_email" data-error="Input valid email" required="">
                                </div>
                                <div class="help-block with-errors text-danger"></div>
                                </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>パスワード</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-unlock" aria-hidden="true"></i></span>
                                            <input value="<?= $input['login_password'] ?? '' ?>" type="password" id="inputPassword" data-minlength="6" name="login_password" class="form-control" data-error="Password to short" required="">
                                        </div>
                                        <div class="help-block with-errors text-danger"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="checkbox checkbox-primary">
                                    <input id="checkbox_remember" type="checkbox" name="remember" <?= isset($input['remember']) && $input['remember'] === 'on' ? 'checked': '' ?> >
                                    <label for="checkbox_remember"> 次回から入力を省略</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                <input type="submit" class="btn btn-primary btn-lg btn-block" value="ログイン" name="submit">
                                </div>
                            </div>
                    <div class="clear"></div>
                    <a href="<?= site_url('forgot_password')?>">パスワードを忘れた場合</a>
                </div>
            </div>
        </div>
    <?php echo form_close() ?>
    <script src="<?= site_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?= site_url('assets/js/start_session.js') ?>"></script>

</div>

</body>

</html>