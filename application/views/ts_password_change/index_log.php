<?php
$segment = 2;
$page = 'ts_password_change';
if($this->uri->segment(2) == 'search'){
	$segment = 3;
	$page = 'ts_password_change/search';
}
?>		
	<div class="card mb-3">
        <div class="card-body">
            <div id="accordion" role="tablist">
                <div class="">
                <div class="" role="tab" id="headingOne">
				<a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
					<i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('ts_password_log_search_title')?>
				</a>
                </div>
                <form action="/ts_password_change/search" method="GET" autocomplete="off">
                    <div id="collapseOne" class="collapse  <?= $segment == 3 ? 'show' : '';?>" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                        <div class="card-body">
                            <input type="hidden" name="item_page" value="<?= $data['per_page'] ?? 0 ?>" class="form-control">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('ts_password_log_code')?></label>
                                    <input type="text" name="code" value="<?= $input['code'] ?? '' ?>" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('ts_password_log_name')?></label>
                                    <input type="text" name="name" value="<?= $input['name'] ?? '' ?>" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">アクセス期間(from)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input name='use_start'' id='use_start' value="<?= $input['use_start'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">アクセス期間(to)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input name='use_end' id='use_end' value="<?= $input['use_end'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>
                            </div>
                            <div class="form text-right">
                                <button role="button" class="btn btn-info" href="#"><i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('ts_password_log_search_btn')?></button>

                                <a class="btn btn-secondary" href="/ts_password_change"><i class="fa fa-fw fa-close"> </i> <?= $this->lang->line('ts_password_log_clear_btn')?></a>
                            </div>
                        </div>
                    </div>
				
			    </form>

                </div>
            </div>
        </div>							
    </div>

		<div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <?php $this->load->view('templates/per_page', ['per_page' => $data['per_page'] ?? 0]);?>
                    </div>
                </div>
            </div>
			<div class="card-body">
                <?php $this->load->view('templates/alert');?>

				<table class="table table-responsive-col table-striped">
					<thead>
					<tr>
						<th scope="col-2"><?= $this->lang->line('ts_password_log_code')?></th>
						<th scope="col-1"><?= $this->lang->line('ts_password_log_name')?></th>
						<th scope="col-1">アクセス期限</th>
						<th scope="col-1">アクション</th>
					</tr>
					</thead>
					<tbody>
				<?php 
				if(!empty($data['ts_password_log'])):
                foreach ($data['ts_password_log'] as $key => $value):
                ?>
					<tr>
						<td><a href="<?= site_url('/companies/update/'.$value["id"])?>"><?= $value['code']??''?></a></td>
						<td><?= $value['name']??''?></td>
						<td>
                            <?= $value['ts_password_use_end'] ?? '' ?>
                        </td>
						<td>
                            <?php
                                $modal_id = '';
                                $button_class = 'warning';
                                $button_label = $this->lang->line('ts_pw_change_button_list_emp');
                            ?>

                            <?php if($permissions['ts_password_change'] == FULL_PERMISSION):?>
                                <a href="<?= site_url('/ts_password_change/change?company_id='.$value["company_id"])?>">
                                <button  role="button" href="<?= site_url('/ts_password_change/change?company_id='.$value["company_id"])?>" class="btn btn-<?= $button_class ?>">
								<?= $button_label ?>
							    </button>
                            </a>
                            <?php endif; ?>


						</td>
					</tr>
				<?php 
				endforeach;
				endif;
				?>

                <?php if(empty($data['ts_password_log'])):?>
                <tr>
                    <td colspan="12">
                <?= $this->lang->line('ts_password_log_no_result_found')?>
                    </td>
                </tr>
                <?php endif; ?>
					
					</tbody>
				</table>

				<!-- Pagination -->
                <?php
                $pagination =  pagination(base_url($page), $data['total'] ?? 0 , $data['per_page'] ?? 0, $segment);
                ?>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <?= $pagination['statistic'];?>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <nav aria-label="">
                            <?= $pagination['pagination'];?>
                        </nav>
                    </div>
                </div>

				<!-- End Pagination -->

			</div>
		</div>

        <script src="<?= site_url('assets/js/companies/custom.js') ?>"></script>
