<script>
    $("button[data-toggle='modal']").click(function(){
        var core_company_id = $(this).attr("data-core-company-id");
        var emp_id = $(this).attr("data-emp-id");
        $("#form_ts_password_change input[name='core_company_id']").val(core_company_id);
        $("#form_ts_password_change input[name='employee_id']").val(emp_id);
    });
</script>

<?php echo form_open("/ts_password_change/change_password/".$this->input->get('company_id'), ['autocomplete' => 'off', 'method' => 'POST', 'id' => 'form_ts_password_change']); ?>
    <input type="hidden" value="" name="core_company_id">
    <input type="hidden" value="" name="employee_id">
    <input type="hidden" value="<?= $this->session->userdata('user_login')['user_id']?>" name="user_id">
    <div class="modal fade" id="modal_enable_admin" tabindex="-1" role="dialog" aria-labelledby="modal_start_service_label" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title" id="modal_start_service_label"><span></span><?= $this->lang->line('ts_pw_change_enable_popup_title') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body form-inline">
                    <div class="form-group">
                        <label for="exampleInputName2" style='padding: 0 30px 0 20px'><?= $this->lang->line('ts_pw_change_title') ?>  </label>
                        <input style="width:300px;" type="password" class="form-control" placeholder='<?= $this->lang->line('ts_pw_change_title') ?>' name="ts_password" style='padding: 0 15px 0 15px' maxlength='30'>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('ui_close_btn') ?></button>
                    <button type="submit" class="btn btn-success"><?= $this->lang->line('ts_pw_change_button_change') ?></button>
                </div>
            </div>
        </div>
    </div>
</form>
