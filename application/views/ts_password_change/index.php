<?php
$segment = 3;
$page = 'ts_password_change/change';
if($this->uri->segment(3) == 'search'){
	$segment = 4;
	$page = 'ts_password_change/change/search';
}

?>
	<div class="card mb-3">
        <div class="card-body">
            <div id="accordion" role="tablist">
                <div class="">
                <div class="" role="tab" id="headingOne">
				<a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
					<i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('company_search_title')?>
				</a>
                </div>
                <form action="/ts_password_change/change/search?company_id=<?= $company_id?>" method="GET" autocomplete="off">
                    <div id="collapseOne" class="collapse  <?= $segment == 4 ? 'show' : '';?>" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                        <div class="card-body">

                             <input type="hidden" name="company_id" value="<?= $company_id??$this->input->get('company_id') ?>" class="form-control">

                            <div class="form-row">
                            <div class="form-group col-md-3">
                                <label class=''><?= $this->lang->line('ts_pw_change_code')?></label>
                                <input type="text" name="employee_code" value="<?= $input['employee_code'] ?? '' ?>" class="form-control" maxlength=50>
                            </div>
                            <div class="form-group col-md-3">
                                <label class=''><?= $this->lang->line('ts_pw_change_emp_name')?></label>
                                <input type="text" name="employee_name" value="<?= $input['employee_name'] ?? '' ?>" class="form-control" maxlength=100>
                            </div>
                            <div class="form-group col-md-3">
                                <label ><?= $this->lang->line('ts_pw_change_enrollment_flag_setting')?></label>
                                <div class="input-group">
                                    <select name="flag_setting" class="form-control filter_text">
                                    <?php
                                        foreach (OPTION_TS_PASSWORD_FLAG_SETTING as $key => $value) {
                                            $selected = !empty($input['flag_setting']) && $key == $input['flag_setting'] ? 'selected' : empty($input['flag_setting']) && $key == 1 ? 'selected' : '' ;
                                            echo "<option {$selected} value={$key}>{$value}</option>";
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>

                            </div>

                            <div class="form text-right">
                                <button role="button" class="btn btn-info" href="#"><i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('ui_search_btn')?></button>

                                <a class="btn btn-secondary" href="<?= '/'.$this->uri->uri_string ?>"><i class="fa fa-fw fa-close"> </i><?= $this->lang->line('ui_clear_btn')?></a>
                            </div>
                        </div>
                    </div>

			    </form>

                </div>
            </div>
        </div>
    </div>

		<div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <?php $this->load->view('templates/per_page', ['per_page' => $data['per_page'] ?? 0]);?>
                    </div>
                </div>
            </div>
			<div class="card-body">
                <?php $this->load->view('templates/alert');?>

				<table class="table table-responsive-col table-striped">
					<thead>
					<tr>
						<th scope="col-2">顔写真</th>
						<th scope="col-1">企業名</th>
						<th scope="col-1">社員番号</th>
						<th scope="col-1">氏名</th>
						<th scope="col-1">所属グループ</th>
						<th scope="col-1">役職</th>
						<th scope="col-1">在籍区分</th>
                        <th scope="col-1">設定区分</th>
                        <th scope="col-1">パスワード設定</th>
					</tr>
					</thead>
					<tbody>
				<?php
				if(!empty($data['ts_password_change'])):
                foreach ($data['ts_password_change'] as $key => $value):
                    $ts_password_type = empty($value['ts_password']) ? 3 : 4;
                ?>
					<tr>
						<td>
                            <div class="personal-profile__image">
                                <img id="avatar-<?= $value['photo_id']??''?>" width="45" height="45" core-company-id="<?= $value['core_company_id'] ?>" core-company-code="<?= $value['company_code'] ?>" src="../../../assets/images/avatars/bg.png" alt="" avatar-id="<?= $value['photo_id']??''?>" data-src="../../../assets/images/avatars/loading_icon.gif">
                            </div>
                        </td>
						<td><?= $value['company_name']??''?></td>
						<td>
                            <?= $value['employee_code'] ?? '' ?>
                        </td>
                        <td><?= $value['full_name'] ?? '' ?></td>
                        <td><?= $value['dept_name'] ?? '' ?></td>
                        <td><?= $value['job_title'] ?? '' ?></td>
						<td>
						<?php
							if(isset($value['enrollment_status']) && $value['enrollment_status'] == 0):
							    echo $this->lang->line('ts_pw_change_enrollment_status_enrolled');
							endif;
							if(isset($value['enrollment_status']) && $value['enrollment_status'] == 1):
							    echo $this->lang->line('ts_pw_change_enrollment_status_retirement');
							endif;
							if(isset($value['enrollment_status']) && $value['enrollment_status'] == 2):
							    echo $this->lang->line('ts_pw_change_enrollment_status_absence');
							endif;
						?>
						</td>
                        <td><?=OPTION_TS_PASSWORD_FLAG_SETTING[$ts_password_type] ?></td>
						<td>
                            <?php
                                $modal_id = 'modal_enable_admin';
                                $button_class = 'warning';
                                $button_label = $this->lang->line('ts_pw_change_button_edit');
                            ?>

                            <?php if($permissions['ts_password_change'] == FULL_PERMISSION):?>
							<button data-core-company-id="<?= $value['core_company_id'] ?>"  data-core-company-code="<?= $value['company_code'] ?>" data-emp-code="<?= $value['employee_code'] ?>" data-emp-id="<?= $value['employee_id'] ?>" data-admin-company-id="<?= $value['admin_company_id'] ?>" data-toggle="modal" data-target="#<?= $modal_id ?>" role="button" href="#" class="btn btn-<?= $button_class ?> btn_change_ts_pass">
								<?= $button_label ?>
							</button>
                            <?php endif; ?>


						</td>
					</tr>
				<?php
				endforeach;
				endif;
				?>

                <?php if(empty($data['ts_password_change'])):?>
                <tr>
                    <td colspan="12">
                <?= $this->lang->line('employee_no_result_found')?>
                    </td>
                </tr>
                <?php endif; ?>

					</tbody>
				</table>

                <?php $this->load->view('ts_password_change/modals/change_password');?>
				<!-- Pagination -->
                <?php
                $pagination =  pagination(base_url($page), $data['total'] ?? 0 , $data['per_page'] ?? 0, $segment);
                ?>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <?= $pagination['statistic'];?>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <nav aria-label="">
                            <?= $pagination['pagination'];?>
                        </nav>
                    </div>
                </div>

				<!-- End Pagination -->

			</div>
		</div>

        <script src="<?= site_url('assets/js/employees/custom.js') ?>"></script>
        <script src="<?= site_url('assets/js/avatar/avatar.js') ?>"></script>
