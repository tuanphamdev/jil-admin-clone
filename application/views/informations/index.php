<?php
$segment = 2;
$page = 'informations';
if($this->uri->segment(2) == 'search'){
	$segment = 3;
	$page = 'informations/search';
}
?>		
	<div class="card mb-3">
        <div class="card-body">
            <div id="accordion" role="tablist">
                <div class="">
                <div class="" role="tab" id="headingOne">
				<a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
					<i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('ui_search_title')?>
				</a>
                </div>
                <form action="/informations/search" method="GET">
                    <div id="collapseOne" class="collapse  <?= $segment == 3 ? 'show' : '';?>" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                        <div class="card-body">

                            <input type="hidden" name="item_page" value="<?= $data['per_page'] ?? 0 ?>" class="form-control">

                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('information_title')?></label>
                                    <input type="text" name="title" value="<?= $input['title'] ?? '' ?>" class="form-control">
                                </div>


                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('information_created_from')?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                        <input name='created_at_from' id='created_at_from' value="<?= $input['created_at_from'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('information_created_to')?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                        <input name='created_at_to' id='created_at_to' value="<?= $input['created_at_to'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>


                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('information_status')?></label>
                                    <select name="is_open" class="form-control">
                                        <option value="" <?= isset($input['is_open']) && $input['is_open'] == '' ? 'selected' : '' ?> ><?= $this->lang->line('ui_option_all')?></option>
                                        <option value=0 <?= isset($input['is_open']) && $input['is_open'] == '0' ? 'selected' : '' ?> ><?= $this->lang->line('information_off')?></option>
                                        <option value=1 <?= isset($input['is_open']) && $input['is_open'] == '1' ? 'selected' : '' ?> ><?= $this->lang->line('information_on')?></option>
                                    </select>
                                </div>

                            </div>
                            
                            <div class="form text-right">
                                <button role="button" class="btn btn-info" href="#"><i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('ui_search_btn')?></button>

                                <a class="btn btn-secondary" href="/informations"><i class="fa fa-fw fa-close"> </i> <?= $this->lang->line('ui_clear_btn')?></a>
                            </div>
                        </div>
                    </div>
				
			    </form>

                </div>
            </div>
        </div>							
    </div>

		<div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <?php $this->load->view('templates/per_page', ['per_page' => $data['per_page'] ?? 0]);?>
                    </div>
                </div>
            </div>
			<div class="card-body">
                <?php $this->load->view('templates/alert');?>

				<table class="table table-responsive-col table-striped">
					<thead>
					<tr>
						<th scope="col-2"><?= $this->lang->line('information_title')?></th>
						<th scope="col-1"><?= $this->lang->line('information_creation')?></th>
						<th scope="col-1"><?= $this->lang->line('information_expiration')?></th>
                        <th scope="col-1"><?= $this->lang->line('information_status')?></th>
                        <th scope="col-1"><?= $this->lang->line('information_edit_function')?></th>

					</tr>
					</thead>
					<tbody>
				<?php 
				if(!empty($data['information'])):
				foreach ($data['information'] as $key => $value):

				?>
					<tr>
                        <td> <a target="_blank" href="<?= $value['link_url']??''?>"> <?= $value['title']??''?> </a> </td>

						<td>
                            <?= convertToLocalTimezone($value['created_at'])??''?>
						</td>
						<td><?= $value['expiration_date']??''?></td>
						<td>
						<?php
							echo isset($value['is_open']) && $value['is_open'] == 1
							? $this->lang->line('information_on')
							: $this->lang->line('information_off');
						?>
						</td>
						<td>
                            <?php
                                $modal_id = isset($value['is_open']) && $value['is_open'] == 1 ? 'modal_off' : 'modal_on';
                                $button_class = isset($value['is_open']) && $value['is_open'] == 1 ? 'warning' : 'success';
                                $button_label = $this->lang->line(isset($value['is_open']) && $value['is_open'] == 1 ? 'information_off' : 'information_on');
                            ?>

                            <?php if($permissions['information'] == FULL_PERMISSION):?>
							<button data-information-title="<?= $value['title'] ?>"  data-information-id="<?= $value['id'] ?>"  data-toggle="modal" data-target="#<?= $modal_id ?>" role="button" href="#" class="btn btn-<?= $button_class ?>">
								<?= $button_label ?>
							</button>
                            <?php endif; ?>


						</td>
					</tr>
				<?php 
				endforeach;
				endif;
				?>

                <?php if(empty($data['information'])):?>
                <tr>
                    <td colspan="12">
                <?= $this->lang->line('information_no_result_found')?>
                    </td>
                </tr>
                <?php endif; ?>
					
					</tbody>
				</table>

                <?php $this->load->view('informations/modals/modal_off');?>
                <?php $this->load->view('informations/modals/modal_on');?>

				<!-- Pagination -->
                <?php
                $pagination =  pagination(base_url($page), $data['total'] ?? 0 , $data['per_page'] ?? 0, $segment);
                ?>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <?= $pagination['statistic'];?>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <nav aria-label="">
                            <?= $pagination['pagination'];?>
                        </nav>
                    </div>
                </div>

				<!-- End Pagination -->

			</div>
		</div>

        <script src="<?= site_url('assets/js/companies/custom.js') ?>"></script>
