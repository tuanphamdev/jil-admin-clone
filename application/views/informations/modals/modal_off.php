<script>
    $("button[data-toggle='modal']").click(function(){
        var information_id = $(this).attr("data-information-id");
        var information_title = $(this).attr("data-information-title");
        $(".modal-body span").html('【'+ information_title +'】');
        $("#form_off input[name='information_id']").val(information_id);
        $("#form_off input[name='information_title']").val(information_title);
    });
</script>

<?php echo form_open('/informations/do_off', ['method' => 'POST', 'id' => 'form_off']); ?>
    <input type="hidden" value="" name="information_id">
    <input type="hidden" value="" name="information_title">
    <div class="modal fade" id="modal_off" tabindex="-1" role="dialog" aria-labelledby="modal_off_label" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_off_label"><span></span><?= $this->lang->line('information_off_title') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><span></span><?= $this->lang->line('information_off_message_line1') ?? '' ?></p>
                    <p><?= $this->lang->line('information_off_message_line2') ?? '' ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('ui_close_btn') ?></button>
                    <button type="submit" class="btn btn-warning"><?= $this->lang->line('ui_stop_btn') ?></button>
                </div>
            </div>
        </div>
    </div>
</form>
