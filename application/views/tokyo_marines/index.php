<?php
$segment = 2;
$page = 'tokyo_marines';
if($this->uri->segment(2) == 'search'){
	$segment = 3;
	$page = 'tokyo_marines/search';
}
?>		
	<div class="card mb-3">
        <div class="card-body">
            <div id="accordion" role="tablist">
                <div class="">
                <div class="" role="tab" id="headingOne">
				<a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
					<i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('company_search_title')?>
				</a>
                </div>
                <form action="/tokyo_marines/search" method="GET" autocomplete="off">
                    <div id="collapseOne" class="collapse  <?= $segment == 3 ? 'show' : '';?>" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                        <div class="card-body">

                            <input type="hidden" name="item_page" value="<?= $data['per_page'] ?? 0 ?>" class="form-control">

                            <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for=""><?= $this->lang->line('company_code')?></label>
                                <input type="text" name="code" value="<?= $input['code'] ?? '' ?>" class="form-control">
                            </div>
                            <div class="form-group col-md-3">
                                <label for=""><?= $this->lang->line('company_name')?></label>
                                <input type="text" name="name" value="<?= $input['name'] ?? '' ?>" class="form-control">
                            </div>

                            <div class="form-group col-md-3">
                                <label for=""><?= $this->lang->line('company_status')?></label>
                                <select name="status" class="form-control">
                                    <option value="" ><?= $this->lang->line('ui_option_all') ?></option>
                                    <option value=0 <?= isset($input['status']) && $input['status'] === '0' ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_active')?></option>
                                    <option value=1 <?= isset($input['status']) && $input['status'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_active')?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for=""><?= $this->lang->line('company_is_tokyo_marine')?></label>
                                <select name="is_tokyo_marine" class="form-control">
                                    <option value="" ><?= $this->lang->line('ui_option_all') ?></option>
                                    <option value=0 <?= isset($input['is_tokyo_marine']) && $input['is_tokyo_marine'] == '0' ? 'selected' : '' ?> ><?= $this->lang->line('tokyo_marine_no_use_flag')?></option>
                                    <option value=1 <?= isset($input['is_tokyo_marine']) && $input['is_tokyo_marine'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('tokyo_marine_use_flag')?></option>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for=""><?= $this->lang->line('company_agreement_start_date_from')?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                        <input name='start_date_from' id='start_date_from' value="<?= $input['start_date_from'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label for=""><?= $this->lang->line('company_agreement_start_date_to')?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                        <input name='start_date_to' id='start_date_to' value="<?= $input['start_date_to'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                </div>
                            </div>

                            <div class="form-group col-md-3">
                                <label for=""><?= $this->lang->line('company_agreement_end_date_from')?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                        <input name='end_date_from' id='end_date_from' value="<?= $input['end_date_from'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <label for=""><?= $this->lang->line('company_agreement_end_date_to')?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                        <input name='end_date_to' id='end_date_to' value="<?= $input['end_date_to'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                </div>
                            </div>

                            


                            </div>
                            
                            <div class="form text-right">
                                <button role="button" class="btn btn-info" href="#"><i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('company_search_btn')?></button>

                                <a class="btn btn-secondary" href="/tokyo_marines"><i class="fa fa-fw fa-close"> </i> <?= $this->lang->line('company_clear_btn')?></a>
                            </div>
                        </div>
                    </div>
				
			    </form>

                </div>
            </div>
        </div>							
    </div>

		<div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <?php $this->load->view('templates/per_page', ['per_page' => $data['per_page'] ?? 0]);?>
                    </div>
                </div>
            </div>
            
			<div class="card-body">
                <?php $this->load->view('templates/alert');?>

				<table class="table table-responsive-col table-striped">
					<thead>
					<tr>
						<th scope="col-2"><?= $this->lang->line('company_code')?></th>
						<th scope="col-1"><?= $this->lang->line('company_name')?></th>
						<th scope="col-1"><?= $this->lang->line('company_application_name_title')?></th>
						<th scope="col-1"><?= $this->lang->line('company_agreement_type')?></th>
						<th scope="col-1"><?= $this->lang->line('company_service_type')?></th>
						<th scope="col-1"><?= $this->lang->line('company_agreement_num_account')?></th>
						<th scope="col-1"><?= $this->lang->line('company_status')?></th>
						<th scope="col-1"><?= $this->lang->line('company_edit_function')?></th>
					</tr>
					</thead>
					<tbody>
				<?php 
				if(!empty($data['company'])):
				foreach ($data['company'] as $key => $value):

                ?>
					<tr>
						<td><?= $value['code']??''?></td>
						<td><?= $value['name']??''?></td>
						<td>
                            <?= $value['application_furigana_surname'] ?? '' ?>
                            <?= $value['application_furigana'] ?? '' ?>
                            <br/>
                            <?= $value['application_surname'] ?? '' ?>
                            <?= $value['application_name'] ?? '' ?>
                        </td>
						<td>
						<?php 
							$agree_type = $this->config->item('agreement_type')[$value['type']??''];
							echo $this->lang->line('company_agree_'.$agree_type);
						?>
						</td>
						<td>
						<?php
                        if(!empty($value['jinjer_db'])){
                            echo '<p class="h6">'.$this->lang->line('company_plan_jinjer_db').'</p>';
                        }
                        if(!empty($value['kintai'])){
                            echo '<p class="h6">'.$this->lang->line('company_plan_kintai').'</p>';
                        }
                        if(!empty($value['roumu'])){
                            echo '<p class="h6">'.$this->lang->line('company_plan_roumu').'</p>';
                        }
                        if(!empty($value['jinji'])){
                            echo '<p class="h6">'.$this->lang->line('company_plan_jinji').'</p>';
                        }
                        if(!empty($value['keihi'])){
                            echo '<p class="h6">'.$this->lang->line('company_plan_keihi').'</p>';
                        }
                        if(!empty($value['my_number'])){
                            echo '<p class="h6">'.$this->lang->line('company_plan_my_number').'</p>';
                        }
                        if(!empty($value['work_vital'])){
                            echo '<p class="h6">'.$this->lang->line('company_plan_work_vital').'</p>';
                        }
						?>
						</td>
						<td><?= $value['num_account']??''?></td>
						<td>
						<?php
							echo isset($value['is_tokyo_marine']) && $value['is_tokyo_marine'] == 1 
							? $this->lang->line('tokyo_marine_use_flag') 
							: $this->lang->line('tokyo_marine_no_use_flag');
						?>
						</td>
						<td>
                            <?php
                                $modal_id = isset($value['is_tokyo_marine']) && $value['is_tokyo_marine'] == 1 ? 'modal_no_use_tokyo_marine' : 'modal_use_tokyo_marine';
                                $button_class = isset($value['is_tokyo_marine']) && $value['is_tokyo_marine'] == 1 ? 'warning' : 'success';
                                $button_label = $this->lang->line(isset($value['is_tokyo_marine']) && $value['is_tokyo_marine'] == 1 ? 'tokyo_marine_use_flag' : 'tokyo_marine_no_use_flag');
                            ?>

                            <?php if($permissions['tokyo_marine'] == FULL_PERMISSION):?>
							<button data-company-name="<?= $value['name'] ?>" data-start-date="<?= $value['start_date'] ?>"  data-end-date="<?= $value['end_date'] ?>" data-company-id="<?= $value['id'] ?>"  data-company-code="<?= $value['code'] ?>" data-toggle="modal" data-target="#<?= $modal_id ?>" role="button" href="#" class="btn btn-<?= $button_class ?>">
								<?= $button_label ?>
							</button>
                            <?php endif; ?>


						</td>
					</tr>
				<?php 
				endforeach;
				endif;
				?>

                <?php if(empty($data['company'])):?>
                <tr>
                    <td colspan="12">
                <?= $this->lang->line('company_no_result_found')?>
                    </td>
                </tr>
                <?php endif; ?>
					
					</tbody>
				</table>

				<!-- Pagination -->
                <?php
                $pagination =  pagination(base_url($page), $data['total'] ?? 0 , $data['per_page'] ?? 0, $segment);
                ?>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <?= $pagination['statistic'];?>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <nav aria-label="">
                            <?= $pagination['pagination'];?>
                        </nav>
                    </div>
                </div>

				<!-- End Pagination -->

			</div>
		</div>
        <?php $this->load->view('tokyo_marines/modals/use_tokyo_marine', ['use_tokyo_marine_message' => '']);?>
        <?php $this->load->view('tokyo_marines/modals/no_use_tokyo_marine', ['no_use_tokyo_marine_message' => '']);?>

        <script src="<?= site_url('assets/js/companies/custom.js') ?>"></script>
