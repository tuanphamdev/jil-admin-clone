<script>
    $("button[data-toggle='modal']").click(function(){
        var company_id = $(this).attr("data-company-id");
        var company_name = $(this).attr("data-company-name");
        $(".modal-body span").html('【'+company_name+'】');
        $("#form_no_use_tokyo_marine input[name='company_id']").val(company_id);
        $("#form_no_use_tokyo_marine input[name='company_name']").val(company_name);
    });
</script>

<?php echo form_open("/tokyo_marines/no_use_tokyo_marine", ['autocomplete' => 'off', 'method' => 'POST', 'id' => 'form_no_use_tokyo_marine']); ?>
    <input type="hidden" value="" name="company_id">
    <input type="hidden" value="" name="company_name">
    <div class="modal fade" id="modal_no_use_tokyo_marine" tabindex="-1" role="dialog" aria-labelledby="modal_no_use_tokyo_marine_label" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_no_use_tokyo_marine_label"><span></span><?= $this->lang->line('tokyo_marine_no_use_title') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><span></span><?= $this->lang->line('tokyo_marine_no_use_message_line1') ?? '' ?></p>
                    <p><?= $this->lang->line('tokyo_marine_no_use_message_line2') ?? '' ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('ui_close_btn') ?></button>
                    <button type="submit" class="btn btn-warning"><?= $this->lang->line('tokyo_marine_no_use_btn') ?></button>
                </div>
            </div>
        </div>
    </div>
</form>
