<div class="form text-left">
    <?php $this->load->view('templates/alert');?>
    <?php $this->load->view('templates/alert_non_session');?>
</div>
<div class="card mb-6">
    <div class="card-body survey_monkey_body">
        <?php echo form_open('/survey_monkey/do_create', ['method' => 'POST', 'novalidate' => '', 'autocomplete' => 'off', 'id'=>'form_survey_monkey']); ?>
        <div class="form-group row">
            <label class="col-md-2">表示サービス</label>
            <div class="col-sm-8">
                <div class="services_name form-inline">
        <?php foreach (SURVEY_MONKEY_SERVICES_LABEL as $key => $service_name): ?>
            <div class="form-group">
                <input type="checkbox"
                       value="<?= $key ?>"
                       id="service_<?= $key ?>"
                       <?php echo isset($data['services']) && in_array($key, $data['services']) || !isset($data['services']) ? 'checked' : '' ?>
                       name="services[]">
                <label for="service_<?= $key ?>"><?= $service_name ?></label>
            </div>
        <?php endforeach;?>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2">表示期間</label>
            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input class="form-control datepicker" name="start_date" placeholder="yyyy-mm-dd" value="<?= $data['start_date'] ?? date('Y-m-d') ?>">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input class="form-control datepicker" name="end_date" placeholder="yyyy-mm-dd" value="<?= $data['end_date'] ?? ''?>">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2">埋め込みコード</label>
            <div class="col-md-9">
                <textarea class="form-control"
                          name="script_content"
                          cols="27"
                          rows="5"
                          class="input-maxlength"><?= $data['script_content'] ?? ''?></textarea>
            </div>
        </div>
        <div class="col align-self-center">
            <div class="form text-center">
                <button type="button" class="btn btn-secondary btn-md btn-preview_survey_monkey"><?= $this->lang->line('btn_preview')?></button>
                <?php if($permissions['survey_monkey'] == FULL_PERMISSION):?>
                    <button type="submit" class="btn btn-primary btn-md"><?= $this->lang->line('btn_save')?></button>
                <?php endif; ?>
            </div>
        </div>
        <?php echo form_close() ; ?>
    </div>
</div>
<div id="preview_survey_monkey" style="display: none">
    <div class="script_content_area">
    </div>
    <div class="survey_info">
        <div class="background-opacity">
        </div>
        <div class="survey_check_box">
            <input type="checkbox" id="survey_employee_status" value="1" name="survey_employee_status">
            <label for="survey_employee_status">このアンケートを今後表示しない。</label>
        </div>
    </div>
</div>
<script src="<?= site_url('assets/js/survey_monkey/custom.js') ?>"></script>

