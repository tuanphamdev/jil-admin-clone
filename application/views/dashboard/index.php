<div class="card">
    <div class="card-body">
        <div class="alert alert-danger alert-dismissible fade show center" role="alert">
            <?= $this->lang->line('top_msg_error_permission')?>
        </div>
    </div>
</div>

