<script>
    $("button[data-toggle='modal']").click(function(){
        var role_id = $(this).attr("data-role-id");
        var role_name = $(this).attr("data-role-name");
        $(".modal-title span").html('【'+role_name+'】');
        $("#form_delete_role input[name='role_id']").val(role_id);
    });
</script>

<?php echo form_open('/roles/delete', ['method' => 'POST', 'id' => 'form_delete_role']); ?>
    <input type="hidden" value="" name="role_id">
    <div class="modal fade" id="modal_delete_role" tabindex="-1" role="dialog" aria-labelledby="modal_delete_role_label" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_delete_role_label"><?= $this->lang->line('role_delete_label') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-title"><span></span><?= $this->lang->line('role_delete_message') ?? '' ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('ui_close_btn') ?></button>
                    <button type="submit" class="btn btn-danger"><?= $this->lang->line('role_delete') ?></button>
                </div>
            </div>
        </div>
    </div>
</form>
