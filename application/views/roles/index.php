<div class="card mb-6">
    <div class="card-header text-right">
        <?php if($permissions['role'] == FULL_PERMISSION):?>
        <a role="button" class="btn btn-primary" href="/roles/create"><i class="fa fa-pencil bigfonts bigfonts" aria-hidden="true"> </i><?= $this->lang->line('role_btn_create')?></a>
        <?php endif; ?>
    </div>
    <div class="card-body">
        <?php $this->load->view('templates/alert');?>
        <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col"><?= $this->lang->line('role_name')?></th>
                <th scope="col"><?= $this->lang->line('role_num_of_people')?></th>
                <th scope="col"><?= $this->lang->line('role_delete')?></th>
                </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($role)) :
            foreach ($role['data']['role'] as $role) :
            ?>
                <tr>
                    <td>
                        <a href="<?= site_url('roles/update/'.$role['id'])?>"><?= $role['name'] ?></a>
                    </td>
                    <td>
                        <?= $role['user_counter'] ?>
                    </td>
                    <td>
                        <?php if($permissions['role'] == FULL_PERMISSION):?>
                            <?php if($role['user_counter'] === 0):?>
                                <button data-role-id="<?= $role['id'] ?? ''?>"  data-role-name="<?= $role['name'] ?? '' ?>" data-toggle="modal" data-target="#modal_delete_role" role="button" href="#" class="btn btn-danger">
                                <i class="fa fa-times"></i>
                                </button>
                            <?php endif;?>
                        <?php endif;?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php endif; ?>

            <?php if(empty($role)):?>
                <tr>
                    <td colspan="12">
                        <?= $this->lang->line('role_no_result_found')?>
                    </td>
                </tr>
            <?php endif; ?>

            </tbody>
        </table>

    </div>
</div>

<?php $this->load->view('roles/modals/delete_role', ['start_service_message' => '']);?>
