<div class="card mb-3">						
	<div class="card-body">
        <?php $this->load->view('templates/alert');?>
		
		<?php echo form_open('/roles/do_create', ['method' => 'POST']); ?>
			<div class="form-group">
			<label for="name"><?= $this->lang->line('role_name')?></label>
			<input type="text" name="name" value="<?= $input['name'] ?? '' ?>" class="form-control" placeholder="">
			</div>

			<div class="form-group">
			<label for=""><?= $this->lang->line('role_detail_form')?></label>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th style="width: 30%"><?= $this->lang->line('role_feature_name')?></th>
						<th style="width: 70%"><?= $this->lang->line('role_detail')?></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class='text-center' colspan="2"><?= $this->lang->line('menu_manage_company')?></td>
					</tr>
					<tr>
						<td scope="row"><?= $this->lang->line('menu_company')?></td>
						<td>
							<div class="form-inline">
								<div class="radio radio-primary">
									<input id="1" type="radio" name="company" value="3" <?= isset($input['company']) && $input['company'] == 3 ? 'checked' : '' ?> >
									<label for="1"> <?= $this->lang->line('role_all')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="2" type="radio" name="company" value="2" <?= isset($input['company']) && $input['company'] == 2 ? 'checked' : '' ?> >
									<label for="2"> <?= $this->lang->line('role_readonly')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="3" type="radio" name="company" value="1" <?= (isset($input['company']) && $input['company'] == 1) || !isset($input['company']) ? 'checked' : '' ?> >
									<label for="3"> <?= $this->lang->line('role_no_use')?></label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td scope="row"><?= $this->lang->line('menu_tokyo_marine')?></td>
						<td>
							<div class="form-inline">
								<div class="radio radio-primary">
									<input id="tokyo_marine3" type="radio" name="tokyo_marine" value="3" <?= isset($input['tokyo_marine']) && $input['tokyo_marine'] == 3 ? 'checked' : '' ?> >
									<label for="tokyo_marine3"><?= $this->lang->line('role_all')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="tokyo_marine2" type="radio" name="tokyo_marine" value="2" <?= isset($input['tokyo_marine']) && $input['tokyo_marine'] == 2 ? 'checked' : '' ?> >
									<label for="tokyo_marine2"><?= $this->lang->line('role_readonly')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="tokyo_marine1" type="radio" name="tokyo_marine" value="1" <?= (isset($input['tokyo_marine']) && $input['tokyo_marine'] == 1) || !isset($input['tokyo_marine']) ? 'checked' : '' ?> >
									<label for="tokyo_marine1"><?= $this->lang->line('role_no_use')?></label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td scope="row"><?= $this->lang->line('employee_admin_list_title')?></td>
						<td>
							<div class="form-inline">
								<div class="radio radio-primary">
									<input id="employee3" type="radio" name="employee" value="3" <?= isset($input['employee']) && $input['employee'] == 3 ? 'checked' : '' ?> >
									<label for="employee3"><?= $this->lang->line('role_all')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="employee2" type="radio" name="employee" value="2" <?= isset($input['employee']) && $input['employee'] == 2 ? 'checked' : '' ?> >
									<label for="employee2"><?= $this->lang->line('role_readonly')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="employee1" type="radio" name="employee" value="1" <?= (isset($input['employee']) && $input['employee'] == 1) || !isset($input['employee']) ? 'checked' : '' ?> >
									<label for="employee1"><?= $this->lang->line('role_no_use')?></label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class='text-center' colspan="2"><?= $this->lang->line('menu_export_csv')?></td>
					</tr>
					<tr>
						<td scope="row"><?= $this->lang->line('menu_export_csv_saleforce')?></td>
						<td>
							<div class="form-inline">
								<div class="radio radio-primary">
									<input id="7" type="radio" name="csv" value="3" <?= isset($input['csv']) && $input['csv'] == 3 ? 'checked' : '' ?> >
									<label for="7"> <?= $this->lang->line('role_all')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="8" type="radio" name="csv" value="2" <?= isset($input['csv']) && $input['csv'] == 2 ? 'checked' : '' ?> >
									<label for="8"> <?= $this->lang->line('role_readonly')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="9" type="radio" name="csv" value="1" <?= (isset($input['csv']) && $input['csv'] == 1) || !isset($input['csv']) ? 'checked' : '' ?>>
									<label for="9"> <?= $this->lang->line('role_no_use')?></label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class='text-center' colspan="2"><?= $this->lang->line('menu_upload')?></td>
					</tr>
					<tr>
						<td scope="row"><?= $this->lang->line('menu_import_bank_csv')?></td>
						<td>
							<div class="form-inline">
								<div class="radio radio-primary">
									<input id="import3" type="radio" name="import_bank" value="3" <?= isset($input['import_bank']) && $input['import_bank'] == 3 ? 'checked' : '' ?> >
									<label for="import3"><?= $this->lang->line('role_all')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="import2" type="radio" name="import_bank" value="2" <?= isset($input['import_bank']) && $input['import_bank'] == 2 ? 'checked' : '' ?> >
									<label for="import2"><?= $this->lang->line('role_readonly')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="import1" type="radio" name="import_bank" value="1" <?= (isset($input['import_bank']) && $input['import_bank'] == 1) || !isset($input['import_bank']) ? 'checked' : '' ?> >
									<label for="import1"><?= $this->lang->line('role_no_use')?></label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class='text-center' colspan="2"><?= $this->lang->line('menu_config')?></td>
					</tr>
					<tr>
						<td scope="row"><?= $this->lang->line('menu_user')?></td>
						<td>
							<div class="form-inline">
								<div class="radio radio-primary">
									<input id="10" type="radio" name="user" value="3"  <?= isset($input['user']) && $input['user'] == 3 ? 'checked' : '' ?> >
									<label for="10"> <?= $this->lang->line('role_all')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="11" type="radio" name="user" value="2" <?= isset($input['user']) && $input['user'] == 2 ? 'checked' : '' ?> >
									<label for="11"> <?= $this->lang->line('role_readonly')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="12" type="radio" name="user" value="1" <?= (isset($input['user']) && $input['user'] == 1) || !isset($input['user']) ? 'checked' : '' ?> >
									<label for="12"> <?= $this->lang->line('role_no_use')?></label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td scope="row"><?= $this->lang->line('menu_role')?></td>
						<td>
							<div class="form-inline">
								<div class="radio radio-primary">
									<input id="13" type="radio" name="role" value="3" <?= isset($input['role']) && $input['role'] == 3 ? 'checked' : '' ?> >
									<label for="13"> <?= $this->lang->line('role_all')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="14" type="radio" name="role" value="2" <?= isset($input['role']) && $input['role'] == 2 ? 'checked' : '' ?> >
									<label for="14"> <?= $this->lang->line('role_readonly')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="15" type="radio" name="role" value="1" <?= (isset($input['role']) && $input['role'] == 1) || !isset($input['role']) ? 'checked' : '' ?> >
									<label for="15"> <?= $this->lang->line('role_no_use')?></label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td scope="row"><?= $this->lang->line('menu_ip')?></td>
						<td>
							<div class="form-inline">
								<div class="radio radio-primary">
									<input id="16" type="radio" name="ip" value="3" <?= isset($input['ip']) && $input['ip'] == 3 ? 'checked' : '' ?> >
									<label for="16"><?= $this->lang->line('role_all')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="17" type="radio" name="ip" value="2" <?= isset($input['ip']) && $input['ip'] == 2 ? 'checked' : '' ?> >
									<label for="17"><?= $this->lang->line('role_readonly')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="18" type="radio" name="ip" value="1" <?= (isset($input['ip']) && $input['ip'] == 1) || !isset($input['ip']) ? 'checked' : '' ?> >
									<label for="18"><?= $this->lang->line('role_no_use')?></label>
								</div>
							</div>
						</td>
					</tr>
                    <tr>
                        <td scope="row"><?= $this->lang->line('menu_qa_engine')?></td>
                        <td>
                            <div class="form-inline">
                                <div class="form-inline">
                                    <div class="radio radio-primary">
                                        <input id="28" type="radio" name="qa_engine" value="3" <?= isset($input['qa_engine']) && $input['qa_engine'] == 3 ? 'checked' : '' ?> >
                                        <label for="28"><?= $this->lang->line('role_all')?></label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="29" type="radio" name="qa_engine" value="2" <?= isset($input['qa_engine']) && $input['qa_engine'] == 2 ? 'checked' : '' ?> >
                                        <label for="29"><?= $this->lang->line('role_readonly')?></label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="30" type="radio" name="qa_engine" value="1" <?= (isset($input['qa_engine']) && $input['qa_engine'] == 1) || !isset($input['qa_engine']) ? 'checked' : '' ?> >
                                        <label for="30"><?= $this->lang->line('role_no_use')?></label>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td scope="row"><?= $this->lang->line('menu_survey_monkey')?></td>
                        <td>
                            <div class="form-inline">
                                <div class="form-inline">
                                    <div class="radio radio-primary">
                                        <input id="31" type="radio" name="survey_monkey" value="3" <?= isset($input['survey_monkey']) && $input['survey_monkey'] == 3 ? 'checked' : '' ?> >
                                        <label for="31"><?= $this->lang->line('role_all')?></label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="32" type="radio" name="survey_monkey" value="2" <?= isset($input['survey_monkey']) && $input['survey_monkey'] == 2 ? 'checked' : '' ?> >
                                        <label for="32"><?= $this->lang->line('role_readonly')?></label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="33" type="radio" name="survey_monkey" value="1" <?= (isset($input['survey_monkey']) && $input['survey_monkey'] == 1) || !isset($input['survey_monkey']) ? 'checked' : '' ?> >
                                        <label for="33"><?= $this->lang->line('role_no_use')?></label>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
					<tr>
						<td class='text-center' colspan="2"><?= $this->lang->line('menu_ts_password')?></td>
					</tr>
					<tr>
						<td scope="row"><?= $this->lang->line('menu_ts_password_change')?></td>
						<td>
							<div class="form-inline">
								<div class="radio radio-primary">
									<input id="19" type="radio" name="ts_password_change" value="3"  <?= isset($input['ts_password_change']) && $input['ts_password_change'] == 3 ? 'checked' : '' ?> >
									<label for="19"> <?= $this->lang->line('role_all')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="20" type="radio" name="ts_password_change" value="2" <?= isset($input['ts_password_change']) && $input['ts_password_change'] == 2 ? 'checked' : '' ?> >
									<label for="20"> <?= $this->lang->line('role_readonly')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="21" type="radio" name="ts_password_change" value="1" <?= (isset($input['ts_password_change']) && $input['ts_password_change'] == 1) || !isset($input['ts_password_change']) ? 'checked' : '' ?> >
									<label for="21"> <?= $this->lang->line('role_no_use')?></label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td scope="row"><?= $this->lang->line('menu_ts_password_ips')?></td>
						<td>
							<div class="form-inline">
								<div class="radio radio-primary">
									<input id="22" type="radio" name="ts_password_ips" value="3" <?= isset($input['ts_password_ips']) && $input['ts_password_ips'] == 3 ? 'checked' : '' ?> >
									<label for="22"> <?= $this->lang->line('role_all')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="23" type="radio" name="ts_password_ips" value="2" <?= isset($input['ts_password_ips']) && $input['ts_password_ips'] == 2 ? 'checked' : '' ?> >
									<label for="23"> <?= $this->lang->line('role_readonly')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="24" type="radio" name="ts_password_ips" value="1" <?= (isset($input['ts_password_ips']) && $input['ts_password_ips'] == 1) || !isset($input['ts_password_ips']) ? 'checked' : '' ?> >
									<label for="24"> <?= $this->lang->line('role_no_use')?></label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td scope="row"><?= $this->lang->line('menu_ts_password_logs')?></td>
						<td>
							<div class="form-inline">
								<div class="radio radio-primary">
									<input id="25" type="radio" name="ts_password_logs" value="3" <?= isset($input['ts_password_logs']) && $input['ts_password_logs'] == 3 ? 'checked' : '' ?> >
									<label for="25"><?= $this->lang->line('role_all')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="26" type="radio" name="ts_password_logs" value="2" <?= isset($input['ts_password_logs']) && $input['ts_password_logs'] == 2 ? 'checked' : '' ?> >
									<label for="26"><?= $this->lang->line('role_readonly')?></label>
								</div>
								<div class="radio radio-primary">
									<input id="27" type="radio" name="ts_password_logs" value="1" <?= (isset($input['ts_password_logs']) && $input['ts_password_logs'] == 1) || !isset($input['ts_password_logs']) ? 'checked' : '' ?> >
									<label for="27"><?= $this->lang->line('role_no_use')?></label>
								</div>
							</div>
						</td>
					</tr>
					
<!--					<tr>-->
<!--						<td scope="row">--><?php //= $this->lang->line('menu_information')?><!--</td>-->
<!--						<td>-->
<!--							<div class="form-inline">-->
<!--								<div class="radio radio-primary">-->
<!--									<input id="information_full" type="radio" name="information" value="3" --><?php //= isset($input['information']) && $input['information'] == 3 ? 'checked' : '' ?><!-- >-->
<!--									<label for="information_full">--><?php //= $this->lang->line('role_all')?><!--</label>-->
<!--								</div>-->
<!--								<div class="radio radio-primary">-->
<!--									<input id="information_read" type="radio" name="information" value="2" --><?php //= isset($input['information']) && $input['information'] == 2 ? 'checked' : '' ?><!-- >-->
<!--									<label for="information_read">--><?php //= $this->lang->line('role_readonly')?><!--</label>-->
<!--								</div>-->
<!--								<div class="radio radio-primary">-->
<!--									<input id="information_non" type="radio" name="information" value="1" --><?php //= (isset($input['information']) && $input['information'] == 1) || !isset($input['information']) ? 'checked' : '' ?><!-- >-->
<!--									<label for="information_non">--><?php //= $this->lang->line('role_no_use')?><!--</label>-->
<!--								</div>-->
<!--							</div>-->
<!--						</td>-->
<!--					</tr>-->

				</tbody>
			</table>
			</div>

			<br>
			<div class="form-group row align-items-end">
				<div class="col col align-self-center">
					<div class="form text-center">
                        <?php if($permissions['role'] == FULL_PERMISSION):?>
						<button type="submit" id="button-submit-save" class="btn btn-primary btn-md"><?= $this->lang->line('btn_confirm')?></button>
                        <?php endif; ?>
						<a href="<?= site_url('roles')?>" class="btn btn-warning btn-md"><?= $this->lang->line('btn_return')?></a>
					</div>
				</div>
			</div>
		</form>
										
	</div><!-- end card-->					
</div>
