<div class="modal fade" id="csv_upload_bank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?= $this->lang->line('import_csv_title_from')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        </div>
        <div class="modal-body">
        <?php echo form_open("/import_banks/import", ['method' => 'POST', 'enctype' => 'multipart/form-data']); ?>
            <div class="card-body">
                <input data-jfiler-name="files" type="file" name="bank_info" id="csv_bank">
            </div>
        </div>
        <div class="modal-footer">
        <button id="button-submit-save" role="button" class="btn btn-info" href="#"><?= $this->lang->line('import_csv_button')?></button>
        </div>
        </form>
    </div>
    </div>
</div>