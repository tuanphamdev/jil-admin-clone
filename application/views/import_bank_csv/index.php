<?php
$segment = 2;
$page = 'import_banks';
if($this->uri->segment(2) == 'search'){
	$segment = 3;
	$page = 'import_banks/search';
}
?>	
<div class="card mb-3">
    <div class="card-body">
        <div id="accordion" role="tablist">
            <div class="">
                <div class="" role="tab" id="headingOne">
                <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="collapsed">
                    <i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('import_csv_search_title')?>
                </a>
                </div>
                <form action="/import_banks/search" method='GET'>
                    <div id="collapseOne" class="collapse <?= $segment == 3 ? 'show' : '';?>" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                        <div class="card-body">
                            <div class="form-row">
                                <input type="hidden" name="item_page" value="<?= $data['per_page'] ?? 0 ?>" class="form-control">
                                <div class="form-group col-md-3">
                                    <label for="bank_code"><?= $this->lang->line('import_csv_bank_code')?></label>
                                    <input name='bank_code' id='bank_code' value="<?= $input['bank_code'] ?? '' ?>" type="text" class="form-control">
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="branch_code"><?= $this->lang->line('import_csv_branch_code')?></label>
                                    <input name='branch_code' id='branch_code' value="<?= $input['branch_code'] ?? '' ?>" type="text" class="form-control">
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="bank_name"><?= $this->lang->line('import_csv_bank_name')?></label>
                                    <input name='bank_name' id='bank_name' value="<?= $input['bank_name'] ?? '' ?>" type="text" class="form-control">
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="branch_name"><?= $this->lang->line('import_csv_branch_name')?></label>
                                    <input name='branch_name' id='branch_name' value="<?= $input['branch_name'] ?? '' ?>" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form text-right">
                                <button role="button" class="btn btn-info" href="#">
                                    <i class="fa fa-fw fa-search"> </i>
                                    <?= $this->lang->line('export_csv_search_btn')?>
                                </button>
                                <a class="btn btn-secondary" href="/import_banks">
                                    <i class="fa fa-fw fa-close"> </i>
                                    <?= $this->lang->line('export_csv_clear_btn')?>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>							
</div>


<div class="card mb-3">
    <div class="card-body">
        <?php $this->load->view('templates/alert');?>
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <?php $this->load->view('templates/per_page', ['per_page' => $data['per_page'] ?? 0]);?>

                </div>
                <?php if($permissions['import_bank'] == FULL_PERMISSION):?>
                    <div class="col-6 text-right">
                        <a role="button" id="create_bank" href="/import_banks/create" class="btn btn-primary">
                            <i class="fa fa-pencil bigfonts bigfonts" aria-hidden="true"></i>
                            <?= $this->lang->line('bank_create_button')?>
                        </a>
                        <button role="button"
                                id="upload_bank"
                                href="#"
                                class="btn btn-info"
                                data-toggle="modal"
                                data-target="#csv_upload_bank">
                            <?= $this->lang->line('import_csv_button')?>
                        </button>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <table class="table table-responsive-col table-striped">
			<thead>
			<tr>
				<th scope="col"><?= $this->lang->line('import_csv_bank_code')?></th>
				<th scope="col"><?= $this->lang->line('import_csv_branch_code')?></th>
				<th scope="col"><?= $this->lang->line('import_csv_bank_name')?></th>
                <th scope="col"><?= $this->lang->line('import_csv_branch_name')?></th>
                <th scope="col"><?= $this->lang->line('import_csv_branch_post_code')?></th>
                <th scope="col"><?= $this->lang->line('import_csv_branch_address')?></th>
                <th scope="col"><?= $this->lang->line('import_csv_branch_phone_number')?></th>
                <th scope="col"><?= $this->lang->line('bank_status')?></th>
                <th scope="col"></th>
			</tr>
			</thead>
			<tbody>
            <?php 
            if(!empty($data['bank'])) :
                foreach ($data['bank'] as $company) :
            ?>
                <tr>
                    <td><?= $company['bank_code'] ?></td>
                    <td><?= $company['branch_code'] ?></td>
                    <td><?= $company['bank_name'] ?></td>
                    <td><?= $company['branch_name'] ?></td>
                    <td><?= $company['branch_post_code'] ?></td>
                    <td><?= $company['branch_address'] ?></td>
                    <td><?= $company['branch_phone_number'] ?></td>
                    <td><?= BANK_STATUS_LABEL[$company['status']] ?? '有効' ?></td>
                    <td>
                        <a role="button" class="btn btn-primary" href="/import_banks/edit/<?= $company['id'] ?>">
                            <?= $this->lang->line('bank_edit_button')?>
                        </a>
                    </td>
                </tr>
            <?php
                endforeach;
             endif;

            ?>
            <?php if(empty($data['bank'])):?>
                <tr>
                    <td colspan="12">
                        <?= $this->lang->line('import_csv_result_found')?>
                    </td>
                </tr>
            <?php endif; ?>
			</tbody>
        </table>

        <!-- Pagination -->
        <?php
        $pagination =  pagination(base_url($page), $data['total'] ?? 0, $data['per_page'] ?? 0, $segment);
        ?>
        <div class="row">
            <div class="col-sm-12 col-md-5">
                <?= $pagination['statistic'];?>
            </div>
            <div class="col-sm-12 col-md-7">
                <nav aria-label="">
                    <?= $pagination['pagination'];?>
                </nav>
            </div>
        </div>

        <!-- End Pagination -->

    </div>			
    											
</div><!-- end card-->					
<?php $this->load->view('import_bank_csv/modals/csv_upload_bank');?>

<script src="<?= site_url('/assets/js/import_csv/custom.js') ?>"></script>
