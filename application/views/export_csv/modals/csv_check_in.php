<div class="modal fade" id="csv_check_in" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        </div>
        <div class="modal-body">
            <form class="form-inline">
                <div class="form-group">
                <label for="exampleInputName2">対象期間：</label>
                </div>
                <div class="form-group">
                    <input name='start_date_check_in' style="width:170px;" type="text" class="form-control datepicker" id="start_date_check_in">
                </div>
                〜
                <div class="form-group">
                    <input name='end_date_check_in' style="width:170px" type="text" class="form-control datepicker" id="end_date_check_in">
                </div>
        </div>
        <div class="modal-footer">
        <button id='csv_check_in_btn' role="button" class="btn btn-info" href="#">ダウンロード</button>
        </div>
        </form>
    </div>
    </div>
</div>
