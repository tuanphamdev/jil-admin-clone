<?php
$segment = 2;
$page = 'export_csv';
if($this->uri->segment(2) == 'search'){
	$segment = 3;
	$page = 'export_csv/search';
}
?>
<div class="card mb-3">
    <div class="card-body">
        <div id="accordion" role="tablist">
            <div class="">
            <div class="" role="tab" id="headingOne">
            <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="collapsed">
                <i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('export_csv_search_title')?>
            </a>
            </div>
        <form action="/export_csv/search" method='GET'>
            <div id="collapseOne" class="collapse <?= $segment == 3 ? 'show' : '';?>" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                <div class="card-body">

                    <div class="form-row">

                        <input type="hidden" name="item_page" value="<?= $data['per_page'] ?? 0 ?>" class="form-control">

                        <div class="form-group col-md-2">
                            <label for=""><?= $this->lang->line('company_agreement_type')?></label>
                            <select name="type" id="agree_type" class="form-control">
                                <option value="" <?= empty($data['type']) || $data['type'] === ''? 'selected' : '' ?> ><?= $this->lang->line('ui_option_all')?></option>
                                <option value=0 <?= isset($data['type']) && $data['type'] === '0' ? 'selected' : '' ?> ><?= $this->lang->line('ts_password_log_agree_trial')?></option>
                                <option value=1 <?= isset($data['type']) && $data['type'] === '1' ? 'selected' : '' ?> ><?= $this->lang->line('ts_password_log_agree_free')?></option>
                                <option value=2 <?= isset($data['type']) && $data['type'] === '2' ? 'selected' : '' ?> ><?= $this->lang->line('ts_password_log_agree_fee')?></option>
                            </select>
                        </div>

                        <div class="form-group col-md-2">
                            <label for=""><?= $this->lang->line('export_csv_company_code_from')?></label>
                            <input name='code_from' id='code_from' value="<?= $input['code_from'] ?? '' ?>" type="text" class="form-control">
                        </div>

                        <div class="form-group col-md-2">
                            <label for=""><?= $this->lang->line('export_csv_company_code_to')?></label>
                            <input name='code_to' id='code_to' value="<?= $input['code_to'] ?? '' ?>" type="text" class="form-control">
                        </div>

                        <div class="form-group col-md-2">
                            <label for=""><?= $this->lang->line('export_csv_company_name')?></label>
                            <input name='name' value="<?= $input['name'] ?? '' ?>" type="text" class="form-control">
                        </div>

                        <div class="form-group col-md-2">
                            <label for=""><?= $this->lang->line('export_csv_start_date_from')?></label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                    <input name='registered_date_from' id='registered_date_from' value="<?= $input['registered_date_from'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label for=""><?= $this->lang->line('export_csv_start_date_to')?></label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                    <input name='registered_date_to' id='registered_date_to' value="<?= $input['registered_date_to'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                            </div>
                        </div>
                    </div>

                    <div class="form text-right">
                        <button role="button" class="btn btn-info" href="#"><i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('export_csv_search_btn')?></button>

                        <a class="btn btn-secondary" href="/export_csv"><i class="fa fa-fw fa-close"> </i> <?= $this->lang->line('export_csv_clear_btn')?></a>
                    </div>
                </div>
            </div>

        </form>

            </div>
        </div>
    </div>
</div>

<div class="card mb-3">
    <div class="card-body">
        <?php if($permissions['csv'] == FULL_PERMISSION):?>
            <div class="button-list text-left">
                <a role="button" id="csv_company" class="btn btn-info" id="csv_company" href="#"><?= $this->lang->line('export_csv_company')?></a>
                <button role="button" id="check_in" href="#" class="btn btn-info" data-toggle="modal" data-target="#csv_check_in"><?= $this->lang->line('export_csv_check_in')?></button>
                <button role="button" id="tighten" href="#" class="btn btn-info" data-toggle="modal" data-target="#csv_tighten"><?= $this->lang->line('export_csv_tighten')?></button>
                <a role="button" id="csv_company_staff" class="btn btn-info" href="#"><?= $this->lang->line('export_csv_company_staff')?></a>
                <button role="button" id="retire" href="#" class="btn btn-info" data-toggle="modal" data-target="#csv_retire"><?= $this->lang->line('export_csv_retire')?></button>
                <button role="button" id="" class="btn btn-info"><?= $this->lang->line('export_csv_tokyo_marine')?></button>
                <button role="button" id="health_keihi" href="#" class="btn btn-info" data-toggle="modal" data-target="#csv_health_keihi"><?= $this->lang->line('export_csv_headlth_keihi')?></button>
                <button role="button" id="health_salary" href="#" class="btn btn-info" data-toggle="modal" data-target="#csv_health_salary"><?= $this->lang->line('export_csv_health_salary')?></button>
                <button role="button" id="health_signing" href="#" class="btn btn-info" data-toggle="modal" data-target="#csv_health_signing"><?= $this->lang->line('export_csv_headlth_signing')?></button>
            </div>
        <?php endif; ?>
    </div>
</div>


<div class="card mb-3">
    <div class="card-body">
        <?php $this->load->view('templates/alert');?>
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <?php $this->load->view('templates/per_page', ['per_page' => $data['per_page'] ?? 0]);?>
                </div>
            </div>
        </div>
        <table class="table table-responsive-col table-striped">
			<thead>
			<tr>
				<th scope="col"><?= $this->lang->line('export_csv_agree_type')?></th>
				<th scope="col"><?= $this->lang->line('export_csv_company_code')?></th>
				<th scope="col"><?= $this->lang->line('export_csv_company_name')?></th>
				<th scope="col"><?= $this->lang->line('export_csv_tel')?></th>
			</tr>
			</thead>
			<tbody>
            <?php
            if(!empty($data['company'])) :
                foreach ($data['company'] as $comapny) :
            ?>
                <tr>
                    <td>
                        <?php
							$agree_type = $this->config->item('agreement_type')[$comapny['type']??''];
							echo $this->lang->line('company_agree_'.$agree_type);
						?>
                    </td>
                    <td><?= $comapny['code'] ?></td>
                    <td>
                    <?= $comapny['name'] ?>
                    </td>
                    <td><?= $comapny['tel'] ?></td>
                </tr>

            <?php
                endforeach;
             endif;

            ?>
            <?php if(empty($data['company'])):?>
                <tr>
                    <td colspan="12">
                        <?= $this->lang->line('company_no_result_found')?>
                    </td>
                </tr>
            <?php endif; ?>
			</tbody>
        </table>

        <!-- Pagination -->
        <?php
        $pagination =  pagination(base_url($page), $data['total'] ?? 0, $data['per_page'] ?? 0, $segment);
        ?>
        <div class="row">
            <div class="col-sm-12 col-md-5">
                <?= $pagination['statistic'];?>
            </div>
            <div class="col-sm-12 col-md-7">
                <nav aria-label="">
                    <?= $pagination['pagination'];?>
                </nav>
            </div>
        </div>

        <!-- End Pagination -->

    </div>

</div><!-- end card-->
<?php $this->load->view('export_csv/modals/csv_check_in', ['' => '']);?>
<?php $this->load->view('export_csv/modals/csv_tighten', ['' => '']);?>
<?php $this->load->view('export_csv/modals/csv_retire_staff', ['' => '']);?>
<?php $this->load->view('export_csv/modals/csv_health_keihi', ['' => '']);?>
<?php $this->load->view('export_csv/modals/csv_health_salary', ['' => '']);?>
<?php $this->load->view('export_csv/modals/csv_health_signing', []);?>


<script>
var kintai_url = "<?= getenv('JINER_K_API_URL')?>";
var core_url = "<?= getenv('API_CORE_URL')?>";
</script>
<script src="<?= site_url('assets/js/export_csv/custom.js') ?>"></script>
