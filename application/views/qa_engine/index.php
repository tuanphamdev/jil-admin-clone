<div class="card mb-6">
    <div class="card-body">
        <?php echo form_open('/qa_engine/do_create', ['method' => 'POST', 'novalidate' => '']); ?>
        <div class="form-list" style="margin-left: 5%">
            <input id="" type="hidden" name="id" value="<?= $data['id'] ?? '' ?>" >
            <div class="form-group row align-items-end">
                <div class="checkbox checkbox-primary">
                    <input id="checkbox_remember" type="checkbox" name="status" <?= empty($data['id']) || (isset($data['status']) && $data['status']) == 1 ? 'checked': '' ?> >
                    <label for="checkbox_remember"> QA Engineを利用する</label>
                </div>
            </div>
            <div class="form-group col-md-10">
                <label>Information 1</label>
                <textarea style="height:200px" class="form-control" name="information_1" cols="27" class="input-maxlength"><?= $data['information_1'] ?? ''?></textarea>
            </div>
            <div class="form-group col-md-10">
                <label>Information 2</label>
                <textarea style="height:200px" class="form-control" name="information_2" cols="27" class="input-maxlength"><?= $data['information_2'] ?? ''?></textarea>
            </div>
            <div class="col col align-self-center">
                <div class="form-group row">
                    <div class="text-left">
                        <?php if($permissions['qa_engine'] == FULL_PERMISSION):?>
                            <button type="submit" id="button-submit-save" class="btn btn-primary btn-md"><?= $this->lang->line('btn_save')?></button>
                        <?php endif; ?>
                    </div>
                    <div class="form text-left col-md-9">
                        <?php $this->load->view('templates/alert');?>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>