<div class="card mb-6">
    <div class="card-header text-right">
        <?php if($permissions['ts_password_ips'] == FULL_PERMISSION):?>
        <a role="button" class="btn btn-primary" href="/ts_password_ips/create"><i class="fa fa-pencil bigfonts bigfonts" aria-hidden="true"> </i><?= $this->lang->line('ts_password_ip_create_btn')?></a>
        <?php endif; ?>
    </div>
    <div class="card-body">
    <?php $this->load->view('templates/alert');?>
        <table class="table table-hover">
            <thead class='text-center'>
                <tr>
                <th scope="col" colspan=2><?= $this->lang->line('ts_password_ip_field_name')?></th>
                <th scope="col" colspan=2><?= $this->lang->line('ts_password_ip_address')?></th>
                <th scope="col">アクション</th>
                </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($data['ip'])) :
                foreach ($data['ip'] as $val_ip) :
            ?>
                <tr class='text-center'>
                    <td scope="col" colspan=2><a href="ts_password_ips/update/<?= $val_ip['id'] ?? ''?>"><?= $val_ip['name'] ?? ''?></a></td>
                    <td scope="col" colspan=2><a href="ts_password_ips/update/<?= $val_ip['id'] ?? ''?>"><?= $val_ip['ip_address'] ?? ''?></a></td>
                    <td class='text-center'>
                        <?php if($permissions['ip'] == FULL_PERMISSION):?>
                        <button data-ip-id="<?= $val_ip['id'] ?? ''?>"  data-ip-address="<?= $val_ip['ip_address'] ?? '' ?>" data-toggle="modal" data-target="#modal_delete_ip" role="button" href="#" class="btn btn-danger">
                        <i class="fa fa-times"></i>
						</button>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php
                endforeach;
            endif;
            ?>

            <?php if(empty($data['ip'])):?>
                <tr>
                    <td colspan="12">
                        <?= $this->lang->line('ts_password_ip_no_result_found')?>
                    </td>
                </tr>
            <?php endif; ?>

            </tbody>
        </table>
        <?php $this->load->view('ts_password_ips/modals/delete_ip', ['start_service_message' => '']);?>
    </div>
</div>