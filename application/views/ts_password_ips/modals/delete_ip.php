<script>
    $("button[data-toggle='modal']").click(function(){
        var ip_id = $(this).attr("data-ip-id");
        var ip_address = $(this).attr("data-ip-address");
        $(".modal-title span").html('【'+ip_address+'】');
        $("#form_delete_ip input[name='ip_id']").val(ip_id);
    });
</script>

<?php echo form_open('/ts_password_ips/delete', ['method' => 'POST', 'id' => 'form_delete_ip']); ?>
    <input type="hidden" value="" name="ip_id">
    <div class="modal fade" id="modal_delete_ip" tabindex="-1" role="dialog" aria-labelledby="modal_delete_ip_label" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_delete_ip_label">削除確認</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="modal-title"><span></span><?= $this->lang->line('ts_password_ip_delete_message') ?? '' ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('ui_close_btn') ?></button>
                    <button type="submit" class="btn btn-danger"><?= $this->lang->line('ts_password_ip_delete') ?></button>
                </div>
            </div>
        </div>
    </div>
</form>
