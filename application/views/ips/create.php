<div class="card mb-6">
    <div class="card-body">
    <?php $this->load->view('templates/alert');?>
    <?php echo form_open('/ips/do_create', ['method' => 'POST', 'novalidate' => '']); ?>
        <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col" colspan=4><?= $this->lang->line('ip_create_title')?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <input title="" class="form-control ip_address_input" type="number" name="ip_address[]" value="<?= $input['ip_address'][0] ?? '' ?>" placeholder="192" min="0" max="255" required="">
                    </td>
                    <td>
                        <input title="" class="form-control ip_address_input" type="number" name="ip_address[]" value="<?= $input['ip_address'][1] ?? '' ?>" placeholder="168" min="0" max="255" required="">
                    </td>
                    <td>
                        <input title="" class="form-control ip_address_input" type="number" name="ip_address[]" value="<?= $input['ip_address'][2] ?? '' ?>" placeholder="1" min="0" max="255" required="">
                    </td>
                    <td>
                        <input title="" class="form-control ip_address_input" type="number" name="ip_address[]" value="<?= $input['ip_address'][3] ?? '' ?>" placeholder="1" min="0" max="255" required="">
                    </td>

                </tr>
            
            </tbody>
        </table>

        <br>
        <div class="form-group row align-items-end">
            <div class="col col align-self-center">
                <div class="form text-center">
                    <?php if($permissions['ip'] == FULL_PERMISSION):?>
                    <button type="submit" id="button-submit-save" class="btn btn-primary btn-md"><?= $this->lang->line('btn_confirm')?></button>
                    <?php endif; ?>
                    <a href="<?= site_url('ips')?>" class="btn btn-warning btn-md"><?= $this->lang->line('btn_return')?></a>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>