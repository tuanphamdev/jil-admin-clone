<script>
    $("button[data-toggle='modal']").click(function(){
        var admin_code = $('#admin_code').val();
        var core_company_id = $(this).attr("data-core-company-id");
        var core_company_code = $(this).attr("data-core-company-code");
        var admin_company_id = $(this).attr("data-admin-company-id");
        var employee_code = $(this).attr("data-emp-code");
        var company_name = $(this).attr("data-company-name");
        var employee_name = $(this).attr("data-emp-name");
        $(".modal-body span").html('「'+employee_name+'」');
        $("#form_enable_admin input[name='core_company_id']").val(core_company_id);
        $("#form_enable_admin input[name='core_company_code']").val(core_company_code);
        $("#form_enable_admin input[name='admin_company_id']").val(admin_company_id);
        $("#form_enable_admin input[name='employee_code']").val(employee_code);
        $("#form_enable_admin input[name='admin_code']").val(admin_code);
    });
</script>

<?php echo form_open("/admin_employees/enable_admin", ['autocomplete' => 'off', 'method' => 'POST', 'id' => 'form_enable_admin']); ?>
    <input type="hidden" value="" name="core_company_id">
    <input type="hidden" value="" name="core_company_code">
    <input type="hidden" value="" name="admin_company_id">
    <input type="hidden" value="" name="employee_code">
    <input type="hidden" value="" name="admin_code">
    <div class="modal fade" id="modal_enable_admin" tabindex="-1" role="dialog" aria-labelledby="modal_start_service_label" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_start_service_label"><span></span><?= $this->lang->line('employee_enable_admin_popup_title') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><?= $this->lang->line('employee_enable_admin_popup_message_line_1')?>
                    <span></span>
                    <?= $this->lang->line('employee_enable_admin_popup_message_line_2')?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('ui_close_btn') ?></button>
                    <button type="submit" class="btn btn-success"><?= $this->lang->line('ui_start_btn') ?></button>
                </div>
            </div>
        </div>
    </div>
</form>
