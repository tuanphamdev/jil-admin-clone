<?php
$segment = 2;
$page = 'admin_employees';
if($this->uri->segment(2) == 'search'){
	$segment = 3;
	$page = 'admin_employees/search';
}
?>
	<div class="card mb-3">
        <div class="card-body">
            <div id="accordion" role="tablist">
                <div class="">
                <div class="" role="tab" id="headingOne">
				<a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
					<i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('company_search_title')?>
				</a>
                </div>
                <form action="/admin_employees/search" method="GET" autocomplete="off">
                    <div id="collapseOne" class="collapse  <?= $segment == 3 ? 'show' : '';?>" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                        <div class="card-body">

                            <!-- <input type="hidden" name="item_page" value="<?= $data['per_page'] ?? 0 ?>" class="form-control"> -->

                            <div class="form-row">
                            <div class="form-group col-md-3">
                                <label class='form-list__required'><?= $this->lang->line('employee_company_code')?></label>
                                <input type="text" name="company_code" value="<?= $input['company_code'] ?? '' ?>" class="form-control">
                            </div>
                            <div class="form-group col-md-3">
                                <label class='form-list__required'><?= $this->lang->line('employee_code')?></label>
                                <input type="text" name="employee_code" value="<?= $input['employee_code'] ?? '' ?>" class="form-control">
                            </div>
                            <div class="form-group col-md-3">
                                <label ><?= $this->lang->line('employee_name')?></label>
                                <input type="text" name="employee_name" value="<?= $input['employee_name'] ?? '' ?>" class="form-control">
                            </div>

                            </div>

                            <div class="form text-right">
                                <button role="button" class="btn btn-info" href="#"><i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('ui_search_btn')?></button>
                                <a class="btn btn-secondary" href="/admin_employees"><i class="fa fa-fw fa-close"> </i> <?= $this->lang->line('ui_clear_btn')?></a>
                            </div>
                        </div>
                    </div>

			    </form>

                </div>
            </div>
        </div>
    </div>

		<div class="card">
			<div class="card-body">
                <?php $this->load->view('templates/alert');?>

				<table class="table table-responsive-col table-striped">
					<thead>
					<tr>
						<th scope="col-2"><?= $this->lang->line('employee_admin_photo')?></th>
						<th scope="col-1"><?= $this->lang->line('employee_admin_name')?></th>
						<th scope="col-1"><?= $this->lang->line('employee_admin_code')?></th>
						<th scope="col-1"><?= $this->lang->line('employee_admin_last_name')?></th>
						<th scope="col-1"><?= $this->lang->line('employee_admin_dept')?></th>
						<th scope="col-1"><?= $this->lang->line('employee_admin_job_tile')?></th>
						<th scope="col-1"><?= $this->lang->line('employee_admin_enrollment_status')?></th>
						<th scope="col-1"><?= $this->lang->line('employee_admin_admin_status')?></th>
                        <th scope="col-1"><?= $this->lang->line('employee_edit_function')?></th>
					</tr>
					</thead>
					<tbody>
				<?php
				if(!empty($data['employee'])):
				foreach ($data['employee'] as $key => $value):

                ?>
					<tr>
						<td>
                            <div class="personal-profile__image">
                                <img id="avatar-<?= $value['photo_id']??''?>" width="45" height="45" core-company-id="<?= $value['core_company_id'] ?>" core-company-code="<?= $value['company_code'] ?>" src="../../../assets/images/avatars/bg.png" alt="" avatar-id="<?= $value['photo_id']??''?>" data-src="../../../assets/images/avatars/loading_icon.gif">
                            </div>

                    </div>
                        </td>
						<td><?= $value['company_name']??''?></td>
						<td>
                            <?= $value['employee_code'] ?? '' ?>
                        </td>
                        <td><?= $value['full_name'] ?? '' ?></td>
                        <td><?= $value['dept_name'] ?? '' ?></td>
                        <td><?= $value['job_title'] ?? '' ?></td>
						<td>
						<?php
							if(isset($value['enrollment_status']) && $value['enrollment_status'] == 0):
							    echo $this->lang->line('employee_admin_enrollment_status_enrolled');
							endif;
							if(isset($value['enrollment_status']) && $value['enrollment_status'] == 1):
							    echo $this->lang->line('employee_admin_enrollment_status_retirement');
							endif;
							if(isset($value['enrollment_status']) && $value['enrollment_status'] == 2):
							    echo $this->lang->line('employee_admin_enrollment_status_absence');
							endif;
						?>
						</td>
                        <td>

                            <?php if($value['is_admin'] == 1):?>
                            <input id="admin_code" type="hidden" value="<?= $value['employee_code'] ?>">
                                <i class="fa fa-circle-o"></i>
                            <?php endif; ?>
                            <?php if($value['is_admin'] == 0):?>
                                <i class="fa fa-close"></i>
                            <?php endif; ?>
                        </td>
						<td>
                            <?php
                                $modal_id = 'modal_enable_admin';
                                $button_class = 'warning';
                                $button_label = $this->lang->line('employee_admin_button_edit');
                            ?>

                            <?php if($permissions['employee'] == FULL_PERMISSION && $value['is_admin'] != 1):?>
							<button data-core-company-id="<?= $value['core_company_id'] ?>"  data-core-company-code="<?= $value['company_code'] ?>" data-emp-code="<?= $value['employee_code'] ?>" data-emp-name="<?= $value['full_name'] ?>" data-admin-company-id="<?= $value['admin_company_id'] ?>" data-toggle="modal" data-target="#<?= $modal_id ?>" role="button" href="#" class="btn btn-<?= $button_class ?>">
								<?= $button_label ?>
							</button>
                            <?php endif; ?>


						</td>
					</tr>
				<?php
				endforeach;
				endif;
				?>

                <?php if(empty($data['employee'])):?>
                <tr>
                    <td colspan="12">
                <?= $this->lang->line('employee_no_result_found')?>
                    </td>
                </tr>
                <?php endif; ?>

					</tbody>
				</table>

                <?php $this->load->view('employees/modals/enable_admin');?>
				<!-- Pagination -->
                <?php
                $pagination =  pagination(base_url($page), $data['total'] ?? 0 , $data['per_page'] ?? 0, $segment);
                ?>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <?= $pagination['statistic'];?>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <nav aria-label="">
                            <!-- <?= $pagination['pagination'];?> -->
                        </nav>
                    </div>
                </div>

				<!-- End Pagination -->

			</div>
		</div>

        <script src="<?= site_url('assets/js/employees/custom.js') ?>"></script>
        <script src="<?= site_url('assets/js/avatar/avatar.js') ?>"></script>
