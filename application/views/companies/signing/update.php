<div class="form-group row">
    <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_signing')?></label>
    <div class="col-sm-8">
        <select name="signing" class="form-control is_contract">
            <option value=0 <?= isset($input['signing']) && $input['signing'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
            <option value=1 <?= isset($input['signing']) && $input['signing'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_use')?></option>
        </select>
    </div>
</div>

<div class="form-group row option_maximum_signing"  <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label"></label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7">
            <span style="padding-right: 5px; margin: auto;"><?= $this->lang->line('company_maximum_signing_before')?></span>
            <input name="maximum_signing"
                   maxlength="100"
                   value="<?= $input['maximum_signing'] ?? ''?>"
                   type="text"
                   class="form-control"
                   value="">
            <span style="padding-left: 5px; margin: auto;"><?= $this->lang->line('company_maximum_signing_after')?></span>
        </div>
    </div>
</div>

<div class="form-group row option_signing_folder" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_signing_folder')?></label>
    <div class="col-sm-8">
        <select name="signing_folder" class="form-control">
            <option value=0 <?= isset($input['signing_folder']) && $input['signing_folder'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
            <option value=1 <?= isset($input['signing_folder']) && $input['signing_folder'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_use')?></option>
        </select>
    </div>
</div>
<div class="form-group row option_signing_folder_range" <?= isset($input['signing_folder']) && $input['signing_folder'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label"></label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <label class="col-sm-3 col-form-label"><?= $this->lang->line('company_plan_signing_folder_range')?></label>
            <div class="col-sm-8 form-inline">
                <div class="radio radio-primary radio-padding-left">
                    <input id="signing_folder_range_on_flag" type="radio" name="signing_folder_range" <?= (isset($input['signing_folder_range']) && $input['signing_folder_range'] == 1) ? 'checked' : '' ?> value=1>
                    <label for="signing_folder_range_on_flag"> <?= $this->lang->line('signing_folder_range_on_flag')?></label>
                </div>
                <div class="radio radio-primary">
                    <input id="signing_folder_range_off_flag" type="radio" name="signing_folder_range" <?= (isset($input['signing_folder_range']) && $input['signing_folder_range'] == 0) || !isset($input['signing_folder_range']) ? 'checked' : '' ?> value=0>
                    <label for="signing_folder_range_off_flag"> <?= $this->lang->line('signing_folder_range_off_flag')?></label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group row option_signing_internal_workflow" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_signing_internal_workflow') ?>
    </label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="signing_internal_workflow_on_flag" type="radio" name="signing_internal_workflow" <?= (isset($input['signing_internal_workflow']) && $input['signing_internal_workflow'] == 1) ? 'checked' : '' ?> value=1>
                <label for="signing_internal_workflow_on_flag"> <?= $this->lang->line('signing_internal_workflow_on_flag')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="signing_internal_workflow_off_flag" type="radio" name="signing_internal_workflow" <?= (isset($input['signing_internal_workflow']) && $input['signing_internal_workflow'] == 0) || !isset($input['signing_internal_workflow']) ? 'checked' : '' ?> value=0>
                <label for="signing_internal_workflow_off_flag"> <?= $this->lang->line('signing_internal_workflow_off_flag')?></label>
            </div>
        </div>
    </div>
</div>

<div class="form-group row option_signing_batch_import" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_signing_batch_import') ?>
    </label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="signing_batch_import_on_flag" type="radio" name="signing_batch_import" <?= (isset($input['signing_batch_import']) && $input['signing_batch_import'] == 1) ? 'checked' : '' ?> value=1>
                <label for="signing_batch_import_on_flag"> <?= $this->lang->line('signing_batch_import_on_flag')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="signing_batch_import_off_flag" type="radio" name="signing_batch_import" <?= (isset($input['signing_batch_import']) && $input['signing_batch_import'] == 0) || !isset($input['signing_batch_import']) ? 'checked' : '' ?> value=0>
                <label for="signing_batch_import_off_flag"> <?= $this->lang->line('signing_batch_import_off_flag')?></label>
            </div>
        </div>
    </div>
</div>

<div class="form-group row signing_options" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_import_signed_file') ?>
    </label>
    <div class="col-sm-8">
        <select name="import_signed_file" class="form-control">
            <option value=0 <?= isset($input['import_signed_file']) && $input['import_signed_file'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
            <option value=1 <?= isset($input['import_signed_file']) && $input['import_signed_file'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_use')?></option>
        </select>
    </div>
</div>
<div class="form-group row option_maximum_import_signed_file"  <?= isset($input['import_signed_file']) && $input['import_signed_file'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label"></label>
    <div class="col-sm-8">
        <div class="input-group col-sm-3">
            <span></span>
            <input name="maximum_import_signed_file"
                   maxlength="100"
                   value="<?= $input['maximum_import_signed_file'] ?? ''?>"
                   type="text"
                   class="form-control">
            <span style="padding-left: 5px; margin: auto;">
                <?= $this->lang->line('company_maximum_import_signed_file_after')?>
            </span>
        </div>
    </div>
</div>
<div class="form-group row option_maximum_import_signed_file" <?= isset($input['import_signed_file']) && $input['import_signed_file'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label"></label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="import_signed_file_timestamp_on_flag"
                       type="radio"
                       name="import_signed_file_timestamp" <?= (isset($input['import_signed_file_timestamp']) && $input['import_signed_file_timestamp'] == 1) ? 'checked' : '' ?>
                       value=1>
                <label for="import_signed_file_timestamp_on_flag"> <?= $this->lang->line('company_import_signed_file_timestamp_on')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="import_signed_file_timestamp_off_flag"
                       type="radio"
                       name="import_signed_file_timestamp" <?= (isset($input['import_signed_file_timestamp']) && $input['import_signed_file_timestamp'] == 0) || !isset($input['import_signed_file_timestamp']) ? 'checked' : '' ?>
                       value=0>
                <label for="import_signed_file_timestamp_off_flag"> <?= $this->lang->line('company_import_signed_file_timestamp_off')?></label>
            </div>
        </div>
    </div>
</div>
<div class="form-group row signing_options" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_ip_limit') ?>
    </label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="ip_limit_on_flag" type="radio" name="ip_limit" <?= (isset($input['ip_limit']) && $input['ip_limit'] == 1) ? 'checked' : '' ?> value=1>
                <label for="ip_limit_on_flag"> <?= $this->lang->line('company_agree_use')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="ip_limit_off_flag" type="radio" name="ip_limit" <?= (isset($input['ip_limit']) && $input['ip_limit'] == 0) || !isset($input['ip_limit']) ? 'checked' : '' ?> value=0>
                <label for="ip_limit_off_flag"> <?= $this->lang->line('company_agree_no_use')?></label>
            </div>
        </div>
    </div>
</div>
<div class="form-group row signing_options" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_send_sms') ?>
    </label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="send_sms_on_flag" type="radio" name="send_sms" <?= (isset($input['send_sms']) && $input['send_sms'] == 1) ? 'checked' : '' ?> value=1>
                <label for="send_sms_on_flag"> <?= $this->lang->line('company_agree_use')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="send_sms_off_flag" type="radio" name="send_sms" <?= (isset($input['send_sms']) && $input['send_sms'] == 0) || !isset($input['send_sms']) ? 'checked' : '' ?> value=0>
                <label for="send_sms_off_flag"> <?= $this->lang->line('company_agree_no_use')?></label>
            </div>
        </div>
    </div>
</div>
<div class="form-group row signing_options" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_electronic_signature_long_term') ?>
    </label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="electronic_signature_long_term_on_flag" type="radio" name="electronic_signature_long_term" <?= (isset($input['electronic_signature_long_term']) && $input['electronic_signature_long_term'] == 1) || !isset($input['electronic_signature_long_term']) ? 'checked' : '' ?> value=1>
                <label for="electronic_signature_long_term_on_flag"> <?= $this->lang->line('company_agree_use')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="electronic_signature_long_term_off_flag" type="radio" name="electronic_signature_long_term" <?= (isset($input['electronic_signature_long_term']) && $input['electronic_signature_long_term'] == 0)  ? 'checked' : '' ?> value=0>
                <label for="electronic_signature_long_term_off_flag"> <?= $this->lang->line('company_agree_no_use')?></label>
            </div>
        </div>
    </div>
</div>
<div class="form-group row signing_options" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_electronic_signature_timestamp') ?>
    </label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="electronic_signature_timestamp_on_flag" type="radio" name="electronic_signature_timestamp" <?= (isset($input['electronic_signature_timestamp']) && $input['electronic_signature_timestamp'] == 1) ? 'checked' : '' ?> value=1>
                <label for="electronic_signature_timestamp_on_flag"> <?= $this->lang->line('company_agree_use')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="electronic_signature_timestamp_off_flag" type="radio" name="electronic_signature_timestamp" <?= (isset($input['electronic_signature_timestamp']) && $input['electronic_signature_timestamp'] == 0) || !isset($input['electronic_signature_timestamp']) ? 'checked' : '' ?> value=0>
                <label for="electronic_signature_timestamp_off_flag"> <?= $this->lang->line('company_agree_no_use')?></label>
            </div>
        </div>
    </div>
</div>
<div class="form-group row signing_options" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_electronic_signature_authentication') ?>
    </label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="electronic_signature_authentication_on_flag" type="radio" name="electronic_signature_authentication" <?= (isset($input['electronic_signature_authentication']) && $input['electronic_signature_authentication'] == 1) ? 'checked' : '' ?> value=1>
                <label for="electronic_signature_authentication_on_flag"> <?= $this->lang->line('company_agree_use')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="electronic_signature_authentication_off_flag" type="radio" name="electronic_signature_authentication" <?= (isset($input['electronic_signature_authentication']) && $input['electronic_signature_authentication'] == 0) || !isset($input['electronic_signature_authentication']) ? 'checked' : '' ?> value=0>
                <label for="electronic_signature_authentication_off_flag"> <?= $this->lang->line('company_agree_no_use')?></label>
            </div>
        </div>
    </div>
</div>

<div class="form-group row signing_options" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_send_function_digital_signature') ?>
    </label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="send_function_digital_signature_on_flag"
                       type="radio"
                       name="send_function_digital_signature" <?= (isset($input['send_function_digital_signature']) && $input['send_function_digital_signature'] == 1) ? 'checked' : '' ?> value=1>
                <label for="send_function_digital_signature_on_flag"> <?= $this->lang->line('company_agree_use')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="send_function_digital_signature_off_flag"
                       type="radio"
                       name="send_function_digital_signature" <?= (isset($input['send_function_digital_signature']) && $input['send_function_digital_signature'] == 0) || !isset($input['send_function_digital_signature']) ? 'checked' : '' ?>
                       value=0>
                <label for="send_function_digital_signature_off_flag"> <?= $this->lang->line('company_agree_no_use')?></label>
            </div>
        </div>
    </div>
</div>

<div class="form-group row signing_options" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_imprint_registration') ?>
    </label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="imprint_registration_on_flag"
                       type="radio"
                       name="imprint_registration" <?= (isset($input['imprint_registration']) && $input['imprint_registration'] == 1) ? 'checked' : '' ?> value=1>
                <label for="imprint_registration_on_flag"> <?= $this->lang->line('company_agree_use')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="imprint_registration_off_flag"
                       type="radio"
                       name="imprint_registration" <?= (isset($input['imprint_registration']) && $input['imprint_registration'] == 0) || !isset($input['imprint_registration']) ? 'checked' : '' ?>
                       value=0>
                <label for="imprint_registration_off_flag"> <?= $this->lang->line('company_agree_no_use')?></label>
            </div>
        </div>
    </div>
</div>
<div class="form-group row signing_options" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_attach_file_receiver') ?>
    </label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="attach_file_receiver_on_flag"
                       type="radio"
                       name="attach_file_receiver" <?= (isset($input['attach_file_receiver']) && $input['attach_file_receiver'] == 1) ? 'checked' : '' ?> value=1>
                <label for="attach_file_receiver_on_flag"> <?= $this->lang->line('company_agree_use')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="attach_file_receiver_off_flag"
                       type="radio"
                       name="attach_file_receiver" <?= (isset($input['attach_file_receiver']) && $input['attach_file_receiver'] == 0) || !isset($input['attach_file_receiver']) ? 'checked' : '' ?>
                       value=0>
                <label for="attach_file_receiver_off_flag"> <?= $this->lang->line('company_agree_no_use')?></label>
            </div>
        </div>
    </div>
</div>
<div class="form-group row signing_options" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_attach_file_sender') ?>
    </label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="attach_file_sender_on_flag"
                       type="radio"
                       name="attach_file_sender" <?= (isset($input['attach_file_sender']) && $input['attach_file_sender'] == 1) ? 'checked' : '' ?> value=1>
                <label for="attach_file_sender_on_flag"> <?= $this->lang->line('company_agree_use')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="attach_file_sender_off_flag"
                       type="radio"
                       name="attach_file_sender" <?= (isset($input['attach_file_sender']) && $input['attach_file_sender'] == 0) || !isset($input['attach_file_sender']) ? 'checked' : '' ?>
                       value=0>
                <label for="attach_file_sender_off_flag"> <?= $this->lang->line('company_agree_no_use')?></label>
            </div>
        </div>
    </div>
</div>
<div class="form-group row signing_options" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_alert_function') ?>
    </label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="alert_function_on_flag"
                       type="radio"
                       name="alert_function" <?= (isset($input['alert_function']) && $input['alert_function'] == 1) ? 'checked' : '' ?> value=1>
                <label for="alert_function_on_flag"> <?= $this->lang->line('company_agree_use')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="alert_function_off_flag"
                       type="radio"
                       name="alert_function" <?= (isset($input['alert_function']) && $input['alert_function'] == 0) || !isset($input['alert_function']) ? 'checked' : '' ?>
                       value=0>
                <label for="alert_function_off_flag"> <?= $this->lang->line('company_agree_no_use')?></label>
            </div>
        </div>
    </div>
</div>
<div class="form-group row signing_options" <?= isset($input['signing']) && $input['signing'] == 1 ? '' : "style='display:none'" ?>>
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('company_plan_language_support') ?>
    </label>
    <div class="col-sm-8">
        <div class="input-group col-sm-7 ">
            <div class="radio radio-primary radio-padding-left">
                <input id="language_support_on_flag"
                       type="radio"
                       name="language_support" <?= (isset($input['language_support']) && $input['language_support'] == 1) ? 'checked' : '' ?> value=1>
                <label for="language_support_on_flag"> <?= $this->lang->line('company_agree_use')?></label>
            </div>
            <div class="radio radio-primary">
                <input id="language_support_off_flag"
                       type="radio"
                       name="language_support" <?= (isset($input['language_support']) && $input['language_support'] == 0) || !isset($input['language_support']) ? 'checked' : '' ?>
                       value=0>
                <label for="language_support_off_flag"> <?= $this->lang->line('company_agree_no_use')?></label>
            </div>
        </div>
    </div>
</div>