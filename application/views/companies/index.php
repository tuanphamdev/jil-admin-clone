<?php
$segment = 2;
$page = 'companies';
if($this->uri->segment(2) == 'search'){
	$segment = 3;
	$page = 'companies/search';
}
?>		
	<div class="card mb-3">
        <div class="card-body">
            <div id="accordion" role="tablist">
                <div class="">
                <div class="" role="tab" id="headingOne">
				<a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
					<i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('company_search_title')?>
				</a>
                </div>
                <form action="/companies/search" method="GET" autocomplete="off">
                    <div id="collapseOne" class="collapse  <?= $segment == 3 ? 'show' : '';?>" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                        <div class="card-body"> 
                            <input type="hidden" name="item_page" value="<?= $data['per_page'] ?? 0 ?>" class="form-control">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('company_code')?></label>
                                    <input type="text" name="code" value="<?= $input['code'] ?? '' ?>" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('company_name')?></label>
                                    <input type="text" name="name" value="<?= $input['name'] ?? '' ?>" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('company_status')?></label>
                                    <select name="status" class="form-control">
                                        <option value="" ><?= $this->lang->line('ui_option_all') ?></option>
                                        <option value=0 <?= isset($input['status']) && $input['status'] === '0' ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_active')?></option>
                                        <option value=1 <?= isset($input['status']) && $input['status'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_active')?></option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('company_agreement_type')?></label>
                                    <select name="type" class="form-control">
                                        <option value="" ><?= $this->lang->line('ui_option_all') ?></option>
                                        <option value=0 <?= isset($input['type']) && $input['type'] == '0' ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_trial')?></option>
                                        <option value=1 <?= isset($input['type']) && $input['type'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_free')?></option>
                                        <option value=2 <?= isset($input['type']) && $input['type'] == 2 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_fee')?></option>
                                        <option value=3 <?= isset($input['type']) && $input['type'] == 3 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_fee_sub_account')?></option>
                                        <option value=4 <?= isset($input['type']) && $input['type'] == 4 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_cancel_contract')?></option>
                                        <option value=5 <?= isset($input['type']) && $input['type'] == 5 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_agency')?></option>
                                        <option value=6 <?= isset($input['type']) && $input['type'] == 6 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_business')?></option>
                                        <option value=7 <?= isset($input['type']) && $input['type'] == 7 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_development')?></option>
                                        <option value=8 <?= isset($input['type']) && $input['type'] == 8 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_division')?></option>
                                        <option value=9 <?= isset($input['type']) && $input['type'] == 9 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_other')?></option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('company_agreement_start_date_from')?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input name='start_date_from' id='start_date_from' value="<?= $input['start_date_from'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('company_agreement_start_date_to')?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input name='start_date_to' id='start_date_to' value="<?= $input['start_date_to'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('company_agreement_end_date_from')?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input name='end_date_from' id='end_date_from' value="<?= $input['end_date_from'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for=""><?= $this->lang->line('company_agreement_end_date_to')?></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input name='end_date_to' id='end_date_to' value="<?= $input['end_date_to'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>
                            </div>
                            <div class="form text-right">
                                <button role="button" class="btn btn-info" href="#"><i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('company_search_btn')?></button>
                                <a class="btn btn-secondary" href="/companies"><i class="fa fa-fw fa-close"> </i> <?= $this->lang->line('company_clear_btn')?></a>
                            </div>
                        </div>
                    </div>
				
			    </form>
                </div>
            </div>
        </div>							
    </div>

    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <?php $this->load->view('templates/per_page', ['per_page' => $data['per_page'] ?? 0]);?>
                </div>
                <?php if($permissions['company'] == FULL_PERMISSION):?>
                <div class="col text-right">
                    <a role="button" class="btn btn-primary" href="/companies/create"><i class="fa fa-pencil bigfonts bigfonts" aria-hidden="true"> </i> <?= $this->lang->line('company_create_btn')?></a>
                    <button class="btn btn-change_status" role="button" >
                        <?= $this->lang->line('company_change_status_show_modal_btn')?>
                    </button>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="card-body">
            <?php $this->load->view('templates/alert');?>
            <div class="alert alert-danger" id="error-checkbox" style="display: none">
                <?= '変更する企業を選択して下さい' ?>
                <button type="button" class="close">
                    <span>×</span>
                </button>
            </div>


            <table class="table table-responsive-col table-striped">
                <thead>
                <tr>
                    <th scope="col-1"><input type="checkbox" value="all" name="companies_checkbox" class="checkbox_all"></th>
                    <th scope="col-2"><?= $this->lang->line('company_code')?></th>
                    <th scope="col-1"><?= $this->lang->line('company_name')?></th>
                    <th scope="col-1"><?= $this->lang->line('company_application_name_title')?></th>
                    <th scope="col-1"><?= $this->lang->line('company_agreement_type')?></th>
                    <th scope="col-1"><?= $this->lang->line('company_service_type')?></th>
                    <th scope="col-1"><?= $this->lang->line('company_agreement_num_account')?></th>
                    <th scope="col-1"><?= $this->lang->line('company_status')?></th>
                    <th scope="col-1"><?= $this->lang->line('company_edit_function')?></th>
                    <th scope="col-1"></th>
                </tr>
                </thead>
                <tbody>
            <?php
            if(!empty($data['company'])):
            foreach ($data['company'] as $key => $value):

            ?>
                <tr>
                    <td> <input type="checkbox" name="companies_checkbox" value="<?= $value["id"] ?>" class="checkbox_one"></td>
                    <td><a href="<?= site_url('/companies/update/' . $value["id"])?>"><?= $value['code']??''?></a></td>
                    <td><?= $value['name']??''?></td>
                    <td>
                        <?= $value['application_furigana_surname'] ?? '' ?>
                        <?= $value['application_furigana'] ?? '' ?>
                        <br/>
                        <?= $value['application_surname'] ?? '' ?>
                        <?= $value['application_name'] ?? '' ?>
                    </td>
                    <td>
                    <?php
                        $agree_type = $this->config->item('agreement_type')[$value['type']??''];
                        echo $this->lang->line('company_agree_'.$agree_type);
                    ?>
                    </td>
                    <td>
                    <?php
                    if(!empty($value['jinjer_db'])){
                        echo '<p class="h6">'.$this->lang->line('company_plan_jinjer_db').'</p>';
                    }
                    if(!empty($value['kintai'])){
                        echo '<p class="h6">'.$this->lang->line('company_plan_kintai').'</p>';
                    }
                    if(!empty($value['roumu'])){
                        echo '<p class="h6">'.$this->lang->line('company_plan_roumu').'</p>';
                    }
                    if(!empty($value['jinji'])){
                        echo '<p class="h6">'.$this->lang->line('company_plan_jinji').'</p>';
                    }
                    if(!empty($value['keihi'])){
                        echo '<p class="h6">'.$this->lang->line('company_plan_keihi').'</p>';
                    }
                    if(!empty($value['my_number'])){
                        echo '<p class="h6">'.$this->lang->line('company_plan_my_number').'</p>';
                    }
                    if(!empty($value['work_vital'])){
                        echo '<p class="h6">'.$this->lang->line('company_plan_work_vital').'</p>';
                    }
                    if(!empty($value['salary'])){
                        echo '<p class="h6">'.$this->lang->line('company_plan_salary').'</p>';
                    }
                    if(!empty($value['workflow'])){
                        echo '<p class="h6">'.$this->lang->line('company_plan_workflow').'</p>';
                    }
                    if(!empty($value['employee_contract'])){
                        echo '<p class="h6">'.$this->lang->line('company_plan_employee_contract').'</p>';
                    }
                    if(!empty($value['signing'])){
                        echo '<p class="h6">'.$this->lang->line('company_plan_signing').'</p>';
                    }
                    if(!empty($value['year_end_tax_adjustment'])){
                        echo '<p class="h6">'.$this->lang->line('company_plan_year_end_tax_adjustment').'</p>';
                    }
                    ?>
                    </td>
                    <td><?= $value['num_account']??''?></td>
                    <td>
                    <?php
                        echo isset($value['status']) && $value['status'] == 1
                        ? $this->lang->line('company_agree_active')
                        : $this->lang->line('company_agree_no_active');
                    ?>
                    </td>
                    <td> <!-- 再送信 -->
                        <?php
                            $modal_id = 'modal_resend_account_info';
                            $button_label = $this->lang->line('company_resend_account_info');
                        ?>

                        <?php if($permissions['company'] == FULL_PERMISSION && jp_current_date() <= $value['end_date']):?>
                        <button data-company-name="<?= $value['name'] ?>" data-company-id="<?= $value['id'] ?>"  data-toggle="modal" data-target="#<?= $modal_id ?>" role="button" href="#" class="btn btn-resend-email">
                            <?= $button_label ?>
                        </button>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php
                            $modal_id = isset($value['status']) && $value['status'] == 1 ? 'modal_stop_service' : 'modal_start_service';
                            $button_class = isset($value['status']) && $value['status'] == 1 ? 'warning' : 'success';
                            $button_label = $this->lang->line(isset($value['status']) && $value['status'] == 1 ? 'company_stop_service' : 'company_start_service');
                        ?>
                        <?php if($permissions['company'] == FULL_PERMISSION && jp_current_date() <= $value['end_date']):?>
                        <button data-company-name="<?= $value['name'] ?>" data-start-date="<?= $value['start_date'] ?>"  data-end-date="<?= $value['end_date'] ?>" data-company-id="<?= $value['id'] ?>"  data-company-code="<?= $value['code'] ?>" data-toggle="modal" data-target="#<?= $modal_id ?>" role="button" href="#" class="btn btn-<?= $button_class ?>">
                            <?= $button_label ?>
                        </button>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php
            endforeach;
            endif;
            ?>

            <?php if(empty($data['company'])):?>
            <tr>
                <td colspan="12">
            <?= $this->lang->line('company_no_result_found')?>
                </td>
            </tr>
            <?php endif; ?>
                </tbody>
            </table>

            <?php $this->load->view('companies/modals/stop_service', ['stop_service_message' => '']);?>
            <?php $this->load->view('companies/modals/start_service', ['start_service_message' => '']);?>
            <?php $this->load->view('companies/modals/resend_account_info', ['message' => '']);?>
            <?php $this->load->view('companies/modals/change_status', ['message' => '']);?>

            <!-- Pagination -->
            <?php
            $pagination =  pagination(base_url($page), $data['total'] ?? 0 , $data['per_page'] ?? 0, $segment);
            ?>
            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <?= $pagination['statistic'];?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <nav aria-label="">
                        <?= $pagination['pagination'];?>
                    </nav>
                </div>
            </div>
            <!-- End Pagination -->
        </div>
    </div>

    <script src="<?= site_url('assets/js/companies/custom.js') ?>"></script>
    <script src="<?= site_url('assets/js/companies/change_status.js') ?>"></script>
