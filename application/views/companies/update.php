
<div class="card mb-3">
    <div class="card-body">
        <?php $this->load->view('templates/alert');?>
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#company_info" role="tab" aria-controls="home" aria-selected="true">企業情報</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#contract_info" role="tab" aria-controls="profile" aria-selected="false">契約情報</a>
                    </li>
                </ul>
            </div>

            <?php echo form_open('/companies/do_update/' . $input['id'] ?? '0', ['autocomplete' => 'off', 'method' => 'POST', 'id' => 'form_update_company']); ?>

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="company_info" role="tabpanel" aria-labelledby="home-tab">
                        <div class="card-body">

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_code')?></label>
                                <div class="col-sm-8">
                                    <input name="code" value="<?= $input['code'] ?? ''?>" type="text"   maxlength="10" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">
                                    <?php
                                    echo $this->lang->line('company_enigma_code');
                                    ?>
                                </label>
                                <div class="col-sm-8 form-inline">
                                    <?php
                                        $enigma_icon_status = '<i id="enigma_status"></i>';
                                        $enigma_code_background = '';
                                        if (isset($input['enigma_version']) && $input['enigma_version'] > 0) {
                                            if (isset($input['enigma_status']) && $input['enigma_status'] === '1') {
                                                $enigma_icon_status = '<i id="enigma_status" class="fa fa-check-circle enigma-flag-green"></i>';
                                                $enigma_code_background = 'enigma-code-valid';
                                            }
                                            else {
                                                $enigma_icon_status = '<i id="enigma_status" class="fa fa-close enigma-flag-red"></i>';
                                                $enigma_code_background = 'enigma-code-invalid';
                                            }
                                        }
                                    ?>

                                    <input id="enigma_code" name="enigma_code" value="<?= $input['enigma_code'] ?? ''?>" type="text" maxlength="20"  class="form-control <?=$enigma_code_background;?>">
                                    <label id="enigma_name" style="margin-left: 15px;" for="enigma_code"> <?= $input['enigma_name'] ?? ''?></label>
                                    <?php
                                        echo $enigma_icon_status;
                                    ?>
                                    <label id="enigma_search"></label>
                                    <input id="enigma_version_value" type="hidden" value="<?= $input['enigma_version'] ?? '0'; ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_enigma_version')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="enigma_version_no_use" type="radio" name="enigma_version"  <?= (isset($input['enigma_version']) && $input['enigma_version'] == 0) || !isset($input['enigma_version'])? 'checked' : '' ?> value=0>
                                        <label for="enigma_version_no_use"> <?= $this->lang->line('company_enigma_version_no_use')?></label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="enigma_version_ver_1" type="radio" name="enigma_version"  <?= (isset($input['enigma_version']) && $input['enigma_version'] == 1)? 'checked' : '' ?> value=1>
                                        <label for="enigma_version_ver_1"> <?= $this->lang->line('company_enigma_version_1')?></label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="enigma_version_ver_2" type="radio" name="enigma_version"  <?= (isset($input['enigma_version']) && $input['enigma_version'] == 2)? 'checked' : '' ?> value=2>
                                        <label for="enigma_version_ver_2"> <?= $this->lang->line('company_enigma_version_2')?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_name')?></label>
                                <div class="col-sm-8">
                                    <input name="name" value="<?= $input['name'] ?? ''?>" type="text"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_name_furigana')?></label>
                                <div class="col-sm-8">
                                    <input name="name_furigana" value="<?= $input['name_furigana'] ?? ''?>" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_representative_name')?></label>
                                <div class="col-sm-8">
                                    <input name="representative_name" value="<?= $input['representative_name'] ?? ''?>" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_representative_name_furigana')?></label>
                                <div class="col-sm-8">
                                    <input name="representative_name_furigana" value="<?= $input['representative_name_furigana'] ?? ''?>" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_representative_title')?></label>
                                <div class="col-sm-8">
                                    <input name="representative_title" value="<?= $input['representative_title'] ?? ''?>" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_tel')?></label>
                                <div class="col-sm-8">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <input type="tel" name="tel[0]" value="<?= $input['tel'][0] ?? ''?>" placeholder="00000" class="form-control" size="6" maxlength="5">
                                        </div>
                                        <div class="form-group dash">
                                            <input type="tel" name="tel[1]" value="<?= $input['tel'][1] ?? ''?>" placeholder="0000" class="form-control" size="5" maxlength="4">
                                        </div>
                                        <div class="form-group dash">
                                            <input type="tel" name="tel[2]" value="<?= $input['tel'][2] ?? ''?>"  placeholder="0000" class="form-control" size="5" maxlength="4">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label dash"><?= $this->lang->line('company_fax')?></label>
                                <div class="col-sm-8">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <input type="tel" name="fax[0]" value="<?= $input['fax'][0] ?? ''?>"  placeholder="00000" class="form-control" size="6" maxlength="5">
                                        </div>
                                        <div class="form-group dash">
                                            <input type="tel" name="fax[1]" value="<?= $input['fax'][1] ?? ''?>" placeholder="0000" class="form-control" size="5" maxlength="4">
                                        </div>
                                        <div class="form-group dash">
                                            <input type="tel" name="fax[2]" value="<?= $input['fax'][2] ?? ''?>" placeholder="0000" class="form-control" size="5" maxlength="4">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_zip')?></label>
                                <div class="col-sm-8">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <input type="tel" name="zip[0]" value="<?= $input['zip'][0] ?? ''?>"  placeholder="000" class="form-control" size="6" maxlength="3">
                                        </div>
                                        <div class="form-group dash">
                                            <input type="tel" name="zip[1]" value="<?= $input['zip'][1] ?? ''?>" placeholder="0000" class="form-control" size="6" maxlength="4">
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_prefecture')?></label>
                                <div class="col-sm-8">
                                    <select class="form-control filter_text" id="prefecture" name="prefecture">
                                        <option value="" ><?= $this->lang->line('company_option_null')?></option>
                                        <?php
                                        if(!empty($prefecture)){
                                            foreach ($prefecture as $pre_id => $pre_val) {
                                                $prefecture = $input['prefecture'] ?? '';
                                                $selected = $prefecture == $pre_val['id'] ? 'selected' : '';
                                                echo "<option data-iso_id={$pre_val['iso_id']} value={$pre_val['id']} {$selected}>{$pre_val['name']}</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_city')?></label>
                                <div class="col-sm-8">
                                    <select class="form-control filter_text" id="city" name="city">
                                        <option value="" ><?= $this->lang->line('company_option_null')?></option>
                                        <?php
                                        if(!empty($city)){
                                            foreach ($city as $city_id => $city_val) {
                                                $city = $input['city'] ?? '';
                                                $selected = $city == $city_val['id'] ? 'selected' : '';
                                                echo "<option value={$city_val['id']} {$selected}>{$city_val['name']}</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_address')?></label>
                                <div class="col-sm-8">
                                    <input name="address" value="<?= $input['address'] ?? ''?>" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_address_furigana')?></label>
                                <div class="col-sm-8">
                                    <input name="address_furigana" value="<?= $input['address_furigana'] ?? ''?>" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_application_surname')?></label>
                                <div class="col-sm-8">
                                    <input name="application_surname" value="<?= $input['application_surname'] ?? ''?>" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_application_name')?></label>
                                <div class="col-sm-8">
                                    <input name="application_name" value="<?= $input['application_name'] ?? ''?>" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_application_furigana_surname')?></label>
                                <div class="col-sm-8">
                                    <input name="application_furigana_surname" value="<?= $input['application_furigana_surname'] ?? ''?>" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_application_furigana')?></label>
                                <div class="col-sm-8">
                                    <input name="application_furigana" value="<?= $input['application_furigana'] ?? ''?>" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_application_email')?></label>
                                <div class="col-sm-8">
                                    <input name="application_email" value="<?= $input['application_email'] ?? ''?>" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_registered_date')?></label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                        <input name="registered_date" value="<?= $input['registered_date'] ?? ''?>" type="text" class="form-control datepicker" value="" size=15 placeholder="yyyy-mm-dd">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_is_tokyo_marine')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="is_tokyo_marine_no_use" type="radio" name="is_tokyo_marine"  <?= (isset($input['is_tokyo_marine']) && $input['is_tokyo_marine'] == 0) || !isset($input['is_tokyo_marine']) ? 'checked' : '' ?> value=0>
                                        <label for="is_tokyo_marine_no_use"> <?= $this->lang->line('tokyo_marine_no_use_flag')?></label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="is_tokyo_marine_use" type="radio" name="is_tokyo_marine"  <?= (isset($input['is_tokyo_marine']) && $input['is_tokyo_marine'] == 1) ? 'checked' : '' ?> value=1>
                                        <label for="is_tokyo_marine_use"> <?= $this->lang->line('tokyo_marine_use_flag')?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_ai_option')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="is_using_ai_off" type="radio" name="is_using_ai"  <?= (isset($input['is_using_ai']) && $input['is_using_ai'] == 0) || !isset($input['is_using_ai']) ? 'checked' : '' ?> value = 0>
                                        <label for="is_using_ai_off"> <?= $this->lang->line('company_is_using_ai_flag_off')?></label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="is_using_ai_on" type="radio" name="is_using_ai"  <?= (isset($input['is_using_ai']) && $input['is_using_ai'] == 1) ? 'checked' : '' ?> value = 1>
                                        <label for="is_using_ai_on"> <?= $this->lang->line('company_is_using_ai_flag_on')?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_ultra_option')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="is_using_ultra_off" type="radio" name="is_using_ultra"  <?= (isset($input['is_using_ultra']) && $input['is_using_ultra'] == 0) || !isset($input['is_using_ultra']) ? 'checked' : '' ?> value = 0>
                                        <label for="is_using_ultra_off"> <?= $this->lang->line('company_is_using_ultra_flag_off')?></label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="is_using_ultra_on" type="radio" name="is_using_ultra"  <?= (isset($input['is_using_ultra']) && $input['is_using_ultra'] == 1) ? 'checked' : '' ?> value = 1>
                                        <label for="is_using_ultra_on"> <?= $this->lang->line('company_is_using_ultra_flag_on')?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_docomo_option')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="is_docomo_off" type="radio" name="is_docomo"  <?= (isset($input['is_docomo']) && $input['is_docomo'] == 0) || !isset($input['is_docomo']) ? 'checked' : '' ?> value = 0>
                                        <label for="is_docomo_off"> <?= $this->lang->line('company_is_docomo_flag_off')?></label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="is_docomo_on" type="radio" name="is_docomo"  <?= (isset($input['is_docomo']) && $input['is_docomo'] == 1) ? 'checked' : '' ?> value = 1>
                                        <label for="is_docomo_on"> <?= $this->lang->line('company_is_docomo_flag_on')?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row docomo-identifier_account" <?= isset($input['is_docomo']) && $input['is_docomo'] == 1 ? '' : "style='display:none'" ?>>
                                <label class="col-sm-4 col-form-label">
                                    <?= $this->lang->line('company_d_account_id')?>
                                </label>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <input name="d_account_id"
                                               value="<?= htmlentities($input['d_account_id']) ?? ''?>"
                                               type="text"
                                               class="form-control"
                                               maxlength="<?= DOCOMO_IDENTIFIER_ACCOUNT_MAX_LENGTH ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- contract_info -->
                    <div class="tab-pane fade" id="contract_info" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_agreement_type')?></label>
                                <div class="col-sm-8">
                                    <select name="type" class="form-control">
                                        <option value=0 <?= isset($input['type']) && $input['type'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_trial')?></option>
                                        <option value=1 <?= isset($input['type']) && $input['type'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_free')?></option>
                                        <option value=2 <?= isset($input['type']) && $input['type'] == 2 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_fee')?></option>
                                        <option value=3 <?= isset($input['type']) && $input['type'] == 3 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_fee_sub_account')?></option>
                                        <option value=4 <?= isset($input['type']) && $input['type'] == 4 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_cancel_contract')?></option>
                                        <option value=5 <?= isset($input['type']) && $input['type'] == 5 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_agency')?></option>
                                        <option value=6 <?= isset($input['type']) && $input['type'] == 6 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_business')?></option>
                                        <option value=7 <?= isset($input['type']) && $input['type'] == 7 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_development')?></option>
                                        <option value=8 <?= isset($input['type']) && $input['type'] == 8 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_division')?></option>
                                        <option value=9 <?= isset($input['type']) && $input['type'] == 9 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_other')?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_period')?></label>
                                <div class="col-sm-2">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                        <input name="start_date" id='start_date' value="<?= $input['start_date'] ?? ''?>" type="text" class="form-control datepicker" value="" size="15" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                        <input name="end_date" id='end_date' value="<?= $input['end_date'] ?? ''?>" type="text" class="form-control datepicker" value="" size="15" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_agreement_num_account')?></label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input name="num_account" value="<?= $input['num_account'] ?? ''?>" type="text" class="form-control" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_jinjer_db')?></label>
                                <div class="col-sm-8">
                                    <?php $style = 'style="display:none"' ?>
                                    <select name="jinjer_db" class="form-control">
                                        <option value=0
                                                <?= $jinjer_DB_status === 1 ? $style : '' ?>
                                            <?= isset($input['jinjer_db']) && $input['jinjer_db'] == 0  ? 'selected' : '' ?>
                                        >
                                            <?= $this->lang->line('company_agree_no_use')?>
                                        </option>
                                        <option value=1
                                                <?= $jinjer_DB_status === 0 ? $style : '' ?>
                                            <?= isset($input['jinjer_db']) && $input['jinjer_db'] == 1 ? 'selected' : '' ?>
                                        >
                                            <?= $this->lang->line('company_agree_use')?>
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_kintai')?></label>
                                <div class="col-sm-8">
                                    <select name="kintai" class="form-control is_contract">
                                        <option value=0 <?= isset($input['kintai']) && $input['kintai'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
                                        <option value=1 <?= isset($input['kintai']) && $input['kintai'] == 1 ? 'selected' : '' ?>><?= $this->lang->line('company_agree_use')?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row option_paid_management" <?= isset($input['kintai']) && $input['kintai'] == 1 ? '' : "style='display:none'" ?>>
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_option_paid_management')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="option_paid_management_no_use" type="radio" name="is_paid_management" checked value="0" <?= isset($input['is_paid_management']) && $input['is_paid_management'] == 0 ? 'checked' : '' ?>>
                                        <label for="option_paid_management_no_use"> 利用しない</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="option_paid_management_use" type="radio" name="is_paid_management" value="1" <?= isset($input['is_paid_management']) && $input['is_paid_management'] == 1 ? 'checked' : '' ?>>
                                        <label for="option_paid_management_use"> 利用する</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row option_paid_management" <?= isset($input['kintai']) && $input['kintai'] == 1 ? '' : "style='display:none'" ?>>
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_option_shift_management')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="option_shift_management_no_use" type="radio" name="is_shift_management" checked value="0" <?= isset($input['is_shift_management']) && $input['is_shift_management'] == 0 ? 'checked' : '' ?>>
                                        <label for="option_shift_management_no_use"> 利用しない</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="option_shift_management_use" type="radio" name="is_shift_management" value="1" <?= isset($input['is_shift_management']) && $input['is_shift_management'] == 1 ? 'checked' : '' ?>>
                                        <label for="option_shift_management_use"> 利用する</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row option_paid_management" <?= isset($input['kintai']) && $input['kintai'] == 1 ? '' : "style='display:none'" ?>>
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_option_overtime_management')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="option_overtime_management_no_use" type="radio" name="is_overtime_management" checked value="0" <?= isset($input['is_overtime_management']) && $input['is_overtime_management'] == 0 ? 'checked' : '' ?>>
                                        <label for="option_overtime_management_no_use"> 利用しない</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="option_overtime_management_use" type="radio" name="is_overtime_management" value="1" <?= isset($input['is_overtime_management']) && $input['is_overtime_management'] == 1 ? 'checked' : '' ?>>
                                        <label for="option_overtime_management_use"> 利用する</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row option_paid_management" <?= isset($input['kintai']) && $input['kintai'] == 1 ? '' : "style='display:none'" ?>>
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_kintai_sense_link')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="option_kintai_sense_link_no_use" type="radio" name="kintai_sense_link" value="0" <?= isset($input['kintai_sense_link']) && $input['kintai_sense_link'] == 0 || !isset($input['kintai_sense_link']) ? 'checked' : '' ?>>
                                        <label for="option_kintai_sense_link_no_use"> 利用しない</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="option_kintai_sense_link_use" type="radio" name="kintai_sense_link" value="1" <?= isset($input['kintai_sense_link']) && $input['kintai_sense_link'] == 1 ? 'checked' : '' ?>>
                                        <label for="option_kintai_sense_link_use"> 利用する</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row option_paid_management" <?= isset($input['kintai']) && $input['kintai'] == 1 ? '' : "style='display:none'" ?>>
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_kintai_alligate')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="option_kintai_alligate_no_use" type="radio" name="kintai_alligate" value="0" <?= isset($input['kintai_alligate']) && $input['kintai_alligate'] == 0 || !isset($input['kintai_alligate']) ? 'checked' : '' ?>>
                                        <label for="option_kintai_alligate_no_use"> 利用しない</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="option_kintai_alligate_use" type="radio" name="kintai_alligate" value="1" <?= isset($input['kintai_alligate']) && $input['kintai_alligate'] == 1 ? 'checked' : '' ?>>
                                        <label for="option_kintai_alligate_use"> 利用する</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_roumu')?></label>
                                <div class="col-sm-8">
                                    <select name="roumu" class="form-control is_contract">
                                        <option value=0 <?= isset($input['roumu']) && $input['roumu'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
                                        <option value=1 <?= isset($input['roumu']) && $input['roumu'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_use')?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_jinji')?></label>
                                <div class="col-sm-8">
                                    <select name="jinji" class="form-control is_contract">
                                        <option value=0 <?= isset($input['jinji']) && $input['jinji'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
                                        <option value=1 <?= isset($input['jinji']) && $input['jinji'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_use')?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_keihi')?></label>
                                <div class="col-sm-8">
                                    <select name="keihi" class="form-control is_contract">
                                        <option value=0 <?= isset($input['keihi']) && $input['keihi'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
                                        <option value=1 <?= isset($input['keihi']) && $input['keihi'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_use')?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row option_ai_ocr" <?= isset($input['keihi']) && $input['keihi'] == 1 ? '' : "style='display:none'" ?>>
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_option_AI_OCR')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="option_ai_ocr_no_use" type="radio" name="is_using_ai_ocr" checked value="0" <?= isset($input['is_using_ai_ocr']) && $input['is_using_ai_ocr'] == 0 ? 'checked' : '' ?>>
                                        <label for="option_ai_ocr_no_use"> 利用しない</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="option_ai_ocr_use" type="radio" name="is_using_ai_ocr" value="1" <?= isset($input['is_using_ai_ocr']) && $input['is_using_ai_ocr'] == 1 ? 'checked' : '' ?>>
                                        <label for="option_ai_ocr_use"> 利用する</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row option_timestamp" <?= isset($input['keihi']) && $input['keihi'] == 1 ? '' : "style='display:none'" ?>>
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_option_timestamp')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="option_timestamp_no_use" type="radio" name="timestamp" checked value="0" <?= isset($input['timestamp']) && $input['timestamp'] == 0 ? 'checked' : '' ?>>
                                        <label for="option_timestamp_no_use"> 利用しない</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="option_timestamp_use" type="radio" name="timestamp" value="1" <?= isset($input['timestamp']) && $input['timestamp'] == 1 ? 'checked' : '' ?>>
                                        <label for="option_timestamp_use"> 利用する</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row option_ebook_store" <?= isset($input['keihi']) && $input['keihi'] == 1 ? '' : "style='display:none'" ?>>
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_option_ebook_store')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="option_ebook_store_no_use" type="radio" name="is_ebook_store" checked value="0" <?= isset($input['is_ebook_store']) && $input['is_ebook_store'] == 0 ? 'checked' : '' ?>>
                                        <label for="option_ebook_store_no_use"> 利用しない</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="option_ebook_store_use" type="radio" name="is_ebook_store" value="1" <?= isset($input['is_ebook_store']) && $input['is_ebook_store'] == 1 ? 'checked' : '' ?>>
                                        <label for="option_ebook_store_use"> 利用する</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row option_credit_card" <?= isset($input['keihi']) && $input['keihi'] == 1 ? '' : "style='display:none'" ?>>
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_option_credit_card')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="option_credit_card_no_use" type="radio" name="is_credit_card" checked value="0" <?= isset($input['is_credit_card']) && $input['is_credit_card'] == 0 ? 'checked' : '' ?>>
                                        <label for="option_credit_card_no_use"> 利用しない</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="option_credit_card_use" type="radio" name="is_credit_card" value="1" <?= isset($input['is_credit_card']) && $input['is_credit_card'] == 1 ? 'checked' : '' ?>>
                                        <label for="option_credit_card_use"> 利用する</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_my_number')?></label>
                                <div class="col-sm-8">
                                    <select name="my_number" class="form-control is_contract">
                                        <option value=0 <?= isset($input['my_number']) && $input['my_number'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
                                        <option value=1 <?= isset($input['my_number']) && $input['my_number'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_use')?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_work_vital')?></label>
                                <div class="col-sm-8">
                                    <select name="work_vital" class="form-control is_contract">
                                        <option value=0 <?= isset($input['work_vital']) && $input['work_vital'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
                                        <option value=1 <?= isset($input['work_vital']) && $input['work_vital'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_use')?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_salary')?></label>
                                <div class="col-sm-8">
                                    <select name="salary" class="form-control is_contract">
                                        <option value=0 <?= isset($input['salary']) && $input['salary'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
                                        <option value=1 <?= isset($input['salary']) && $input['salary'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_use')?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row option_payslip" <?= isset($input['salary']) && $input['salary'] == 1 ? '' : "style='display:none'" ?>>
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_option_payslip')?></label>
                                <div class="col-sm-8 form-inline">
                                    <div class="radio radio-primary radio-padding-left">
                                        <input id="option_payslip_no_use" type="radio" name="is_payslip" checked value="0" <?= isset($input['is_payslip']) && $input['is_payslip'] == 0 ? 'checked' : '' ?>>
                                        <label for="option_payslip_no_use"> 利用しない</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input id="option_payslip_use" type="radio" name="is_payslip" value="1" <?= isset($input['is_payslip']) && $input['is_payslip'] == 1 ? 'checked' : '' ?>>
                                        <label for="option_payslip_use"> 利用する</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_workflow')?></label>
                                <div class="col-sm-8">
                                    <select name="workflow" class="form-control is_contract">
                                        <option value=0 <?= isset($input['workflow']) && $input['workflow'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
                                        <option value=1 <?= isset($input['workflow']) && $input['workflow'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_use')?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_employee_contract')?></label>
                                <div class="col-sm-8">
                                    <select name="employee_contract" class="form-control is_contract">
                                        <option value=0 <?= isset($input['employee_contract']) && $input['employee_contract'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
                                        <option value=1 <?= isset($input['employee_contract']) && $input['employee_contract'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_use')?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_bi')?></label>
                                <div class="col-sm-8">
                                    <select name="bi" class="form-control is_contract">
                                        <option value=0 <?= isset($input['bi']) && $input['bi'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
                                        <option value=1 <?= isset($input['bi']) && $input['bi'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_use')?></option>
                                    </select>
                                </div>
                            </div>

                            <?php
                            $this->load->view('companies/signing/update');
                            ?>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"><?= $this->lang->line('company_plan_year_end_tax_adjustment')?></label>
                                <div class="col-sm-8">
                                    <select name="year_end_tax_adjustment" class="form-control is_contract">
                                        <option value=0 <?= isset($input['year_end_tax_adjustment']) && $input['year_end_tax_adjustment'] == 0 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_no_use')?></option>
                                        <option value=1 <?= isset($input['year_end_tax_adjustment']) && $input['year_end_tax_adjustment'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('company_agree_use')?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-group row align-items-end">
                    <div class="col col align-self-center">
                        <div class="form text-center">
                            <?php if($permissions['company'] == FULL_PERMISSION):?>
                            <button type="submit" id="button-submit-save" class="btn btn-primary btn-md"><?= $this->lang->line('btn_confirm')?></button>
                            <?php endif; ?>
                            <a href="<?= site_url('companies')?>" class="btn btn-warning btn-md"><?= $this->lang->line('btn_return')?></a>
                        </div>
                    </div>
                </div>
        </div>
        <input name="id" value="<?= $input['id'] ?? ''?>" type="hidden">
        <input name="company_id" value="<?= $input['id'] ?? ''?>" type="hidden">
        <input name="plan_id" value="<?= $input['plan_id'] ?? ''?>" type="hidden">
        <input name="agreement_id" value="<?= $input['agreement_id'] ?? ''?>" type="hidden">
        <input name="plan_signing_id" value="<?= $input['plan_signing_id'] ?? ''?>" type="hidden">
        <input name="status" value="<?= $input['status'] ?? ''?>" type="hidden">
        <input name="stop_date" value="<?= $input['stop_date'] ?? ''?>" type="hidden">
        </form>
    </div>
</div>
<script src="<?= site_url('assets/js/companies/custom.js') ?>"></script>
