<script>
    $("button[data-toggle='modal']").click(function(){
        var company_id = $(this).attr("data-company-id");
        var company_name = $(this).attr("data-company-name");
        var start_date = $(this).attr("data-start-date");
        var end_date = $(this).attr("data-end-date");
        $(".modal-body span").html('【'+company_name+'】');
        $("#form_start_service input[name='company_id']").val(company_id);
        $("#form_start_service input[name='start_date']").val(start_date);
        $("#form_start_service input[name='end_date']").val(end_date);
        $("#form_start_service input[name='company_name']").val(company_name);
    });
</script>

<?php echo form_open("/companies/start_service", ['autocomplete' => 'off', 'method' => 'POST', 'id' => 'form_start_service']); ?>
    <input type="hidden" value="" name="company_id">
    <input type="hidden" value="" name="start_date">
    <input type="hidden" value="" name="end_date">
    <input type="hidden" value="" name="company_name">
    <div class="modal fade" id="modal_start_service" tabindex="-1" role="dialog" aria-labelledby="modal_start_service_label" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_start_service_label"><span></span><?= $this->lang->line('company_start_service_title') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><span></span><?= $this->lang->line('company_start_service_message_line1') ?? '' ?></p>
                    <p><?= $this->lang->line('company_start_service_message_line2') ?? '' ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('ui_close_btn') ?></button>
                    <button type="submit" class="btn btn-success"><?= $this->lang->line('ui_start_btn') ?></button>
                </div>
            </div>
        </div>
    </div>
</form>
