<div class="modal fade" tabindex="-1" role="dialog" id="modal_change_status" aria-labelledby="modal_change_status_label" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_change_status_label"><span></span><?= $this->lang->line('company_modal_change_status_title') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <?php echo form_open("/companies/change_type_contract_batch", ['autocomplete' => 'off', 'method' => 'POST', 'id' => 'form-change_type_contract']); ?>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">
                            <?= $this->lang->line('company_modal_type_change_label');?>
                        </label>
                        <div class="col-sm-8 form-inline">
                            <div class="radio radio-primary radio-padding-left">
                                <input type="radio" name="type_change" value="type_contract" checked id="type_contract_flag">
                                <label for="type_contract_flag"> <?= $this->lang->line('company_modal_type_contract_label');?></label>
                            </div>

                            <div class="radio radio-primary">
                                <input type="radio" name="type_change" value="duration_contract" id="duration_contract_flag">
                                <label for="duration_contract_flag"> <?= $this->lang->line('company_modal_duration_contract_label');?></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row type_contract_area">
                        <label class="col-sm-3 col-form-label"><?= $this->lang->line('company_modal_type_contract_label')?></label>
                        <div class="col-sm-8">
                            <select name="type_contract" class="form-control">
                                <option value="" ><?= $this->lang->line('company_option_null') ?></option>
                                <option value=0 ><?= $this->lang->line('company_agree_trial')?></option>
                                <option value=1 ><?= $this->lang->line('company_agree_free')?></option>
                                <option value=2 ><?= $this->lang->line('company_agree_fee')?></option>
                                <option value=3 ><?= $this->lang->line('company_agree_fee_sub_account')?></option>
                                <option value=4 ><?= $this->lang->line('company_agree_cancel_contract')?></option>
                                <option value=5 ><?= $this->lang->line('company_agree_agency')?></option>
                                <option value=6 ><?= $this->lang->line('company_agree_business')?></option>
                                <option value=7 ><?= $this->lang->line('company_agree_development')?></option>
                                <option value=8 ><?= $this->lang->line('company_agree_division')?></option>
                                <option value=9 ><?= $this->lang->line('company_agree_other')?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row duration_contract_area" style="display: none">
                        <label class="col-sm-3 col-form-label"><?= $this->lang->line('company_modal_duration_contract_label')?></label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                <input name="start_date" value="" type="text" class="form-control datepicker" size="15" placeholder="yyyy-mm-dd">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                <input name="end_date" value="" type="text" class="form-control datepicker disable-to-current" size="15" placeholder="yyyy-mm-dd">
                            </div>
                        </div>
                    </div>
                    <div class="direction_message">
                        <?= $this->lang->line('company_modal_direction_message') ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('ui_close_btn') ?></button>
                    <button type="button" class="btn btn-change_status-store"><?= $this->lang->line('company_change_status_btn') ?></button>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</div>
