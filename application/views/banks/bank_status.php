<div class="form-group row">
    <label class="col-sm-4 col-form-label">
        <?= $this->lang->line('bank_status')?>
    </label>
    <div class="col-sm-8 form-inline">
        <div class="radio radio-primary radio-padding-left">
            <input id="bank_status_on" type="radio" name="status"  <?= (isset($input['status']) && $input['status'] == BANK_STATUS_VALID) ? 'checked' : '' ?> value="<?= BANK_STATUS_VALID ?>">
            <label for="bank_status_on"> <?= $this->lang->line('bank_status_flag_on')?></label>
        </div>
        <div class="radio radio-primary">
            <input id="bank_status_off" type="radio" name="status"  <?= (isset($input['status']) && $input['status'] == BANK_STATUS_INVALID) ? 'checked' : '' ?> value="<?= BANK_STATUS_INVALID ?>">
            <label for="bank_status_off"> <?= $this->lang->line('bank_status_flag_off')?></label>
        </div>
    </div>
</div>