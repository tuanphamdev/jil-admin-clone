<div class="card mb-3">
    <div class="card-body">
        <?php $this->load->view('templates/alert');?>
        <?php echo form_open('/import_banks/do_create', ['method' => 'POST', 'autocomplete' => 'off']); ?>
        <div class="form-group row">
            <label class="col-sm-4 col-form-label asterisk_required">
                <?= $this->lang->line('bank_code')?>
            </label>
            <div class="col-sm-8">
                <input name="bank_code" value="<?= $input['bank_code'] ?? ''?>" type="text" class="form-control" maxlength="4">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label asterisk_required">
                <?= $this->lang->line('branch_code')?>
            </label>
            <div class="col-sm-8">
                <input name="branch_code" value="<?= $input['branch_code'] ?? ''?>" type="text" class="form-control" maxlength="3">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label asterisk_required">
                <?= $this->lang->line('bank_name')?>
            </label>
            <div class="col-sm-8">
                <input name="bank_name" value="<?= $input['bank_name'] ?? ''?>" type="text" class="form-control" >
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label asterisk_required">
                <?= $this->lang->line('bank_name_kana')?>
            </label>
            <div class="col-sm-8">
                <input name="bank_name_kana" value="<?= $input['bank_name_kana'] ?? ''?>" type="text" class="form-control" >
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label asterisk_required">
                <?= $this->lang->line('branch_name')?>
            </label>
            <div class="col-sm-8">
                <input name="branch_name" value="<?= $input['branch_name'] ?? ''?>" type="text" class="form-control" >
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label asterisk_required">
                <?= $this->lang->line('branch_name_kana')?>
            </label>
            <div class="col-sm-8">
                <input name="branch_name_kana" value="<?= $input['branch_name_kana'] ?? ''?>" type="text" class="form-control" >
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label asterisk_required">
                <?= $this->lang->line('branch_post_code')?>
            </label>
            <div class="col-sm-8">
                <div class="form-inline">
                    <div class="form-group">
                        <input type="tel" name="branch_post_code[0]" value="<?= $input['branch_post_code'][0] ?? ''?>"  placeholder="000" class="form-control" maxlength="3" size="5">
                    </div>
                    <div class="form-group dash">
                        <input type="tel" name="branch_post_code[1]" value="<?= $input['branch_post_code'][1] ?? ''?>" placeholder="0000" class="form-control" maxlength="4" size="5">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label asterisk_required">
                <?= $this->lang->line('branch_address')?>
            </label>
            <div class="col-sm-8">
                <input name="branch_address" value="<?= $input['branch_address'] ?? ''?>" type="text" class="form-control" >
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label asterisk_required">
                <?= $this->lang->line('branch_phone_number')?>
            </label>
            <div class="col-sm-8">
                <div class="form-inline">
                    <div class="form-group">
                        <input type="tel" name="branch_phone_number[0]" value="<?= $input['branch_phone_number'][0] ?? ''?>" placeholder="0000" class="form-control" size="6" maxlength="5">
                    </div>
                    <div class="form-group dash">
                        <input type="tel" name="branch_phone_number[1]" value="<?= $input['branch_phone_number'][1] ?? ''?>" placeholder="0000" class="form-control" size="5" maxlength="4">
                    </div>
                    <div class="form-group dash">
                        <input type="tel" name="branch_phone_number[2]" value="<?= $input['branch_phone_number'][2] ?? ''?>"  placeholder="0000" class="form-control" size="5" maxlength="4">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label">
                <?= $this->lang->line('draft_clearing_place')?>
            </label>
            <div class="col-sm-8">
                <input name="draft_clearing_place" value="<?= $input['draft_clearing_place'] ?? ''?>" type="text" class="form-control" maxlength="5" >
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label">
                <?= $this->lang->line('bank_line_code')?>
            </label>
            <div class="col-sm-8">
                <input name="sort" value="<?= $input['sort'] ?? 0?>" type="text" class="form-control" maxlength="2" >
            </div>
        </div>

        <!--        this sprint don't implemant status -->
        <?php //$this->load->view(''); ?>

        <div class="form-group row align-items-end">
            <div class="col col align-self-center">
                <div class="form text-center">
                    <?php if($permissions['import_bank'] == FULL_PERMISSION):?>
                        <button type="submit" id="button-submit-save" class="btn btn-primary btn-md">
                            <?= $this->lang->line('btn_confirm')?>
                        </button>
                    <?php endif;?>
                    <a href="<?= site_url('import_banks')?>" class="btn btn-warning btn-md">
                        <?= $this->lang->line('btn_return')?>
                    </a>
                </div>
            </div>
        </div>
        <?= form_close() ?>
    </div>
</div>