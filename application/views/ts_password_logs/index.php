<?php
$segment = 2;
$page = 'ts_password_logs';
if($this->uri->segment(2) == 'search'){
	$segment = 3;
	$page = 'ts_password_logs/search';
}

?>		
	<div class="card mb-3">
        <div class="card-body">
            <div id="accordion" role="tablist">
                <div class="">
                <div class="" role="tab" id="headingOne">
				<a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
					<i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('ts_password_log_search_title')?>
				</a>
                </div>
                <form action="/ts_password_logs/search" method="GET" autocomplete="off">
                    <div id="collapseOne" class="collapse  <?= $segment == 3 ? 'show' : '';?>" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                        <div class="card-body">
                            <input type="hidden" name="item_page" value="<?= $data['per_page'] ?? 0 ?>" class="form-control">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="">日付(from)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input name='created_date_start' id='created_date_start' value="<?= $input['created_date_start'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">日付(to)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input name='created_date_end' id='created_date_end' value="<?= $input['created_date_end'] ?? '' ?>" type="text" class="form-control datepicker" value="" placeholder="yyyy-mm-dd">
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label >対応者氏名</label>
                                    <div class="input-group">
                                        <select class="form-control filter_text" name="user_id">
                                            <option value="" ><?= $this->lang->line('company_option_null')?></option>
                                            <?php
                                            if(!empty($users)){
                                                foreach ($users as  $user) {
                                                    $user_id = $input['user_id'] ?? '';
                                                    $selected = $user_id == $user['id'] ? 'selected' : '';
                                                    echo "<option value={$user['id']} {$selected}>{$user['name']}</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                               
                            </div>
                            

                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="">対象企業ID</label>
                                    <input type="text" name="company_code" maxlength='10' value="<?= $input['company_code'] ?? '' ?>" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">対象者社員番号</label>
                                    <input type="text" name="employee_code" maxlength='50' value="<?= $input['employee_code'] ?? '' ?>" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">対象者氏名</label>
                                    <input type="text" name="employee_name" maxlength='100' value="<?= $input['employee_name'] ?? '' ?>" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label >操作種別</label>
                                    <div class="input-group">
                                        <select name="action_type" class="form-control filter_text">
                                            <option value="" <?= empty($input['action_type']) || (isset($input['action_type']) && $input['action_type'] === '') ? 'selected' : ''?>>全て選択</option>
                                            <option value=0 <?= isset($input['action_type']) && $input['action_type'] === '0' ? 'selected' : ''?>><?= OPTION_TS_PASSWORD_ACTION_TYPE[0]?></option>
                                            <option value=1 <?= !empty($input['action_type']) ? 'selected' : ''?>><?= OPTION_TS_PASSWORD_ACTION_TYPE[1]?></option>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="form text-right">
                                <button role="button" class="btn btn-info" href="#"><i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('ts_password_log_search_btn')?></button>

                                <a class="btn btn-secondary" href="/ts_password_logs"><i class="fa fa-fw fa-close"> </i> <?= $this->lang->line('ts_password_log_clear_btn')?></a>
                            </div>
                        </div>
                    </div>
				
			    </form>

                </div>
            </div>
        </div>							
    </div>

		<div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <?php $this->load->view('templates/per_page', ['per_page' => $data['per_page'] ?? 0]);?>
                    </div>
                </div>
            </div>
			<div class="card-body">
                <?php $this->load->view('templates/alert');?>

				<table class="table table-responsive-col table-striped">
					<thead>
					<tr>
						<th scope="col-2">日付</th>
						<th scope="col-2">対応者氏名</th>
                        <th scope="col-2">対象企業名</th>
                        <th scope="col-2">対象者社員番号</th>
                        <th scope="col-2">対象者氏名</th>
                        <th scope="col-2">操作種別</th>
					</tr>
					</thead>
					<tbody>
                <?php 
				if(!empty($data['ts_password_log'])):
                    foreach ($data['ts_password_log'] as $key => $value):
                    ?>
                        <tr>
                            <td><?= date('Y/m/d H:i:s', strtotime(convertToLocalTimezone($value['created_at']))) ?></td>
                            <td><?= $value['user_name']??''?></td>
                            <td><?= $value['company_name']??''?></td>
                            <td><?= $value['code']??''?></td>
                            <td><?= $value['emp_name']??''?></td>
                            <td><?= OPTION_TS_PASSWORD_ACTION_TYPE[$value['action_type']]??''?></td>
                        </tr>
                    <?php 
                    endforeach;
                endif;
				?>

                <?php if(empty($data['ts_password_log'])):?>
                <tr>
                    <td colspan="12">
                    <?= $this->lang->line('ts_password_log_no_result_found')?>
                    </td>
                </tr>
                <?php endif; ?>
					
					</tbody>
				</table>

				<!-- Pagination -->
                <?php
                $pagination =  pagination(base_url($page), $data['total'] ?? 0 , $data['per_page'] ?? 0, $segment);
                ?>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <?= $pagination['statistic'];?>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <nav aria-label="">
                            <?= $pagination['pagination'];?>
                        </nav>
                    </div>
                </div>
				<!-- End Pagination -->
			</div>
		</div>

        <script src="<?= site_url('assets/js/companies/custom.js') ?>"></script>
