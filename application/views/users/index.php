<?php
$segment = 2;
$page = 'users';
if($this->uri->segment(2) == 'search'){
	$segment = 3;
	$page = 'users/search';
}
?>
<div class="card mb-3">
	<div class="card-body">
		<div id="accordion" role="tablist">
			<div class="">
			<div class="" role="tab" id="headingOne">
			<a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
				<i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('company_search_title')?>
			</a>
			</div>
			<form action="/users/search" method="GET">
			<div id="collapseOne" class="collapse <?= $segment == 3 ? 'show' : '';?>" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
					<div class="card-body">

						<div class="form-row">

							<input type="hidden" name="item_page" value="<?= $data['per_page'] ?? 0?>" class="form-control">
						
							<div class="form-group col-md-4">
								<label for=""><?= $this->lang->line('user_name')?></label>
								<input type="text" name="name" value="<?= $input['name'] ?? '' ?>" class="form-control">
							</div>

							<div class="form-group col-md-4">
								<label for=""><?= $this->lang->line('user_status')?></label>
								<select name="status" class="form-control">
									<option value="" <?= isset($input['status']) && $input['status'] == '' ? 'selected' : '' ?> ><?= $this->lang->line('ui_option_all')?></option>
                                    <option value=1 <?= isset($input['status']) && $input['status'] == 1 ? 'selected' : '' ?> ><?= $this->lang->line('user_status_on_btn')?></option>
                                    <option value=2 <?= isset($input['status']) && $input['status'] == 2 ? 'selected' : '' ?> ><?= $this->lang->line('user_status_off_btn')?></option>
                                </select>
							</div>

							<div class="form-group col-md-4 btn-seach-user">
								<button role="button" class="btn btn-info" href="#"><i class="fa fa-fw fa-search"> </i> <?= $this->lang->line('company_search_btn')?></button>

								<a class="btn btn-secondary" href="/users"><i class="fa fa-fw fa-close"> </i> <?= $this->lang->line('company_clear_btn')?></a>
							</div>

						</div>
					</div>
				</div>
			
			</form>

			</div>
		</div>
	</div>							
</div>
	

<div class="card">
	<div class="card-header">
		<div class="row">
			<div class="col">
				<?php $this->load->view('templates/per_page', ['per_page' => $data['per_page'] ?? 0]);?>
			</div>
			<?php if($permissions['user'] == FULL_PERMISSION):?>
			<div class="col text-right">
				<a role="button" class="btn btn-primary" href="<?= site_url("/users/create") ?>"><i class="fa fa-pencil bigfonts bigfonts" aria-hidden="true"> </i><?= $this->lang->line('user_create_title')?></a>
			</div>
			
			<?php endif; ?>
		</div>
	</div>
	<div class="card-body">
	<?php $this->load->view('templates/alert');?>
		<table class="table table-responsive-col table-striped">
			<thead>
			<tr>
				<th scope="col"><?= $this->lang->line('user_name')?></th>
				<th scope="col"><?= $this->lang->line('user_email')?></th>
				<th scope="col"><?= $this->lang->line('user_role_id')?></th>
				<th scope="col"><?= $this->lang->line('user_status')?></th>
			</tr>
			</thead>
			<tbody>
			<?php
			if(!empty($data['user'])):
				foreach ($data['user'] as $key_user => $val_user) :
			?>
			<tr>
				<td><a href="/users/update/<?= $val_user['id']??'' ?>"><?= $val_user['name']??'' ?></a></td>
				<td><?= $val_user['email']??'' ?></td>
				<td><?= $val_user['role']['name']??'' ?></td>
				<td><?= isset($val_user['status']) && $val_user['status'] == 1 ? $this->lang->line('user_status_active') : $this->lang->line('user_status_no_active'); ?></td>
			</tr>
			<?php
				endforeach;
			endif;
			?>

			<?php if(empty($data['user'])):?>
                <tr>
                    <td colspan="12">
                        <?= $this->lang->line('user_no_result_found')?>
                    </td>
                </tr>
                <?php endif; ?>
			</tbody>
		</table>
        <!-- Pagination -->
        <?php
        $pagination =  pagination(base_url($page), $data['total'] ?? 0, $data['per_page'] ?? 0, $segment);
        ?>
        <div class="row">
            <div class="col-sm-12 col-md-5">
                <?= $pagination['statistic'];?>
            </div>
            <div class="col-sm-12 col-md-7">
                <nav aria-label="">
                    <?= $pagination['pagination'];?>
                </nav>
            </div>
        </div>

        <!-- End Pagination -->
	</div>							
</div>
