<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>

    <!-- Core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="/assets/images/favicon.ico">
	<!-- Custom CSS -->
	<link href="/assets/css/login.css" rel="stylesheet">

    <!-- Checkboxes style -->
    <link href="/assets/css/bootstrap-checkbox.css" rel="stylesheet">
</head>

<body>


<div class="container h-100">
    <div class="row justify-content-center align-items-center logo_login">
        <img src="<?= site_url('/assets/images/logo.png') ?>" class="img-fluid" >
        
    </div>
	<div class="row justify-content-center align-items-center">
	
		<div class="card">
			<h4 class="card-header text-center"><?= $this->lang->line('auth_reissue_password_title')?></h4>
           
			<div class="card-body">
            <?php $this->load->view('templates/alert');?>
                <?php echo form_open("/do_reissue_password/".$token, ['novalidate' => '']); ?>                        
                        <div class="row">	
                            <div class="col-md-12">    
                            <div class="form-group">
                            <label></label>
                            <div class="input-group">
                                <input name="password" type="password" value="<?= $input['password']?>" class="form-control" required="" placeholder="<?= $this->lang->line('auth_placeholder_password')?>">								  
                            </div>								
                            <div class="help-block with-errors text-danger"></div>
                            </div>
                            </div>
                        </div>

                        <div class="row">	
                            <div class="col-md-12">    
                            <div class="form-group">
                            <label></label>
                            <div class="input-group">
                                <!-- <span class="input-group-addon"><i class="fa fa-envelope-open" aria-hidden="true"></i></span> -->
                                <input name="password_confirm" type="password" value="<?= $input['password_confirm']?>" class="form-control" required="" placeholder="<?= $this->lang->line('auth_placeholder_password_confirm')?>">								  
                            </div>								
                            <div class="help-block with-errors text-danger"></div>
                            </div>
                            </div>
                        </div>
                        
                        <div class="row"></div>
                        
                        <div class="row">
                            <div class="col-md-12">
                            <input type="submit" class="btn btn-primary btn-lg btn-block" value="<?= $this->lang->line('auth_send_forgot_mail_btn')?>" name="">
                            </div>
                        </div>
                </form>

                <div class="clear"></div> 
                    
			</div>	
			
		</div>	
		
    </div>
    
    </form>
</div>

</body>

</html>