<div class="card mb-3">
    <div class="card-body">
    <?php $this->load->view('templates/alert');?>
        <?php echo form_open('/users/do_create', ['method' => 'POST', 'novalidate' => '', 'autocomplete' => 'off']); ?>
            <div class="form-group row">
                <label for="" class="col-sm-4 col-form-label"><?= $this->lang->line('user_name')?></label>
                <div class="col-sm-8">
                    <input name="name" value="<?= $input['name'] ?? ''?>" type="text" class="form-control" placeholder="">
                </div>
            </div>

            <div class="form-group row">
                <label for="" class="col-sm-4 col-form-label"><?= $this->lang->line('user_name_furigana')?></label>
                <div class="col-sm-8">
                    <input name="name_furigana" value="<?= $input['name_furigana'] ?? ''?>" type="text" class="form-control" placeholder="">
                </div>
            </div>

            <div class="form-group row">
                <label for="" class="col-sm-4 col-form-label"><?= $this->lang->line('user_email')?></label>
                <div class="col-sm-8">
                    <input name="email" value="<?= $input['email'] ?? ''?>" type="email" class="form-control" placeholder="">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="" class="col-sm-4 col-form-label"><?= $this->lang->line('user_password')?></label>
                <div class="col-sm-8">
                    <input name="password" value="<?= $input['password'] ?? ''?>" type="password" class="form-control" placeholder="">
                </div>
            </div>

            <div class="form-group row">
                <label for="" class="col-sm-4 col-form-label"><?= $this->lang->line('user_role_id')?></label>
                <div class="col-sm-8">
                    <select name="role_id" class="form-control">
                        <?php
                            if(!empty($roles)){
                                foreach ($roles as $key_role => $val_role) {
                                    echo "<option value={$val_role['id']}>{$val_role['name']}</option>";
                                }
                            }
                        ?>
                        
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="" class="col-sm-4 col-form-label"><?= $this->lang->line('user_status')?></label>
                <div class="col-sm-8 form-inline">
                    <div class="radio radio-primary">
                        <input id="status" type="radio" name="status" <?= (isset($input['status']) && $input['status'] == 1) || !isset($input['status']) ? 'checked' : '' ?> value=1 >
                        <label for="status"> <?= $this->lang->line('user_status_on_btn')?></label>
                    </div>

                    <div class="radio radio-primary">
                        <input id="status2" type="radio" name="status"  <?= isset($input['status']) && $input['status'] == 2 ? 'checked' : '' ?> value=2>
                        <label for="status2"> <?= $this->lang->line('user_status_off_btn')?></label>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="" class="col-sm-4 col-form-label"><?= $this->lang->line('user_send_mail')?></label>
                <div class="col-sm-8 form-inline">
                    <div class="radio radio-primary">
                        <input id="is_send_mail" type="radio" name="is_send_mail"  <?= (isset($input['is_send_mail']) && $input['is_send_mail'] == 1) || !isset($input['is_send_mail'])  ? 'checked' : '' ?> value=1>
                        <label for="is_send_mail"> する</label>
                    </div>

                    <div class="radio radio-primary">
                        <input id="is_send_mail2" type="radio" name="is_send_mail"  <?= isset($input['is_send_mail']) && $input['is_send_mail'] == 2 ? 'checked' : '' ?> value=2>
                        <label for="is_send_mail2"> しない</label>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
            <div class="col-sm-8">
            
            </div>
            </div>
        
            <div class="form-group row align-items-end">
                <div class="col col align-self-center">
                    <div class="form text-center">
                        <?php if($permissions['user'] == FULL_PERMISSION):?>
                        <button type="submit" id="button-submit-save" class="btn btn-primary btn-md"><?= $this->lang->line('btn_confirm')?></button>
                        <?php endif;?>
                        <a href="<?= site_url('users')?>" class="btn btn-warning btn-md"><?= $this->lang->line('btn_return')?></a>
                    </div>
                </div>
            </div>
        </form>
    </div>							
</div>