<?php
defined('BASEPATH') or exit('No direct script access allowed');

$lang['my_form_validation_valid_furigana']      = '{field}は全角カタカナで入力してください。';
$lang['my_form_validation_valid_zip']   = '{field}は3桁と4桁の半角数字で入力してください。';
$lang['my_form_validation_valid_tel']    = '{field}は半角数字２～５桁-１～４桁-３～４桁、全桁を合わせて10または11桁で入力してください。';
$lang['my_form_validation_valid_date_format']   = '{field}が正しくありません。「YYYY/MM/DD」形式で指定してください。';
$lang['my_form_validation_valid_furigana_address']      = '{field}は全角のカタカナ、半角の英数字、ハイフンで入力してください。';
$lang['my_form_validation_valid_natural_greater_than_zero']    = '{field}は先頭で0を登録することはできません。';
$lang['my_form_validation_valid_enigma_code']    = '{field}は半角20英数字記号以内で入力してください。';
$lang['my_form_validation_valid_greater_than_date']   = '{field}を正しく入力してください。';
