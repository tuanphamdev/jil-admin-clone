<?php
$lang['role_list_title'] = '権限一覧';
$lang['role_create_title'] = '権限登録';
$lang['role_update_title'] = '権限編集';
$lang['role_btn_create'] = '権限追加';
$lang['role_name'] = '名称';
$lang['role_num_of_people'] = '利用人数';
$lang['role_delete'] = '削除';
$lang['role_detail_form'] = '権限詳細';
$lang['role_feature_name'] = '機能名';
$lang['role_detail'] = '詳細';
$lang['role_feat_company_manager'] = '企業管理機能';
$lang['role_feat_agreement_manager'] = '契約管理機能';
$lang['role_feat_csv_manager'] = 'CSVデータ出力';
$lang['role_feat_setting_manager'] = '設定機能';
$lang['role_company'] = '企業情報';
$lang['role_agreement'] = '契約管理';
$lang['role_export_csv'] = 'SalesforceCSV出力';
$lang['role_user'] = 'ユーザ';
$lang['role_role'] = '権限';
$lang['role_ip'] = 'IP設定';
$lang['role_all'] = '全機能';
$lang['role_readonly'] = '閲覧のみ';
$lang['role_no_use'] = '利用しない';
$lang['role_no_result_found'] = 'ロールが見つかりません。';
$lang['role_delete_label'] = '削除確認';
$lang['role_delete_message'] = 'を削除します。よろしいですか？';
$lang['role_delete_success'] = '権限を削除しました。';
$lang['role_failed_success'] = '権限の削除が失敗しました。';
$lang['role_duplicate'] = '権限が重複しています。';


//=====================================================
$lang['role_create_failed'] = 'ロールの作成に失敗しました。';
$lang['role_create_success'] = 'ロールの作成に成功しました。';
$lang['role_update_failed'] = 'ロールの更新に失敗しました。';
$lang['role_update_success'] = 'ロールの更新に成功しました。';
$lang['role_delete_failed'] = 'ロールの削除に失敗しました。';
$lang['role_delete_success'] = 'ロールの削除が成功しました。';
