<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package CodeIgniter
 * @author  EllisLab Dev Team
 * @copyright   Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright   Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://codeigniter.com
 * @since   Version 1.0.0
 * @filesource
 */
defined('BASEPATH') or exit('No direct script access allowed');

$lang['email_must_be_array'] = 'メールアドレスが必ず配列です。';
$lang['email_invalid_address'] = '不正なメールアドレス: %s';
$lang['email_attachment_missing'] = 'メールアドレスが確定できません: %s';
$lang['email_attachment_unreadable'] = 'メールアドレスの添付ファイルをオープンできません: %s';
$lang['email_no_from'] = 'Fromがないメールを送信できません。';
$lang['email_no_recipients'] = 'To, Cc, or Bccを入力して送信してください。';
$lang['email_send_failure_phpmail'] = 'PHP mail()を使っているメールを送信できません。サーバではこのメソッドがあるメールを送信する構成がない可能性があります。';
$lang['email_send_failure_sendmail'] = 'PHP Sendmailを使っているメールを送信できません。サーバではこのメソッドがあるメールを送信する構成がない可能性があります。';
$lang['email_send_failure_smtp'] = 'PHP SMTPを使っているメールを送信できません。サーバではこのメソッドがあるメールを送信する構成がない可能性があります。';
$lang['email_sent'] = 'このプロトコルを利用して、メッセージを送信できました。: %s';
$lang['email_no_socket'] = 'メールの送信のために、socketをオープンできません。各設定を確認してください。';
$lang['email_no_hostname'] = 'SMTPホストネームが定義されていません。';
$lang['email_smtp_error'] = 'SMTPのエラーが発生しました。: %s';
$lang['email_no_smtp_unpw'] = 'エラー：ユーザネーム及びパスワードのSMTPを必ずアサインしてください。';
$lang['email_failed_smtp_login'] = 'AUTH LOGINコマンドの送信が失敗しました。エラー: %s';
$lang['email_smtp_auth_un'] = 'ユーザネームが確定できません。エラー: %s';
$lang['email_smtp_auth_pw'] = 'パスワードが確定できません。エラー: %s';
$lang['email_smtp_data_failure'] = 'データを送信できません。: %s';
$lang['email_exit_status'] = 'ステータスコードが既に存在しています。: %s';
