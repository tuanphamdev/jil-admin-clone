<?php
$lang['ts_pw_change_list_title'] = 'TSパスワード設定';
$lang['ts_pw_change_title'] = 'TSパスワード';
$lang['ts_pw_change_photo'] = '写真';
$lang['ts_pw_change_name'] = '企業名';
$lang['ts_pw_change_code'] = '社員番号';
$lang['ts_pw_change_last_name'] = '氏名';
$lang['ts_pw_change_dept'] = '所属グループ';
$lang['ts_pw_change_job_tile'] = '役職';
$lang['ts_pw_change_action_field'] = 'パスワード設定';
$lang['ts_pw_change_enrollment_status_enrolled'] = '在籍';
$lang['ts_pw_change_enrollment_status_retirement'] = '退職';
$lang['ts_pw_change_enrollment_status_absence'] = '休職';
$lang['ts_pw_change_enrollment_status'] = '在籍区分';
$lang['ts_pw_change_enrollment_flag_setting'] = '設定区分';
$lang['ts_pw_change_button_change'] = '設定';
$lang['ts_pw_change_button_list_emp'] = '詳細';


$lang['ts_pw_change_company_code'] = '企業コード';
$lang['ts_pw_change_emp_name'] = '従業員氏名';
$lang['ts_pw_change_name'] = '従業員氏名';
$lang['ts_pw_change_no_result_found'] = '検索結果がありません。';
$lang['ts_pw_change_edit_function'] = '編集機能';
$lang['ts_pw_change_button_edit'] = '変更';
$lang['ts_pw_change_change_role_success'] = 'TSパスワード更新が成功しました。';
$lang['ts_pw_change_enable_popup_title'] = 'TSパスワード確認';
$lang['ts_pw_change_enable_popup_message_line_1'] = '確認したらマスタアカウントが';
$lang['ts_pw_change_enable_popup_message_line_2'] = 'に変更されます。以前のマスタアカウントは権限がなくなり、ログインできなくなることもございますので、よろしいですか？';
