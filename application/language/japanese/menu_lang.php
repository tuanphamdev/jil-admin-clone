<?php
$lang['menu_manage_company'] = '契約管理';
$lang['menu_company'] = '契約企業';
$lang['menu_config'] = '設定';
$lang['menu_ts_password'] = 'TSパスワード';
$lang['menu_ts_password_change'] = 'TSパスワード設定';
$lang['menu_ts_password_ips'] = 'IP設定';
$lang['menu_ts_password_logs'] = '操作ログ';
$lang['menu_export_csv'] = 'CSVデータ出力';
$lang['menu_export_csv_saleforce'] = 'CSV出力';
$lang['menu_ip'] = 'IP設定';
$lang['menu_user'] = 'ユーザ';
$lang['menu_role'] = '権限';
$lang['menu_information'] = '通知設定';
$lang['menu_upload'] = 'メンテナンス';
$lang['menu_import_bank_csv'] = '全銀';
$lang['menu_tokyo_marine'] = '東京海上';
$lang['menu_admin_employee'] = 'マスタアカウント変更';
$lang['menu_qa_engine'] = 'QA Engine';
$lang['menu_survey_monkey'] = '顧客アンケート';
