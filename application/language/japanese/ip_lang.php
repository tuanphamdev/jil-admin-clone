<?php
$lang['ip_list_title'] = 'IP設定';
$lang['ip_create_title'] = 'IP登録';
$lang['ip_update_title'] = 'IP編集';
$lang['ip_address'] = '許可するIP';
$lang['ip_delete'] = '削除';
$lang['ip_create_btn'] = 'IP登録';
$lang['ip_no_result_found'] = 'IPが見つかりません。';
$lang['ip_create_failed'] = 'IPの作成が失敗しました。';
$lang['ip_update_failed'] = 'IPの更新が失敗しました。';
$lang['ip_delete_message'] = 'を削除します。よろしいですか？';
$lang['ip_delete_label'] = '削除確認';
$lang['ip_delete_success'] = 'IPを削除しました。';
$lang['ip_failed_success'] = 'IPの削除が失敗しました。';
$lang['ip_duplicate'] = 'IPが重複しています。';
$lang['ip_create_success'] = 'IPを作成しました。';
$lang['ip_update_success'] = 'IPを更新しました。';
