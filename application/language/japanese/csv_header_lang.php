<?php
// Header get staff use kintai
$lang['csv_header_staff_use_kintai_company_code'] = '企業ID';
$lang['csv_header_staff_use_kintai_company_name'] = '企業名';
$lang['csv_header_staff_use_kintai_dept_main'] = '所属グループ（主務）';
$lang['csv_header_staff_use_kintai_employee_code'] = '社員番号';
$lang['csv_header_staff_use_kintai_last_name'] = '職場氏名（氏）';
$lang['csv_header_staff_use_kintai_first_name'] = '職場氏名（名）';
$lang['csv_header_staff_use_kintai_date_of_birth'] = '生年月日';
$lang['csv_header_staff_use_kintai_gender'] = '性別';

// Header get retire staff
$lang['csv_header_retire_staff_company_code'] = '企業ID';
$lang['csv_header_retire_staff_employee_code'] = '社員番号';
$lang['csv_header_retire_staff_retire_day'] = '退職日';

// Header get tokyo marine staff
$lang['csv_header_tokyo_marine_staff_employee_id'] = 'jinjer ID';
$lang['csv_header_tokyo_marine_staff_company_code'] = '企業コード';
$lang['csv_header_tokyo_marine_staff_employee_code'] = '社員番号';
$lang['csv_header_tokyo_marine_staff_dept_name'] = '所属グループ名（主務）';
$lang['csv_header_tokyo_marine_staff_last_name'] = '氏（職場氏）';
$lang['csv_header_tokyo_marine_staff_first_name'] = '名（職場氏）';
$lang['csv_header_tokyo_marine_staff_date_of_birth'] = '生年月日';
$lang['csv_header_tokyo_marine_staff_gender'] = '性別';
$lang['csv_header_tokyo_marine_staff_phone_number'] = '電話番号(個人)';
$lang['csv_header_tokyo_marine_staff_email'] = 'E-Mail(社用)';
$lang['csv_header_tokyo_marine_staff_postcode'] = '郵便番号';
$lang['csv_header_tokyo_marine_staff_pref_name'] = '都道府県';
$lang['csv_header_tokyo_marine_staff_city_name'] = '市区町村';
$lang['csv_header_tokyo_marine_staff_chrome_street'] = '丁目･番地';
$lang['csv_header_tokyo_marine_staff_building_name'] = '建物名';
$lang['csv_header_tokyo_marine_staff_is_tokyo_marine'] = '東京海上フラグ';
$lang['csv_header_tokyo_marine_staff_last_name_kana'] = '氏カナ（職場氏名）';
$lang['csv_header_tokyo_marine_staff_first_name_kana'] = '名カナ（職場氏名）';
$lang['csv_header_tokyo_marine_staff_employment_classification'] = '雇用区分 ';
$lang['csv_header_tokyo_marine_staff_enrollment_status'] = '在籍区分';
$lang['csv_header_tokyo_marine_staff_retirement_date'] = '退職年月日';
$lang['csv_header_tokyo_marine_staff_retirement_classification'] = '退職区分';
$lang['csv_header_tokyo_marine_staff_retirement_reason'] = '退職理由';
$lang['csv_header_tokyo_marine_staff_total_timecard'] = '勤怠の打刻実績が3ヶ月実績なし';

// Header import bank
$lang['csv_header_import_bank_code'] = '銀行コード';
$lang['csv_header_import_bank_branch_code'] = '支店コード';
$lang['csv_header_import_bank_name_kana'] = '銀行名カナ';
$lang['csv_header_import_bank_name'] = '銀行名';
$lang['csv_header_import_bank_branch_name_kana'] = '支店名カナ';
$lang['csv_header_import_bank_branch_name'] = '支店名';
$lang['csv_header_import_bank_zip_code'] = '郵便番号';
$lang['csv_header_import_bank_address'] = '住所';
$lang['csv_header_import_bank_phone'] = '電話番号';
$lang['csv_header_import_bank_draft_clearing_place'] = '手形交換場所';
$lang['csv_header_import_bank_sort_by'] = '並び順';

$lang['csv_header_company_agree_fee'] = '課金対象';
