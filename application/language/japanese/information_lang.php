<?php
$lang['information_title'] = 'タイトル';
$lang['information_detail'] = '詳細';
$lang['information_link_url'] = 'リンク';
$lang['information_status'] = '状況';
$lang['information_created_from'] = '作成日 (from)';
$lang['information_created_to'] = '作成日 (to)';
$lang['information_expiration'] = '期限日';
$lang['information_creation'] = '作成日';
$lang['information_edit_function'] = '編集機能';

$lang['information_no_result_found'] = '検索結果がありません。';
$lang['information_on'] = '表示';
$lang['information_off'] = '非表示';

$lang['information_list_title'] = '通知設定';


$lang['information_off_title'] = '通知停止';
$lang['information_on_title'] = '通知再開';
$lang['information_off_message_line1'] = 'の通知を停止します。';
$lang['information_off_message_line2'] = 'よろしければ、「停止」ボタンを押してください。';
$lang['information_on_message_line1'] = 'の通知を再開します。';
$lang['information_on_message_line2'] = 'よろしければ、「再開」ボタンを押してください。';
$lang['information_off_success'] = '[%s] 通知を停止しました。';
$lang['information_on_success'] = '[%s] 通知を再開しました。';
$lang['information_create_success'] = 'create success';
$lang['information_update_success'] = 'update success';
