<?php
$lang['btn_confirm'] = '確認';
$lang['btn_return'] = '戻る';
$lang['btn_logout'] = 'ログアウト';
$lang['btn_save'] = '保存';
$lang['btn_preview'] = 'プレビュー';


$lang['company_list_title'] = '企業一覧';
$lang['company_create_title'] = '企業登録';
$lang['company_update_title'] = '編集';
$lang['company_code'] = '企業コード';
$lang['company_name'] = '企業名';
$lang['company_application_name_title'] = '申し込み者名';

$lang['company_service_type'] = 'サービス種類';
$lang['company_agreement_num_account'] = 'アカウント数';
$lang['company_status'] = '状況';
$lang['company_edit_function'] = '編集機能';
$lang['company_name_furigana'] = '会社名（フリガナ）';
$lang['company_representative_name'] = '';

$lang['company_agreement_type'] = '契約タイプ';
$lang['company_agreement_start_date'] = '開始日';
$lang['company_agreement_end_date'] = '終了日';
$lang['company_agreement_start_date_from'] = '開始日(from)';
$lang['company_agreement_start_date_to'] = '開始日(to)';
$lang['company_agreement_end_date_from'] = '終了日(from)';
$lang['company_agreement_end_date_to'] = '終了日(to)';


$lang['company_is_tokyo_marine'] = '東京海上フラグ';
$lang['company_representative_name'] = '代表者氏名';
$lang['company_representative_name_furigana'] = '代表者氏名（フリガナ）';
$lang['company_representative_title'] = '代表者役職名';
$lang['company_tel'] = '電話番号';
$lang['company_fax'] = 'FAX番号';
$lang['company_zip'] = '郵便番号';
$lang['company_prefecture'] = '都道府県';
$lang['company_city'] = '市区町村';
$lang['company_address'] = '丁目・番地・建物名・部屋番号';
$lang['company_address_furigana'] = '住所(フリガナ)';
$lang['company_application_name'] = '申込者（名）';
$lang['company_application_surname'] = '申込者（氏）';
$lang['company_application_furigana'] = '申込者（名）（フリガナ）';
$lang['company_application_furigana_surname'] = '申込者（氏）（フリガナ）';
$lang['company_application_email'] = '申込者メールアドレス';
$lang['company_registered_date'] = '申込日';
$lang['company_period'] = '期間';
$lang['company_plan_jinjer_db'] = 'jinjer-db';
$lang['company_plan_kintai'] = '勤怠管理';
$lang['company_option_paid_management'] = '有給管理';
$lang['company_option_shift_management'] = 'シフト管理';
$lang['company_option_overtime_management'] = '予実管理（残業管理）';
$lang['company_plan_roumu'] = '労務管理';
$lang['company_plan_jinji'] = '人事管理';
$lang['company_plan_keihi'] = '経費精算';
$lang['company_option_AI_OCR'] = 'AI-OCR';
$lang['company_option_ebook_store'] = 'モバイルSuica連携';
$lang['company_option_credit_card'] = 'クレジットカード連携';
$lang['company_option_timestamp'] = '電子帳簿保存法';
$lang['company_plan_my_number'] = 'マイナンバー';
$lang['company_plan_work_vital'] = 'ワークバイタル';
$lang['company_plan_salary'] = '給与';
$lang['company_option_payslip'] = '給与明細';
$lang['company_plan_workflow'] = 'ワークフロー';
$lang['company_plan_employee_contract'] = '雇用契約';
$lang['company_plan_bi'] = 'jinjer Board';
$lang['company_plan_signing'] = 'Signing';
$lang['company_plan_signing_folder'] = 'フォルダ';
$lang['company_plan_signing_folder_range'] = '公開範囲';
$lang['company_plan_year_end_tax_adjustment'] = '年調収集';
$lang['company_plan_signing_internal_workflow'] = '社内ワークフロー';
$lang['company_plan_signing_batch_import'] = '書類一括送信';
$lang['company_plan_import_signed_file'] = '書類インポート(締結)';
$lang['company_plan_maximum_import_signed_file'] = 'インポート上限数';
$lang['company_plan_import_signed_file_timestamp'] = '書類インポート種別';
$lang['company_plan_ip_limit'] = 'IP制限';
$lang['company_plan_send_sms'] = 'SMS送信';
$lang['company_plan_electronic_signature_long_term'] = '電子署名（長期署名）';
$lang['company_plan_electronic_signature_timestamp'] = '電子サイン（タイムスタンプのみ）';
$lang['company_plan_electronic_signature_authentication'] = '電子サイン（認証のみ）';
$lang['company_plan_send_function_digital_signature'] = '送信機能(電子署名あり/なし)';
$lang['company_plan_imprint_registration'] = '印影登録';
$lang['company_plan_attach_file_receiver'] = '添付ファイル(受信者)';
$lang['company_plan_attach_file_sender'] = '添付ファイル(送信者)';
$lang['company_plan_alert_function'] = 'アラート機能';
$lang['company_plan_language_support'] = '言語対応';


$lang['company_agree_no_use'] = '利用しない';
$lang['company_agree_use'] = '利用する';
$lang['company_agree_trial'] = 'トライアル';
$lang['company_agree_free'] = '無料';
$lang['company_agree_fee'] = '有料';
$lang['company_agree_fee_sub_account'] = '有料（サブアカウント）';
$lang['company_agree_cancel_contract'] = '解約';
$lang['company_agree_agency'] = '代理店';
$lang['company_agree_business'] = '業務提携';
$lang['company_agree_development'] = '開発';
$lang['company_agree_division'] = '事業部';
$lang['company_agree_other'] = 'その他';
$lang['company_agree_all'] = '全て';
$lang['company_plan_all'] = '全て';
$lang['company_stop_service'] = 'サービス停止';
$lang['company_start_service'] = 'サービス再開';
$lang['company_search_btn'] = '検索';
$lang['company_clear_btn'] = 'クリア';
$lang['company_create_btn'] = '企業登録';
$lang['company_change_status_show_modal_btn'] = '一括ステータス変更';
$lang['company_search_title'] = '検索条件';
$lang['company_agree_active'] = '使用';
$lang['company_agree_no_active'] = '使用なし';
$lang['company_option_null'] = '未選択';
$lang['company_enigma_version_no_use'] = '未選択';
$lang['company_enigma_code'] = 'engima企業コード';
$lang['company_enigma_version'] = 'バージョン';
$lang['company_enigma_version_1'] = 'バージョン 1';
$lang['company_enigma_version_2'] = 'バージョン 2';
$lang['company_check_enigma_button'] = '検索';
$lang['company_resend_account_info'] = 'メール再送';
$lang['company_maximum_signing_before'] = '1ヶ月あたりの書類の送信上限数：';
$lang['company_maximum_signing_after'] = '件まで';
$lang['company_maximum_import_signed_file_after'] = '件まで';


$lang['signing_folder_range_on_flag'] = '利用する';
$lang['signing_folder_range_off_flag'] = '利用しない';

$lang['signing_internal_workflow_on_flag'] = '利用する';
$lang['signing_internal_workflow_off_flag'] = '利用しない';
$lang['signing_batch_import_on_flag'] = '利用する';
$lang['signing_batch_import_off_flag'] = '利用しない';
$lang['company_import_signed_file_timestamp_on'] = 'タイムスタンプあり';
$lang['company_import_signed_file_timestamp_off'] = 'タイムスタンプなし';


$lang['company_plan_kintai_sense_link'] = 'SenseLink';
$lang['company_plan_kintai_alligate'] = 'ALLIGATE';

//------------------------------------------------
$lang['company_create_failed'] = '企業の作成が失敗しました。';
$lang['company_duplicate'] = '{field}が既に登録されています。';
$lang['company_create_success'] = '企業の作成が成功しました。';
$lang['company_update_success'] = '企業の更新が成功しました。';
$lang['company_no_result_found'] = '検索結果がありません。';
$lang['company_stop_service_title'] = 'サービス停止';
$lang['company_start_service_title'] = 'サービス再開';
$lang['company_stop_service_message_line1'] = 'のサービスを停止します。';
$lang['company_stop_service_message_line2'] = 'よろしければ、「停止」ボタンを押してください。';

$lang['company_start_service_message_line1'] = 'のサービスを再開します。';
$lang['company_start_service_message_line2'] = 'よろしければ、「再開」ボタンを押してください。';

$lang['company_stop_service_success'] = '[%s] サービスを停止しました。';
$lang['company_start_service_success'] = '[%s] サービスを再開しました。';
$lang['company_create_success_mail_title'] = '【jinjer】お客様情報登録完了のご案内';

$lang['company_stop_all_service'] = 'サービス利用を選択してください。';

$lang['company_agreement_start_date_less_than_end_date'] = '契約の終了日が必ず開始日より未来の日付で入力してください。';
$lang['company_agreement_current_date_less_than_end_date'] = '終了日は現在日より未来の日付で入力してください。';

$lang['company_agreement_start_from_less_than_start_to'] = '開始日の入力に誤りがあります。( from > to )';
$lang['company_agreement_end_from_less_than_end_to'] = '終了日の入力に誤りがあります。( from > to )';
$lang['company_enigma_code_version_required'] = 'enigma企業コードを入力してください。';
$lang['company_enigma_code_duplicate'] = 'enigma企業コードが既に登録されています。';

$lang['company_ai_option'] = 'AI';
$lang['company_is_using_ai_flag_off'] = '利用しない';
$lang['company_is_using_ai_flag_on'] = '利用する';

$lang['company_info'] = '企業情報';

$lang['company_ultra_option'] = 'ultra';
$lang['company_is_using_ultra_flag_off'] = '利用しない';
$lang['company_is_using_ultra_flag_on'] = '利用する';

$lang['d_account_id_unique_error'] = 'このアカウント識別子は登録できません。';
$lang['company_d_account_id'] = 'アカウント識別子';
$lang['company_docomo_option'] = 'ビジネスdアカウント連携';
$lang['company_is_docomo_flag_off'] = '利用しない';
$lang['company_is_docomo_flag_on'] = '利用する';

$lang['company_resend_account_info_title'] = 'メール再送';
$lang['company_resend_account_info_message_line1'] = 'の契約しているアカウント情報を再送信します';
$lang['company_resend_account_info_message_line2'] = 'よろしければ、「メール再送」ボタンを押してください。';
$lang['company_resend_account_info_btn'] = 'メール再送';
$lang['company_resend_account_info_success'] = '[%s] メール再送信しました。';

$lang['company_maximum_signing_option'] = '送信上限数';
$lang['company_modal_change_status_title'] = '一括ステータス変更';
$lang['company_change_status_btn'] = '変更';
$lang['company_modal_type_change_label'] = '変更内容';
$lang['company_modal_type_contract_label'] = '契約タイプ';
$lang['company_modal_duration_contract_label'] = '契約期間';
$lang['company_modal_direction_message'] = "ステータスを一括変更します。<br>よろしければ、「変更」ボタンを押してください。";
$lang['company_modal_type_change_success'] = "ステータスを一括変更しました。";
$lang['company_fail_update_contract_message'] = "企業ID：%s　入力された契約期間が正しくないため、登録できませんでした。";
