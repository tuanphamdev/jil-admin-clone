<?php
$lang['btn_confirm'] = '確認';
$lang['btn_return'] = '戻る';
$lang['btn_logout'] = 'ログアウト';


$lang['ts_password_log_list_title'] = 'アクセス許可企業一覧';
$lang['ts_password_log_create_title'] = '企業登録';
$lang['ts_password_log_update_title'] = '編集';
$lang['ts_password_log_code'] = '企業ID';
$lang['ts_password_log_name'] = '企業名';
$lang['ts_password_log_application_name_title'] = '申し込み者名';

$lang['ts_password_log_service_type'] = 'サービス種類';


$lang['ts_password_log_agreement_type'] = '契約タイプ';
$lang['ts_password_log_agreement_start_date'] = '開始日';
$lang['ts_password_log_agreement_end_date'] = '終了日';
$lang['ts_password_log_agreement_start_date_from'] = '開始日(from)';
$lang['ts_password_log_agreement_start_date_to'] = '開始日(to)';
$lang['ts_password_log_agreement_end_date_from'] = '終了日(from)';
$lang['ts_password_log_agreement_end_date_to'] = '終了日(to)';
$lang['ts_password_log_plan_jinjer_db'] = 'jinjer-db';
$lang['ts_password_log_plan_kintai'] = '勤怠管理';
$lang['ts_password_log_plan_roumu'] = '労務管理';
$lang['ts_password_log_plan_jinji'] = '人事管理';
$lang['ts_password_log_plan_keihi'] = '経費精算';
$lang['ts_password_log_plan_my_number'] = 'マイナンバー';
$lang['ts_password_log_plan_work_vital'] = 'ワークバイタル';
$lang['ts_password_log_plan_salary'] = '給与';
$lang['ts_password_log_plan_workflow'] = 'ワークフロー';
$lang['ts_password_log_agree_no_use'] = '利用しない';
$lang['ts_password_log_agree_use'] = '利用する';
$lang['ts_password_log_agree_trial'] = 'トライアル';
$lang['ts_password_log_agree_free'] = '無料';
$lang['ts_password_log_agree_fee'] = '有料';
$lang['ts_password_log_agree_all'] = '全て';
$lang['ts_password_log_plan_all'] = '全て';
$lang['ts_password_log_search_btn'] = '検索';
$lang['ts_password_log_clear_btn'] = 'クリア';
$lang['ts_password_log_search_title'] = '検索条件';



//------------------------------------------------

$lang['ts_password_log_no_result_found'] = '検索結果がありません。';
