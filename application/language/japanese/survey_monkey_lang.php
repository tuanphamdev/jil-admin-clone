<?php
$lang['survey_monkey_create_success'] = '入力した情報が正常に登録されました。';
$lang['survey_monkey_create_failed'] = '入力した情報が登録できませんでした。';
$lang['survey_monkey_services'] = '表示サービス';
$lang['survey_monkey_start_date'] = '表示期間（前）';
$lang['survey_monkey_end_date'] = '表示期間（後）';
$lang['survey_monkey_script_content'] = '埋め込みコード';
