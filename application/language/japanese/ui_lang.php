<?php
defined('BASEPATH') or exit('No direct script access allowed');

$lang['ui_close_btn']       = '戻る';
$lang['ui_stop_btn']        = '停止';
$lang['ui_start_btn']       = '再開';
$lang['ui_service_label']   = 'サービス';
$lang['ui_option_all'] = 'すべて';

$lang['ui_search_btn'] = '検索';
$lang['ui_clear_btn'] = 'クリア';
$lang['ui_search_title'] = '検索条件';
$lang['ui_submit_btn'] = '確認';
