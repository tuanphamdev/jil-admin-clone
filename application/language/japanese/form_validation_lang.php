<?php
defined('BASEPATH') or exit('No direct script access allowed');

$lang['form_validation_required']   = '{field}を入力してください。';
$lang['form_validation_isset']  = '{field}に値を入力してください。';
$lang['form_validation_valid_email']    = '{field}はメールアドレス形式で入力してください。';
$lang['form_validation_valid_emails']   = '{field}はメールアドレスの形式で入力してください。';
$lang['form_validation_valid_url']  = '{field}はURLの形式で入力してください。';
$lang['form_validation_valid_ip']   = '{field}はIPの形式で入力してください。';
$lang['form_validation_valid_mac']  = '{field}はMACのアドレスで入力してください。';
$lang['form_validation_min_length'] = '{field}は最小{param}桁数で入力してください。';
$lang['form_validation_max_length'] = '{field}は最大{param}桁数で入力してください。';
$lang['form_validation_exact_length']   = '{field}は{param}桁数で入力してください。';
$lang['form_validation_alpha']  = '{field}は英字で入力してください。 ';
$lang['form_validation_alpha_numeric']  = '{field}は半角英数字で入力してください。';
$lang['form_validation_alpha_numeric_spaces']   = '{field}は半角数字及びスペースで入力してください。';
$lang['form_validation_alpha_dash'] = '{field}は半角数字、アンダースコア、ハイフンで入力してください。';
$lang['form_validation_numeric']    = '{field}は半角数字で入力してください。';
$lang['form_validation_is_numeric'] = '{field}は数字文字で入力してください。';
$lang['form_validation_integer']    = '{field}は整数で入力してください。';
$lang['form_validation_regex_match']    = '{field}は正しいフォーマットで入力してください。';
$lang['form_validation_matches']    = '{field}は{param}の値に一致していません。';
$lang['form_validation_differs']    = '{field}は{param}とちがう値で入力してください。';
$lang['form_validation_is_unique'] = '{field}はユニークの値で入力してください。';


$lang['form_validation_is_natural']     = '{field}は数字で入力してください。';
$lang['form_validation_is_natural_no_zero'] = '{field}は0より大きな数値で入力してください。';



$lang['form_validation_decimal']    = '{field}は小数点を含めて入力してください。';
$lang['form_validation_less_than']  = '{field}は{param}より小さいな値を入力してください。';
$lang['form_validation_less_than_equal_to'] = '{field}は{param}以下の値を入力してください。';
$lang['form_validation_greater_than']   = '{field}は{param}より大きな値で入力してください。';
$lang['form_validation_greater_than_equal_to']  = '{field}は{param}以上の値で入力してください。.';
$lang['form_validation_error_message_not_set']  = '{field}でエラーがありません。';
$lang['form_validation_in_list']    = '{field}は{param}のいずれかで入力してください。';
