<?php
$lang['employee_admin_list_title'] = 'マスタアカウント変更';
$lang['employee_admin_photo'] = '写真';
$lang['employee_admin_name'] = '企業名';
$lang['employee_admin_code'] = '社員番号';
$lang['employee_admin_last_name'] = '氏名';
$lang['employee_admin_dept'] = '所属グループ';
$lang['employee_admin_job_tile'] = '役職';
$lang['employee_admin_enrollment_status'] = '在籍区分';
$lang['employee_admin_admin_status'] = 'マスタアカウント';
$lang['employee_admin_enrollment_status_enrolled'] = '在籍';
$lang['employee_admin_enrollment_status_retirement'] = '退職';
$lang['employee_admin_enrollment_status_absence'] = '休職';


$lang['employee_company_code'] = '企業コード';
$lang['employee_code'] = '社員番号';
$lang['employee_name'] = '従業員氏名';
$lang['employee_no_result_found'] = '検索結果がありません。';
$lang['employee_edit_function'] = '編集機能';
$lang['employee_admin_button_edit'] = '変更';
$lang['employee_change_role_success'] = '[%s]更新が成功しました。';
$lang['employee_enable_admin_popup_title'] = '確認';
$lang['employee_enable_admin_popup_message_line_1'] = '確認したらマスタアカウントが';
$lang['employee_enable_admin_popup_message_line_2'] = 'に変更されます。以前のマスタアカウントは権限がなくなり、ログインできなくなることもございますので、よろしいですか？';
