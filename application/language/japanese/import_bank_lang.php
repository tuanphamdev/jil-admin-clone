<?php
$lang['import_bank_success'] = '銀行の情報の取り込みに成功しました。';
$lang['import_bank_failed'] = '銀行の情報の取り込みに失敗しました。';
$lang['import_csv_search_title'] = '検索条件';
$lang['import_csv_list_title'] = '全銀一覧';
$lang['import_csv_button'] = 'アップロード';
$lang['import_csv_title_from'] = 'CSVアップロード';

$lang['import_csv_bank_code'] = '銀行コード';
$lang['import_csv_branch_code'] = '支店コード';
$lang['import_csv_bank_name'] = '銀行名';
$lang['import_csv_branch_name'] = '支店名';
$lang['import_csv_result_found'] = '検索結果がありません。';
$lang['import_csv_bank_name_kana'] = '銀行名カナ';
$lang['import_csv_branch_name_kana'] = '支店名カナ';
$lang['import_csv_branch_post_code'] = '郵便番号';
$lang['import_csv_branch_address'] = '住所';
$lang['import_csv_branch_phone_number'] = '電話番号';
$lang['import_csv_draft_clearing_place'] = '手形交換場所';
$lang['import_csv_sort'] = '並び順';

$lang['import_bank_csv_file_required'] = 'ファイルを入れてください。';

$lang['import_bank_csv_line'] = '行目';
