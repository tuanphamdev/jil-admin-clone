<?php
$lang['ts_password_ip_list_title'] = 'IP設定';
$lang['ts_password_ip_create_title'] = 'IP登録';
$lang['ts_password_ip_update_title'] = 'IP編集';
$lang['ts_password_ip_address'] = '許可するIP';
$lang['ts_password_ip_delete'] = '削除';
$lang['ts_password_ip_field_name'] = '名前';
$lang['ts_password_ip_create_btn'] = 'IP登録';
$lang['ts_password_ip_no_result_found'] = 'IPが見つかりません。';
$lang['ts_password_ip_create_failed'] = 'IPの作成が失敗しました。';
$lang['ts_password_ip_update_failed'] = 'IPの更新が失敗しました。';
$lang['ts_password_ip_delete_message'] = 'を削除します。よろしいですか？';
$lang['ts_password_ip_delete_label'] = '削除確';
$lang['ts_password_ip_delete_success'] = 'IPを削除しました。';
$lang['ts_password_ip_failed_success'] = 'IPの削除が失敗しました。';
$lang['ts_password_ip_duplicate'] = 'IPが重複しています。';
$lang['ts_password_ip_create_success'] = 'IPアドレスを設定しました。';
$lang['ts_password_ip_update_success'] = 'IPを更新しました。';

$lang['qa_engine_create_failed'] = 'QA Engineの作成が失敗しました。';
