<?php
$lang['auth_login_failed'] = 'メールアドレス、パスワードを確認してください。';
$lang['auth_login_email'] = 'メールアドレス';
$lang['auth_login_password'] = 'パスワード';
$lang['auth_login_password_confirm'] = '確認用パスワード';
$lang['auth_reset_password_header'] = 'メールアドレスを入力してください。';
$lang['auth_email_address_title'] = 'メールアドレス';
$lang['auth_account_status_off'] = '退職したユーザがログインできません。';
$lang['auth_send_forgot_mail_btn'] = '送信';
$lang['auth_send_forgot_success'] = 'メールを送信しました。';
$lang['auth_placeholder_password'] = 'パスワード';
$lang['auth_placeholder_password_confirm'] = '確認用パスワード';
$lang['auth_reissue_password_title'] = 'パスワードを入力してください。';
$lang['auth_confirm_password_failed'] = 'パスワードと確認用パスワードが一致していません。';
