<?php
$lang['bank_status_flag_off'] = '無効';
$lang['bank_status_flag_on'] = '有効';
$lang['bank_status'] = '有効ステータス';
$lang['bank_line_code'] = '並びコード';
$lang['draft_clearing_place'] = '手形交換所番号';
$lang['branch_phone_number'] = '電話番号';
$lang['branch_address'] = '住所';
$lang['branch_post_code'] = '郵便番号';
$lang['branch_name_kana'] = '支店名（フリガナ）';
$lang['branch_name'] = '支店名';
$lang['bank_name_kana'] = '銀行名（フリガナ）';
$lang['bank_name'] = '銀行名';
$lang['branch_code'] = '支店コード';
$lang['bank_code'] = '銀行コード';
$lang['bank_create_title'] = '銀行登録';
$lang['bank_edit_title'] = '銀行編集';
$lang['bank_edit_button'] = '編集';
$lang['bank_create_button'] = '銀行登録';
$lang['bank_status_filter_valid'] = '有効な銀行データのみ表示する';
$lang['bank_store_success'] = '銀行データを保存しました。';
$lang['bank_master_no_result_found'] = 'この銀行はシステムにありません。';
