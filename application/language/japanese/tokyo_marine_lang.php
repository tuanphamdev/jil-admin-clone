<?php

$lang['tokyo_marine_use_flag'] = '利用する';
$lang['tokyo_marine_no_use_flag'] = '利用しない';
$lang['tokyo_marine_use_title'] = '東京海上利用';
$lang['tokyo_marine_use_message_line1'] = 'の東京海上を利用します。';
$lang['tokyo_marine_use_message_line2'] = 'よろしければ、「利用する」ボタンを押してください。';
$lang['tokyo_marine_no_use_title'] = '東京海上利用しません';
$lang['tokyo_marine_no_use_message_line1'] = 'の東京海上を利用しません。';
$lang['tokyo_marine_no_use_message_line2'] = 'よろしければ、「利用しない」ボタンを押してください。';
$lang['tokyo_marine_no_result_found'] = '検索結果がありません。';
$lang['tokyo_marine_use_success'] = '【%s】の東京海上を利用します。';
$lang['tokyo_marine_no_use_success'] = '【%s】の東京海上を利用しません。';
$lang['tokyo_marine_list_title'] = '東京海上一覧';
$lang['tokyo_marine_create_title'] = '企業登録';
$lang['tokyo_marine_update_title'] = '編集';
$lang['tokyo_marine_create_success'] = '企業の作成が成功しました。';
$lang['tokyo_marine_update_success'] = '企業の更新が成功しました。';

$lang['tokyo_marine_no_use_btn'] = '利用しない';
$lang['tokyo_marine_use_btn'] = '利用する';
