<?php
$lang['text_rest_invalid_api_key'] = 'APIキーが無効です。 %s'; // %sがREST APIのキーです。
$lang['text_rest_invalid_credentials'] = 'ログインの情報が正しくありません。';
$lang['text_rest_ip_denied'] = 'IPがアクセスできません。';
$lang['text_rest_ip_unauthorized'] = 'IPが認証されていません。';
$lang['text_rest_unauthorized'] = '認証されていません。';
$lang['text_rest_ajax_only'] = 'AJAXのリクエストだけが許可されます。';
$lang['text_rest_api_key_unauthorized'] = 'このAPIキーがリクエストされたコントローラーへアクセスできません。';
$lang['text_rest_api_key_permissions'] = 'このAPIキーが権限がありません。';
$lang['text_rest_api_key_time_limit'] = 'このAPIキーがメソッドの時間制限を超えました。';
$lang['text_rest_ip_address_time_limit'] = 'このIPアドレスがメソッドの時間制限を超えました。';
$lang['text_rest_unknown_method'] = 'メソッドが確定されません。';
$lang['text_rest_unsupported'] = 'プロトコルがサポートされません。';
