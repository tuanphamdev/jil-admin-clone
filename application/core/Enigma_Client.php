<?php defined('BASEPATH') or exit('No direct script access allowed');

class Enigma_Client
{
    private $ci;
    protected $_client;

    public function __construct()
    {
        $this->ci            = & get_instance();
        $this->_client = new GuzzleHttp\Client();
    }

    protected function get_base_uri($version = ENIGMA_V2)
    {
        if ($version === ENIGMA_V1) {
            return getenv('ENIGMA_API_BASE_URL_V1');
        }
        return getenv('ENIGMA_API_BASE_URL_V2');
    }

    protected function get_token($company_code, $version = ENIGMA_V2)
    {
        $this->ci->load->library('enigma_token');
        if ($version === ENIGMA_V1) {
            $this->ci->enigma_token->key = getenv('ENIGMA_API_KEY_V1');
            $iss = getenv('ENIGMA_API_BASE_URL_V1');
        } else {
            $this->ci->enigma_token->key = getenv('ENIGMA_API_KEY_V2');
            $iss = getenv('ENIGMA_API_BASE_URL_V2');
        }

        return $this->ci->enigma_token->get_token_test($company_code, $iss);
    }

    protected function post($url, $params)
    {

        try {
            $response = $this->_client->post($url, $params);
            if ($content = json_decode($response->getBody()->getContents(), true)) {
                $this->ci->monolog->logger(Monolog::INFO)->info(json_encode($content), array('request:'.$url => $params));
                $content['status'] = true;
                return $content;
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            $this->ci->monolog->logger(Monolog::EXCEPTION)->info($e, array('request:'.$url => $params));
            if (is_subclass_of($e, \GuzzleHttp\Exception\RequestException::class)) {
                if ($e->hasResponse()) {
                    $response = $e->getResponse();
                    $content  = json_decode($response->getBody()->getContents(), true) ?? array();
                    if (! empty($content)) {
                        if (isset($content['errors'])) {
                            return array(
                                'status' => false,
                                'error' => array(
                                    $content['message']
                                )
                            );
                        }
                    } else {
                        return array(
                            'status' => false,
                            'error' => array(
                                $response->getReasonPhrase()
                            )
                        );
                    }
                }
            }
            return array(
                'status' => false,
                'error' => array(
                    'Connection refused.'
                )
            );
        }
    }

    protected function get($url, $query)
    {
        try {
            $response = $this->_client->get($url, $query);
            if ($content = json_decode($response->getBody()->getContents(), true)) {
                $this->ci->monolog->logger(Monolog::INFO)->info(json_encode($content), array('request:'.$url => $query));
                $content['status'] = true;
                return $content;
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            $this->ci->monolog->logger(Monolog::EXCEPTION)->info($e, array('request:'.$url => $query));
            if (is_subclass_of($e, \GuzzleHttp\Exception\RequestException::class)) {
                if ($e->hasResponse()) {
                    $response = $e->getResponse();
                    $content  = json_decode($response->getBody()->getContents(), true) ?? array();
                    if (! empty($content)) {
                        if (isset($content['errors'])) {
                            return array(
                                'status' => false,
                                'error' => array(
                                    $content['message']
                                )
                            );
                        }
                    } else {
                        return array(
                            'status' => false,
                            'error' => array(
                                $response->getReasonPhrase()
                            )
                        );
                    }
                }
            }
            return array(
                'status' => false,
                'error' => array(
                    'Connection refused.'
                )
            );
        }
    }
}
