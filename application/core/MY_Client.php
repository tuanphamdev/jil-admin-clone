<?php defined('BASEPATH') or exit('No direct script access allowed');

class MY_Client
{
    private $ci;
    protected $_client;
    protected $_api_core_url;

    public function __construct()
    {
        $this->ci            = & get_instance();
        $this->_api_core_url = getenv('API_CORE_URL');
        $default             =  array(
            'headers' => array(
                'Api-Token' => $this->ci->session->has_userdata('api_token') ? $this->ci->session->userdata('api_token') : null,
                'ADMIN-API-KEY' => getenv('API_KEY'),
                'Lg' => 'japanese'
            ),
            'base_uri' => getenv('API_BASE_URL'),
        );

        $this->_client = new GuzzleHttp\Client($default);
    }

    protected function post($url, $params)
    {
        try {
            $response = $this->_client->post($url, $params);
            if ($content = json_decode($response->getBody()->getContents(), true)) {
                $this->ci->monolog->logger(Monolog::INFO)->info(json_encode($content), array('request:'.$url => $params));
                return $content;
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            $this->ci->monolog->logger(Monolog::EXCEPTION)->info($e, array('request:'. $url => $params));
            if (is_subclass_of($e, \GuzzleHttp\Exception\RequestException::class)) {
                if ($e->hasResponse()) {
                    $response = $e->getResponse();
                    $content  = json_decode($response->getBody()->getContents(), true) ?? array();
                    if (! empty($content)) {
                        return $content;
                    } else {
                        return array(
                            'status' => false,
                            'error' => array(
                                $response->getReasonPhrase()
                            )
                        );
                    }
                }
            }
            return array(
                'status' => false,
                'error' => array(
                    'Connection refused.'
                )
            );
        }
    }

    protected function get($url, $query)
    {
        try {
            $response = $this->_client->get($url, $query);
            if ($content = json_decode($response->getBody()->getContents(), true)) {
                $this->ci->monolog->logger(Monolog::INFO)->info(json_encode($content), array('request:'.$url => $query));
                return $content;
            } else {
                throw new Exception();
            }
        } catch (Exception $e) {
            $this->ci->monolog->logger(Monolog::EXCEPTION)->info($e, array('request:'.$url => $query));
            //return array();
            if (is_subclass_of($e, \GuzzleHttp\Exception\RequestException::class)) {
                if ($e->hasResponse()) {
                    $response = $e->getResponse();
                    $content  = json_decode($response->getBody()->getContents(), true) ?? array();
                    if (! empty($content)) {
                        return $content;
                    } else {
                        return array(
                            'status' => false,
                            'error' => array(
                                $response->getReasonPhrase()
                            )
                        );
                    }
                }
            }
            return array(
                'status' => false,
                'error' => array(
                    'Connection refused.'
                )
            );
        }
    }
}
