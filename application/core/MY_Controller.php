<?php defined('BASEPATH') or exit('No direct script access allowed');
use Firebase\JWT\JWT;
use Jinjer\libraries\User_info;

abstract class MY_Controller extends CI_Controller
{
    const ALERT_DANGER = 'danger';
    const ALERT_WARNING = 'warning';
    const ALERT_SUCCESS = 'success';

    protected $role_filter;
    protected $user_info;
    abstract protected function set_role_filter();

    public function __construct()
    {
        parent::__construct();

        $this->load->model(['ip_model']);

        if (!$this->ip_model->allow(get_access_ip())) {
            $this->show_403();
        }

        if (empty($this->session->userdata('is_logged')) && $this->router->fetch_class() != 'Login') {
            $this->load->helper('url');
            $url_callback = current_url();
            $params   = $_SERVER['QUERY_STRING'];
            if (!empty($params)) {
                $url_callback .= '?' . $params;
            }
            $this->session->set_userdata('url_callback', $url_callback);
            redirect('login');
        } elseif ($this->session->userdata('is_logged') === true) {
            $this->get_user_info();
        }

        if ($this->check_permission() === false) {
            redirect('top');
        }
    }

    protected function get_user_info()
    {
        $user_login = $this->session->userdata('user_login');
        $api_token = $this->session->userdata('api_token');

        //save user info into a singleton
        $this->user_info = \Jinjer\libraries\User_info::instance();
        $this->user_info->email = $user_login['email'];
        $this->user_info->password = $user_login['password'];
        $this->user_info->api_token = $api_token;
        $this->user_info->user_id = $user_login['user_id'];
        $this->user_info->user_name = $user_login['user_name'];
    }



    private function check_permission()
    {
        $not_check_permission = [
            'Login',
            'User.logout',
            'Top',
        ];
        if (in_array($this->router->fetch_class(), $not_check_permission)
            || in_array($this->router->fetch_class().'.'.$this->router->fetch_method(), $not_check_permission)) {
            return true;
        }
        $roles = $this->get_permissions();
        if (!empty($roles)) {
            $method = $this->router->fetch_method();
            if (!empty($this->role_filter)) {
                if (isset($this->role_filter[$method]) === false) {
                    return true;
                }
                foreach ($this->role_filter[$method] as $item => $permission) {
                    if (isset($roles[$item]) && $permission <= $roles[$item]) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    protected function get_permissions()
    {
        $default = [
            'company' => READ_PERMISSION,
            'user' => READ_PERMISSION,
            'role' => READ_PERMISSION,
            'csv' => READ_PERMISSION,
            'ip' => READ_PERMISSION,
            'information' => NON_PERMISSION,
            'import_bank' => READ_PERMISSION,
            'tokyo_marine' => READ_PERMISSION,
            'employee' => READ_PERMISSION,
            'qa_engine' => FULL_PERMISSION,
            'survey_monkey' => FULL_PERMISSION,
        ];
        $roles = $this->session->userdata('roles');
        foreach ($roles as $role => $permission) {
            $default[$role] = $permission;
        }
        return $default;
    }

    protected function show_alert($type, $message)
    {

        if (empty($message)) {
            return;
        }
        if (is_array($message)) {
            $message = implode("<br/>", $message);
        }

        $this->session->set_flashdata('alert', ['type' => $type, 'message' => $message]);
    }

    protected function show_alert_danger($message)
    {
        $this->show_alert(self::ALERT_DANGER, $message);
    }
    protected function show_alert_warning($message)
    {
        $this->show_alert(self::ALERT_WARNING, $message);
    }
    protected function show_alert_success($message)
    {
        $this->show_alert(self::ALERT_SUCCESS, $message);
    }

    protected function set_flashdata($key, $value)
    {
        $this->session->set_flashdata($key, $value);
    }

    protected function get_flashdata($key)
    {
        return $this->session->flashdata($key);
    }

    protected function get_tel_number_pattern($data)
    {
        return implode('-', remove_empty_elements($data));
    }

    protected function tel_number_pattern_to_array($data)
    {
        return explode('-', $data);
    }

    protected function get_ip_number_pattern($data)
    {
        return implode('.', remove_empty_elements($data));
    }

    protected function ip_number_pattern_to_array($data)
    {
        return explode('.', $data);
    }

    protected function show_403()
    {
        $this->output->set_status_header(403)
            ->set_content_type('text/html')
            ->set_output($this->load->view('errors/html/error_403', [], true))
            ->_display();
        exit;
    }

    protected function validation_errors()
    {
        $string = strip_tags($this->form_validation->error_string());
        return array_filter(explode(PHP_EOL, trim($string, PHP_EOL)));
    }

    /**
     * Send Mail
     * @param string $to
     * @param string $subject
     * @param string $message
     * @return boolean
     */
    protected function send_email($to, $subject, $message)
    {
        $this->load->library('email');

        $this->email->initialize(array(
            'protocol' => $this->email->protocol,
            'smtp_host' => $this->email->smtp_host,
            'smtp_user' => $this->email->smtp_user,
            'smtp_pass' => $this->email->smtp_pass,
            'smtp_port' => $this->email->smtp_port,
            'crlf' => $this->email->crlf,
            'newline' => $this->email->newline
        ));
        if (!empty($to) && !empty($subject) && !empty($message)) {
            $this->email->from(getenv('EMAIL_FROM'));
            $this->email->to($to);
            $this->email->bcc(getenv('EMAIL_BCC'));
            $this->email->subject($subject);
            $this->email->message($message);
            if ($this->email->send()) {
                return true;
            }
        }
        return false;
    }
}
