<?php defined('BASEPATH') or exit('No direct script access allowed');

require(APPPATH . 'core/REST_Controller.php');
use Restserver\Libraries\REST_Controller;
use Firebase\JWT\JWT;

class Api_Controller extends REST_Controller
{

    const COMPANY_SEARCH = 'company_search';
    const USER_SEARCH = 'user_search';
    const INFORMATION_SEARCH = 'information_search';
    const BANK_SEARCH = 'bank_search';
    const EMPLOYEE_SEARCH = 'employee_search';
    const TS_PASSWORD_LOG_SEARCH = 'ts_password_log_search';

    public function __construct()
    {
        parent::__construct();

        /*if (is_array($this->response->lang))
        {
            $this->load->language('application', $this->response->lang[0]);
        }
        else
        {
            $this->load->language('application', $this->response->lang);
        }*/
    }

    /**
     * Response failure
     *
     * @param array $error
     * @param string $http_code
     */
    protected function response_failure(array $error, $http_code)
    {
        return $this->response([
            $this->config->item('rest_status_field_name') => false,
            $this->config->item('rest_message_field_name') => $error
        ], $http_code);
    }

    /**
     * Response success
     *
     * @param $data
     * @param string $http_code
     */
    protected function response_success($data, $http_code)
    {
        return $this->response([
            $this->config->item('rest_status_field_name') => true, 'data' => $data
        ], $http_code);
    }

    /**
     * Get paginate params
     *
     * @return array
     */
    protected function paginate()
    {
        $per_page = (int)$this->get('per_page');
        $curr_page = (int)$this->get('curr_page');

        $max_item_per_page = $this->config->item('max_item_per_page');
        $min_item_per_page = $this->config->item('min_item_per_page');

        $per_page = ($per_page > $max_item_per_page) ? $max_item_per_page : $per_page;
        $per_page = ($per_page < $min_item_per_page) ? $min_item_per_page : $per_page;
        $curr_page = $curr_page > 1 ? $curr_page : 1;

        return ['per_page' => $per_page, 'curr_page' => $curr_page];
    }

    protected function query_search($type, $filter = [])
    {
        $query = null;
        $get = !empty($filter) ? $filter : $this->get();
        if (!empty($get)) {
            switch ($type) {
                case self::COMPANY_SEARCH:
                    $code = $get['code'] ?? '';
                    $name = $get['name'] ?? '';
                    $registered_date_from = $get['registered_date_from'] ?? '';
                    $registered_date_to = $get['registered_date_to'] ?? '';
                    $code_from = $get['code_from'] ?? '';
                    $code_to = $get['code_to'] ?? '';
                    $status = $get['status'] ?? '';
                    $type = $get['type'] ?? '';
                    $start_date_from = $get['start_date_from'] ?? '';
                    $start_date_to = $get['start_date_to'] ?? '';
                    $end_date_from = $get['end_date_from'] ?? '';
                    $end_date_to = $get['end_date_to'] ?? '';
                    $is_tokyo_marine = $get['is_tokyo_marine'] ?? '';
                    if ($code != '') {
                        $query['companies.code LIKE'] = '%'.$code.'%';
                    }
                    if ($name != '') {
                        $query['companies.name LIKE'] = '%'.$name.'%';
                    }
                    if ($registered_date_from != '') {
                        $query['companies.registered_date >='] = $registered_date_from;
                    }
                    if ($registered_date_to != '') {
                        $query['companies.registered_date <='] = $registered_date_to;
                    }
                    if ($code_from != '') {
                        $query['companies.code >='] = (int) $code_from;
                    }
                    if ($code_to != '') {
                        $query['companies.code <='] = (int) $code_to;
                    }
                    if ($status != '') {
                        $query['companies.status'] = $status;
                    }
                    if ($type != '') {
                        $query['agreements.type'] = $type;
                    }
                    if ($start_date_from != '' && $start_date_to != '') {
                        $query['agreements.start_date >='] = $start_date_from;
                        $query['agreements.start_date <='] = $start_date_to;
                    } elseif ($start_date_from != '') {
                        $query['agreements.start_date >='] = $start_date_from;
                    } elseif ($start_date_to != '') {
                        $query['agreements.start_date <='] = $start_date_to;
                    }

                    if ($end_date_from != '' && $end_date_to != '') {
                        $query['agreements.end_date >='] = $end_date_from;
                        $query['agreements.end_date <='] = $end_date_to;
                    } elseif ($end_date_from != '') {
                        $query['agreements.end_date >='] = $end_date_from;
                    } elseif ($end_date_to != '') {
                        $query['agreements.end_date <='] = $end_date_to;
                    }

                    if ($is_tokyo_marine != '') {
                        $query['companies.is_tokyo_marine'] = $is_tokyo_marine;
                    }

                    break;

                case self::USER_SEARCH:
                    $name = $get['name'] ?? '';
                    $status = $get['status'] ?? '';

                    if ($name != '') {
                        $query['users.name LIKE'] = '%'.$name.'%';
                    }

                    if ($status != '') {
                        $query['users.status'] = $status;
                    }
                    
                    break;
                case self::INFORMATION_SEARCH:
                    $created_at_from = $get['created_at_from'] ?? '';
                    $created_at_to = $get['created_at_to'] ?? '';
                    $title = $get['title'] ?? '';
                    $is_open = $get['is_open'] ?? '';
                    if ($created_at_from != '') {
                        $query['DATE(informations.created_at) >='] = $created_at_from;
                    }
                    if ($created_at_to != '') {
                        $query['DATE(informations.created_at) <='] = $created_at_to;
                    }
                    if ($title != '') {
                        $query['informations.title'] = $title;
                    }
                    if ($is_open != '') {
                        $query['informations.is_open'] = $is_open;
                    }
                    break;

                case self::BANK_SEARCH:
                    $bank_code = $get['bank_code'] ?? '';
                    $branch_code = $get['branch_code'] ?? '';
                    $bank_name = $get['bank_name'] ?? '';
                    $branch_name = $get['branch_name'] ?? '';
//                    $status = $get['status'] ?? BANK_STATUS_VALID;
                    if ($bank_code != '') {
                        $query['bank_master.bank_code'] = $bank_code;
                    }
                    if ($branch_code != '') {
                        $query['bank_master.branch_code'] = $branch_code;
                    }
                    if ($bank_name != '') {
                        $query['bank_master.bank_name'] = $bank_name;
                    }
                    if ($branch_name != '') {
                        $query['bank_master.branch_name'] = $branch_name;
                    }
//                    if ($status != '') {
//                        $query['bank_master.status'] = $status;
//                    }
                    break;

                case self::EMPLOYEE_SEARCH:
                    $query['company_code'] = $get['company_code'] ?? '';
                    $query['employee_code'] = $get['employee_code'] ?? '';
                    $employee_name = $get['employee_name'] ?? '';

                    if ($employee_name != '') {
                        $query['employee_name'] = $employee_name;
                    }

                    break;
                case self::TS_PASSWORD_LOG_SEARCH:
                    $code = $get['code'] ?? '';
                    $name = $get['name'] ?? '';
                    break;
                default:
                    break;
            }
        }

        return $query;
    }
}
