<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Sound upload path
|--------------------------------------------------------------------------
|
| Set path to where the sounds placed
|
*/
$config['upload_sound_path'] = './uploads/sounds/';
$config['upload_image_path'] = './uploads/images/';
$config['allowed_image_types'] = 'gif|jpg|png|jpeg';
$config['allowed_sound_types'] = 'mp3';
$config['max_image_size'] = 1024; // 1 MB
$config['max_sound_size'] = 5120; // 5 MB

$config['max_item_per_page'] = 50;
$config['min_item_per_page'] = 10;

$config['support_languages'] = array('japanese');
$config['language_files'] = array(
    'rest_controller',
    'form_validation',
    'company',
    'csv',
    'user',
    'role',
    'ip',
    'menu',
    'my_form_validation',
    'ui',
    'information'
);

$config['agreement_type'] = [
    0 => 'trial',
    1 => 'free',
    2 => 'fee',
    3 => 'fee_sub_account',
    4 => 'cancel_contract',
    5 => 'agency',
    6 => 'business',
    7 => 'development',
    8 => 'division',
    9 => 'other'
];

/*
|--------------------------------------------------------------------------
| Permission
|--------------------------------------------------------------------------
|
| Permission setting
|
*/
$config['role_permissions'] = [
    'company',
    'csv',
    'user',
    'role',
    'ip',
    'tokyo_marine'
];
