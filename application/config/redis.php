<?php

$config[ 'socket_type' ] = 'tcp';

$config[ 'host' ] = getenv('REDIS_HOST');

$config[ 'password' ] = null;

$config[ 'port' ] = '6379';

$config[ 'timeout' ] = 0;

