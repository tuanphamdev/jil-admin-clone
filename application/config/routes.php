<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route = Luthier\Route::getRoutes();

/*$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//login controller
$route['login']['GET'] = 'Login/index';
$route['forgot_password']['GET'] = 'Login/forgot_password';

//companies controller
$route['companies']['GET'] = 'Company/index';
$route['companies/page']['GET'] = 'Company/index';
$route['companies/page/(:num)']['GET'] = 'Company/index/$1';
$route['companies/create']['GET'] = 'Company/create';
$route['companies/update/(:num)']['GET'] = 'Company/update/$1';
$route['companies/cities/(:num)']['GET'] = 'Company/cities/$1';
$route['companies/start_service']['POST'] = 'Company/start_service';
$route['companies/stop_service']['POST'] = 'Company/stop_service';

$route['companies/do_create']['POST'] = 'Company/do_create';
$route['companies/do_update/(:num)']['POST'] = 'Company/do_update/$1';

//ip address controller
$route['ips']['GET'] = 'Ip/index';
$route['ips/create']['GET'] = 'Ip/create';
$route['ips/update']['GET'] = 'Ip/update';

$route['ips/do_create']['POST'] = 'Ip/do_create';
$route['ips/do_update']['POST'] = 'Ip/do_update';

//user controller
$route['users']['GET'] = 'User/index';
$route['users/create']['GET'] = 'User/create';
$route['users/update/(:num)']['GET'] = 'User/update/$1';

$route['users/do_create']['POST'] = 'User/do_create';
$route['users/do_update']['POST'] = 'User/do_update';

$route['do_login']['POST'] = 'User/do_login';
$route['reissue_password']['GET'] = 'User/reissue_password';
$route['logout']['GET'] = 'User/logout';


//role controller
$route['roles']['GET'] = 'Role/index';
$route['roles/create']['GET'] = 'Role/create';
$route['roles/update']['GET'] = 'Role/update';

$route['roles/do_create']['POST'] = 'Role/do_create';
$route['roles/do_update']['POST'] = 'Role/do_update';

//export CSV
$route['export_csv']['GET'] = 'ExportCSV/index';
$route['export_csv/page']['GET'] = 'ExportCSV/index';
$route['export_csv/page/(:num)']['GET'] = 'ExportCSV/index/$1'; */



/*
| -------------------------------------------------------------------------
| API ROUTING
| -------------------------------------------------------------------------
*/

// API companies
/*
$route['api/v1/companies/list']['GET'] = 'api/v1/company_api/list';
$route['api/v1/companies/detail']['GET'] = 'api/v1/company_api/detail';
$route['api/v1/companies/create']['POST'] = 'api/v1/company_api/create';
$route['api/v1/companies/update']['POST'] = 'api/v1/company_api/update';
$route['api/v1/companies/delete']['POST'] = 'api/v1/company_api/delete';
$route['api/v1/companies/stop_service']['POST'] = 'api/v1/company_api/stop_service';
$route['api/v1/companies/start_service']['POST'] = 'api/v1/company_api/start_service';*/

// API users
/*$route['api/v1/users/auth']['POST'] = 'api/v1/user_api/auth';
$route['api/v1/users/list']['GET'] = 'api/v1/user_api/list';
$route['api/v1/users/detail']['GET'] = 'api/v1/user_api/detail';
$route['api/v1/users/create']['POST'] = 'api/v1/user_api/create';
$route['api/v1/users/update']['POST'] = 'api/v1/user_api/update';
$route['api/v1/users/delete']['POST'] = 'api/v1/user_api/delete';*/

// API roles
//$route['api/v1/roles/list']['GET'] = 'api/v1/role_api/list';

// Export csv
//$route['api/v1/export_csv/list']['GET'] = 'api/v1/export_csv_api/list';

