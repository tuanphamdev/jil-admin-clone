<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| The signing algorithm.
|--------------------------------------------------------------------------
|
| Supported algorithms are 'HS256', 'HS384', 'HS512' and 'RS256'
|
*/
$config['alg'] = 'HS256';
/*
|--------------------------------------------------------------------------
|
| You can add a leeway to account for when there is a clock skew times between
| the signing and verifying servers. It is recommended that this leeway should
| not be bigger than a few minutes.
|
| Source: http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html#nbfDef
|
|--------------------------------------------------------------------------
*/

$config['leeway'] = 60;

/*
|--------------------------------------------------------------------------
| The secret key.
|--------------------------------------------------------------------------
*/

$config['secret_key'] = getenv('JWT_SECRET');
