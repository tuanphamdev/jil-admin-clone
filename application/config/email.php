<?php
$config['protocol'] = getenv('PROTOCOL');
$config['smtp_host'] = getenv('SMTP_HOST');
$config['smtp_user'] = getenv('SMTP_USER');
$config['smtp_pass'] = getenv('SMTP_PASS');
$config['mailpath'] = getenv('MAILPATH');
$config['smtp_port'] = getenv('SMTP_PORT');
$config['crlf'] = "\r\n";
$config['newline'] = "\r\n";
$config['charset'] = 'utf-8';
$config['wordwrap'] = TRUE;
$config['mailtype'] = 'html';
