<?php
namespace Jinjer\traits;

trait Company_validation_trait
{

    /**
     * Company validate
     *
     * validation for company model
     * belong to business of model
     *
     * @param $str
     * @param $field
     * @return bool
     */
    public function company_validate($str, $field)
    {
        switch ($field) {
            case 'code':
                $this->set_message('company_validate', $this->CI->lang->line('company_duplicate'));
                return ($this->CI->company_model->as_array()->where(array($field => $str, 'deleted_at' => null))->count_rows() === 0);
                break;
            case 'name':
                break;
            default:
                break;
        }
        return true;
    }
}
