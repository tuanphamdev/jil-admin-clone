<?php
use Phinx\Seed\AbstractSeed;

class KeyAPISeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    
    public function run()
    {
        $this->table('keys')->truncate();
        $data = [
            
            [
                'key' => getenv('API_KEY'),
                'level' => 1,
                'ignore_limits' => 0,
                'date_created' => 1,
            ]

        ];
        $this->table('keys')->insert($data)->save();
    }
}
