<?php
use Phinx\Seed\AbstractSeed;

class RolePermissionTokyoMarineSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    
    public function run()
    {
        
        $this->query("SET foreign_key_checks = 0");
        $data = [
            
            [
                'item' => 'tokyo_marine',
                'permission' => '3',
                'role_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
            ],
        ];
        $this->table('role_permissions')->insert($data)->save();
        $this->query("SET foreign_key_checks = 1");
    }
}
