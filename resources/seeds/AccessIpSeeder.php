<?php

use Phinx\Seed\AbstractSeed;

class AccessIpSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $this->table('access_ips')->truncate();
        $data = [
            [
                'ip_address' => '111.101.74.8',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],

            //IP neolab vn
            [
                'ip_address' => '113.164.16.14',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ];
        $this->table('access_ips')->insert($data)->save();

    }
}
