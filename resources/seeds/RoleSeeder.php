<?php
use Phinx\Seed\AbstractSeed;

class RoleSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    
    public function run()
    {
        
        $this->query("SET foreign_key_checks = 0");
        $this->table('roles')->truncate();
        $data = [
            
            [
                'name' => 'マスター',
                'created_at' => date('Y-m-d H:i:s'),
            ]

        ];
        $this->table('roles')->insert($data)->save();
        $this->query("SET foreign_key_checks = 1");
    }
}
