<?php
use Phinx\Seed\AbstractSeed;

class RolePermissionSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    
    public function run()
    {
        
        $this->query("SET foreign_key_checks = 0");
        $this->table('role_permissions')->truncate();
        $data = [
            
            [
                'item' => 'company',
                'permission' => '3',
                'role_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'item' => 'user',
                'permission' => '3',
                'role_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'item' => 'role',
                'permission' => '3',
                'role_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'item' => 'ip',
                'permission' => '3',
                'role_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'item' => 'csv',
                'permission' => '3',
                'role_id' => '1',
                'created_at' => date('Y-m-d H:i:s'),
            ]

        ];
        $this->table('role_permissions')->insert($data)->save();
        $this->query("SET foreign_key_checks = 1");
    }
}
