<?php
use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    
    public function run()
    {

        $this->query("SET foreign_key_checks = 0");
        $this->table('users')->truncate();
        $data = [
            
            [
                'role_id' => '1',
                'name' => 'master',
                'name_furigana' => '',
                'email' => 'master@jinjer.biz',
                'password' => '$2y$10$fQ.ViGgPXMQq8LvE8bCUHOSKeXhuDC1DKU5iS.Ul27trj051iIbOG',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'role_id' => '1',
                'name' => 'tester',
                'name_furigana' => '',
                'email' => 'tester@jinjer.biz',
                'password' => '$2y$10$fQ.ViGgPXMQq8LvE8bCUHOSKeXhuDC1DKU5iS.Ul27trj051iIbOG',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
            ]

        ];
        $this->table('users')->insert($data)->save();
        $this->query("SET foreign_key_checks = 1");
    }
}
