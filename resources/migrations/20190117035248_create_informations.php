<?php


use Phinx\Migration\AbstractMigration;

class CreateInformations extends AbstractMigration
{
    public function up(){
        $this->table('informations', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'charset' => 'utf8mb4', 'collation' => 'utf8mb4_bin'])
        ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
        ->addColumn('title', 'string', ['limit' => 255, 'null' => false])
        ->addColumn('detail', 'text', ['null' => false])
        ->addColumn('link_url', 'text', ['null' => true])
        ->addColumn('expiration_date', 'date')
        ->addColumn('is_open', 'boolean', ['signed' => false, 'default' => 1])
        ->addColumn('created_at', 'datetime')
        ->addColumn('updated_at', 'datetime', ['null' => true])
        ->addColumn('deleted_at', 'datetime', ['null' => true])
        ->save();
    }

    public function down(){
        $this->table('informations')->drop();
    }
}
