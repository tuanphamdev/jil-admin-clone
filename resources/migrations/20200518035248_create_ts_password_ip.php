<?php


use Phinx\Migration\AbstractMigration;

class CreateTsPasswordIp extends AbstractMigration
{
    public function up(){
        $this->table('ts_password_ips', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'charset' => 'utf8mb4', 'collation' => 'utf8mb4_bin'])
        ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
        ->addColumn('name', 'string', ['limit' => 255, 'null' => false])
        ->addColumn('ip_address', 'string', ['limit' => 255, 'null' => false])
        ->addColumn('created_at', 'datetime')
        ->addColumn('updated_at', 'datetime', ['null' => true])
        ->addColumn('deleted_at', 'datetime', ['null' => true])
        ->save();
    }

    public function down(){
        $this->table('ts_password_ips')->drop();
    }
}
