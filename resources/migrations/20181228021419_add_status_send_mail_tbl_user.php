<?php


use Phinx\Migration\AbstractMigration;

class AddStatusSendMailTblUser extends AbstractMigration
{
    public function up()
    {
        $this->table('users')
            ->addColumn('status', 'integer', ['limit' => 3, 'null' => true])
            ->addColumn('is_send_mail', 'integer', ['limit' => 3, 'null' => true])
            ->save();
    }

    public function down()
    {
        $this->table('users')
            ->removeColumn(['status', 'is_send_mail'])
            ->save();
    }
}
