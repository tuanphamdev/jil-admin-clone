<?php

use Phinx\Migration\AbstractMigration;

class AddColumnIsCreditCardKeihi extends AbstractMigration
{
    public function up()
    {
        $this->table('companies')
            ->addColumn('is_credit_card', 'boolean', ['null' => false, 'signed' => false, 'default' => 0, 'after' => 'is_ebook_store'])
            ->save();

        $this->table('plans')
            ->addColumn('credit_card', 'integer', ['default' => 0, 'after' => 'ebook_store'])
            ->addColumn('date_enable_ebook_store', 'datetime', ['null' => true, 'after' => 'ebook_store'])
            ->addColumn('date_enable_credit_card', 'datetime', ['null' => true, 'after' => 'credit_card'])
            ->save();
    }

    public function down()
    {
        $this->table('companies')
            ->removeColumn('is_credit_card')
            ->save();

        $this->table('plans')
            ->removeColumn('credit_card')
            ->removeColumn('date_enable_ebook_store')
            ->removeColumn('date_enable_credit_card')
            ->save();
    }
}
