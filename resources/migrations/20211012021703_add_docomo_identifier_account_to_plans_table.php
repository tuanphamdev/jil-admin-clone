<?php

use Phinx\Migration\AbstractMigration;

class AddDocomoIdentifierAccountToPlansTable extends AbstractMigration
{

    public function up()
    {
        $this->table('plans')
            ->addColumn('d_account_id', 'string', ['default' => null, 'after' => 'docomo', 'limit' => 100])
            ->save();
    }

    public function down()
    {
        $this->table('plans')
            ->removeColumn('d_account_id')
            ->save();
    }
}
