<?php

use Phinx\Migration\AbstractMigration;

class CreateAgreements extends AbstractMigration
{
    public function up()
    {
        $this->table('agreements', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])
              ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
              ->addColumn('company_id', 'biginteger', ['limit' => 20, 'signed' => false])
              ->addColumn('type', 'integer')
              ->addColumn('start_date', 'date',['null' => true])
              ->addColumn('end_date', 'date')
              ->addColumn('num_account', 'integer', ['default' => 0])
              ->addColumn('created_at', 'datetime')
              ->addColumn('updated_at', 'datetime', ['null' => true])
              ->addColumn('deleted_at', 'datetime', ['null' => true])
              ->addForeignKey('company_id', 'companies', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION', 'constraint' => 'fk_agreements_companies1'])
              ->save();
    }

    public function down()
    {
        $this->table('agreements')->dropForeignKey(['company_id','user_id'])->drop();
    }
}
