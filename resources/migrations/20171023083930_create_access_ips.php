<?php

use Phinx\Migration\AbstractMigration;

class CreateAccessIps extends AbstractMigration
{
    public function up()
    {
        $this->table('access_ips', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])
            ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
            ->addColumn('ip_address', 'string', ['limit' => 15])
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime', ['null' => true])
            ->addColumn('deleted_at', 'datetime', ['null' => true])
            ->save();
    }

    public function down()
    {
        $this->table('access_ips')->drop();
    }
}
