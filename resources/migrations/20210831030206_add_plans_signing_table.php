<?php

use Phinx\Migration\AbstractMigration;

class AddPlansSigningTable extends AbstractMigration
{
    public function up()
    {
        $this->table('plans_signing', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'charset' => 'utf8mb4', 'collation' => 'utf8mb4_bin'])
            ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
            ->addColumn('company_id', 'biginteger', ['limit' => 20, 'signed' => false])
            ->addColumn('plan_id', 'biginteger', ['limit' => 20, 'signed' => false])
            ->addColumn('signing_folder', 'integer', ['default' => 0])
            ->addColumn('signing_folder_range', 'integer',  ['default' => 0])
            ->addColumn('signing_internal_workflow', 'integer', ['default' => 0 ])
            ->addColumn('signing_batch_import', 'integer',  ['default' => 0])
            ->addColumn('import_signed_file', 'integer',  ['default' => 0, 'null' => false])
            ->addColumn('maximum_import_signed_file', 'string', ['limit' => 100,'null' => true])
            ->addColumn('ip_limit', 'integer',  ['default' => 0])
            ->addColumn('send_sms', 'integer',  ['default' => 0])
            ->addColumn('send_function_digital_signature', 'integer',  ['default' => 0])
            ->addColumn('imprint_registration', 'integer',  ['default' => 0])
            ->addColumn('attach_file_receiver', 'integer', ['default' => 0 ])
            ->addColumn('attach_file_sender', 'integer',  ['default' => 0])
            ->addColumn('alert_function', 'integer',  ['default' => 0, 'null' => false])
            ->addColumn('language_support', 'integer',  ['default' => 0])
            ->addColumn('date_enable_signing_folder', 'datetime', ['null' => true])
            ->addColumn('date_enable_signing_folder_range', 'datetime', ['null' => true])
            ->addColumn('date_enable_signing_internal_workflow', 'datetime', ['null' => true])
            ->addColumn('date_enable_signing_batch_import', 'datetime', ['null' => true])
            ->addColumn('date_enable_import_signed_file', 'datetime', ['null' => true])
            ->addColumn('date_enable_ip_limit', 'datetime', ['null' => true])
            ->addColumn('date_enable_send_sms', 'datetime', ['null' => true])
            ->addColumn('date_enable_send_function_digital_signature', 'datetime', ['null' => true])
            ->addColumn('date_enable_imprint_registration', 'datetime', ['null' => true])
            ->addColumn('date_enable_attach_file_receiver', 'datetime', ['null' => true])
            ->addColumn('date_enable_attach_file_sender', 'datetime', ['null' => true])
            ->addColumn('date_enable_alert_function', 'datetime', ['null' => true])
            ->addColumn('date_enable_language_support', 'datetime', ['null' => true])
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime', ['null' => true])
            ->addColumn('deleted_at', 'datetime', ['null' => true])
            ->addForeignKey('company_id', 'companies', 'id', [
                'delete' => 'NO_ACTION',
                'update' => 'NO_ACTION',
                'constraint' => 'fk_plans_singing_company_id_companies_id'
            ])
            ->addForeignKey('plan_id', 'plans', 'id', [
                'delete' => 'NO_ACTION',
                'update' => 'NO_ACTION',
                'constraint' => 'fk_plans_signing_plan_id_plans_id'
            ])
            ->save();
    }

    public function down()
    {
        $this->dropTable('plans_signing');
    }
}
