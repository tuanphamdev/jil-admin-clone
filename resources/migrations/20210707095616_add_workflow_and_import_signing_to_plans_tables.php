<?php

use Phinx\Migration\AbstractMigration;

class AddWorkflowAndImportSigningToPlansTables extends AbstractMigration
{
    public function up()
    {
        $this->table('plans')
            ->addColumn('signing_internal_workflow', 'integer', ['default' => 0, 'after' => 'signing'])
            ->addColumn('signing_batch_import', 'integer',  ['default' => 0, 'after' => 'signing_folder'])
            ->addColumn('date_enable_signing_internal_workflow', 'datetime', ['null' => true])
            ->addColumn('date_enable_signing_batch_import', 'datetime', ['null' => true])
            ->save();
    }

    public function down()
    {
        $this->table('plans')
            ->removeColumn('signing_internal_workflow')
            ->removeColumn('signing_batch_import')
            ->removeColumn('date_enable_signing_internal_workflow')
            ->removeColumn('date_enable_signing_batch_import')
            ->save();
    }
}
