<?php


use Phinx\Migration\AbstractMigration;

class AddStopDateAgreement extends AbstractMigration
{
    public function up()
    {
        $this->table('agreements')
            ->addColumn('stop_date', 'datetime', ['null' => true])
            ->save();
    }

    public function down()
    {
        $this->table('agreements')
            ->removeColumn(['stop_date'])
            ->save();
    }
}
