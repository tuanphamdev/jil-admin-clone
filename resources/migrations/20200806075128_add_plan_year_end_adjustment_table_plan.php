<?php

use Phinx\Migration\AbstractMigration;

class AddPlanYearEndAdjustmentTablePlan extends AbstractMigration
{
    public function up()
    {
        $this->table('plans')
            ->addColumn('year_end_tax_adjustment', 'integer', ['default' => 0, 'after' => 'employee_contract'])
            ->save();
    }

    public function down()
    {
        $this->table('plans')
            ->removeColumn(['year_end_tax_adjustment'])
            ->save();
    }
}
