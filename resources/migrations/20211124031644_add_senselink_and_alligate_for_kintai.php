<?php

use Phinx\Migration\AbstractMigration;

class AddSenselinkAndAlligateForKintai extends AbstractMigration
{
    public function up()
    {
        $this->table('plans')
            ->addColumn('kintai_sense_link', 'integer',  ['default' => 0])
            ->addColumn('kintai_alligate', 'integer',  ['default' => 0])
            ->addColumn('date_enable_kintai_sense_link', 'datetime', ['null' => true])
            ->addColumn('date_enable_kintai_alligate', 'datetime', ['null' => true])
            ->save();
    }

    public function down()
    {
        $this->table('plans')
            ->removeColumn('kintai_sense_link')
            ->removeColumn('kintai_alligate')
            ->removeColumn('date_enable_kintai_sense_link')
            ->removeColumn('date_enable_kintai_alligate')
            ->save();
    }
}
