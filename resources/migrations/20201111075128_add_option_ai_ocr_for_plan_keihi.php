<?php

use Phinx\Migration\AbstractMigration;

class AddOptionAiOcrForPlanKeihi extends AbstractMigration
{
    public function up()
    {
        $this->table('companies')
            ->addColumn('is_using_ai_ocr', 'boolean', ['null' => false, 'signed' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table('companies')
            ->removeColumn(['is_using_ai_ocr'])
            ->save();
    }
}
