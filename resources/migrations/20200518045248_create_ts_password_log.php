<?php


use Phinx\Migration\AbstractMigration;

class CreateTsPasswordLog extends AbstractMigration
{
    public function up(){
        $this->table('ts_password_logs', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'charset' => 'utf8mb4', 'collation' => 'utf8mb4_bin'])
        ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
        ->addColumn('user_id', 'biginteger', ['limit' => 20, 'signed' => false, 'null' => false])
        ->addColumn('company_id', 'biginteger', ['limit' => 20, 'signed' => false, 'null' => false])
        ->addColumn('employee_id', 'biginteger', ['limit' => 20, 'signed' => false, 'null' => false])
        ->addColumn('action_type', 'integer', ['limit' => 1, 'comment' => '0: 設定, 1: 解除'])
        ->addColumn('created_at', 'datetime')
        ->addColumn('updated_at', 'datetime', ['null' => true])
        ->addColumn('deleted_at', 'datetime', ['null' => true])
        ->save();
    }

    public function down(){
        $this->table('ts_password_logs')->drop();
    }
}
