<?php

use Phinx\Migration\AbstractMigration;

class AddFolderToPlanTable extends AbstractMigration
{
    public function up()
    {
        $this->table('plans')
            ->addColumn('signing_folder', 'integer', ['default' => 0, 'after' => 'signing'])
            ->addColumn('signing_folder_range', 'integer',  ['default' => 0, 'after' => 'signing_folder'])
            ->addColumn('date_enable_signing_folder', 'datetime', ['null' => true])
            ->addColumn('date_enable_signing_folder_range', 'datetime', ['null' => true])
            ->save();
    }

    public function down()
    {
        $this->table('plans')
            ->removeColumn('signing_folder')
            ->removeColumn('signing_folder_range')
            ->removeColumn('date_enable_signing_folder')
            ->removeColumn('date_enable_signing_folder_range')
            ->save();
    }
}
