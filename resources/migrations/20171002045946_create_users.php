<?php

use Phinx\Migration\AbstractMigration;

class CreateUsers extends AbstractMigration
{

    public function up()
      {
          $this->table('users', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])
              ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
              ->addColumn('role_id', 'biginteger', ['limit' => 20, 'signed' => false])
              ->addColumn('name', 'string', ['limit' => 255])
              ->addColumn('name_furigana', 'string', ['limit' => 255])
              ->addColumn('email', 'string', ['limit' => 255])
              ->addColumn('password', 'string', ['limit' => 255])
              ->addColumn('reset_password_token', 'string', ['limit' => 255, 'null' => true])
              ->addColumn('created_at', 'datetime')
              ->addColumn('updated_at', 'datetime', ['null' => true])
              ->addColumn('deleted_at', 'datetime',['null' => true])
              ->addForeignKey('role_id', 'roles', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION', 'constraint' => 'fk_users_roles1'])
              ->save();
      }

    public function down()
    {
        $this->table('users')->dropForeignKey(['role_id'])->drop();
    }
}
