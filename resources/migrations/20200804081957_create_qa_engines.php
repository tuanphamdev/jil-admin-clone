<?php

use Phinx\Migration\AbstractMigration;

class CreateQaEngines extends AbstractMigration
{
    public function up(){
        $this->table('qa_engines', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'charset' => 'utf8mb4', 'collation' => 'utf8mb4_bin'])
            ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
            ->addColumn('status', 'integer', ['limit' => 255, 'default' => 0, 'null' => true])
            ->addColumn('information_1', 'text', ['null' => true])
            ->addColumn('information_2', 'text', ['null' => true])
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime', ['null' => true])
            ->addColumn('deleted_at', 'datetime', ['null' => true])
            ->save();
    }

    public function down(){
        $this->table('qa_engines')->drop();
    }
}
