<?php

use Phinx\Migration\AbstractMigration;

class AddColumnIsDocomo extends AbstractMigration
{
    public function up()
    {
        $this->table('companies')
            ->addColumn('is_docomo', 'boolean', ['null' => false, 'signed' => false, 'default' => 0, 'after' => 'is_using_ultra'])
            ->save();

        $this->table('plans')
            ->addColumn('docomo', 'integer', ['default' => 0, 'after' => 'ultra'])
            ->save();
    }

    public function down()
    {
        $this->table('companies')
            ->removeColumn('is_docomo')
            ->save();
        $this->table('plans')
            ->removeColumn('docomo')
            ->save();
    }
}
