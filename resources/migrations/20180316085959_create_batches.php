<?php

use Phinx\Migration\AbstractMigration;

class CreateBatches extends AbstractMigration
{
    public function up()
    {
        $this->table('batch_logs', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])
            ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
            ->addColumn('name', 'string', ['limit' => 255])
            ->addColumn('content', 'text', ['null' => true])
            ->addColumn('response', 'text', ['null' => true])
            ->addColumn('created_at', 'datetime', ['null' => true])
            ->addColumn('updated_at', 'datetime', ['null' => true])
            ->addColumn('deleted_at', 'datetime', ['null' => true])
            ->save();
    }

    public function down()
    {
        $this->table('batch_logs')->drop();
    }
}
