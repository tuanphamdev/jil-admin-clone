<?php

use Phinx\Migration\AbstractMigration;

class AddIsUsingUltraToTableCompanies extends AbstractMigration
{
    public function up()
    {
        $this->table('companies')
            ->addColumn('is_using_ultra', 'boolean', ['null' => false, 'signed' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table('companies')
            ->removeColumn('is_using_ultra')
            ->save();
    }
}
