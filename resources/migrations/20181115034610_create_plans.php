<?php

use Phinx\Migration\AbstractMigration;

class CreatePlans extends AbstractMigration
{
    public function up()
    {
        $this->table('plans', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])
            ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
            ->addColumn('company_id', 'biginteger', ['limit' => 20, 'signed' => false])
            ->addColumn('jinjer_db', 'integer', ['default' => 1])
            ->addColumn('kintai', 'integer', ['default' => 0])
            ->addColumn('roumu', 'integer', ['default' => 0])
            ->addColumn('jinji', 'integer', ['default' => 0])
            ->addColumn('keihi', 'integer', ['default' => 0])
            ->addColumn('my_number', 'integer', ['default' => 0])
            ->addColumn('work_vital', 'integer', ['default' => 0])
            ->addColumn('agreement_id', 'biginteger', ['limit' => 20, 'signed' => false])
           
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime', ['null' => true])
            ->addColumn('deleted_at', 'datetime', ['null' => true])
            ->addForeignKey('company_id', 'companies', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION', 'constraint' => 'fk_plans_companies1'])
            ->addForeignKey('agreement_id', 'agreements', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION', 'constraint' => 'fk_plans_agreement1'])
            ->save();
    }

    public function down()
    {
        $this->table('plans')->dropForeignKey(['company_id', 'agreement_id'])->drop();
    }
}
