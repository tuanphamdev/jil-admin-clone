<?php

use Phinx\Migration\AbstractMigration;

class AddSigningImportTimeStampeAndElectronicSignature extends AbstractMigration
{
    public function up()
    {
        $this->table('plans_signing')
            ->addColumn('import_signed_file_timestamp', 'integer',  ['default' => 0, 'after' => 'maximum_import_signed_file'])
            ->addColumn('electronic_signature_long_term', 'integer',  ['default' => 0])
            ->addColumn('electronic_signature_timestamp', 'integer',  ['default' => 0])
            ->addColumn('electronic_signature_authentication', 'integer',  ['default' => 0])
            ->addColumn('date_enable_electronic_signature_long_term', 'datetime', ['null' => true])
            ->addColumn('date_enable_electronic_signature_timestamp', 'datetime', ['null' => true])
            ->addColumn('date_enable_electronic_signature_authentication', 'datetime', ['null' => true])
            ->save();
    }

    public function down()
    {
        $this->table('plans_signing')
            ->removeColumn('import_signed_file_timestamp')
            ->removeColumn('electronic_signature_long_term')
            ->removeColumn('electronic_signature_timestamp')
            ->removeColumn('electronic_signature_authentication')
            ->removeColumn('date_enable_electronic_signature_long_term')
            ->removeColumn('date_enable_electronic_signature_timestamp')
            ->removeColumn('date_enable_electronic_signature_authentication')
            ->save();
    }
}
