<?php

use Phinx\Migration\AbstractMigration;

class CreateRoles extends AbstractMigration
{
    public function up()
    {
        $this->table('roles', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])
              ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
              ->addColumn('name', 'string', ['limit' => 128])
              ->addColumn('created_at', 'datetime')
              ->addColumn('updated_at', 'datetime', ['null' => true])
              ->addColumn('deleted_at', 'datetime', ['null' => true])

              ->addColumn('created_by_id', 'biginteger', ['limit' => 20, 'signed' => false, 'null' => true])
              ->addColumn('updated_by_id', 'biginteger', ['limit' => 20, 'signed' => false, 'null' => true])
              ->save();
    }

    public function down()
    {
        $this->table('roles')->drop();

    }
}
