<?php

use Phinx\Migration\AbstractMigration;

class AddPlanWorkflowTablePlan extends AbstractMigration
{
    public function up()
    {
        $this->table('plans')
            ->addColumn('workflow', 'integer', ['default' => 0, 'after' => 'work_vital'])
            ->save();
    }

    public function down()
    {
        $this->table('plans')
            ->removeColumn(['workflow'])
            ->save();
    }
}
