<?php


use Phinx\Migration\AbstractMigration;

class AddPlanSalaryTblPlan extends AbstractMigration
{
    public function up()
    {
        $this->table('plans')
            ->addColumn('salary', 'integer', ['default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table('plans')
            ->removeColumn(['salary'])
            ->save();
    }
}
