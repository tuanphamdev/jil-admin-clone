<?php


use Phinx\Migration\AbstractMigration;

class ColEngimaCodeVersionTblCompanies extends AbstractMigration
{
    public function up()
    {
        $this->table('companies')
            ->addColumn('enigma_code', 'string', ['limit' => 20, 'null' => true])
            ->addColumn('enigma_version', 'integer', ['limit' => 3, 'null' => true, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table('companies')
            ->removeColumn(['enigma_code', 'enigma_version'])
            ->save();
    }
}
