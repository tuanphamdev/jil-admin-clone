<?php

use Phinx\Migration\AbstractMigration;

class AddDateExportToPlansTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->table('plans')
            ->addColumn('ai_ocr', 'integer', ['default' => 0])
            ->addColumn('ultra', 'integer', ['default' => 0])
            ->addColumn('date_enable_kintai', 'datetime', ['null' => true])
            ->addColumn('date_enable_roumu', 'datetime', ['null' => true])
            ->addColumn('date_enable_jinji', 'datetime', ['null' => true])
            ->addColumn('date_enable_keihi', 'datetime', ['null' => true])
            ->addColumn('date_enable_ai_ocr', 'datetime', ['null' => true])
            ->addColumn('date_enable_my_number', 'datetime', ['null' => true])
            ->addColumn('date_enable_work_vital', 'datetime', ['null' => true])
            ->addColumn('date_enable_salary', 'datetime', ['null' => true])
            ->addColumn('date_enable_workflow', 'datetime', ['null' => true])
            ->addColumn('date_enable_employee_contract', 'datetime', ['null' => true])
            ->addColumn('date_enable_signing', 'datetime', ['null' => true])
            ->addColumn('date_enable_year_end_tax_adjustment', 'datetime', ['null' => true])
            ->addColumn('date_enable_ultra', 'datetime', ['null' => true])
            ->save();
    }

    public function down()
    {
        $this->table('plans')
            ->removeColumn('ai_ocr')
            ->removeColumn('ultra')
            ->removeColumn('date_enable_kintai')
            ->removeColumn('date_enable_roumu')
            ->removeColumn('date_enable_jinji')
            ->removeColumn('date_enable_keihi')
            ->removeColumn('date_enable_ai_ocr')
            ->removeColumn('date_enable_my_number')
            ->removeColumn('date_enable_work_vital')
            ->removeColumn('date_enable_salary')
            ->removeColumn('date_enable_workflow')
            ->removeColumn('date_enable_employee_contract')
            ->removeColumn('date_enable_signing')
            ->removeColumn('date_enable_year_end_tax_adjustment')
            ->removeColumn('date_enable_ultra')
            ->save();
    }
}
