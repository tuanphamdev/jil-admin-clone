<?php

use Phinx\Migration\AbstractMigration;


class AddPlanSigningToPlan extends AbstractMigration
{
    public function up()
    {
        $this->table('plans')
            ->addColumn('signing', 'integer', ['default' => 0, 'after' => 'bi'])
            ->save();
    }

    public function down()
    {
        $this->table('plans')
            ->removeColumn(['signing'])
            ->save();
    }
}
