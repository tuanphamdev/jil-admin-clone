<?php

use Phinx\Migration\AbstractMigration;

class CreateSurveyMonkeyTable extends AbstractMigration
{
    public function up()
    {
        $this->table('survey_monkey', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'charset' => 'utf8mb4', 'collation' => 'utf8mb4_bin'])
            ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
            ->addColumn('start_date', 'date', ['null' => true])
            ->addColumn('end_date', 'date', ['null' => true])
            ->addColumn('services', 'string', ['limit' => 250, 'null' => false])
            ->addColumn('creator_id', 'biginteger', ['null'=> false])
            ->addColumn('script_content', 'text', ['null' => true])
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime', ['null' => true])
            ->addColumn('deleted_at', 'datetime', ['null' => true])
            ->save();
    }

    public function down()
    {
        $this->dropTable('survey_monkey');
    }
}
