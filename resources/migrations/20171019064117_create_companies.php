<?php

use Phinx\Migration\AbstractMigration;

class CreateCompanies extends AbstractMigration
{
    public function up()
    {
        $this->table('companies', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])
            ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
            ->addColumn('created_by_id', 'biginteger', ['limit' => 20, 'signed' => false])
            ->addColumn('updated_by_id', 'biginteger', ['limit' => 20, 'signed' => false, 'null' => true])
            ->addColumn('code', 'string', ['limit' => 255])
            ->addColumn('name', 'string', ['limit' => 255])
            ->addColumn('name_furigana', 'string', ['limit' => 255])
            ->addColumn('representative_name', 'string', ['limit' => 255])
            ->addColumn('representative_name_furigana', 'string', ['limit' => 255])
            ->addColumn('representative_title', 'string', ['limit' => 255])
            ->addColumn('tel', 'string', ['limit' => 20])
            ->addColumn('fax', 'string', ['limit' => 20, 'null' => true])
            ->addColumn('zip', 'string', ['limit' => 8, 'null' => true])
            ->addColumn('prefecture','string', ['limit'=> 10, 'null' => false])
            ->addColumn('city','string', ['limit'=> 10, 'null' => false])
            ->addColumn('address','string', ['limit'=> 255, 'null' => false])
            ->addColumn('address_furigana','string', ['limit' => 255])
            ->addColumn('application_name', 'string', ['limit' => 255])
            ->addColumn('application_surname', 'string', ['limit' => 255])
            ->addColumn('application_furigana', 'string', ['limit' => 255])
            ->addColumn('application_furigana_surname', 'string', ['limit' => 255])
            ->addColumn('application_email', 'string', ['limit' => 255])
            ->addColumn('registered_date', 'date')
            ->addColumn('status', 'integer', ['limit' => 6])
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime', ['null' => true])
            ->addColumn('deleted_at', 'datetime',['null' => true])
            ->addForeignKey('created_by_id', 'users', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION', 'constraint' => 'fk_companies_users1'])
            ->addForeignKey('updated_by_id', 'users', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION', 'constraint' => 'fk_companies_users2'])
            ->save();
    }

    public function down()
    {
        $this->table('companies')->dropForeignKey(['creator_id'])->drop();

    }
}
