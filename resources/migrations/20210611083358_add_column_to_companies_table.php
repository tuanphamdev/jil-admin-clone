<?php

use Phinx\Migration\AbstractMigration;

class AddColumnToCompaniesTable extends AbstractMigration
{
    public function up()
    {
        $this->table('companies')
            ->addColumn('is_paid_management', 'boolean', ['null' => false, 'signed' => false, 'default' => 0])
            ->addColumn('is_shift_management', 'boolean', ['null' => false, 'signed' => false, 'default' => 0])
            ->addColumn('is_overtime_management', 'boolean', ['null' => false, 'signed' => false, 'default' => 0])
            ->addColumn('is_ebook_store', 'boolean', ['null' => false, 'signed' => false, 'default' => 0])
            ->addColumn('is_payslip', 'boolean', ['null' => false, 'signed' => false, 'default' => 0])
            ->save();

        $this->table('plans')
            ->addColumn('paid_management', 'integer', ['default' => 0])
            ->addColumn('shift_management', 'integer', ['default' => 0])
            ->addColumn('overtime_management', 'integer', ['default' => 0])
            ->addColumn('ebook_store', 'integer', ['default' => 0])
            ->addColumn('payslip', 'integer', ['default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table('companies')
            ->removeColumn('is_paid_management')
            ->removeColumn('is_shift_management')
            ->removeColumn('is_overtime_management')
            ->removeColumn('is_ebook_store')
            ->removeColumn('is_payslip')
            ->save();

        $this->table('plans')
            ->removeColumn('paid_management')
            ->removeColumn('shift_management')
            ->removeColumn('overtime_management')
            ->removeColumn('ebook_store')
            ->removeColumn('payslip')
            ->save();
    }
}
