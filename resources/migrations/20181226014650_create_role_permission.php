<?php


use Phinx\Migration\AbstractMigration;

class CreateRolePermission extends AbstractMigration
{
    public function up()
    {
        $this->table('role_permissions', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])
            ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
            ->addColumn('item', 'string', ['limit' => 64])
            ->addColumn('permission', 'integer')
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime', ['null' => true])
            ->addColumn('deleted_at', 'datetime', ['null' => true])

            ->addColumn('role_id', 'biginteger', ['limit' => 20, 'signed' => false, 'null' => true])
            ->addForeignKey('role_id', 'roles', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION', 'constraint' => 'fk_role_permissions_roles'])
            ->save();
    }

    public function down()
    {
        $this->table('role_permissions')->drop();

    }
}
