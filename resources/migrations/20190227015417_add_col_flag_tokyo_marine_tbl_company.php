<?php


use Phinx\Migration\AbstractMigration;

class AddColFlagTokyoMarineTblCompany extends AbstractMigration
{
    public function up()
    {
        $this->table('companies')
            ->addColumn('is_tokyo_marine', 'integer', ['limit' => 3, 'null' => true, 'default' => 1])
            ->save();
    }

    public function down()
    {
        $this->table('companies')
            ->removeColumn(['is_tokyo_marine'])
            ->save();
    }
}
