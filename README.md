# Jinjer Admin PHP


# Prerequisites
 - PHP ^7.1
 - Mysql ^5.7
 - Codeigniter 3.x 

# Variables
Copy file .env.example to .env and update DB variables

# Installing
Run docker in root source folder.
```
$ docker-compose up

or
Run docker in background
$ docker-compose up -d
```

Run Composer install
```
$ docker exec jinjeradmin_api_1 bash -c "composer install"

or
$ docker exec -it jinjeradmin_api_1 bash
$ composer install
```

Run Migrate data
```
$ docker exec jinjeradmin_api_1 bash -c "php index.php migrate"

or
$ docker exec -it jinjeradmin_api_1 bash
$ composer migrate
```

# Running the tests
Run Unittest (test folder ./application/tests)
```
$ ./vendor/bin/phpunit -c ./application/tests
```
or
```
$ docker exec jinjeradmin_api_1 bash -c "./vendor/bin/phpunit -c ./application/tests"

or
$ docker exec -it jinjeradmin_api_1 bash
$ composer test
```

